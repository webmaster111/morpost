<?php

date_default_timezone_set('Europe/Kiev');

define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('SITE_PATH', DOCUMENT_ROOT . '/');
define('CLASSES', DOCUMENT_ROOT . '/protected/classes/');
define('CONTROLLERS', DOCUMENT_ROOT . '/protected/controllers/');
define('MODULES', DOCUMENT_ROOT . '/protected/modules/');
define('SUBSYSTEM', DOCUMENT_ROOT . '/protected/subsystem/');
define('LIBRARY', DOCUMENT_ROOT . '/protected/libraries/');

require_once(SITE_PATH . 'protected/libraries/redirect.php');
require_once(SITE_PATH . 'protected/config.php');
require_once(SITE_PATH . 'vendor/autoload.php');
require_once(CLASSES . 'initializer.php');
//THINK CLIENT SERVICE
$admin_path = '/admin';
  if(isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], $admin_path) === false && $_SERVER['REQUEST_METHOD'] === 'GET'){
      if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')){
           if(file_exists('think-client-service/think_client.php'))
           {
               include_once('think-client-service/think_client.php');
               if(function_exists('think_client_html_replace')){
                   ob_start('think_client_html_replace');
               }
           }
       }
  }
  //END THINK CLIENT SERVICE
$router = new Router($registry, $db);
echo $router->getParams();
