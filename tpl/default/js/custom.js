//MAGNIFIC POPUP
$(document).ready(function () {
    $('.images-block').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        }
    });

    let box = $('#cart');
    // let top = box.offset().top - parseFloat(box.css('marginTop').replace(/auto/, 0));
    // $(window).scroll(function () {
    //     if ($(window).width() > 767 || true) {
    //         if ($(window).scrollTop() < top) {
    //             box.removeClass('sticky');
    //             box.css('width', '100%');
    //         } else {
    //             box.css('width', Math.ceil(box.width()));
    //             box.addClass('sticky');
    //         }
    //     }
    // });
});

$('.popup-with-form').magnificPopup({
    type: 'inline',
    focus: '#name'
});

$(document).on('click', '.js-form-submit', function (e) {
    let name =  $('#form-popup form input[name=name]').val();
    let phone =  $('#form-popup form input[name=phone]').val();
    let text =  $('#form-popup form textarea').val();
    let form_type =  $('#form-popup form input[name=form_type]').val();

    let form = document.querySelector('#form-popup form');
    let fields = form.querySelectorAll('#form-popup form input');

    removeValidation(form);

    let check = checkFieldsPresence(form, fields);

    if (check >= 1){
        return
    }

    $('#spinner').html(ShowSpinner());
    $.ajax({
        type: "POST",
        url: "/ajax/feedback/feedback",
        data: {name: name, phone: phone, text: text,form_type: form_type},
        dataType: 'json',
        cache: false,
        success: function (data) {
            $('#spinner').html('Спасибо за Вашу заявку');
            $('#spinner').html(data.message);
            $('#form-popup form input[name=name]').val(' ');
            $('#form-popup form input[name=phone]').val(' ');
            $('#form-popup form textarea').val(' ');
        }
    });
});

$(document).on('click', '.js-fast-submit', function (e) {

    e.preventDefault()
    let name =  $('#fast-popup form input[name=name]').val();
    let phone =  $('#fast-popup form input[name=phone]').val();
    let form_type =  $('#fast-popup form input[name=form_type]').val();

    let form = document.querySelector('#fast-popup form');
    let fields = form.querySelectorAll('#fast-popup form input');

    removeValidation(form);

    let check = checkFieldsPresence(form, fields);

    if (check >= 1){
        return
    }

    $('#spinner').html(ShowSpinner());
    $.ajax({
        type: "POST",
        url: "/ajax/feedback/feedback",
        data: {name: name, phone: phone, form_type: form_type},
        dataType: 'json',
        cache: false,
        success: function (data) {
            $('#spinner').html('Спасибо за Вашу заявку');

            $('#fast-popup form input[name=name]').val(' ');
            $('#fast-popup form input[name=phone]').val(' ');
        }
    });
});

$(document).on('click', '.js-cooperation-submit', function (e) {
    e.preventDefault()
    let name =  $('#cooperation-popup form input[name=name]').val();
    let phone =  $('#cooperation-popup form input[name=phone]').val();
    let company_name =  $('#cooperation-popup form input[name=company_name]').val();
    let form_type =  $('#cooperation-popup form input[name=form_type]').val();

    let form = document.querySelector('#cooperation-popup form');
    let fields = form.querySelectorAll('#cooperation-popup form input');

    removeValidation(form);

    let check = checkFieldsPresence(form, fields);

    if (check >= 1){
        return
    }

    $('#spinner').html(ShowSpinner());
    $.ajax({
        type: "POST",
        url: "/ajax/feedback/feedback",
        data: {name: name, phone: phone, company_name: company_name, form_type: form_type},
        dataType: 'json',
        cache: false,
        success: function (data) {
            $('#spinner').html('Спасибо за Вашу заявку');

            $('#cooperation-popup form input[name=name]').val(' ');
            $('#cooperation-popup form input[name=phone]').val(' ');
            $('#cooperation-popup form input[name=company_name]').val(' ');
        }
    });
});


function send_apply() {
    let name = $('#send_apply form input[name=name]').val();
    let phone = $('#send_apply form input[name=phone]').val();
    let email = $('#send_apply form input[name=email]').val();
    let form_type = $('#send_apply form input[name=form_type]').val();

    let list_service = document.getElementsByName("checkbox[]");
    let list_service_val = [];

    let list_staff = document.getElementsByName("staff[]");
    let list_staff_val = [];

    let list_duration = document.getElementsByName("duration[]");
    let list_duration_val = [];

    for (let i= 0; i<list_service.length;i++) {
        if (list_service[i].checked === true) {
            list_service_val.push(list_service[i].value);
        }
    }

    for (let i= 0; i<list_staff.length;i++) {
        if (list_staff[i].checked) {
            list_staff_val.push(list_staff[i].value);
        }
    }
    for (let i= 0; i<list_duration.length;i++) {
        if (list_duration[i].checked) {
            list_duration_val.push(list_duration[i].value);
        }
    }

    $('#spinner').html(ShowSpinner());
    $.ajax({
        type: "POST",
        url: "/ajax/feedback/feedback",
        data: {name:name, form_type: form_type, email: email,
            phone: phone, list_service_check:list_service_val,
            list_staff_check:list_staff_val, list_duration_check:list_duration_val},
        dataType: 'json',
        cache: false,
        success: function (data) {
            $('#spinner').html('Спасибо за Вашу заявку');

            $('#send_apply form input[name=name]').val(' ');
            $('#send_apply form input[name=phone]').val(' ');
        }
    });
}

function generateError(text) {
    let error = document.createElement('div');
    error.className = 'error';
    error.style.color = 'red';
    error.innerHTML = text;
    return error;
}

function removeValidation(form) {
    let errors = form.querySelectorAll('.error');

    for (let i = 0; i < errors.length; i++) {
        errors[i].remove();
    }
}

function checkFieldsPresence(form, fields) {
    let response = 0;
    for (let i = 0; i < fields.length; i++) {
        if (!fields[i].value) {
            response++;
            let error = generateError('не заполнено');
            form[i].parentElement.insertBefore(error, fields[i]);
        }
    }

    return response;
}

function ShowSpinner() {

    return '<div id="quick_buy_spiner"  style="visibility:visible; text-align:center; padding: 5px;">' +
        '<svg aria-hidden="true" focusable="false"' +
        'data-prefix="fas" data-icon="spinner" role="img" xmlns="http://www.w3.org/2000/svg"' +
        'viewBox="0 0 512 512" class="svg-inline--fa fa-spinner fa-w-16 fa-spin fa-lg"' +
        'style="height: 30px;color: #00BE53;">' +
        '<path fill="currentColor" d="M304 48c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-48 368c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zm208-208c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zM96 256c0-26.51-21.49-48-48-48S0 229.49 0 256s21.49 48 48 48 48-21.49 48-48zm12.922 99.078c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.491-48-48-48zm294.156 0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.49-48-48-48zM108.922 60.922c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.491-48-48-48z" class="">' +
        '</path>' +
        '</svg>' +
        '</div>';
}

(function ($) {

    "use strict";

    // TOOLTIP
    $(".header-links .fa, .tool-tip").tooltip({
        placement: "bottom"
    });
    $(".btn-wishlist, .btn-compare, .display .fa").tooltip('hide');

    // TABS
    $('.nav-tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

})(jQuery);