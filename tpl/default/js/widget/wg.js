var ssGadget = {
    closure: function (e) {
        let t = 2, s = "https://{domain}.customer.smartsender.eu",
            n = "https://{domain}.customer.smartsender.eu/api/i/widgets/{sign}",
            a = {name: "в контакты", link: "/contacts"}, r = {
                titles: {
                    skype: "Skype Messenger",
                    viber: "Viber Messenger",
                    instagram: "Instagram Direct",
                    facebook: "Facebook Messenger",
                    telegram: "Telegram Messenger",
                    vkontakte: "Vkontakte Messenger"
                },
                deep: {
                    map: {vkontakte: "r", facebook: "ref", viber: "context", telegram: "start"},
                    hashed: ["vkontakte"],
                    supported: ["viber", "facebook", "telegram", "vkontakte"]
                },
                links: {app: ["viber", "skype"], url: ["facebook", "telegram", "instagram", "vkontakte"]}
            };

        function l(e, t) {
            let s = "w:{sign}".replace("{sign}", e.sign), n = r.deep.supported.includes(t.type),
                a = r.links.app.includes(t.type), l = r.deep.hashed.includes(t.type), i = t[a ? "app" : "link"];
            if (n) {
                i += (a ? "&" : l ? "#" : "?") + r.deep.map[t.type] + "=" + btoa(s)
            }
            return i
        }

        !function (e, i) {
            !function () {
                let e = document.createElement("link");
                // e.rel = "stylesheet", e.href = "https://customer.smartsender.eu/css/client/gd.css?f=1", document.head.appendChild(e)
                e.rel = "stylesheet", e.href = "/tpl/default/js/widget/widget.css", document.head.appendChild(e)
            }();
            let o = n.replace("{domain}", e).replace("{sign}", i), c = new XMLHttpRequest;
            c.open("GET", o), c.setRequestHeader("X-Requested-With", "XMLHttpRequest"), c.onreadystatechange = function () {
                if (4 === this.readyState && 200 === this.status) {
                    let e = JSON.parse(this.responseText);
                    !function (e) {
                        s = s.replace("{domain}", e.domain);
                        let n = e.content.resource, i = document.createElement("div");
                        i.setAttribute("class", "widget-preview");
                        let o = document.createElement("div");
                        o.setAttribute("class", "messenger");
                        let c = document.createElement("div");
                        c.setAttribute("id", "messenger__block"), c.setAttribute("class", "messenger__block");
                        for (let t = 0; t < e.channels.length; t++) {
                            let s = e.channels[t], n = document.createElement("a");
                            n.setAttribute("rel", "nofollow"), n.setAttribute("target", "_blank"), n.setAttribute("href", l(e, s)), n.setAttribute("class", "messenger__btn messenger__btn_{channel}".replace("{channel}", s.type));
                            let a = document.createElement("div");
                            a.setAttribute("class", "messenger__btn-caption"), a.innerHTML = r.titles[s.type], n.appendChild(a), c.appendChild(n)
                        }
                        let d = document.createElement("div");
                        d.setAttribute("class", "messenger_owner");
                        let m = document.createElement("p");
                        m.innerHTML = "Перейти";
                        let g = document.createElement("a");
                        g.setAttribute("href", a.link), g.setAttribute("_target", "blank"), g.innerHTML = a.name, m.innerHTML += " " + g.outerHTML, d.appendChild(m), c.appendChild(d), o.appendChild(c);
                        let p = document.createElement("div");
                        p.setAttribute("class", "messenger__switch messenger_switch-toggle"), p.setAttribute("style", "background-color: {color};".replace("{color}", n.background.hex));
                        let u = document.createElement("div");
                        u.setAttribute("id", "messenger__static"), u.setAttribute("class", "messenger__static");
                        let h = document.createElement("div");
                        h.setAttribute("class", "messenger__static-caption"), h.setAttribute("style", "color: {color};".replace("{color}", n.text.color.hex)), h.innerHTML = n.text.content, u.innerHTML = (b = n.text.color, '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="messenger__static-icon" width="20" height="20" viewBox="0 0 20 20" version="1.1"><g id="Canvas" transform="translate(-825 -308)"><g id="Vector"><use xlink:href="#path0_fill" transform="translate(825 308)" fill="#FFFFFF"/></g></g><defs><path id="path0_fill" d="M 19 4L 17 4L 17 13L 4 13L 4 15C 4 15.55 4.45 16 5 16L 16 16L 20 20L 20 5C 20 4.45 19.55 4 19 4ZM 15 10L 15 1C 15 0.45 14.55 0 14 0L 1 0C 0.45 0 0 0.45 0 1L 0 15L 4 11L 14 11C 14.55 11 15 10.55 15 10Z"/></defs></svg>'.replace("{hex}", b.hex)), u.appendChild(h);
                        var b;
                        let _ = document.createElement("div");
                        _.setAttribute("id", "messenger__close"), _.setAttribute("class", "messenger__close"), _.innerHTML = function (e) {
                            return '<svg class="messenger__close-icon"><g id="Canvas" transform="translate(-829 -1155)"><g id="Shape"><use xlink:href="#path1_fill" transform="matrix(0.707107 0.707107 -0.707107 0.707107 835 1151.52)" fill="#FFFFFF"/></g></g><defs><path id="path1_fill" fill-rule="evenodd" d="M 14 6L 8 6L 8 0L 6 0L 6 6L 0 6L 0 8L 6 8L 6 14L 8 14L 8 8L 14 8L 14 6Z"/></defs></svg>'.replace("{hex}", e.hex)
                        }(n.text.color), p.appendChild(u), p.appendChild(_);
                        for (let e = 0; e < t; e++) {
                            let e = document.createElement("div");
                            e.setAttribute("class", "messenger__pulsation"), p.appendChild(e)
                        }
                        p.addEventListener("click", function () {
                            !function (e) {
                                e.classList.toggle("active"), document.getElementById("messenger__block").classList.toggle("messenger-show"), document.getElementById("messenger__static").classList.toggle("messenger-hide"), document.getElementById("messenger__close").classList.toggle("messenger-show");
                                let t = document.getElementsByClassName("messenger__pulsation");
                                for (let e = 0; e < t.length; e++) t[e].classList.toggle("messenger-stop")
                            }(p)
                        }), o.appendChild(p), i.appendChild(o), document.body.appendChild(i)
                    }(e)
                }
            }, c.send()
        }(e.getAttribute("data-domain"), e.getAttribute("data-target"))
    }, create: function () {
        if (!1 === ssGadget.state) {
            let e = document.getElementById("ss-gadget");
            ssGadget.closure(e)
        }
        ssGadget.state = !0
    }, state: !1, timeout: 2e3
};
document.addEventListener("DOMContentLoaded", function () {
    ssGadget.create()
}), setTimeout(ssGadget.closure, ssGadget.timeout);