'use strict';
$(function () {

    $(window).on('scroll', function () {
        if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100)
            $("button.go-top").fadeIn();
        else $("button.go-top").fadeOut();
    });

    $("button.go-top").click(function () {
        $("html, body").animate({scrollTop: 0}, "slow");
        return false;
    });

    $('.btn-cart-quick').click(function () {
        let img = $('.images-block img').attr('src');
        let id = $('.input-number').attr('data-id');
        let amount = $('.input-number').attr("value");
        $('.input-number-modal').attr("value", amount);
        $('.input-number-modal').attr("data-id", id);
        $('.left_quick').html(`<img src="${img}">`);
    });

    $('.quantity-right-plus').click(function (e) {
        e.preventDefault();
        let QC = $(this).parents('.quantity-control').find('input');
        let quantity = parseInt(QC.val());
        QC.val(quantity + 1);
        if (QC.data('cart-id'))
            updateQuantity(quantity + 1, QC.data('cart-id'));
    });

    $('.quantity-left-minus').click(function (e) {
        e.preventDefault();
        let QC = $(this).parents('.quantity-control').find('input');
        let quantity = parseInt(QC.val());
        if (quantity > 1) {
            if (QC.data('cart-id'))
                updateQuantity(quantity - 1, QC.data('cart-id'));
            QC.val(quantity - 1);
        }
    });

    $('.catalog-title-xs').click(function () {
       $('.catalog-list-xs').slideToggle();
    });
    $('.rating').click(function () {
        Swal.fire({
            type: 'success',
            text: 'Рейтинг товара будет изменен!!!',
        })
    });

    $('input[data-type="phone"]').each(function (key, value) {
        $(value).mask('(099) 999-99-99', {
            translation: {
                '9': {pattern: /[0-9]/},
                '0': {pattern: /0/}
            }
        });
    });

    $('input[data-type="letters"]').attr('maxlength', 35).mask('Z', {
        translation: {
            'Z': {
                pattern: /[а-яА-Яa-zA-Z\s]/,
                recursive: true
            }
        }
    });

    $(document).on('click', 'button.burger-button, .close-mobile-menu', function () {
        console.log('ok');
        $('.mobile-hidden').toggleClass("active");
    });

    $(document).on('click', '.tabs-content h3.tab-title', function () {
        $(this).toggleClass('active');
    });

    $('.reviews_slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $(document).on('click', '.messenger_switch-toggle', function () {
        $(this).toggleClass('active');
        $(this).closest('.widget-preview').find('#messenger__block').toggleClass('messenger-show');
        $(this).closest('.widget-preview').find('#messenger__static').toggleClass('messenger-hide');
        $(this).closest('.widget-preview').find('#messenger__close').toggleClass('messenger-show');
        $(this).closest('.widget-preview').find('#messenger__pulsation').toggleClass('messenger-stop');
    });

    $(document).on('click', '.cooperation_switch-toggle', function () {
        $(this).toggleClass('active');
        $.magnificPopup.open({
            items: {
                src: '#cooperation-popup', // can be a HTML string, jQuery object, or CSS selector
                type: 'inline'
            }
        });
    });

    $(document).on('click', '.show_coperation', function () {
        $.magnificPopup.open({
            items: {
                src: '#cooperation-popup', // can be a HTML string, jQuery object, or CSS selector
                type: 'inline'
            }
        });
    });

    if ($(window).width() < 768)
        $('#add-comment-form textarea').attr('rows', '2');

    autosize($('textarea'));

    $('#header-account-icon').click(function () {
        let showAM = $(this).attr('data-showam');
        if(showAM != 'active'){
            $(this).attr('data-showam', 'active');
            $(this).closest('.header-account').find('.header-links').addClass('active');
        }else{
            $(this).attr('data-showam', 'false');
            $(this).closest('.header-account').find('.header-links').removeClass('active');
        }
    });
    $('#header-search-icon').click(function () {
        let showFS = $(this).attr('data-showfs');
        if(showFS != 'active'){
            $(this).attr('data-showfs', 'active');
            $(this).closest('.header-search').find('#search').addClass('active');
        }else{
            $(this).attr('data-showfs', 'false');
            $(this).closest('.header-search').find('#search').removeClass('active');
        }
    });
    $('.has-childs').click(function () {
        let show_childs = $(this).attr('data-show-childs');
        if(show_childs != 'active'){
            $(this).attr('data-show-childs', 'active');
            $(this).addClass('active');
        }else{
            $(this).attr('data-show-childs', 'false');
            $(this).removeClass('active');
        }
    });

    $('#navToggle').click(function () {
        $(this).toggleClass("active");
        $(".overlay").toggleClass("open");
        // this line ▼ prevents content scroll-behind
        $("body").toggleClass("locked");
    });

    $('.js-dynamic-show-hide').click(function () {
        $(this).hide().prev('.js-dynamic-height').removeClass('active');
    });

    $(document).on('click', '.tabs-header > li.tab-button', function () {
        let tab_id = '.tab_' + $(this).data('infoblock-id');
        $(this).closest('.tabs-header').find('li.tab-button').removeClass('active');
        $(this).addClass('active');
        console.log($(this).closest('.tabs-wrapper'));
        $(this).closest('.tabs-wrapper').find('.tabs-content .tab-content').removeClass('active');
        $(this).closest('.tabs-wrapper').find('.tabs-content ' + tab_id).addClass('active');
    });

    $('.tabs-header > li.tab-button:nth-child(1)').click();

});

// start DELIVERY PICKERS
$(document).on('change', '#delivery', function () {
    let id = parseInt($(this).val());
    if (id === 4) show_NP_cities();
    else disableSelects_delivery();
});

function disableSelects_delivery() {
    $('#delivery_address').show();
    $('#delivery_city').html('');
    $('#delivery_department').html('');
}

$(document).on('change', '#delivery_city select', function () {
    $('#delivery_department select').attr('disabled', 'disabled').html('');
    show_NP_departments($(this).find(':selected').data('ref'));
});


function show_NP_cities() {
    $('#delivery_address').hide();
    $('#delivery_city').html(ShowSpinner());
    $.ajax({
        type: "POST",
        url: "/ajax/novaposhta/getcities",
        data: {name: 'delivery_city', required: true},
        dataType: "json",
        cache: false,
        success: function (data) {
            if (data.html) $('#delivery_city').html(data.html);
        },
        error: () => console.log("novaposhta | delivery city ERROR")
    });
}

function ShowSpinner() {
    return '<div class="form-group" style="height: 34px;display: flex;justify-content: center;align-items: center">' +
        '<i class="fa fa-spinner fa-spin"></i>' +
        '</div>';
}

function show_NP_departments(ref) {
    if (ref) {
        $('#delivery_department').html(ShowSpinner());
        $.ajax({
            type: "POST",
            url: "/ajax/novaposhta/getdepartments",
            data: {name: 'delivery_department', required: true, ref: ref},
            dataType: "json",
            cache: false,
            success: function (data) {
                if (data.html) $('#delivery_department').html(data.html);
            },
            error: () => console.log("show_NP_departments ERROR")
        });
    }
}


$(document).on('click', '.catalog-menu-button', function (e) {

    $('nav.catalog-menu').slideToggle();
    $(this).toggleClass('active');
});

$(document).on('click', '.header-phones .drop-arrow', function (e) {
    $('.header-phones .interesting-list').css('height', 'auto');
});

$(document).on('click', '.add-video', function (e) {
    $(this).toggleClass('active').next().slideToggle('fast');
});

$(document).on('change', '.comment-file input', function () {
    const name = $(this).val().split(/\\|\//).pop();
    const truncated = name.length > 20
        ? name.substr(name.length - 20)
        : name;
    if (truncated) {
        $(this).parent().find('.text-holder').css('display', 'none');
        $(this).parent().find('.upload-file').css('display', 'block').html(truncated);
    } else {
        $(this).parent().find('.text-holder').css('display', 'block');
        $(this).parent().find('.upload-file').css('display', 'none');
    }
});

$(document).on('click', '.comment-file .text-holder,.comment-file .upload-file', function () {
    $(this).parent().find('input').click();
});

$(document).on('click', '.product-photo-mini', function () {
    let main = $('.product-photo'),
        current = $(this),
        image = current.find('img').attr('src');
    main.find('img').attr('src', image);
    console.log($(this).find('img').attr('src'));
});


//custom checkbox
$(document).on('click', '.custom-checkbox', function (e) {
    e.preventDefault();
    $(this).toggleClass('checked');
    let input = $(this).find('input');
    if (input.attr('checked')) input.removeAttr('checked');
    else input.attr('checked', 'checked')
});

$(document).on('click', '.custom-radio', function (e) {
    e.preventDefault();
    $('.custom-radio input[name="' + $(this).find('input').attr('name') + '"]').each(function (key, value) {
        $(value).removeAttr('checked').parent().removeClass('checked');
    });
    $(this).addClass('checked').find('input').attr('checked', 'checked');
});


function resetBorders(form, color) {
    form.find('input , textarea').each(function (key, value) {
        $(value).css('border-color', color);
    });
}

// custom validation
function CustomValidateForm(form) {
    resetBorders($(form), '#dedede');
    let no_errors = true, color = 'rgba(255,0,0,0.5)';
    $(form).find('.validate').each(function (key, value) {
        if ($(value).val().length < 4) {
            no_errors = false;
            $(value).css('border-color', color);
        }
    });
    $(form).find('.validate-email').each(function (key, value) {
        if (!(/^[a-zA-Z0-9_.-]+@([a-zA-Z0-9-]+.)+[a-zA-Z0-9]{2,4}$/.test($(value).val()))) {
            no_errors = false;
            $(value).css('border-color', color);
        }
    });
    $(form).find('.validate-phone').each(function (key, value) {
        if (!(/^([+][0-9]{1,3})?([(]{1}[0-9]{2,4}[)])?([0-9 .-/]{7,20})((x|ext|extension)[ ]?[0-9]{2,4})?$/.test($(value).val()))) {
            no_errors = false;
            $(value).css('border-color', color);
        }
    });

    $(form).find('.validate-pas-r').each(function (key, value) {
        if ($(value).val() !== $(form).find('input[name="password-repeat"]').val()) {
            no_errors = false;
            $(form).find('input[name="password-repeat"]').css('border-color', color);
            $(value).css('border-color', color);
        }
    });
    return no_errors;
}


// -------- custom modal
$(document).on('click', '.custom-modal', function () {
    $(this).fadeOut(300);
});
$(document).on('click', '.custom-modal .close-modal', function () {
    $(this).parents('.custom-modal').fadeOut(300);
});
$(document).on('click', '[data-m-target]', function () {
    $($(this).data('m-target')).css("display", "flex").hide().fadeIn(300);
});
$(document).on('click', '.custom-modal .content', function (e) {
    e.stopPropagation();
});

// --- sweet
function showInfoSweet(message, confirm = false, type = 'info', timer = false) {
    if (timer)
        swal({
            showConfirmButton: confirm,
            type: type,
            html: message,
            timer: 2000,
            onBeforeOpen: () => {
                Swal.showLoading()
            }
        });
    else
        swal({
            showCloseButton: true,
            showConfirmButton: confirm,
            type: type,
            html: message
        });
}

function getBrowserInfo() {
    var t,v = undefined;
    if (window.chrome) t = 'Chrome';
    else if (window.opera) t = 'Opera';
    else if (document.all) {
        t = 'IE';
        var nv = navigator.appVersion;
        var s = nv.indexOf('MSIE')+5;
        v = nv.substring(s,s+1);
    }
    else if (navigator.appName) t = 'Netscape';
    return {type:t,version:v};
}
function bookmark(a){
    var url = window.document.location;
    var title = window.document.title;
    var b = getBrowserInfo();
    if (b.type == 'IE' && 8 >= b.version && b.version >= 4) window.external.AddFavorite(url,title);
    else if (b.type == 'Opera') {
        a.href = url;
        a.rel = "sidebar";
        a.title = url+','+title;
        return true;
    }
    else if (b.type == "Netscape") window.sidebar.addPanel(title,url,"");
    else alert("Нажмите CTRL-D, чтобы добавить страницу в закладки.");
    return false;
}

