<div class="container" data-test="test">
    <div class="row">
        <div class="col-md-4 col-xs-12 col-sm-12">
            <?= $vars['menu_catalogs'] ?>
        </div>
        <div class="col-md-8 col-xs-12 col-sm-12">
            <?= isset($vars['breadcrumbs']) ? htmlspecialchars_decode($vars['breadcrumbs']) : '' ?>
            <h1 class="main-heading2 category-name">
                <?= $vars['page']['name'] ?>
            </h1>
            <?= htmlspecialchars_decode($vars['page']['body']) ?>
        </div>
    </div>
</div>

