$(document).ready(function () {
    if ($('#fchs').length > 0) {
        var $tb = $('#fchs'),
            $tr = $tb.find('tbody tr').eq(0).detach();

        rebuildTb($tb, $tr);

        $(document).on('change', '.fch-holder input', function () {
            onEditField();
        });

        $(document).on('click', '#add_fch', function () {
            $new_tr = $tr.clone();
            $tb.append($new_tr);
            $new_tr.find('input').val('').first().focus();
        });

        $(document).on('click', '.del-fch', function () {
            var $tr = $(this).closest('tr.fch-holder');

            if ($tr.siblings().length > 0) {
                $tr.remove();
            } else {
                $tr.find('input').val('');
            }

            onEditField();
        });
    }
});

function onEditField() {
    var dt = '';

    $('#fchs .fch-holder').each(function (id) {
        var $this = $(this),
            $name = $this.children('td').children('[data-data="name"]'),
            $value = $this.children('td').children('[data-data="value"]'),
            name = $name.val().replace(/[=>\|]/g, ''),
            value = $value.val().replace(/[=>\|]/g, '');

        $name.val(name);
        $value.val(value);

        if (name !== '' && value !== '') {
            if (dt !== '') {
                dt += '||';
            }
            dt += name + '=>' + value;
        }
    });

    $('#feats').html(dt)
}

function rebuildTb($tb, $tr) {
    var feats = $('#feats').val();

    if (feats !== '' && feats !== null) {

        var feats_arr = feats.split('||');

        feats_arr.forEach(function (item) {
            var name = item.split('=>')[0],
                value = item.split('=>')[1],
                $new_tr = $tr.clone();

            $new_tr.find('input[data-data="name"]').val(name);
            $new_tr.find('input[data-data="value"]').val(value);

            $tb.append($new_tr);
        });

    } else {
        $tb.append($tr);
    }
}