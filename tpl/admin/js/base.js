$(document).ready(function () {

    if ($(window).outerWidth() < 992) {
        setTimeout(function () {
            $('#cke_body').addClass('cke_collapsed').append("<div class='cke__button_collapse'><i class='fa fa-chevron-down'></i></div>");
        }, 1000);
    }

    $(document).on('click', '.cke__button_collapse', function () {
        $(this).find('.fa').toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
        $('#cke_body').toggleClass('cke_collapsed');
    });

    //
    $(document).on("click", '#resetExportSelect', function () {
        $('.exportSelect').val('');
    });

    //
    $(document).on('change', "select[name='minute'], select[name='hour'], select[name='day'], select[name='month'], select[name='weekday']", function () {
        var common_option =
            $("select[name=minute] :selected").val() + ' ' +
            $("select[name=hour] :selected").val() + ' ' +
            $("select[name=day] :selected").val() + ' ' +
            $("select[name=month] :selected").val() + ' ' +
            $("select[name=weekday] :selected").val();
        //alert(common_option);
        if ($("#common_options [value='" + common_option + "']").length == 0)
            common_option = '--';
        $("#common_options [value='" + common_option + "']").attr('selected', 'selected');
    });

    //$("a[rel='lightbox']").fancybox({});

    //
    $(document).on('change', "input[name='price_price[]']", function () {
        if ($('#one_price').prop('checked')) {
            var price = $(this).val();
            $("input[name='price_price[]']").each(function () {
                $(this).val(price);
            });
        }
    });

    //
    $(document).on('change', "input[name='purch_price[]']", function () {
        var id = $(this).parent().parent().attr('id');
        var price = $(this).val();
        var discount = $("input[name='price_margin[]']").val();
        purchPrice(price, discount, id);
    });

    //
    $(document).on('change', "input[name='price_margin[]']", function () {
        var id = $(this).parent().parent().attr('id');
        var price = $("input[name='purch_price[]']").val();
        var discount = $(this).val();
        purchPrice(price, discount, id);
    });

    //
    $(document).on('change', "input[name='price_discount[]']", function () {
        var id = $(this).parent().parent().attr('id');
        var price = $("input[name='price_basePrice[]']").val();
        var discount = $(this).val();
        basePrice(price, discount, id);
    });

    //
    $(document).on('change', "input[name='type_discount']", function () {
        var id = $('#load_price tr:first').attr('id');
        var price = $("#load_price input[name='price_basePrice[]']:first").val();
        var discount = $("#load_price  input[name='price_discount[]']:first").val();
        basePrice(price, discount, id);
    });

    //
    $(document).on('change', "input[name='price_basePrice[]']", function () {
        var id = $(this).parent().parent().attr('id');
        var price = $(this).val();
        var discount = $("input[name='price_discount[]']").val();
        basePrice(price, discount, id);
    });

    // COPY PROMPT
    $(document).on('click', '.copy_prompt', function () {
        $('#copy_prompt').show();
    });

    //
    $(document).on('click', '.adm-close', function () {
        $('#copy_prompt').hide();
    });

    //
    $(document).on('click', ".showAllParams", function () {
        var status = '';
        if ($(this).attr('href') == 'show') {
            $('.params_group div').show(200);
            status = 'show';
        }
        else {
            $('.params_group div').hide(200);
            status = 'hide';
        }
        $(".params_group").each(function () {
            var id = $(this).children('div').attr('id');
            $.cookie(id, status);
        });
        return false;
    });

    //
    $('#content_basic textarea').autoResize();

    //
    if ($('.cm-calendar').length > 0) {
        $('.cm-calendar').appendDtpicker({
            "dateFormat": "YYYY-MM-DD hh:mm",
            "locale": "ru"
        });
    }

    ///display active tab
    var tab = $.cookie('active_tab');
    if ($('#' + tab).length > 0) {
        $(".cm-js").removeClass('cm-active');
        $('#' + tab).addClass('cm-active');
        $('.cm-tabs').addClass('hidden');
        $('#content_' + tab).removeClass('hidden');
    }

    $(document).on("change", ".cm-check-changes :input", function () {
        $(".cm-check-changes").addClass('tb_was_change');
    });

    $(document).on('click', ".extra-tools .cm-confirm", function () {
        $(".cm-check-changes").removeClass('tb_was_change');
        window.onbeforeunload = false;
    });

    $(document).on('submit', ".cm-check-changes", function () {
        $(".cm-check-changes").removeClass('tb_was_change');
        window.onbeforeunload = false;
    });

    $(document).on('click', ".extra-tools .cm-confirm", function () {
        $(".cm-check-changes").removeClass('tb_was_change');
        window.onbeforeunload = false;
    });

    $(document).on("change", ".tb_was_change :input", function () {
        window.onbeforeunload = function () {
            return "Are you sure you wish to leave this delightful page?";
        }
    });

    if ($('.cm-active.hidden_li').length > 0) {
        $(".hidden_li").show();
    }

    $(document).on('click', ".see-all", function () {
        if ($(".hidden_li").css('display') == 'none') {
            $(".hidden_li").show();
            $(this).html('Скрыть ...');
        }
        else {
            $(".hidden_li").hide();
            $(this).html('Показать все ...');
        }
    });

    //
    $(document).on('click', '.pos_block', function () {
        $('.pos_block').removeClass('hov_position');
        $(this).addClass('hov_position');
        $('input[name=watermark_position]').val($(this).attr('id'));
    });

    //
    $(document).on('click', 'input[name=watermark_type]', function () {
        $('.type_watermark_div').hide();
        if ($(this).val() == 0) $('#text_image_watermark').show();
        else $('#image_watermark').show();
        $('input[name=watermark_position]').val($(this).attr('id'));
    });

    // TODO: Maybe delete
    $("div").parent(".lBlock");
    autoHide();
    ////////////////////////


    //
    $(document).on('click', '.cm-notification-close', function () {
        $(".notification-e").parent(this).empty();
        $(".notification-n").parent(this).empty();
    });

    $(document).on('click', '.cm-external-focus', function () {
        var id = $(this).attr('rev');
        $('#' + id).focus();
    });

    $(document).on('click', '.cm-save-and-close', function () {
        var action = '/admin/' + $('#action').val();
        $('form[name=page_update_form]').attr('action', action);
    });

    $(document).on('click', '.cm-save-and-close-product', function () {
        var action = $('.back-link').attr('href');
        $('form[name=page_update_form]').attr('action', action);
    });

    $(document).on('click', '.check_all', function () {
        $('.check-item').prop('checked', true);
        $('.check_all2').prop('checked', true);
    });

    $(document).on('click', '.check_all2', function () {//alert($('.check').prop('checked'));
        if ($('.check-item').prop('checked')) $('.check-item').prop('checked', false);
        else $('.check-item').prop('checked', true);
    });

    $(document).on('click', '.uncheck_all', function () {
        $('.check-item').prop('checked', false);
        $('.check_all2').prop('checked', false);
    });

    $(document).on('click', 'input[type=checkbox]', function () {//alert($('.check').prop('checked'));
        var id = $(this).attr('id');
        var cl = $(this).attr('class');
        if (id != '') {
            if ($(this).prop('checked')) $('.' + id).prop('checked', true);
            else $('.' + id).prop('checked', false);
        }
        if (cl != '') {
            if ($(this).prop('checked')) $('#' + cl).prop('checked', true);
        }
    });

    $(document).on('click', '.cm-save-and-close', function () {
        var action = '/admin/' + $('#action').val();
        $('form[name=page_update_form]').attr('action', action);
    });

    $("#ajax_loading_box").ajaxStart(function () {
        $(this).show();
    });

    $("#ajax_loading_box").ajaxStop(function () {
        $(this).hide();
    });

    $(document).on('click', ".cm-js", function () {
        var id2 = $(this).attr('id');
        var id = 'content_' + id2;
        $('.cm-js').removeClass('cm-active');
        $(this).addClass('cm-active');
        $('.cm-tabs').addClass('hidden');
        $('#' + id).removeClass('hidden');
        $.cookie('active_tab', id2);
    });

    $(document).on('click', '#sw_select_RU_wrap_', function () {
        if ($('#select_RU_wrap_').css('display') == 'block') {
            $('#select_RU_wrap_').css('display', 'none');
        } else {
            $('#select_RU_wrap_').css('display', 'block');
        }
    });

    $(document).on('click', ".notification-content", function () {
        var id = $(this).attr('id').split('_')[1];
        closeNotification(id, false);
    });

    checkCompareItems();

    $(document).on('click', "#startcompare", function () {
        checkCompareItems();
    });

    //
    $(document).on('click', ".cm-confirm", function () {
        let q_str = 'Вы уверены что хотите удалить ', question = 'данную запись';
        if ($(this).data('question')) question = $(this).data('question');
        if ($(this).data('q-line')) q_str = $(this).data('q-line');
        return confirm(q_str + question + ' ?');
    });

    //
    $(document).on("click", ".burger", function () {
        $(".header-menu__list").toggleClass('header-menu__list_open');
        $('.header-menu__item .dropdown').attr('style', '').removeClass('dropdown_open');
        $(".burger").toggleClass('burger_activate');
    });

    $(document).on('click', '.header-menu__item', function (e) {

        var $this = $(this),
            $dropdown = $this.children('.dropdown'),
            width = $(window).width();

        if (width <= 768) {
            if (!$dropdown.hasClass('dropdown_open')) {
                height = $dropdown.children('.dropdown__list').height();

                // console.log(height);

                $dropdown.css({
                    height: height
                }).addClass('dropdown_open')
            } else {
                $dropdown.css({
                    height: 0
                }).removeClass('dropdown_open')
            }

            $this.siblings().each(function () {
                var $subling = $(this),
                    $sub_dropdown = $subling.children('.dropdown');

                $sub_dropdown.css({
                    height: 0
                }).removeClass('dropdown_open')
            })
        }

    });

    $(window).resize(function () {
        var width = $(window).width();

        if (width >= 992) {
            $('#navigation ul li:nth-of-type(2)').show();
            $('#cke_body').removeClass('cke_collapsed').find(".cke__button_collapse").remove();
        } else {
            $('#navigation ul li:nth-of-type(2)').hide();
            if (!$(".cke__button_collapse").length) {
                $('#cke_body').addClass('cke_collapsed').append("<div class='cke__button_collapse'><i class='fa fa-chevron-down'></i></div>");
            }
        }

        if (width >= 480) {
            $('.header-menu__item .dropdown').attr('style', '').removeClass('dropdown_open');
            $(".burger").removeClass('burger_activate');
        }

    });
    $(document).on('click', '#read_all', function () {
        if ($(this).prop('checked')) $(".read_chmod").prop('checked', 'checked');
        else $(".read_chmod").prop('checked', false);
    });

    $(document).on('click', '#add_all', function () {
        if ($(this).prop('checked')) $(".add_chmod").prop('checked', 'checked');
        else $(".add_chmod").prop('checked', false);
    });

    $(document).on('click', '#edit_all', function () {
        if ($(this).prop('checked')) $(".edit_chmod").prop('checked', 'checked');
        else $(".edit_chmod").prop('checked', false);
    });

    $(document).on('click', '#del_all', function () {
        if ($(this).prop('checked')) $(".del_chmod").prop('checked', 'checked');
        else $(".del_chmod").prop('checked', false);
    });

    // Subsystem
    $(document).on('click', '#config_all', function () {
        if ($(this).prop('checked')) $(".config_chmod").prop('checked', true);
        else $(".config_chmod").prop('checked', false);
    });

    $(document).on('click', '#help_all', function () {
        if ($(this).prop('checked')) $(".help_chmod").prop('checked', 'checked');
        else $(".help_chmod").prop('checked', false);
    });

    $(document).on('click', '#translate_all', function () {
        if ($(this).prop('checked')) $(".translate_chmod").prop('checked', 'checked');
        else $(".translate_chmod").prop('checked', false);
    });

    $(document).on('click', '#chmod_all', function () {
        if ($(this).prop('checked')) $(".chmod_chmod").prop('checked', 'checked');
        else $(".chmod_chmod").prop('checked', false);
    });

    $(document).on('click', '.del_chmod', function () {
        var id = $(this).val();
        if ($(this).prop('checked')) $("#read" + id).prop('checked', 'checked');
    });

    $(document).on('click', '.add_chmod', function () {
        var id = $(this).val();
        if ($(this).prop('checked')) $("#read" + id).prop('checked', 'checked');
    });

    $(document).on('click', '.edit_chmod', function () {
        var id = $(this).val();
        if ($(this).prop('checked')) $("#read" + id).prop('checked', 'checked');
    });

    $(document).on('click', '.read_chmod', function () {
        var id = $(this).val();
        if ($(this).prop('checked') != 'checked') {
            $("#add" + id).prop('checked', false);
            $("#edit" + id).prop('checked', false);
            $("#del" + id).prop('checked', false);
        }
    });

    // Toggle subsystem params
    $(document).on('click', '#navigation li.cm-active .fa', function (e) {
        var $this = $(this);

        $this.closest('#navigation').find('ul li:nth-of-type(2)').slideToggle();
    });

    // Toggle product-list filters
    $(document).on('click', '.filter-toggle', function (e) {
        e.preventDefault();

        var $this = $(this),
            $filters = $('#ds_15014');

        $filters.toggleClass('toggle_on_mobile');

    });


});

/////////////
// FUNCTIONS
/////////////

//
function select_common_option() {
    var option = $('#common_options').val();
    if (option != "--") {
        var option_array = option.split(" ");
        $("select[name=minute] option[value='" + option_array[0] + "']").attr('selected', 'selected');
        $("select[name=hour] option[value='" + option_array[1] + "']").attr('selected', 'selected');
        $("select[name=day] option[value='" + option_array[2] + "']").attr('selected', 'selected');
        $("select[name=month] option[value='" + option_array[3] + "']").attr('selected', 'selected');
        $("select[name=weekday] option[value='" + option_array[4] + "']").attr('selected', 'selected');
    }
}

//
function strip_tags(str, allowed_tags) {
    var key = '', allowed = false;
    var matches = [];
    var allowed_array = [];
    var allowed_tag = '';
    var i = 0;
    var k = '';
    var html = '';
    var replacer = function (search, replace, str) {
        return str.split(search).join(replace);
    };
    // Build allowes tags associative array
    if (allowed_tags) {
        allowed_array = allowed_tags.match(/([a-zA-Z0-9]+)/gi);
    }
    str += '';
    // Match tags
    matches = str.match(/(<\/?[\S][^>]*>)/gi);
    // Go through all HTML tags
    for (key in matches) {
        if (isNaN(key)) {
            // IE7 Hack
            continue;
        }
        // Save HTML tag
        html = matches[key].toString();
        // Is tag not in allowed list? Remove from str!
        allowed = false;
        // Go through all allowed tags
        for (k in allowed_array) {            // Init
            allowed_tag = allowed_array[k];
            i = -1;
            if (i != 0) {
                i = html.toLowerCase().indexOf('<' + allowed_tag + '>');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('<' + allowed_tag + ' ');
            }
            if (i != 0) {
                i = html.toLowerCase().indexOf('</' + allowed_tag);
            }
            // Determine
            if (i == 0) {
                allowed = true;
                break;
            }
        }
        if (allowed) {
            str = replacer(html, "", str); // Custom replace. No regexing
        }
    }
    return str;
}

//
function discount2(price, discount) {
    if ($("input[name='type_discount']:checked").val() == 0) {
        return (price * (discount / 100))
    }
    else {
        return discount;
    }
}

//
function basePrice(price, discount, id) {
    if ($('#one_price').prop('checked')) {
        $("input[name='price_discount[]']").each(function () {
            $(this).val(discount);
        });
        $("input[name='price_basePrice[]']").each(function () {
            $(this).val(price);
        });
        if (price != 0)
            $("input[name='price_price[]']").each(function () {
                if (discount != 0) {
                    var discount3 = discount2(price, discount);
                    var price2 = price - discount3;
                }
                else var price2 = price;
                $(this).val(price2);
            });
    }
    else if (price != 0) {
        if (discount != 0) {
            var discount3 = discount2(price, discount);
            var price2 = price - discount3;
        }
        else var price2 = price;
        $("#" + id + " input[name='price_price[]']").val(price2);
    }
}

//
function purchPrice(price, discount, id) {
    if ($('#one_price').prop('checked')) {
        $("input[name='price_margin[]']").each(function () {
            $(this).val(discount);
        });
        $("input[name='purch_price[]']").each(function () {
            $(this).val(price);
        });
        if (price != 0) {
            $("input[name='price_basePrice[]']").each(function () {
                if (discount != 0) {
                    var discount3 = discount2(price, discount);
                    var price2 = Math.round(price) + Math.round(discount3);
                }
                else var price2 = Math.round(price);
                $(this).val(price2);
            });
        }
        price = $("input[name='price_basePrice[]']").val();
        discount = $("input[name='price_discount[]']").val();
        basePrice(price, discount, id);
    }
    else if (price != 0) {
        if (discount != 0) {
            var discount3 = discount2(price, discount);
            var price2 = Math.round(price) + Math.round(discount3);
        }
        else var price2 = Math.round(price);
        $("#" + id + " input[name='price_basePrice[]']").val(price2);
        discount = $("input[name='price_discount[]']").val();
        basePrice(price2, discount, id);
    }
}

//
function info_box(typeM, message) { //error information warning success
    showNotification({
        message: message,
        type: typeM
        /*
         'duration': 0, // display duration
         'autoClose' : false,
         */
    });
}

//
function fn_switch_default_box(holder, prefix, default_id) {
    var default_box = $('#' + prefix + '_' + default_id);
    var checked_items = $('input[id^=' + prefix + '_].checkbox:checked').not(default_box).length + holder.checked ? 1 : 0;
    if (checked_items == 0) {
        default_box.attr('disabled', 'disabled');
        default_box.prop('checked', 'checked');
    } else {
        default_box.removeAttr('disabled');
    }
}

//
function autoHide() {
    $('.cm-auto-hide').each(function () {
        var id = str_replace('notification_', '', $(this).attr('id')); // FIXME: not good
        if (($(this).hasClass('product-notification-container') || $(this).hasClass('notification-content')) && typeof(notice_displaying_time) != 'undefined') {
            closeNotification(id, true, false, notice_displaying_time * 1000);
        } else {
            closeNotification(id, true);
        }
    });
}

//
function str_replace(search, replace, subject) {
    return subject.split(search).join(replace);
}

//
function closeNotification(key, delayed, no_fade, delay) {
    var DELAY = typeof(delay) == 'undefined' ? 5000 : delay;
    if (delayed == true) {
        if (DELAY != 0) {
            var timeout_key = setTimeout(function () {
                closeNotification(key);
            }, DELAY);
            return timeout_key;
        }
        return true;
    }
    var notification = parent.window != window ? $('#notification_' + key, parent.document) : $('#notification_' + key);
    if (notification.hasClass('cm-ajax-close-notification')) {
        var id = key.indexOf('__') != -1 ? key.substr(0, key.indexOf('__')) : key;
        jQuery.ajaxRequest(fn_url(index_script + '?close_notification=' + id), {hidden: true});
    }
    if (no_fade) {
        notification.remove();
    } else {
        notification.fadeOut('slow', function () {
            notification.remove();
        });
    }
}

// Открытие/Закрытие формы входа/восстановления пароля
function showFormPass() {
    if ($('#main_column_login').css('display') == 'none') {
        $('#main_column_pass').fadeOut();
        $('#main_column_login').fadeIn();
        $('#main_column_login input').first().focus();
    }
    else {
        $('#main_column_login').fadeOut();
        $('#main_column_pass').fadeIn();
        $('#main_column_pass input').first().focus();
    }
}

//
function showParams(id) {
    if ($('#params_group' + id).css('display') == 'none') {
        $('#params_group' + id).show(200);
        $.cookie('params_group' + id, 'show');
    }
    else {
        $('#params_group' + id).hide(200);
        $.cookie('params_group' + id, 'hide');
    }
}

//
function checkCompareItems() {
    if ($("#startcompare").is(":checked"))
        $("#compare_wrapper").show();
    else
        $("#compare_wrapper").hide();
}