//Sender module js functionality

$(document).ready(function(){
	/////////start of document ready
	$(document).on('keyup', '#sms_text', function(){
		var cyr_len=$('#sms_length_cyr').val();
		var lat_len=$('#sms_length_lat').val();
		var max_cyr_len=$('#max_sms_length_cyr').val();
		var max_lat_len=$('#max_sms_length_lat').val();
		var text=$('#sms_text').val();
		var reg = /[А-Яа-я]{1,}/;
		var symbol;
		var sms;
		if(reg.test(text)){
			if(text.length<=max_cyr_len){
				sms = Math.floor(text.length/cyr_len)+1;
				symbol=(cyr_len*sms)-text.length;
				if(sms!=4){
					$('#sms_symbol_counter').text(symbol);
					$('#sms_counter').text(sms);
				}
				else{
					$('#sms_symbol_counter').text(0);
					$('#sms_counter').text(3);
				}
				if(symbol<=5) { $('#sms_symbol_counter').addClass('few_sym_left'); }
				else { $('#sms_symbol_counter').removeClass('few_sym_left'); }
			}
			else{
				$('#sms_text').val(text.slice(0,-1));
			}
		}
		else{
			if(text.length<=max_lat_len){
				sms = Math.floor(text.length/lat_len)+1;
				symbol=(lat_len*sms)-text.length;
				if(sms!=4){
					$('#sms_symbol_counter').text(symbol);
					$('#sms_counter').text(sms);
				}
				else{
					$('#sms_symbol_counter').text(0);
					$('#sms_counter').text(3);
				}
			}
			else{
				$('#sms_text').val(text.slice(0,-1));
			}
		}
	});

	$(document).on('click', '#translit', function(){
		var text=$('#sms_text').val();
		$.ajax({type: "POST", url: "/admin/ajax/sender/translit", data: {text:text}, cache: false, success: function(data){
			$('#sms_text').val(data);
		}});
		setTimeout(function(){
			$('#sms_text').trigger('keyup');
		}, 300);

		return false;
	});

	$(document).on('change', '#notification_type', function(){
		var type=$('#notification_type').val();
		$.ajax({type: "POST", url: "/admin/ajax/sender/GetTemplatesOptions", data: {type:type}, cache: false, success: function(data){
			$('#notification_template').html(data);
		}});
		return false;
	});

	$(document).on('click', '#edit_template', function(){
		$('.preview_content').slideToggle();
		$('.edit_content').slideToggle();
	});
	$(document).on('click', '#close_edit_template', function(){
		$('.preview_content').slideToggle();
		$('.edit_content').slideToggle();
	});

	$(document).on('change', '.send_one_set_template', function(){
		var id=$('.send_one_set_template').val();
		var type=$('#notification_type').val();
		var desc=$(this).attr('data-description');
		var clean=$(this).attr('data-clean');

		if(id!=0){
			$.ajax({type: "POST", url: "/admin/ajax/sender/GetTemplateSendOne", data: {id:id, clean: clean}, dataType: 'json', cache: false, success: function(data){
				if(type=='email'){
					$('#'+type+'_theme').val(data.caption);
					tinyMCE.get('elm1').setContent(data.text, {format : 'raw'});
					if(desc=='true') {$('#'+type+'_description').html(data.description);}
				}
				else{
					$('#'+type+'_theme').val(data.caption);
					$('#'+type+'_text').html(data.text);
					if(desc=='true') {$('#'+type+'_description').html(data.description);}
				}
			}});
		}
		setTimeout(function(){
			$('#sms_text').trigger('keyup');
		}, 300);
	});

	$(document).on('click', '.check_stat', function(){
		var target=$(this).attr('target');
		console.log(target);
		if($(this).attr('checked')){
			$('.check_'+target).attr('checked','checked');
		}
		else{
			$('.check_'+target).removeAttr('checked');
		}
	});

	$(document).on('change', '.template_preview_selector', function(){
		var id=$('.template_preview_selector').val();
		if(id!=0){
			$.ajax({type: "POST", url: "/admin/ajax/sender/TemplatePreview", data: {id:id}, cache: false, success: function(html){
				$('#template_preview').html(html);
			}});
		}
		else{
			$('#template_preview').html('Выберите шаблон');
		}
	});

	$(document).on('change', '#notification_type', function(){
		var type=$(this).val();
		$('.notification_blocks').addClass('hidden');
		$('#'+type+'_edit_block').removeClass('hidden');
	});
	/////////end of document ready
});