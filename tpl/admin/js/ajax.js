$(document).ready(function () {

    const manual_button = document.getElementsByClassName('manual-button')[0];
    const manual_control = document.querySelectorAll('.manual-control');
    if (manual_button) {
	   manual_button.addEventListener('click', function () {
		  if (this.innerHTML == 'Выкл') {
			 this.innerHTML = 'Вкл';
			 manual_control.forEach(e => {
				e.classList.add('show-element');
			 });
		  } else {
			 this.innerHTML = 'Выкл';
			 manual_control.forEach(e => {
				e.classList.remove('show-element');
			 });
		  }
		  $.ajax({
			 type: "POST",
			 url: "/admin/ajax/product/manualControl",
			 data: {status: this.innerHTML},
			 success: function (data) {
			 }
		  });
	   });
    }
    //
    $(document).on('click', '#add_event', function (e) {
	   e.preventDefault();
	   var id = $('input[name=id]').val();
	   var text = $('#event_text').val();
	   var dataString = 'id=' + id + '&text=' + text;
	   if (text.length > 0) {
		  $.ajax({
			 type: "POST", url: "/admin/ajax/users/addevent", data: dataString, cache: false, success:
				function (html) {
				    $('#event_list').html(html);
				    $('#event_text').val('');
				}
		  });
	   }
    });

    // Сортировка таблицы
    $('.tb_sort').tableDnD({
	   onDragClass: "hover"
	});

	$(document).on('mouseup', ".tb_sort .move", function () {
		var isLoadPrice = false;

		if ( $("#load_price") > 0 ) {
			isLoadPrice = true;
		}
		sortA(isLoadPrice);

	});

	// Инит сортировочного списка таблицы в админке
	$('.sortable__list').sortable({
		stop: function(event,ui){
			var isLoadPrice = false;

			if ( $("#load_price") > 0 ) {
				isLoadPrice = true;
			}
			sortB(isLoadPrice);
		}
	});


	//
	$(document).on('click', '.catalog-status', function () {
		$(this).parent().text().trim() == 'Вкл.' ? turn = 'Выключить' : turn = 'Включить';
		return confirm(turn + ' все товары из каталога?');
	});

	// Active
		$(document).on('click', '.active_status', function () {
			var tb = $("#action").val();
			var tb2 = $("#action2").val();
			var id = $(this).attr('id');
			var dataString = 'id=' + id + '&tb=' + tb + '&tb2=' + tb2;
			$.ajax({
				type: "POST",
				url: "/admin/ajax/active",
				dataType: 'json',
				data: dataString,
				cache: false,
				success: function (data) {
					if (!data.access) {
						$('#' + id).html(data.active);
					}
					//alert(tb);
					$('#message').html(data.message);
					autoHide();

					if (tb == 'modules') {
						location.reload();
					}
				}
			});
		});

	const rotate = document.querySelectorAll('.rotateImg');
	rotate.forEach(e => {
		e.addEventListener('click', function () {
			let photo = [];
			if (e.parentElement.classList.contains('more_photo')) {
				let cheak_photo = document.querySelectorAll('.check-item');
				cheak_photo.forEach(e => {
					if (e.checked) photo.push(e.getAttribute('data-src'));
				});
			} else if (e.parentElement.classList.contains('one_photo')) {
				photo.push(document.getElementById('current_photo').value);
			}
			let rotate = e.getAttribute('data-rotate');
			$.ajax({
				type: "POST",
				url: "/admin/ajax/product/rotateImgs",
				data: {photo: photo, rotate: rotate},
				success: function (data) {
				}
			});
		});
	});
	const watermark = document.querySelectorAll('.watermark');
	watermark.forEach(e => {
		e.addEventListener('click', function () {
			let photo = [];
			if (e.getAttribute('data-count') == 'more') {
				let cheak_photo = document.querySelectorAll('.check-item');
				cheak_photo.forEach(e => {
					if (e.checked) photo.push(e.getAttribute('data-src'));
				});
			} else if (e.getAttribute('data-count') == 'one') {
				photo.push(document.getElementById('current_photo').value);
			}
			let table = e.getAttribute('data-table');
			$.ajax({
				type: "POST",
				url: "/admin/ajax/product/setWaterMarks",
				data: {photo: photo, table: table},
				success: function (data) {
				}
			});
		});
	});
	//
	if ($('#other_product').length > 0) {
		var id = $('input[name=id]').val(),
			module = $('input[name = module]').val(),
			dataString = 'id=' + id;
		console.log(module);
		$.ajax({
			type: "POST",
			url: "/admin/ajax/" + module + "/otherproduct",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				$('#other_product').html(data.content);
			}
		});
	}

	//
	$(document).on('change', '#product_add2', function () {
		var id = $('input[name=id]').val(),
				module = $('input[name = module]').val(),
				product_id = $(this).val(),
				dataString = 'id=' + id + '&product_id=' + product_id;

		$.ajax({
			type: "POST",
			url: "/admin/ajax/"+ module +"/otherproduct",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				$('#other_product').html(data.content);
			}
		});
	});

	//
	$(document).on('change', '#catalog_add2', function () {
		var id = $(this).val(),
				module = $('input[name = module]').val();

		if (id != 0 && id != '') {
			var id2 = $('input[name=id]').val();
			var dataString = 'id=' + id + '&id2=' + id2;
			$.ajax({
				type: "POST",
				url: "/admin/ajax/"+ module +"/orderproduct2",
				dataType: 'json',
				data: dataString,
				cache: false,
				success: function (data) {
					//alert(data.content);
					$('#drop-menu').html(data.content);
					$(".drop-menu").remove();
					//$("#drop-menu select[name=213]").remove();
					$("#drop-menu div").remove();
					$("#product_add2").searchable();
				}
			});
		}
	});

	//
	$(document).on('click', '.delother', function() {
		if(confirm('Вы уверены что хотите удалить данную запись?')){
		console.log('del');
		var id=$('input[name=id]').val();
        var del=$(this).attr('href');
        var dataString = 'id='+id+'&del='+del;
        $.ajax({type:"POST",url:"/admin/ajax/product/otherproduct",dataType:'json', data:dataString,cache:false,success:
            function(data)
            {
            	$('#other_product').html(data.content);
            }
        });
		return false;
		} else return false;
	});

	// Удаление одного коммента
	$(document).on('click', '.delcomment', function () {
		var id = $(this).attr('href');
		del_comment(id);
		return false;
	});

	// Удаление нескольких комментов
	$(document).on('click', '.del_comments', function () {
		var items = [];
		$('.comment_id:checked').each(function () {
			var id = $(this).val();
			del_comment(id);
		});
		return false;
	});

	//
	$(document).on('change', '#add_module', function () {
		var id = $(this).val();
		var dataString = 'id=' + id;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/modules/addmodule",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				if (data) {
					$('input[name=sort]').val(data.sort2);
					$('input[name=url]').val(data.url);
					$('#name_module').val(data.name);
					$('#comment_module').val(data.comment);
					$('#tables_module').val(data.tables);
					if (data.photo == 1)$('input[name=create_dir]').attr('checked', 'checked');
					$("#menu_id [value=" + data.sub2 + "]").attr("selected", true);//alert(""+data.sub2+"#menu_id [value='']");
					/*
					 $string = $row['sub']."\r\n".
					 $row['name']."\r\n".
					 $row['controller']."\r\n".
					 $row['url']."\r\n".
					 $row['tables']."\r\n".
					 $row['photo']."\r\n".
					 $row['comment']."\r\n".
					 $row['sort']."\r\n";
					 */
					if (data.config == 1)$('input[name=get_config]').attr('disabled', false);
					if (data.translate == 1)$('input[name=get_translate]').attr('disabled', false);
				}
				else {
					$('#name_module').val('');
					$('#comment_module').val('');
					$('#tables_module').val('');
				}
			}
		});
	});

	//
	$(document).on('change', '#catalog_add', function () {
		var id = $(this).val();
		var dataString = 'id=' + id;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/orders/orderproduct",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				$('#product_add').html(data.content);
			}
		});
	});

	//
	$(document).on('change', '#product_add', function () {
		var id = $(this).val();
		var order_id = $(this).attr('name');
		var dataString = 'id=' + id + '&order_id=' + order_id;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/orders/orderproductview",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				console.log(data);
				$('#order_product').html(data.content);
				$('#total').html(data.total);
			}
		});
	});

	//
	$(document).on('change', '#add_order_email', function() {
		var email = $('#add_order_email').val();
		var dataString = 'email=' + email;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/orders/fillinuserinfo",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function(data) {
				$('#address').val(data.address);
				$('#phone').val(data.phone);
				$('#post_index').val(data.post_index);
				$('#name').val(data.name);
				$('#city').val(data.city);
			}
		});
	});

	//
	$(document).on('click', '.config_price', function () {
		var id = $(this).attr('href');
		if ($('#config' + id).length > 0) {
			$('#config' + id).remove();
		}
		else {
			var photo_id = $('#photo_price' + id).val();
			var texture_id = $('#texture_price' + id).val();
			var product_id = $("input[name=id]").val();
			var tr_id = $(this).parent().parent().parent().parent().attr('id');
			var dataString = 'id=' + id + '&photo_id=' + photo_id + '&texture_id=' + texture_id + '&product_id=' + product_id;
			$.ajax({
				type: "POST",
				url: "/admin/ajax/product/configprice",
				dataType: 'json',
				data: dataString,
				cache: false,
				success: function (data) {
					$('#config' + id).remove();
					$('#' + tr_id).after(data.content);
				}
			});
		}
		return false;
	});


	$(document).on('mouseup',".move", function () {
		var id = $(this).parent().attr('id');
		sortA(id);
	});

	//
	$(document).on('click', '.price_photo', function () {
		var id = $(this).parent().attr('dir');
		var photo_id = $(this).attr('alt');
		var check = $(this).hasClass('selected');
		$('#config' + id + ' .price_photo').removeClass('selected');
		if (!check) {
			$(this).addClass('selected');
			$('#photo_price' + id).val(photo_id);
		}
		else $('#photo_price' + id).val('');
		return false;
	});

	//
	$(document).on('click', '.price_texture', function () {
		var id = $(this).parent().attr('dir');
		var photo_id = $(this).attr('alt');
		var check = $(this).hasClass('selected');
		$('#config' + id + ' .price_texture').removeClass('selected');
		if (!check) {
			$(this).addClass('selected');
			$('#texture_price' + id).val(photo_id);
		}
		else $('#texture_price' + id).val('');
		return false;
	});

	//
	$(document).on('click', '#addprice', function () {
		var rel = $(this).attr('rel');
		var id = $("input[name=id]").val();
		var dataString = 'id=' + id + '&rel=' + rel;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/product/addprice",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				var price_price = $("input[name='price_price[]']").val();
				var purch_price = $("input[name='purch_price[]']").val();
				var price_margin = $("input[name='price_margin[]']").val();
				var price_discount = $("input[name='price_discount[]']").val();
				var priceBase = $("input[name='price_basePrice[]']").val();
				if (rel == 'add') {
					$('#load_price').append(data.content);
				}
				else $('#load_price').html(data.content);
				if ($('#one_price').attr('checked')) {
					$("input[name='price_price[]']").val(price_price);
					$("input[name='purch_price[]']").val(purch_price);
					$("input[name='price_margin[]']").val(price_margin);
					$("input[name='price_discount[]']").val(price_discount);
					$("input[name='price_basePrice[]']").val(priceBase);
				}
			}
		});
		return false;
	});

	//
	$(document).on('click', '.delprice', function () {
		var conf = confirm('Вы уверены что хотите удалить данную запись?');
		if (conf) {
			var rel = $(this).attr('rel');
			if (rel == 'add') {
				var id = $(this).parent().parent().parent().parent();
				$(id).remove();
			}
			else {
				var id = $(this).attr('href');
				var dataString = 'id=' + id;
				$.ajax({
					type: "POST",
					url: "/admin/ajax/product/delprice",
					dataType: 'json',
					data: dataString,
					cache: false,
					success: function (data) {
						$('#load_price').html(data.content);
					}
				});
			}
		}
		return false;
	});

	$(document).on('click', '#add_iblock', function () {
		let id = $("input[name=id]").val();
		let dataString = 'id=' + id;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/pages/addIBlockRow",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				$('#load_info_block').html(data.output);
			}
		});
		return false;
	});

	$(document).on('click', '.del_iblock', function () {
		let id = $("input[name=id]").val();
		let row_id = $(this).closest('tr').attr('data-row');
		console.log(row_id);
		let dataString = 'id=' + id + '&row_id=' + row_id;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/pages/delIBlockRow",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				$('#load_info_block').html(data.output);
			}
		});
		return false;
	});

	$(document).on('click', '#menu_add_iblock', function () {
		let id = $("input[name=id]").val();
		let dataString = 'id=' + id;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/menu/addIBlockRow",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				$('#load_info_block').html(data.output);
			}
		});
		return false;
	});

	$(document).on('click', '.menu_del_iblock', function () {
		let id = $("input[name=id]").val();
		let row_id = $(this).closest('tr').attr('data-row');
		console.log(row_id);
		let dataString = 'id=' + id + '&row_id=' + row_id;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/menu/delIBlockRow",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				$('#load_info_block').html(data.output);
			}
		});
		return false;
	});

	$(document).on('change', 'select[name=provider]', function () {
		var dataString = 'id=' + $(this).val();
		$.ajax({
			type: "POST",
			url: "/admin/ajax/product/getprovider",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				if (!data.access) {
					$("input[name='price_margin[]']").each(function () {
						$(this).val(data.price);
					});
				}
			}
		});
	});

	// Active
	$(document).on('click', '.del_image', function () {
		var id = $(this).attr('dir');
		var id2 = $(this).attr('id');
		var path = $(this).attr('href');
		var action = $('#action').val();
		var dataString = 'id=' + id + '&path=' + path + '&action=' + action;
		$.ajax({
			type: "POST",
			url: "/admin/ajax/delimage",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				if (!data.access) {
					$('#' + id + ' img').attr('src', '/tpl/admin/images/no_image.jpg');
					$('#' + id2).hide();
				}
			}
		});
		return false;
	});

	// Генерирует случайный пароль
	$(document).on('click', '#generate_new_pass', function (e) {
		e.preventDefault();

		var $this = $(this);

		$.ajax({
			type: "POST",
			url: "/admin/ajax/generatePass",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				// console.log(data);
				$('#input-old_pass, #input-new_pass').val(data);

				if( $this.siblings('.text-success').length ) {
					$this.siblings('.text-success').html('Новый пароль сгенерирован! Сохраните профиль, чтобы отправить письмо с новым паролем клиенту!');
				} else {
					$this.after('<div class="text-success">Новый пароль сгенерирован! Сохраните профиль, чтобы отправить письмо с новым паролем клиенту!</div>');
				}
			}
		});
	});

	// Show|hide modules in admin menu
	// $(document).on('click', '#modules-table .selected-status', function (e) {
	// 	e.preventDefault();
  //
	// 	var $this = $(this),
	// 			id = $this.parents('tr').attr('id').replace('sort', ''),
	// 			is_hidden = 0;
  //
	// 	if( $this.hasClass('status-a') ) {
	// 		$this.removeClass('status-a').addClass('status-d');
	// 		$this.children().text('Выкл.');
	// 		is_hidden = 1;
	// 	} else {
	// 		$this.addClass('status-a').removeClass('status-d');
	// 		$this.children().text('Вкл.')
	// 	}
  //
	// 	var dataString = 'id=' + id + '&is_hidden=' + is_hidden + '&update=true';
  //
	// 	// console.log(dataString);
  //
	// 	$.ajax({
	// 		type: "POST",
	// 		url: "/admin/modules/update",
	// 		dataType: 'json',
	// 		data: dataString,
	// 		cache: false,
	// 		success: function (data) {
	// 			console.log('111');
	// 			$(document).html(data);
	// 		}
	// 	});
	// });

});

/////////////
// FUNCTIONS
/////////////

// Удаление комментария
function del_comment(id) {
	var dataString = 'id=' + id;
	$.ajax({
		type: "POST",
		url: "/admin/ajax/comments/del",
		dataType: 'json',
		data: dataString,
		cache: false,
		success: function (data) {
			$('#comments_load').html(data.content);
			$('#content_comments').show();
		}
	});
}

// Сортировка списка в таблицах админки (меню и т.д.)
// function sortA(isLoadPrice) {
// 	var tb = $("#action").val();
// 	var tb2 = $("#action2").val();
// 	if (isLoadPrice == true) {
// 		tb2 = 'price';
// 		$('.price_config').remove();
// 	}
// 	var arr = serialize_rows(".sortable__list .row");
// 	var dataString = 'arr=' + arr + '&tb=' + tb + '&tb2=' + tb2;
//
// 	// console.log(dataString);
//
// 	$.ajax({
// 		type: "POST",
// 		url: "/admin/ajax/sort",
// 		dataType: 'json',
// 		data: dataString,
// 		cache: false,
// 		success: function (data) {
// 			$('#message').html(data.message);
// 			autoHide();
// 		}
// 	});
// }

function sortA(id) {
	var tb = $("#action").val();
	var tb2 = $("#action2").val();
	if (id == true) {
		tb2 = 'price';
		$('.price_config').remove();
	}
	var arr = $(".tb_sort").tableDnDSerialize();
	var dataString = 'arr=' + arr + '&tb=' + tb + '&tb2=' + tb2;

	console.log(dataString);

	$.ajax({
		type: "POST",
		url: "/admin/ajax/sort",
		dataType: 'json',
		data: dataString,
		cache: false,
		success: function (data) {
			$('#message').html(data.message);
			autoHide();
		}
	});
}

function sortB(isLoadPrice) {
	var tb = $("#action").val();
	var tb2 = $("#action2").val();
	if (isLoadPrice == true) {
		tb2 = 'price';
		$('.price_config').remove();
	}
	var arr = serialize_rows(".sortable__list .row");
	var dataString = 'arr=' + arr + '&tb=' + tb + '&tb2=' + tb2;

	// console.log(dataString);

	$.ajax({
		type: "POST",
		url: "/admin/ajax/sort",
		dataType: 'json',
		data: dataString,
		cache: false,
		success: function (data) {
			$('#message').html(data.message);
			autoHide();
		}
	});
}

// TODO: Описание функции onoroff
function onoroff () {
	$('.selected-status a').html().trim() == 'Вкл.' ? turn = 'Выключить' : turn = 'Включить';
	if (confirm(turn + ' все товары из каталога?')) {
		return true;
	} else {
		return false;
	}
}

function serialize_rows(rows) {
	var $rows = $(rows),
			result = '';

	if( $rows.length > 0 ) {
		$rows.each(function () {
			var rowId = $(this).attr('id');

			if (result.length > 0) {
				result += "#";
			}

			result += '[]=' + rowId;
		});
	}

	return result;
}

function checkMetaUrl($input) {
	$url = $input.val();
	dataString = 'url=' + $url;
	$.ajax({
		type: "POST",
		url: "/admin/ajax/meta/checkUrl",
		dataType: 'json',
		data: dataString,
		cache: false,
		success: function (data) {
			$('#messageMeta').html(data.content);
		}
	});
}