/**
 * Created by www on 29.09.2015.
 */

$(document).on('change', 'input[type=radio]', function () {
	var num = $(this).attr('rel');
	$('td').removeClass('active');
	$('.col-num' + num).addClass('active');
	if (num = 2) $('#album_id').removeAttr('disabled');
	else $('#album_id').attr('disabled', 'disabled');
});
$(document).on('click', '#show_content', function () {
	var content_type = $('#content_type').val();
	var date_from = $('#date_from').val();
	var date_to = $('#date_to').val();
	$.ajax({
		type: "POST",
		url: "/admin/ajax/vkontakte/getContent",
		data: {content_type: content_type, date_from: date_from, date_to: date_to},
		dataType: 'json',
		cache: false,
		success: function (data) {
			if (data != false) {
				$('#publish_button').show();
				$('#push_to_queue').show();
				$('#content_container').html('');
				data.forEach(function (item, i, data) {
					if(item.photo!=''){
					$('#content_container').append('<div class="content_block"><input type="checkbox" class="check-item" value="' + item.id + '"/><img src="/' + item.photo + '"/><hr/><p>' + item.name + '</p><hr/>' +
						'<p class="body_m">' + item.body_m + '</p><hr/>' +
						'<p>http://' + window.location.hostname + '/' + content_type + '/' + item.url + '</p></div>');
					}
				});
			}
		}
	});
});
$(document).on('click', '#publish_button', function () {
	var content_type = $('#content_type').val();
	var post_target = $('input[name=post_in]:checked').val();
	var album = $('#album_id').val();
	var content_id = [];

	if(post_target=='album' && album=='null'){
		alert('Выберите альбом!');
		return false;
	}

	if($('.check-item:checked').length>0){
		$('.check-item:checked').each(function(){
			content_id.push($(this).val());
		});

		console.log('Тип контента: ' + content_type);
		console.log('Куда публиковать: ' + post_target);
		console.log('ID альбома: ' + album);
		console.log('ID объектов контента: ' + content_id);

		$.ajax({
			type: "POST",
			url: "/admin/ajax/vkontakte/publishContent",
			data: {type: content_type, target: post_target, album: album, content: content_id},
			cache: false,
			success: function (data) {
				if (data != '"Error"') {
					alert("Контент опубликован!\r\n");
					console.log(data);
				}
				else{
					alert('Ошибка!');
					console.log(data);
				}
			}
		});
	}
	else {
		alert('Вы не выбрали контент к публикации!');
	}
});
$(document).on('click', '#push_to_queue', function () {
	var content_type = $('#content_type').val();
	var post_target = $('input[name=post_in]:checked').val();
	var album = $('#album_id').val();
	var content_id = [];

	if(post_target=='album' && album=='null'){
		alert('Выберите альбом!');
		return false;
	}

	if($('.check-item:checked').length>0){
		$('.check-item:checked').each(function(){
			content_id.push($(this).val());
		});

		console.log('Тип контента: ' + content_type);
		console.log('Куда публиковать: ' + post_target);
		console.log('ID альбома: ' + album);
		console.log('ID объектов контента: ' + content_id);

		$.ajax({
			type: "POST",
			url: "/admin/ajax/vkontakte/queuePush",
			data: {type: content_type, target: post_target, album: album, content: content_id},
			cache: false,
			success: function (data) {
				if (data != '"Error"') {
					alert("Контент добавлен в очередь!\r\n");
					console.log(data);
				}
				else{
					alert('Ошибка!');
					console.log(data);
				}
			}
		});
	}
	else {
		alert('Вы не выбрали контент для добавления!');
	}
});