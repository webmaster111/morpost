//MAGNIFIC POPUP
$(document).ready(function () {
    $('.images-block').magnificPopup({
	   delegate: 'a',
	   type: 'image',
	   gallery: {
		  enabled: true
	   }
    });

    let box = $('#cart');
    let top = box.offset().top - parseFloat(box.css('marginTop').replace(/auto/, 0));
    $(window).scroll(function () {
	   if ($(window).width() > 767 || true) {
		  if ($(window).scrollTop() < top) {
			 box.removeClass('sticky');
			 box.css('width', '100%');
		  } else {
			 box.css('width', Math.ceil(box.width()));
			 box.addClass('sticky');
		  }
	   }
    });
});

(function ($) {

    "use strict";

    // TOOLTIP
    $(".header-links .fa, .tool-tip").tooltip({
	   placement: "bottom"
    });
    $(".btn-wishlist, .btn-compare, .display .fa").tooltip('hide');

    // TABS
    $('.nav-tabs a').click(function (e) {
	   e.preventDefault();
	   $(this).tab('show');
    });

})(jQuery);