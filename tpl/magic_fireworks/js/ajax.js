const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 4000
});


$(document).ready(function () {
    basket();
    // Add to shop cart
    $(document).on('click', '.buy-product', function () {
	   $.ajax({
		  type: "POST",
		  url: "/ajax/orders/incart",
		  data: $(this).parents('form').serialize(),
		  dataType: "json",
		  cache: false,
		  success: function (data) {
			 toast({
				type: data.status,
				title: data.message
			 });
			 $('#cart .btn').animate({backgroundColor: 'rgba(0,255,00,0.5)'}, 500, 'swing', function () {
				$(this).animate({backgroundColor: 'white'}, 200)
			 });
			 basket();
		  },
		  error: () => console.log('Add to shop cart ERROR')
	   });
    });


    //Search form
    x = '';
    $(document).on('keyup', "#search_query_top", function (e) {
	   clearTimeout(x);
	   if ($(this).val().length > 1) {
		  var word = $(this).val();
		  x = setTimeout(function () {
			 autosearch(word);
		  }, 300);
	   } else $('#results_search').css('display', 'none');
    });

    //Select in searck list
    $(document).on('click', "#results_search li", function () {
	   $('#results_search').hide(200);
    });

    //Hide search list
    $("body").on('click', function () {
	   $('#results_search').hide(200);
    });

    $(document).on('submit', '.contact-form, #callback-modal form, #order-modal  form, #buy-modal form', function (e) {
	   e.preventDefault();
	   sendFeedback($(this));
    });

    $(document).on('submit', '#login-form', function (e) {
	   e.preventDefault();
	   AuthUser($(this));
    });

});


$(document).on('submit', '.subs-form', function (e) {
    e.preventDefault();
    let form = $(this);
    $.ajax({
	   type: "POST",
	   url: "/ajax/mailer/subscribers",
	   data: form.serialize(),
	   dataType: 'json',
	   cache: false,
	   success: function (data) {
		  swal({
			 showCloseButton: true,
			 type: data.status,
			 html: data.message
		  });
		  if (data.status === 'success') {
			 form[0].reset();
		  }
	   },
	   error: () => console.log('subscribers ERROR')
    });
});


$(document).on('click', '.subscribe-modal-btn', function () {
    let subscribeBox = $('#subscribe-modal');
    let validate = subscribeBox.find('.subscribe-validate');
    let email = subscribeBox.find('.subscribe-modal-input-email').val();

    let checkValidateEmail = email.match(/^[a-zA-Z0-9_.-]+@([a-zA-Z0-9-]+.)+[a-zA-Z0-9]{2,4}$/);

    if (checkValidateEmail) {
	   $.ajax({
		  type: 'POST',
		  data: {email},
		  dataType: 'json',
		  url: '/ajax/mailer/subscribers',
		  success: function (data) {
			 if (data.status === 'success') {
				subscribeBox.find('.swal2-success-custom').css('display', 'block');
				subscribeBox.find('.subscribe-modal-content').css('display', 'none');
				subscribeBox.find('.swal2-success-custom-text').text(data.message);
			 } else if (data.status === 'error') {
				validate.text(data.message);
			 }
		  },
		  error: function (e) {
			 validate.text('Что-то пошло не так. Попробуйте позже')
		  }
	   });
    } else {
	   validate.text('Не правильный формат Email');
    }
});

function updateQuantity($number, $cart_id) {
    if ($number && $cart_id)
	   $.ajax({
		  type: "POST",
		  url: "/ajax/orders/quantity",
		  data: 'cat_id=' + $cart_id + '&quantity=' + $number,
		  cache: false,
		  dataType: 'json',
		  success: function (data) {
			 console.log(data);
			 $('.shopping-cart-table td div[data-cart-sum="' + $cart_id + '"]').html(data.sum);
			 basket();
		  },
		  error: () => console.log('updateQuantity ERROR')
	   });
}

function DeleteFromBasket(cart_id, target) {
    Swal.fire({
	   title: 'Вы уверены ?',
	   text: "удаление тавара из корзины",
	   type: 'warning',
	   showCancelButton: true,
	   confirmButtonColor: '#3085d6',
	   cancelButtonColor: '#d33',
	   confirmButtonText: 'Да, удалить !',
	   cancelButtonText: 'Отмена',
    }).then((result) => {
	   if (result.value) {
		  $.ajax({
			 type: "POST",
			 url: "/ajax/orders/deleteproduct",
			 data: 'id_basket=' + cart_id,
			 cache: false,
			 dataType: 'json',
			 success: function (data) {
				console.log(data);
				if (data.count > 0) target.remove();
				else window.location = '/orders/preview';
				basket();
			 },
			 error: () => console.log('')
		  });
	   }
    })
}

$(document).on('submit', '#add-comment-form', function (e) {
    e.stopPropagation();
    let form = $(this);
    if (CustomValidateForm(form)) {
	   $.ajax({
		  type: "POST",
		  url: "/ajax/comments/add",
		  data: new FormData(this),
		  contentType: false,
		  processData: false,
		  dataType: 'json',
		  cache: false,
		  success: function (data) {
			 swal({
				showCloseButton: true,
				type: data.status,
				html: data.message
			 });
			 grecaptcha.reset();
			 if (data.status === 'success') {
				form[0].reset();
			 }
		  },
		  error: () => console.log('add-comment ERROR')
	   });
    }
    return false;
});


//basket
function basket() {
    $.ajax({
	   type: "POST",
	   url: "/ajax/orders/getBasket",
	   cache: false,
	   dataType: 'json',
	   success: function (data) {
		  $('.basket-text').html(data.count_format);
		  if (data.count > 0) {
			 $('.basket-button').addClass('active').find('.count').html(data.count);
			 $('.basket-total-sum').each(function (key, value) {
				$(value).html(data.total);
			 });
		  } else {
			 $('.basket-button').removeClass('active').find('.count').html('');
		  }
	   }
    });
}


function AuthUser(form) {
    if (CustomValidateForm(form)) {
	   $.ajax({
		  type: "POST",
		  url: "/ajax/users/auth",
		  data: form.serialize(),
		  dataType: 'json',
		  cache: false,
		  success: function (data) {
			 console.log(data);
			 if (data.status === 'success') {
				swal({
				    showConfirmButton: false,
				    type: 'success',
				    html: data.message,
				    timer: 1234,
				    onBeforeOpen: () => {
					   Swal.showLoading()
				    },
				    onClose: () => {
					   window.location.reload()
				    }
				});
				form[0].reset();
			 } else showInfoSweet(data.message);
		  },
		  error: () => console.log('AuthUser ERROR')
	   });
    }
}

function sendFeedback(form) {
    if (CustomValidateForm(form)) {
	   $.ajax({
		  type: "POST",
		  url: "/ajax/feedback/feedback",
		  data: form.serialize(),
		  dataType: 'json',
		  cache: false,
		  success: function (data) {
			 if (data.type === 'toast') {
				toast({
				    type: data.status,
				    title: data.message
				})
			 } else {
				swal({
				    showCloseButton: true,
				    type: data.status,
				    html: data.message
				});
			 }
			 grecaptcha.reset();
			 if (data.status === 'success') {
				form[0].reset();
				gtag('event', 'send', {'event_category': 'forma_obr_sv', 'event_action': 'send'});
				setTimeout(function () {
				    form.parents('.custom-modal').fadeOut();
				    form.parents('.modal').modal('hide');
				}, 2000);
			 }
		  },
		  error: () => console.log('feedback ERROR')
	   });
    }
}

function autosearch(word) {
    $.ajax({
	   type: "POST", url: "/ajax/catalog/search", data: 'word=' + word, cache: false, success: function (data) {
		  $('#results_search').html(data).show(200);
	   }
    });
}

// -- add more data to div "load-more"

function showSlideDown() {
    $('.show-slide-down').each(function (key, value) {
	   $(value).slideToggle(function () {
		  $(this).removeClass('show-slide-down');
	   });
    });
}

// -- load more
function getMoreThumbs(elem, module) {
    $('.load-more-gif').addClass('fa-spin');
    var offset_num = $(elem).attr('data-offset');
    console.log(offset_num);
    $.ajax({
	   type: "POST",
	   url: "/ajax/" + module + "/getMore?" + $(elem).attr('data-get'),
	   data: {offset: offset_num, catalog: $(elem).data('catalog')},
	   dataType: 'json',
	   cache: false,
	   success: function (data) {
		  console.log(data);
		  if (data.offset === 0) {
			 $('.load-more-gif').fadeOut();
			 $(elem).fadeOut();
		  } else $(elem).attr('data-offset', data.offset);
		  $(elem).find('.count-more').html(data.count);
		  $('#load-more').append(data.thumbs);
		  showSlideDown();
	   },
	   error: function () {
		  console.log('getMoreThumbs ERROR')
	   }
    });
}
