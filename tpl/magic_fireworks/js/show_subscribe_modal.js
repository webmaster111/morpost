$(document).ready(function () {

    function randInt(min, max) {
	   let rand = min - 0.5 + Math.random() * (max - min + 1);
	   return Math.round(rand);
    }

    console.log($.cookie('showedModal'));
    if (!$.cookie('showedModal')) {
	   if (!$.cookie('newDater')) $.cookie('newDater', randInt(15, 30));

	   window.intervalModal = setInterval(function () {
		  $.cookie('newDater', $.cookie('newDater') - 1);

		  if ($.cookie('newDater') <= 0) {
			 $.cookie('showedModal', 1);
			 clearInterval(window.intervalModal);
			 $('#subscribe-modal').modal('show');
		  }

	   }, 1000);
    }
});