'use strict';
$(function () {

    $(window).on('scroll', function () {
	   if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100)
		  $("button.go-top").fadeIn();
	   else $("button.go-top").fadeOut();
    });

    $("button.go-top").click(function () {
	   $("html, body").animate({scrollTop: 0}, "slow");
	   return false;
    });

    $('.btn-cart-quick').click(function () {
	   let img = $('.images-block img').attr('src');
	   let id = $('.input-number').attr('data-id');
	   let amount = $('.input-number').attr("value");
	   $('.input-number-modal').attr("value", amount);
	   $('.input-number-modal').attr("data-id", id);
	   $('.left_quick').html(`<img src="${img}">`);
    });

    $('.quantity-right-plus').click(function (e) {
	   e.preventDefault();
	   let QC = $(this).parents('.quantity-control').find('input');
	   let quantity = parseInt(QC.val());
	   QC.val(quantity + 1);
	   if (QC.data('cart-id'))
		  updateQuantity(quantity + 1, QC.data('cart-id'));
    });

    $('.quantity-left-minus').click(function (e) {
	   e.preventDefault();
	   let QC = $(this).parents('.quantity-control').find('input');
	   let quantity = parseInt(QC.val());
	   if (quantity > 1) {
		  if (QC.data('cart-id'))
			 updateQuantity(quantity - 1, QC.data('cart-id'));
		  QC.val(quantity - 1);
	   }
    });

    $('.catalog-title-xs').click(function () {
	   $('.catalog-list-xs').slideToggle();
    });
    $('.rating').click(function () {
	   Swal.fire({
		  type: 'success',
		  text: 'Рейтинг товара будет изменен!!!',
	   })
    });

    $('input[data-type="phone"]').each(function (key, value) {
	   $(value).mask('(099) 999-99-99', {
		  translation: {
			 '9': {pattern: /[0-9]/},
			 '0': {pattern: /0/}
		  }
	   });
    });

    $('input[data-type="letters"]').attr('maxlength', 35).mask('Z', {
	   translation: {
		  'Z': {
			 pattern: /[а-яА-Яa-zA-Z\s]/,
			 recursive: true
		  }
	   }
    });

    if ($(window).width() < 768)
	   $('#add-comment-form textarea').attr('rows', '2');

    autosize($('textarea'));

    $('#advantages.owl-carousel').owlCarousel({
	   navText: [
		  '<span class="fa fa-angle-left"></span>',
		  '<span class="fa fa-angle-right"></span>'
	   ],
	   responsive: {
		  0: {
			 nav: true,
			 items: 1
		  },
		  600: {
			 nav: true,
			 items: 2
		  },
		  1000: {
			 nav: false,
			 items: 3
		  }
	   }
    });

    $('#video-slider.owl-carousel').owlCarousel({
	   margin: 30,
	   dots: false,
	   nav: true,
	   navText: [
		  '<span class="fa fa-angle-left"></span>',
		  '<span class="fa fa-angle-right"></span>'
	   ],
	   responsive: {
		  0: {
			 items: 1
		  },
		  600: {
			 items: 2
		  },
		  1000: {
			 items: 3
		  }
	   }
    });

    $('.product-photos-slider').owlCarousel({
	   margin: 30,
	   items: 3,
	   nav: true,
	   dots: false,
	   navText: [
		  '<span class="fa fa-angle-left"></span>',
		  '<span class="fa fa-angle-right"></span>'
	   ],
	   responsive: {
		  0: {
			 items: 2
		  },
		  550: {
			 items: 3
		  },
		  768: {
			 items: 2
		  },
		  1200: {
			 items: 3
		  }
	   }
    });

    $(".slider_main").owlCarousel({
	   loop: false,
	   smartSpeed: 1000,
	   nav: false,
	   dots: false,
	   items: 1
    });
});

// start DELIVERY PICKERS
$(document).on('change', '#delivery', function () {
    let id = parseInt($(this).val());
    if (id === 4) show_NP_cities();
    else disableSelects_delivery();
});

function disableSelects_delivery() {
    $('#delivery_address').show();
    $('#delivery_city').html('');
    $('#delivery_department').html('');
}

$(document).on('change', '#delivery_city select', function () {
    $('#delivery_department select').attr('disabled', 'disabled').html('');
    show_NP_departments($(this).find(':selected').data('ref'));
});


function show_NP_cities() {
    $('#delivery_address').hide();
    $('#delivery_city').html(ShowSpinner());
    $.ajax({
	   type: "POST",
	   url: "/ajax/novaposhta/getcities",
	   data: {name: 'delivery_city', required: true},
	   dataType: "json",
	   cache: false,
	   success: function (data) {
		  if (data.html) $('#delivery_city').html(data.html);
	   },
	   error: () => console.log("novaposhta | delivery city ERROR")
    });
}

function ShowSpinner() {
    return '<div class="form-group" style="height: 34px;display: flex;justify-content: center;align-items: center">' +
	   '<i class="fa fa-spinner fa-spin"></i>' +
	   '</div>';
}

function show_NP_departments(ref) {
    if (ref) {
	   $('#delivery_department').html(ShowSpinner());
	   $.ajax({
		  type: "POST",
		  url: "/ajax/novaposhta/getdepartments",
		  data: {name: 'delivery_department', required: true, ref: ref},
		  dataType: "json",
		  cache: false,
		  success: function (data) {
			 if (data.html) $('#delivery_department').html(data.html);
		  },
		  error: () => console.log("show_NP_departments ERROR")
	   });
    }
}


$(document).on('click', '.catalog-menu-button', function (e) {

    $('nav.catalog-menu').slideToggle();
    $(this).toggleClass('active');
});

$(document).on('click', '.header-phones .drop-arrow', function (e) {
    $('.header-phones .interesting-list').css('height', 'auto');
});

$(document).on('click', '.add-video', function (e) {
    $(this).toggleClass('active').next().slideToggle('fast');
});

$(document).on('change', '.comment-file input', function () {
    const name = $(this).val().split(/\\|\//).pop();
    const truncated = name.length > 20
	   ? name.substr(name.length - 20)
	   : name;
    if (truncated) {
	   $(this).parent().find('.text-holder').css('display', 'none');
	   $(this).parent().find('.upload-file').css('display', 'block').html(truncated);
    } else {
	   $(this).parent().find('.text-holder').css('display', 'block');
	   $(this).parent().find('.upload-file').css('display', 'none');
    }
});

$(document).on('click', '.comment-file .text-holder,.comment-file .upload-file', function () {
    $(this).parent().find('input').click();
});

$(document).on('click', '.product-photo-mini', function () {
    let main = $('.product-photo'),
	   current = $(this),
	   image = current.find('img').attr('src');
    main.find('img').attr('src', image);
    console.log($(this).find('img').attr('src'));
});


//custom checkbox
$(document).on('click', '.custom-checkbox', function (e) {
    e.preventDefault();
    $(this).toggleClass('checked');
    let input = $(this).find('input');
    if (input.attr('checked')) input.removeAttr('checked');
    else input.attr('checked', 'checked')
});

$(document).on('click', '.custom-radio', function (e) {
    e.preventDefault();
    $('.custom-radio input[name="' + $(this).find('input').attr('name') + '"]').each(function (key, value) {
	   $(value).removeAttr('checked').parent().removeClass('checked');
    });
    $(this).addClass('checked').find('input').attr('checked', 'checked');
});


function resetBorders(form, color) {
    form.find('input , textarea').each(function (key, value) {
	   $(value).css('border-color', color);
    });
}

// custom validation
function CustomValidateForm(form) {
    resetBorders($(form), '#dedede');
    let no_errors = true, color = 'rgba(255,0,0,0.5)';
    $(form).find('.validate').each(function (key, value) {
	   if ($(value).val().length < 4) {
		  no_errors = false;
		  $(value).css('border-color', color);
	   }
    });
    $(form).find('.validate-email').each(function (key, value) {
	   if (!(/^[a-zA-Z0-9_.-]+@([a-zA-Z0-9-]+.)+[a-zA-Z0-9]{2,4}$/.test($(value).val()))) {
		  no_errors = false;
		  $(value).css('border-color', color);
	   }
    });
    $(form).find('.validate-phone').each(function (key, value) {
	   if (!(/^([+][0-9]{1,3})?([(]{1}[0-9]{2,4}[)])?([0-9 .-/]{7,20})((x|ext|extension)[ ]?[0-9]{2,4})?$/.test($(value).val()))) {
		  no_errors = false;
		  $(value).css('border-color', color);
	   }
    });

    $(form).find('.validate-pas-r').each(function (key, value) {
	   if ($(value).val() !== $(form).find('input[name="password-repeat"]').val()) {
		  no_errors = false;
		  $(form).find('input[name="password-repeat"]').css('border-color', color);
		  $(value).css('border-color', color);
	   }
    });
    return no_errors;
}


// -------- custom modal
$(document).on('click', '.custom-modal', function () {
    $(this).fadeOut(300);
});
$(document).on('click', '.custom-modal .close-modal', function () {
    $(this).parents('.custom-modal').fadeOut(300);
});
$(document).on('click', '[data-m-target]', function () {
    $($(this).data('m-target')).css("display", "flex").hide().fadeIn(300);
});
$(document).on('click', '.custom-modal .content', function (e) {
    e.stopPropagation();
});

// --- sweet
function showInfoSweet(message, confirm = false, type = 'info', timer = false) {
    if (timer)
	   swal({
		  showConfirmButton: confirm,
		  type: type,
		  html: message,
		  timer: 2000,
		  onBeforeOpen: () => {
			 Swal.showLoading()
		  }
	   });
    else
	   swal({
		  showCloseButton: true,
		  showConfirmButton: confirm,
		  type: type,
		  html: message
	   });
}

$(function () {
    $('#bookmarkme').click(function () {
	   if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
		  window.sidebar.addPanel(document.title, window.location.href, '');
	   } else if (window.external && ('AddFavorite' in window.external)) { // IE Favorite
		  window.external.AddFavorite(location.href, document.title);
	   } else if (window.opera && window.print) { // Opera Hotlist
		  this.title = document.title;
		  return true;
	   } else { // webkit - safari/chrome
		  alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
	   }
    });
});