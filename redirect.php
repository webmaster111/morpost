<?php
    if (preg_match('@/ru@', $_SERVER['REQUEST_URI']) && !preg_match('@/admin@', $_SERVER['REQUEST_URI'])) {
	   $url = str_replace('/ru', '', $_SERVER['REQUEST_URI']);
	   header('HTTP/1.1 301 Moved Permanently');
	   header('Location: https://' . $_SERVER['HTTP_HOST'] . $url);
	   exit();
    }