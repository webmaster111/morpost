<?
class Mails extends Model
{
	static $table='mails';
	static $name='Шаблоны писем';
	private static $mail='<html><head><style>{{style}}</style></head><body>{{content}}</body></html>';

	public function __construct($registry)
	{
		parent::getInstance($registry);
	}

	public static function getObject($registry)
	{
		return new self::$table($registry);
	}

	public function add()
	{
		if(isset($_POST['name'],$_POST['m_header'],$_POST['footer'],$_POST['active'],$_POST['css'])){
			$tp=1;
			$id=$this->db->insert_id('insert into '.self::$table.' set `name`=?,`type`=?,header=?,footer=?,style=?,active=?',[$_POST['name'],1,$_POST['m_header'],$_POST['footer'],$_POST['css'],$_POST['active']]);
			$message=messageAdmin('Стиль письма успешно добавлен!');
		}elseif(isset($_POST['name'],$_POST['m_header'],$_POST['footer'],$_POST['active'],$_POST['title'],$_POST['body'])){
			$tp=0;
			$id=$this->db->insert_id('insert into '.self::$table.' set `name`=?,title=?,`type`=?,header=?,footer=?,body=?,style_id=?,active=?',[$_POST['name'],$_POST['title'],0,$_POST['m_header'],$_POST['footer'],$_POST['body'],$_POST['style_id'],$_POST['active']]);
			$message=messageAdmin('Письмо успешно добавлено!');
		}else{
			$id=$tp=-1;
			$message=messageAdmin('При добавлении возникла ошибка!','error');
		}
		return (isset($_POST['add_open'])?['message'=>$message,'id'=>$id,'type'=>$tp]:$message);
	}

	public function save()
	{
		if(isset($_POST['name'],$_POST['m_header'],$_POST['footer'],$_POST['active'],$_POST['css'])){
			$this->db->query('update '.self::$table.' set `name`=?,`type`=?,header=?,footer=?,style=?,active=? where id=?',[$_POST['name'],1,$_POST['m_header'],$_POST['footer'],$_POST['css'],$_POST['active'],$_POST['id']]);
			$message=messageAdmin('Стиль письма успешно обновлен!');
		}elseif(isset($_POST['name'],$_POST['m_header'],$_POST['footer'],$_POST['active'],$_POST['title'],$_POST['body'])){
			$this->db->query('update '.self::$table.' set `name`=?,title=?,`type`=?,header=?,footer=?,body=?,style_id=?,active=? where id=?',[$_POST['name'],$_POST['title'],0,$_POST['m_header'],$_POST['footer'],$_POST['body'],$_POST['style_id'],$_POST['active'],$_POST['id']]);
			$message=messageAdmin('Письмо успешно обновлено!');
		}else $message=messageAdmin('При обновлении возникла ошибка!','error');
		return $message;
	}

	public function getList($type)
	{
		return $this->find(['select'=>'*','where'=>'type="'.$type.'"','order'=>'sort asc,id desc','paging'=>'30']);
	}

	public function getSiteName()
	{
		return $this->db->cell('select `value` from config where `name`=?',['sitename']);
	}

	public function getData($id)
	{
		return $this->db->row('select title,body,header,footer,style_id from mails where id=?',[$id]);
	}

	public function getFullData($id)
	{
		return $this->db->row('select tb.title,tb.body,tb.header,tb2.header as sheader,tb.footer,tb2.footer as sfooter,tb2.style from mails tb left join mails tb2 on tb.style_id=tb2.id where tb.id=? and tb.active=? and tb2.active=? and tb.type=? and tb2.type=?',[$id,1,1,0,1]);
	}

	public function getStyle($id)
	{
		return $this->db->row('select id,header,footer,style from mails where id=?',[$id]);
	}

	public function getMailBody($vars,$id)
	{
		$dt=$this->db->cell('select body from '.self::$table.' where id=?',[$id]);
		foreach($vars as $i=>$v)$dt=str_replace($i,$v,$dt);
		return $dt;
	}

	public function makeMail($vars,$id)
	{
		$dt=self::getFullData($id);
		if(isset($dt['header'])&&$dt['header']!=NULL&&$dt['header']!=''){
			$head=explode(',',$dt['header']);
			foreach($head as $i=>$h)$dt['sheader']=str_replace($i,$h,$dt['sheader']);
		}
		unset($dt['header']);
		if(isset($dt['footer'])&&$dt['footer']!=NULL&&$dt['footer']!=''){
			$head=explode(',',$dt['footer']);
			foreach($head as $i=>$h)$dt['sfooter']=str_replace($i,$h,$dt['sfooter']);
		}
		unset($dt['footer']);
		foreach($vars as $i=>$v){
			$dt['body']=str_replace($i,$v,$dt['body']);
			$dt['title']=str_replace($i,$v,$dt['title']);
		}
		$sname=self::getSiteName();
		$content=self::getContent((isset($dt['sheader'])&&$dt['sheader']!=NULL&&$dt['sheader']!=''?$dt['sheader']:'').$dt['body'].(isset($dt['sfooter'])&&$dt['sfooter']!=NULL&&$dt['sfooter']!=''?$dt['sfooter']:''),$sname);
		$data['title']=$dt['title'];
		$data['content']=htmlspecialchars_decode(str_replace('{{content}}',$content,str_replace('{{style}}',str_replace('&#037;','%',$dt['style']),self::$mail)));
		return $data;
	}

	private static function getContent($str,$sname)
	{
		return str_replace('{{sitepath}}',($_SERVER['HTTPS']?'https://':'http://').$_SERVER['HTTP_HOST'],str_replace('{{sitename}}',$sname,str_replace('&#037;','%',str_replace('&#039;',"'",$str))));
	}
}