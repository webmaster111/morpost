<?
class MailsController extends BaseController
{
	protected $params;
	protected $db;
	private $left_menu=[['title'=>'Стили писем','url'=>'/admin/mails/act/styles','name'=>'styles']];

	function __construct($registry,$params)
	{
		parent::__construct($registry,$params);
		$this->tb="mails";
		$this->mails=new Mails($this->sets);
	}

	public function indexAction()
	{
		if(strpos($_SERVER['HTTP_REFERER'],'/act/')&&$_SERVER['REQUEST_URI']=='/admin/mails'&&$_SERVER['HTTP_REFERER']!=($_SERVER['HTTPS']?'https://':'http://').$_SERVER['HTTP_HOST'].'/admin/mails/act/styles')header('Location: /admin/mails/act/styles');
		if(isset($this->params['act'])){
			$act=$this->params['act'].'Action';
			return $this->Index($this->$act());
		}
		if(isset($this->params['subsystem']))return $this->Index($this->mails->subsystemAction($this->left_menu));
		if(isset($_POST['update'])||isset($_POST['update_close'])&&!isset($this->params['act']))$vars['message']=$this->mails->save();
		if(isset($this->params['delete']))$vars['message']=$this->mails->delete($this->tb);
		$vars['name']='Письма';
		$vars['list']=$this->view->Render('view.phtml',$this->mails->getList(0));
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$vars['name'],'menu2'=>$this->left_menu]);
		$data['styles']=['letters.css'];
		$data['scripts']=['letters.js'];
		$data['content']=$this->view->Render('list.phtml',$vars);
		return $this->Index($data);
	}

	public function addAction()
	{
		$vars['message']='';
		if(isset($_POST['add']))$vars['message']=$this->mails->add();
		$vars['name']='Новое письмо';
		$vars['styles']=$this->mails->getList(1);
		$data['styles']=['letters.css'];
		$data['scripts']=['letters.js'];
		$data['content']=$this->view->Render('add.phtml',$vars);
		return $this->Index($data);
	}

	public function editAction()
	{
		$vars['message']='';
		if(isset($this->params['delete']))$vars['message']=$this->mails->delete($this->params['delete']);
		if(isset($_POST['update'])||isset($_POST['update_close']))$vars['message']=$this->mails->save();
		$vars['edit']=$this->mails->find((int)$this->params['edit']);
		$vars['name']='Редактировать письмо';
		$vars['styles']=$this->mails->getList(1);
		$data['styles']=['letters.css'];
		$data['scripts']=['letters.js'];
		$data['content']=$this->view->Render('edit.phtml',$vars);
		return $this->Index($data);
	}

	public function stylesAction()
	{
		$vars['name']='Стили писем';
		$vars['list']=$this->view->Render('view_style.phtml',$this->mails->getList(1));
		if(isset($this->params['subsystem']))return $this->Index($this->mails->subsystemAction($this->left_menu));
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb.'/act/styles','name'=>$vars['name'],'menu2'=>[['title'=>'Письма','url'=>'/admin/mails','name'=>'index']]]);
		$data['styles']=['letters.css'];
		$data['scripts']=['letters.js'];
		$data['content']=$this->view->Render('list.phtml',$vars);
		return $data;
	}

	public function addstyleAction()
	{
		$vars['message']='';
		if(isset($_POST['add']))$vars['message']=$this->mails->add();
		$vars['name']='Новый стиль писем';
		$vars['styles']=$this->mails->getList(1);
		$data["breadcrumb"]='<a href="/admin/mails" class="back-link">« Назад в:&nbsp;Шаблонизатор писем</a>';
		$data['styles']=['letters.css'];
		$data['scripts']=['letters.js'];
		$data['content']=$this->view->Render('add_style.phtml',$vars);
		return $data;
	}

	public function editstyleAction()
	{
		$vars['message']='';
		if(isset($this->params['delete']))$vars['message']=$this->mails->delete($this->params['delete']);
		if(isset($_POST['update'])||isset($_POST['update_close']))$vars['message']=$this->mails->save();
		$vars['name']='Редактировать стиль писем';
		$vars['edit']=$this->mails->find((int)$this->params['edit']);
		$data["breadcrumb"]='<a href="/admin/mails" class="back-link">« Назад в:&nbsp;Шаблонизатор писем</a>';
		$data['styles']=['letters.css'];
		$data['scripts']=['letters.js'];
		$data['content']=$this->view->Render('edit_style.phtml',$vars);
		return $data;
	}

	public function lookAction()
	{
		if(isset($_POST['c']))$content=$_POST['c'];
		elseif(isset($_POST['title'],$_POST['header'],$_POST['body'],$_POST['footer'],$_POST['st_id'])){
			$st=$this->mails->getStyle($_POST['st_id']);
			$content=self::impl($st,$_POST['header'],$_POST['body'],$_POST['footer']);
		}elseif(isset($_POST['type'],$_POST['id'])){
			$data=($_POST['type']==1)?$this->mails->getStyle($_POST['id']):$this->mails->getFullData($_POST['id']);
			$content=($_POST['type']==1)?'<html><head><style>'.$data['style'].'</style></head><body>'.$data['header'].$data['footer'].'</body></html>':self::impl(['style'=>$data['style'],'header'=>$data['sheader'],'footer'=>$data['sfooter']],$data['header'],$data['body'],$data['footer']);
		}else $content='';
		return json_encode(['content'=>htmlspecialchars_decode(self::getContent($content,$this->mails->getSiteName()))]);
	}

	private static function impl($st=['style'=>'','header'=>'','footer'=>''],$hd,$bd,$ft)
	{
		if(isset($hd)&&$hd!=NULL&&$hd!=''){
			$head=explode(',',$hd);
			foreach($head as $i=>$h)$st['header']=str_replace($i,$h,$hd);
		}
		if(isset($ft)&&$ft!=NULL&&$ft!=''){
			$head=explode(',',$ft);
			foreach($head as $i=>$h)$st['footer']=str_replace($i,$h,$ft);
		}
		$str=str_replace('{{dt}}',$st['header'].'<div class="content">'.$bd.'</div>'.$st['footer'],str_replace('{{style}}',$st['style'],'<head><style>{{style}}</style></head><body>{{dt}}</body></html>'));
		return $str;
	}

	private static function getContent($str,$sname)
	{
		$str=str_replace('{{sitepath}}',($_SERVER['HTTPS']?'https://':'http://').$_SERVER['HTTP_HOST'],str_replace('{{sitename}}',$sname,str_replace('#043;','+',$str)));
		return $str;
	}
}