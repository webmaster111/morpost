<?
class SocialsController extends BaseController
{
	protected $params;
	protected $db;

	function  __construct($registry, $params)
	{
		parent::__construct($registry, $params);
		$this->tb="socials";
		$this->name="Социальные сети";
		$this->tb_lang=$this->key_lang_admin.'_'.$this->tb;
		$this->registry=$registry;
		$this->soc=new Socials($this->sets);
	}

	public function indexAction()
	{
		$vars['message']='';
		$vars['name']=$this->name;
		if(isset($this->params['subsystem']))return $this->Index($this->soc->subsystemAction());
		if(isset($this->registry['access']))$vars['message']=$this->soc->registry['access'];
		if(isset($_POST['delete']))$vars['message']=$this->soc->del($_POST);
		if(isset($_POST['update']))$vars['message']=$this->soc->update($_POST);
		$vars['list']=$this->view->Render('view.phtml',$this->soc->viewAll($this->settings['soc_paging_adm']));
		$data['scripts']=['bootstrap-drop-modal.js'];
		$data['content']=$this->view->Render('list.phtml',$vars);
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name]);
		return $this->Index($data);
	}

	public function addAction()
	{
		if(isset($_POST))return $this->soc->add($_POST);
		else return json_encode(['message'=>messageAdmin('При добавлении произошла ощибка','error')]);
	}

	public function updateAction()
	{
		if(isset($_POST))return $this->soc->save($_POST);
		else return json_encode(['message'=>messageAdmin('При обновлении произошла ошибка','error')]);
	}

	public function viewAction()
	{
		return json_encode(['content'=>$this->view->Render('view.phtml',array_merge($this->soc->viewAll($this->settings['soc_paging_adm']),['module'=>$this->tb.'/admin']))]);
	}

	public function addImgAction()
	{
		if(isset($_FILES['photo_insert'])){
			$id=(isset($_POST['id'])?$_POST['id']:$this->getNextId());
			$dir='files/'.$this->tb.'/'.$id.'/';
			if(!is_dir($dir))mkdir($dir,0755,true);
			$ext=pathinfo($_FILES['photo_insert']['name'])['extension'];
			$img=$dir.$id.'.'.$ext;
			$img2=$dir.$id.'_'.time().'.'.$ext;
			if(!isset($_POST['id']))copy($_FILES['photo_insert']['tmp_name'],$img);
			copy($_FILES['photo_insert']['tmp_name'],$img2);
			$m=messageAdmin('Изображение успешно загружено на сервер');
		}else{
			$m=messageAdmin('При загрузке изображения возникла ошибка','error');
			$img=$img2='files/default.jpg';
		}
		return json_encode(['message'=>$m,'img'=>$img,'timg'=>$img2]);
	}

	private function getNextId()
	{
		return (intval($this->db->cell('select id from '.$this->tb.' order by id desc'))+1);
	}
}