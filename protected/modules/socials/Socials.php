<?
class Socials extends Model
{
	static $tb='socials';
	static $name='Социальные сети';

	public function __construct($registry)
	{
		parent::getInstance($registry);
	}

	public static function getObject($registry)
	{
		return new self::$tb($registry);
	}

	public function add($post)
	{
		$s=0;
		if(isset($post)&&$post['name']!=""){
			$this->db->insert_id("insert into ".self::$tb." set name=?,link=?,active=?,icon=?,photo=?",[$post['name'],$post['link'],$post['active'],$post['icon'],$post['photo']]);
			$message=messageAdmin('Данные успешно добавлены');
			$s=1;
		}
		else $message=messageAdmin('Заполнены не все обязательные поля','error');
		return json_encode(['message'=>$message,'success'=>$s]);
	}

	public function save($post)
	{
		$s=0;
		if(isset($post)&&$post['name']!=""){
			if(isset($post['tphoto'])&&$post['tphoto']!=$post['photo']&&$post['tphoto']!=''&&$post['tphoto']!=NULL){
				unlink($post['photo']);
				rename($post['tphoto'],$post['photo']);
			}
			$photo=$post['photo'];
			$this->db->query("update ".self::$tb." set name=?,link=?,active=?,icon=?,photo=? where id=?",[$post['name'],$post['link'],$post['active'],$post['icon'],$photo,$post['id']]);
			$message=messageAdmin('Данные успешно добавлены');
			$s=1;
		}
		else $message=messageAdmin('Заполнены не все обязательные поля','error');
		return json_encode(['message'=>$message,'success'=>$s]);
	}

	public function update($post)
	{
		if(isset($post)&&count($post)>1){
			for($i=0;$i<count($post['save_id']);$i++)$this->db->query("update ".self::$tb." set link=? where id=?",[$post['link'][$i],$post['save_id'][$i]]);
			$message=messageAdmin('Данные успешно добавлены');
		}
		else $message=messageAdmin('Заполнены не все обязательные поля','error');
		return $message;
	}

	public function del($post)
	{}

	public function viewAll($pg)
	{
		return self::find(['table'=>self::$tb,'order'=>'tb.sort asc,tb.id desc','paging'=>$pg]);
	}

	public function getSoc()
	{
		return $this->db->rows('select * from '.self::$tb.' tb where active=? order by tb.sort asc,tb.id desc',[1]);
	}
}