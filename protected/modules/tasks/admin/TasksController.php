<?
class TasksController extends BaseController
{
	protected $params;
	protected $registry;

	function __construct($registry,$params)
	{
		parent::__construct($registry,$params);
		$this->key_lang_admin=$this->registry['key_lang_admin'];
		$this->name="Регламентные задания";
		$this->tb="tasks";
		$this->tasks=new Tasks($this->sets);
	}

	public function indexAction()
	{
		$vars['name']=$this->name;
		$vars['message']='';
		$vars['common']=['* * * * *'=>'Раз в минуту','*/5 * * * *' =>'Раз в 5 минут','0,30 * * * *'=>'2 раза в час','0 * * * *'=>'Раз в час','0 0,12 * * *'=>'2 раза в день','0 0 * * *'=>'Раз в день','0 0 * * 0'=>'Раз в неделю','0 0 1 * *'=>'Раз в месяц'];
		if(isset($this->params['subsystem']))return $this->Index($this->tasks->subsystemAction());
		if(isset($this->registry['access'])) $vars['message']=$this->registry['access'];
		if(isset($this->params['delete'])||isset($_POST['delete'])) $vars['message']=$this->tasks->cronDelete();
		elseif(isset($_POST['update'])) $vars['message']=$this->tasks->save();
		elseif(isset($_POST['update_close'])) $vars['message']=$this->tasks->save();
		elseif(isset($_POST['add_close'])) $vars['message']=$this->tasks->add();
		$vars['url']='/admin/'.$this->tb;
		$vars['list']=$this->view->Render('view.phtml',array_merge($this->tasks->find(['paging'=>true,'select'=>'tb.*,MAX(tb2.date_end) AS date_end,MAX(tb2.busy) AS busy','join'=>"LEFT JOIN tasks_log tb2 ON tb.id=tb2.task_id",'group' =>'tb.id','order' =>'tb.date DESC,tb2.date_end DESC']),['common'=>$vars['common']]));
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name]);
		$data['content']=$this->view->Render('list.phtml',$vars);
		return $this->Index($data);
	}

	public function addAction()
	{
		$vars['message']='';
		if(isset($_POST['add'])) $vars['message']=$this->tasks->add();
		$data['content']=$this->view->Render('add.phtml',$vars);
		return $this->Index($data);
	}

	public function editAction()
	{
		$vars['message']='';
		if(isset($_POST['update'])) $vars['message']=$this->tasks->save();
		$vars['edit']=$this->db->row("SELECT tb.*,MAX(tb2.date_end) AS date_end,tb2.busy FROM tasks tb LEFT JOIN tasks_log tb2 ON tb.id=tb2.task_id WHERE tb.id='{$this->params['edit']}' ORDER BY tb2.date_end DESC");
		$vars['edit']['body']=file_get_contents(SITE_PATH.'protected/cron/cron'.$vars['edit']['id'].'.php');
		$vars['logs']=$this->db->rows("SELECT * FROM tasks_log WHERE task_id=? ORDER BY date_start DESC",[$vars['edit']['id']]);
		$data['styles']=['codemirror.css'];
		$data['scripts']=['codemirror.js','css.js','matchbrackets.js','active-line.js','htmlmixed.js','closetag.js','xml.js','php.js','clike.js'];
		$data['content']=$this->view->Render('edit.phtml',$vars);
		return $this->Index($data);
	}
}