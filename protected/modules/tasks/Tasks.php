<?
include(CLASSES.'CronEntry.php');
include(CLASSES.'CrontabManager.php');
include(CLASSES.'CliTool.php');

use php\manager\crontab\CrontabManager;

class Tasks extends Model
{
	static $table='tasks';
	static $name="Регламентные задания";

	public function __construct($registry)
	{
		parent::getInstance($registry);
		$this->crontab=new CrontabManager();
		if(isset($this->settings['max_execution_time']))$max_execution_time=$this->settings['max_execution_time'];
		else $max_execution_time=30;
		if($max_execution_time!=30)$this->command="php -d max_execution_time=".$max_execution_time." ".$this->crontab->cronpath;
		else $this->command="php ".$this->crontab->cronpath;
	}

	public static function getObject($registry)
	{
		return new self::$table($registry);
	}

	public function add()
	{
		$message='';
		if(isset($_POST)){
			$id=$this->db->insert_id("INSERT INTO `".self::$table."` SET `name`=?,`active`=?,`common`=?,date=?",[$_POST['name'],$_POST['active'],$_POST['common'],date('Y-m-d H:i:s')]);
			$file="cron".$id.".php";
			if(!file_exists($this->crontab->cronpath.$file)){
				$text='<?php
/*
*
*/
require_once(dirname(__FILE__)."/cronInit.php");
class cron extends cronInit
{
public $id="'.$id.'";

function __construct($registry,$db)
{
	parent::__construct($registry,$db);
	$this->startCron();
}

function __destruct()
{
	$this->endCron();
}

/*
 *
 *
 */
public function todo()
{
	//to do something
}
}

$cron=new cron($registry,$db);
$cron->todo();';
				$fp=fopen($this->crontab->cronpath.$file,"w");
				fwrite($fp,$text);
				fclose($fp);
			}
			if(!$this->crontab->jobExists($file)){
				$command=$this->command.$file;
				$job=$this->crontab->newJob();
				$job->on($_POST['common'])->doJob($command);
				$this->crontab->add($job);
				$this->crontab->save();
			}
			$hash=$this->crontab->getHash($file);
			if($hash)$this->db->query("UPDATE `".self::$table."` SET hash='$hash' WHERE id='{$id}'");
			$message.=messageAdmin('Данные успешно добавлены');
		}
		//else $message.= messageAdmin('При добавление произошли ошибки','error');
		return $message;
	}

	public function save()
	{
		$this->crontab=new CrontabManager();
		$message='';
		if(isset($this->registry['access']))$message=$this->registry['access'];
		else{
			if(isset($_POST['save_id'])&&is_array($_POST['save_id'])){
				if(isset($_POST['save_id'],$_POST['name'])){
					for($i=0;$i<=count($_POST['save_id'])-1;$i++)$this->db->query("UPDATE `".self::$table."` SET `name`=? WHERE id=?",[$_POST['name'][$i],$_POST['save_id'][$i]]);
					$message.=messageAdmin('Данные успешно сохранены');
				}else $message.=messageAdmin('При сохранение произошли ошибки','error');
			}elseif(isset($_POST)){
				$row=$this->cronSave($_POST['id'],$_POST['common']);
				if($row['active']!=$_POST['active']&&$_POST['active']==0)$this->cronDeleteOne($_POST['id'],false);
                file_put_contents(SITE_PATH . 'protected/cron/cron' . $_POST['id'] . '.php', StringLibrary::sanitize($_POST['body'], true));
				$this->db->query("UPDATE `".self::$table."` SET `name`=?,`active`=?,`hash`=?,`common`=? WHERE `id`=?",[$_POST['name'],$_POST['active'],$row['hash'],$_POST['common'],$_POST['id']]);
				$message.=messageAdmin('Данные успешно сохранены');
			}else $message.=messageAdmin('При сохранение произошли ошибки','error');
		}
		return $message;
	}

	function cronSave($id,$common='')
	{
		$row=$this->db->row("SELECT common,hash,active FROM `".self::$table."` WHERE id=?",[$id]);
		if($row['hash']!=''){
			if($common=='')$common=$row['common'];
			$result=$this->crontab->jobExists("cron".$id.".php");
			if($result){
				$this->crontab->deleteJob($row['hash']);
				$this->crontab->save(false);
			}
			$file='cron'.$id.".php";
			$command=$this->command.$file;
			$job=$this->crontab->newJob();
			$job->on($common)->doJob($command);
			$this->crontab->add($job);
			$this->crontab->save();
			$hash=$this->crontab->getHash($file);
			if($hash)$row['hash']=$hash;
		}
		return $row;
	}

	function cronDelete()
	{
		if(isset($_POST['id'])&&is_array($_POST['id']))for($i=0;$i<=count($_POST['id'])-1;$i++)$this->cronDeleteOne($_POST['id'][$i]);
		elseif(isset($this->params['delete'])&&$this->params['delete']!='')$this->cronDeleteOne($this->params['delete']);
		return $this->delete();
	}

	function cronDeleteOne($id,$deleteFile=true)
	{
		$row=$this->db->row("SELECT hash FROM `".self::$table."` WHERE id=?",[$id]);
		if($row['hash']!=''){
			$this->crontab->deleteJob($row['hash']);
			$this->crontab->save(false);
			$file="cron".$id.".php";
			if($deleteFile&&file_exists($this->crontab->cronpath.$file))unlink($this->crontab->cronpath.$file);
		}
	}

	function active($id)
	{
		$row=$this->db->row("SELECT `active` FROM `".self::$table."` WHERE `id`=?",[$id]);
		if($row['active']==1)$this->cronDeleteOne($id,false);
		else $this->cronSave($id);
	}
}