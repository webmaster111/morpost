<?
class Params extends Model
{
	static $table='params';
	static $name='Фильтры';

	public function __construct($registry)
	{
		parent::getInstance($registry);
	}

	public static function getObject($registry)
	{
		return new self::$table($registry);
	}

	public function add()
	{
		$message='';
		if( isset($_POST['name'], $_POST['sub'], $_POST['url']) ) {
			if( $_POST['sub'] == 0 ) $_POST['sub']=NULL;
			if(!isset($_POST['type'])) $_POST['type'] = 0;

      $insert_id = $this->db->insert_id("INSERT INTO `".self::$table."` 
        SET 
          `url`=?,
          `sub`=?,
          `type`=?,
          active=?",
          [
              $_POST['url'],
              $_POST['sub'],
              $_POST['type'],
              $_POST['active']
          ]
      );

      $languages=$this->db->rows("SELECT * FROM language");

      foreach ($languages as $lang) {
        $this->db->query("INSERT INTO `".$lang['language']."_".self::$table."` 
          SET 
            `name`=?,
            `body`=?,
            `params_id`=?",
            [
                $_POST['name'],
                $_POST['body'],
                $insert_id
            ]
        );
      }

            if ($_POST['url'] == '') $url = StringLibrary::translit($_POST['name'] . '-' . $insert_id);
            else $url = StringLibrary::translit($_POST['url']);

      $url=str_replace('-','',$url);
			$this->checkUrl(self::$table, $url, $insert_id);
			$this->savePhoto($insert_id, $_POST['tmp_image'], self::$table);
			$message .= messageAdmin('Данные успешно добавлены');
		}
		//else $message.= messageAdmin('Заполнены не все обязательные поля','error');
		return $message;
	}

	public function save()
	{
		if(isset($this->registry['access'])) return $this->registry['access'];
		$message='';
		if(isset($_POST['save_id']) && is_array($_POST['save_id'])) {
			if(isset($_POST['save_id'],$_POST['name'])) {
				$count=count($_POST['save_id']) - 1;
				for ($i=0; $i <= $count; $i++) {
                    if ($_POST['url'][$i] == '') $url = StringLibrary::translit($_POST['name'][$i]);
					else $url=$_POST['url'][$i];
					$url=str_replace('-','',$url);
					$this->checkUrl(self::$table,$url,$_POST['save_id'][$i]);
					$this->db->query("UPDATE  `".$this->registry['key_lang_admin']."_".self::$table."` SET `name`=? WHERE params_id=?",[$_POST['name'][$i],$_POST['save_id'][$i]]);
				}
				$message .= messageAdmin('Данные успешно сохранены');
			} else $message .= messageAdmin('Заполнены не все обязательные поля','error');
		} else {
			if(isset($_POST['active'],$_POST['url'],$_POST['id'],$_POST['name'],$_POST['body'])) {
                if ($_POST['url'] == '') $url = StringLibrary::translit($_POST['name'] . '-' . $_POST['id']);
                else $url = StringLibrary::translit($_POST['url']);
				$url=str_replace('-','',$url);
				if($_POST['sub']==0) $sub=NULL;
				else $sub=$_POST['sub'];
				if(!isset($_POST['type'])) $_POST['type']='';
				if(!isset($_POST['rgb'])) $_POST['rgb']='';
				$this->checkUrl(self::$table,$url,$_POST['id']);
				$this->db->query("UPDATE `".self::$table."` SET `active`=?,`sub`=?,`rgb`=?,`url`=?,type=? WHERE `id`=?",[$_POST['active'],$sub,$_POST['rgb'],$url,$_POST['type'],$_POST['id']]);
				$this->db->query("UPDATE `".$this->registry['key_lang_admin']."_".self::$table."` SET `name`=?,`body`=? WHERE `params_id`=?",[$_POST['name'],$_POST['body'],$_POST['id']]);
				$this->savePhoto($_POST['id'],$_POST['tmp_image'],self::$table);
				$message .= messageAdmin('Данные успешно сохранены');
			} else $message .= messageAdmin('Заполнены не все обязательные поля','error');
		}
		return $message;
	}
}