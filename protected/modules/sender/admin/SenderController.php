<?php
/*
* Рассылка сообщений
*/

class SenderController extends BaseController
{
	protected $params;
	protected $db;
	private $left_menu=array(array('title'=>'---------------','url'=>'','name'=>''),
								array('title'=>'Отправить уведомление','url'=>'/admin/sender/act/sendone','name'=>'export'),
								array('title'=>'Массовые уведомления','url'=>'/admin/sender/act/sendmany','name'=>'export'),
								array('title'=>'Шаблоны сообщений','url'=>'/admin/sender/act/templates','name'=>'export'),
								array('title'=>'Настройки модуля','url'=>'/admin/sender/act/configure','name'=>'export'),
								array('title'=>'История рассылки','url'=>'/admin/sender/act/history','name'=>'export'));

	function __construct($registry,$params)
	{
		parent::__construct($registry,$params);
		$this->registry=$registry;
		$this->tb="sender";
		$this->name="Модуль рассылки";
		$this->history="sender_history";
		$this->templates="sender_templates";
		$this->types="sender_types";
		$this->lang_templates=$this->key_lang_admin."_sender_templates";
		$this->sender=new Sender($this->sets);
		$this->gateway=new Gateway($this->sets);
	}

	public function indexAction()
	{
		$settings=Registry::get('user_settings');
		$vars['message']='';
		if(isset($this->params['act'])){
			$act=$this->params['act'].'Action';
			return $this->$act();
		}
		$vars['api_key']=$this->settings['sender_api_key'];//Ключ пользователя
		$vars['balance']=$this->gateway->get_balance(true);//текущий баланс счета
		$vars['setts']=$this->db->rows("SELECT * FROM `config` WHERE `name` LIKE '%sender_%'");
		$vars['name']=$this->name;
		$vars['action']=$this->tb;
		$vars['path']='/act/sender';
		$vars['history']=$this->db->row("SELECT COUNT(id) as count FROM `{$this->history}` WHERE 1");//История уведомлений(кол-во уведомлений)
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name,'menu2'=>$this->left_menu]);
		$data['content']=$this->view->Render('list.phtml',$vars);
		$data['styles']=['sender.css'];
		$data['scripts']=['sender.js'];
		return $this->Index($data);
	}

	public function configureAction()
	{
		$settings=Registry::get('user_settings');
		$vars['message']='';
		if(isset($_POST['save_id'])){
			for($i=0;$i<=count($_POST['save_id'])-1;$i++){
				$id=$_POST['save_id'][$i];
				if(isset($_POST['value'.$id]))$value=$_POST['value'.$id];
				else $value='';
				$this->db->query("UPDATE `config` tb SET `value`=? WHERE `id`=?",[$value,$_POST['save_id'][$i]]);
			}
			if($vars['message']=='')$vars['message']=messageAdmin('Данные успешно сохранены');
		}
		$vars['setts']=$this->db->rows("SELECT * FROM `config` WHERE `name` LIKE '%sender_%'");
		$vars['name']=$this->name;
		$vars['action']=$this->tb;
		$vars['path']='/act/sender';
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name,'menu2'=>$this->left_menu]);
		$data['content']=$this->view->Render('config.phtml',$vars);
		$data['styles']=['sender.css'];
		$data['scripts']=['sender.js'];
		return $this->Index($data);
	}

	public function addAction()
	{
		$vars['message']='';
		$vars['users']=$this->db->rows("SELECT `sender`,`email`,`id` FROM `users`");
		$data['content']=$this->view->Render('add.phtml',$vars);
		return $this->Index($data);
	}

	public function editAction()
	{
		$vars['message']='';
		if(isset($_POST['update']))$vars['message']=$this->sender->save();
		if(isset($_POST['deleteusers']))$this->sender->deleteusers();
		$vars['edit']=$this->sender->find((int)$this->params['edit']);
		$vars['list']=$this->sender->listUsersView($this->params['edit']);
		$vars['users'] =$this->db->rows("SELECT `sender`,`email`,`id` FROM `users`");
		$data['content']=$this->view->Render('edit.phtml',$vars);
		return $this->Index($data);
	}

	public function sendoneAction()
	{
		$settings=Registry::get('user_settings');
		$vars['message']='';
		if(isset($_POST['sms_phone'],$_POST['sms_text'],$_POST['send'])){
			$result=$this->gateway->sms_send($_POST['sms_text'],$_POST['sms_phone'],3,false);
			$result=json_decode($result);
			$message=$result->data[0]->status;
			$vars['message'].=messageAdmin($message,'done');
			$date=date('Y-m-d H:i:s');
			$this->db->query("INSERT INTO `".$this->history."` SET `date_sended`=?,`user_id`=?,`text`=?,`number`=?,`agentik_id`=?,`status`=?,`type`=?,`readed`=?,`clicked`=?,`hash`=?",[$date,0,$_POST['sms_text'],$result->data[0]->phone,$result->data[0]->id,$result->data[0]->status,'sms',0,0,0]);
		}
		if(isset($_POST['email_to'],$_POST['email_theme'],$_POST['email_text'],$_POST['send'])){
			$result=$this->gateway->email_send($_POST['email_text'],$_POST['email_theme'],$_POST['email_to']);
			$result=json_decode($result);
			if($result->success)$message=$result->success;
			elseif($result->error)$message=$result->error;
			$vars['message'].=messageAdmin($message,'done');
			$date=date('Y-m-d H:i:s');
//			$this->db->query("INSERT INTO `".$this->history."` SET `date_sended`=?,`user_id`=?,`text`=?,`number`=?,`agentik_id`=?,`status`=?,`type`=?,`readed`=?,`clicked`=?,`hash`=?",[$date,0,$_POST['sms_text'],$result->data[0]->phone,$result->data[0]->id,$result->data[0]->status,'sms',0,0,0]);
		}
		$vars['sms_length_cyr']=$this->gateway->sms_length_cyr;
		$vars['sms_length_lat']=$this->gateway->sms_length_lat;
		$vars['types']=$this->db->rows("SELECT * FROM `".$this->types."`");
		$vars['templates']=$this->db->rows("SELECT * FROM `".$this->templates."` WHERE `type`=?",array($vars['types'][0]['url']));
		$vars['balance']=$this->gateway->get_balance(true);
		$vars['name']=$this->name;
		$vars['action']=$this->tb;
		$vars['path']='/act/sender';
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name,'menu2'=>$this->left_menu]);
		$data['content']=$this->view->Render('sendone.phtml',$vars);
		$data['styles']=['sender.css'];
		$data['scripts']=['sender.js','jquery.maskedinput.min.js'];
		return $this->Index($data);
	}

	public function sendmanyAction()
	{
		$settings=Registry::get('user_settings');
		$vars['message']='';
		if(isset($_POST['sms_text'],$_POST['phones'],$_POST['check'],$_POST['send'])&&is_array($_POST['phones'])){
			$phones=[];
			$phone_reg='/380[0-9]{1,9}/';
			for($i=0;$i<=count($_POST['phones'])-1;$i++){
				if(isset($_POST['check'][$i])&&$_POST['check'][$i]==1){
					if(preg_match($phone_reg,trim($_POST['phones'][$i]))==true)
					$phones[]=trim($_POST['phones'][$i]);
				}
			}
			$result=$this->gateway->sms_send_many($_POST['sms_text'],$phones,3,false);
			$result=json_decode($result);
			$message=$result->data[0]->status;
			$vars['message'].=messageAdmin($message,'done');
			$date=date('Y-m-d H:i:s');
//			$this->db->query("INSERT INTO `".$this->history."` SET `date_sended`=?,`user_id`=?,`text`=?,`number`=?,`agentik_id`=?,`status`=?,`type`=?,`readed`=?,`clicked`=?,`hash`=?",[$date,0,$_POST['sms_text'],serialize($phones),$result->data[0]->id,$result->data[0]->status,'sms',0,0,0]);
		}

		if(isset($_POST['email_theme'],$_POST['email_text'],$_POST['emails'],$_POST['email_check'],$_POST['send'])&&is_array($_POST['emails'])){
			$mails='';
			for($i=0;$i<=count($_POST['emails'])-1;$i++)if(isset($_POST['email_check'][$i])&&$_POST['email_check'][$i]==1)$mails.=$_POST['emails'][$i].',';
			$mails=substr($mails,0,-1);
			$result=$this->gateway->email_send($_POST['email_text'],$_POST['email_theme'],$mails);
			$result=json_decode($result);
			if($result->success)$message=$result->success;
			elseif($result->error)$message=$result->error;
			$vars['message'].=messageAdmin($message,'done');
			$date=date('Y-m-d H:i:s');
//			$this->db->query("INSERT INTO `".$this->history."` SET `date_sended`=?,`user_id`=?,`text`=?,`number`=?,`agentik_id`=?,`status`=?,`type`=?,`readed`=?,`clicked`=?,`hash`=?",[$date,0,$_POST['sms_text'],serialize($phones),$result->data[0]->id,$result->data[0]->status,'sms',0,0,0]);
		}
		$vars['sms_length_cyr']=$this->gateway->sms_length_cyr;
		$vars['sms_length_lat']=$this->gateway->sms_length_lat;
		$vars['types']=$this->db->rows("SELECT * FROM `".$this->types."`");
		$vars['templates']=$this->db->rows("SELECT * FROM `".$this->templates."` WHERE `type`=?",[$vars['types'][0]['url']]);
		$vars['users']=$this->db->rows("SELECT * FROM `users` WHERE `active`=?",[1]);
		$vars['statuses']=$this->db->rows("SELECT * FROM `user_status` WHERE 1");
		$vars['balance']=$this->gateway->get_balance(true);
		$vars['name']=$this->name;
		$vars['action']=$this->tb;
		$vars['path']='/act/sender';
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name,'menu2'=>$this->left_menu]);
		$data['content']=$this->view->Render('sendmany.phtml',$vars);
		$data['styles']=['sender.css'];
		$data['scripts']=['sender.js','jquery.maskedinput.min.js'];
		return $this->Index($data);
	}

	public function templatesAction()
	{
		$settings=Registry::get('user_settings');
		$vars['message']='';
		if(isset($_POST['sms_phone'],$_POST['sms_text'],$_POST['send'])){
			$result=$this->gateway->sms_send($_POST['sms_text'],$_POST['sms_phone'],3,false);
			$result=json_decode($result);
			$message=$result->data[0]->status;
			$vars['message'].=messageAdmin($message,'done');
			$date=date('Y-m-d H:i:s');
			$this->db->query("INSERT INTO `".$this->history."` SET `date_sended`=?,`user_id`=?,`text`=?,`number`=?,`agentik_id`=?,`status`=?,`type`=?,`readed`=?,`clicked`=?,`hash`=?",[$date,0,$_POST['sms_text'],$result->data[0]->phone,$result->data[0]->id,$result->data[0]->status,'sms',0,0,0]);
		}
		if(isset($_POST['template_id'],$_POST['template_name'],$_POST['template_theme'],$_POST['template_text'],$_POST['template_description']))$vars['message']=$this->sender->SaveTemplate();
		$vars['sms_length_cyr']=$this->gateway->sms_length_cyr;
		$vars['sms_length_lat']=$this->gateway->sms_length_lat;
		$vars['types']=$this->db->rows("SELECT * FROM `".$this->types."`");
		$vars['templates']=$this->db->rows("SELECT * FROM `".$this->templates."` WHERE `type`=?",[$vars['types'][0]['url']]);
		$vars['balance']=$this->gateway->get_balance(true);
		$vars['name']=$this->name;
		$vars['action']=$this->tb;
		$vars['path']='/act/sender';
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name,'menu2'=>$this->left_menu]);
		$data['content']=$this->view->Render('templates.phtml',$vars);
		$data['styles']=['sender.css'];
		$data['scripts']=['sender.js'];
		return $this->Index($data);
	}

	public function addtemplateAction()
	{
		$settings=Registry::get('user_settings');
		$vars['message']='';
		if(isset($_POST['type'],$_POST['add']))$vars['message'].=$this->sender->add_template();
		$vars['sms_length_cyr']=$this->gateway->sms_length_cyr;
		$vars['sms_length_lat']=$this->gateway->sms_length_lat;
		$vars['types']=$this->db->rows("SELECT * FROM `".$this->types."`");
		$vars['modules']=$this->db->rows("SELECT name,controller FROM `modules` WHERE hidden='0'");
		$vars['name']=$this->name;
		$vars['action']=$this->tb;
		$vars['path']='/act/sender';
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name,'menu2'=>$this->left_menu]);
		$data['content']=$this->view->Render('add_template.phtml',$vars);
		$data['styles']=['sender.css'];
		$data['scripts']=['sender.js'];
		return $this->Index($data);
	}

	public function historyAction()
	{
		$settings=Registry::get('user_settings');
		$vars['message']='';
		$vars['history']=$this->sender->ListView();
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name,'menu2'=>$this->left_menu]);
		$data['content']=$this->view->Render('history.phtml',$vars);
		$data['styles']=['sender.css'];
		$data['scripts']=['sender.js'];
		return $this->Index($data);
	}

	function GetTemplateSendOneAction(){
		if(isset($_POST['id'])&&$_POST['id']!=''){
			$template=$this->db->row("SELECT theme as caption,text,description FROM `".$this->templates."` WHERE `id`=?",[$_POST['id']]);
			$reg='/\{\{[0-9A-Za-z\-\_\.\,]{0,}\}\}/';
			if($_POST['clean']=='true'){
				$template['caption']=preg_replace($reg,'',$template['caption']);
				$template['text']=preg_replace($reg,'',$template['text']);
			}
			$template['text']=htmlspecialchars_decode($template['text']);
			$template['description']=htmlspecialchars_decode($template['description']);
		}
		return json_encode($template);
	}

	function GetTemplatesOptionsAction(){
		if(isset($_POST['type'])&&$_POST['type']!=''){
			$options='<option value="0">Выберите шаблон</option>';
			$rows=$this->db->rows("SELECT * FROM `".$this->templates."` WHERE `type`=?",array($_POST['type']));
			foreach($rows as $row)$options.='<option value="'.$row['id'].'">'.$row['name'].'</option>';
		}
		return $options;
	}

	function TemplatePreviewAction(){
		if(isset($_POST['id'])&&$_POST['id']!=''){
			$template=$this->db->row("SELECT * FROM `".$this->templates."` WHERE `id`=?",array($_POST['id']));
			$template=$this->view->Render('template_preview.phtml',array('template'=>$template,'module'=>$this->tb));
		}
		return $template;
	}

	function translitAction(){
		if(isset($_POST['text'])&&$_POST['text']!=''){
			$str=$_POST['text'];
			$table=['А'=>'A','Б'=>'B','В'=>'V','Г'=>'G','Д'=>'D','Е'=>'E','Ё'=>'Yo','Ж'=>'Zh','З'=>'Z','И'=>'I','Й'=>'Y','К'=>'K','Л'=>'L','М'=>'M','Н'=>'N','О'=>'O','П'=>'P','Р'=>'R','С'=>'S','Т'=>'T','У'=>'U','Ф'=>'F','Х'=>'H','Ц'=>'Ts','Ч'=>'Ch','Ш'=>'Sh','Щ'=>'Shch','Ъ'=>'','Ы'=>'I','Ь'=>'','Э'=>'E','Ю'=>'Yu','Я'=>'Ya','а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'yo','ж'=>'zh','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'ts','ч'=>'ch','ш'=>'sh','щ'=>'shch','ъ'=>'','ы'=>'i','ь'=>'','э'=>'e','ю'=>'yu','я'=>'ya'];
			$str=strtr($str,$table);
			$str=mb_strtolower($str);
			return $str;
		}else return false;
	}
}