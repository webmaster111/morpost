<?php
/*
 * вывод каталога компаний и их данных
 */
class SenderController extends BaseController{
	
	protected $params;
	protected $db;
	
	function  __construct($registry, $params)
	{
		parent::__construct($registry, $params);
		$this->tb = "sender";
		$this->registry = $registry;
		$this->sender = new Sender($this->sets);
	}
	
	public function indexAction()
	{
		$vars['translate'] = $this->translation;
		$data['content'] = $this->view->Render('distribution.phtml', $vars);
		return $this->Index($data);
	}
}
?>