<?
class Sender extends Model
{
	static $table='sender';
	static $name='Модуль рассылки';
	
	public function __construct($registry){
		parent::getInstance($registry);
	}

	public static function getObject($registry){
		return new self::$table($registry);
	}
	
	public function add_template(){
		$message='';
		$error='';
		if(isset($_POST['sms_name'],$_POST['sms_text'],$_POST['sms_descr'],$_POST['sms_module'])&&$_POST['type']=="sms"){
			$type=$_POST['type'];
			$name=$_POST['sms_name'];
			$theme='';
			$text=$_POST['sms_text'];
			$module=$_POST['sms_module'];
			$description=$_POST['sms_descr'];
		}elseif(isset($_POST['email_name'],$_POST['email_theme'],$_POST['email_text'],$_POST['email_descr'],$_POST['email_module'])&&$_POST['type']=="email"){
			$type=$_POST['type'];
			$name=$_POST['email_name'];
			$theme=$_POST['email_theme'];
			$text=$_POST['email_text'];
			$module=$_POST['email_module'];
			$description=$_POST['email_descr'];
		}elseif(isset($_POST['push_name'],$_POST['push_theme'],$_POST['push_text'],$_POST['push_descr'],$_POST['push_module'])&&$_POST['type']=="push"){
			$type=$_POST['type'];
			$name=$_POST['push_name'];
			$theme=$_POST['push_theme'];
			$text=$_POST['push_text'];
			$module=$_POST['push_module'];
			$description=$_POST['push_descr'];
		}elseif(isset($_POST['tray_name'],$_POST['tray_text'],$_POST['tray_text'],$_POST['tray_descr'],$_POST['tray_module'])&&$_POST['type']=="tray"){
			$type=$_POST['type'];
			$name=$_POST['tray_name'];
			$theme=$_POST['tray_theme'];
			$text=$_POST['tray_text'];
			$module=$_POST['tray_module'];
			$description=$_POST['tray_descr'];
		}
		if($error==''){
			$date=date('Y-m-d H:i:s');
			$this->db->query("INSERT INTO `sender_templates` SET `type`=?,`name`=?,`theme`=?,`text`=?,`description`=?,`module`=?,`date_add`=?",[$type,$name,$theme,$text,$description,$module,$date]);
			$message=messageAdmin('Шаблон успешно добавлен!','done');
		}
		return $message;
	}

	public function SaveTemplate(){
		if(isset($_POST['template_id'],$_POST['template_name'],$_POST['template_theme'],$_POST['template_text'],$_POST['template_description'])){
			$this->db->query("UPDATE `sender_templates` SET `name`=?,`theme`=?,`text`=?,`description`=? WHERE `id`=?",[$_POST['template_name'],$_POST['template_theme'],$_POST['template_text'],$_POST['template_description'],$_POST['template_id']]);
			$message=messageAdmin('Данные успешно сохранены','done');
		}
		return $message;
	}

	public function listView(){
		$size_page=50;
		if(isset($this->params['page'])&&$this->params['page']>0){
			$page=($this->params['page']-1)*$size_page;
			$limit=$size_page*$this->params['page'];
		}else{
			$this->params['page']=0;
			$page=0;
			$limit=$size_page;
		}
		$rows=$this->db->cell('SELECT COUNT(id) as count FROM `sender_history` WHERE 1');
		$vars['paging']='';
		if($rows>$size_page){
			$class=new Paging($this->registry,$this->params);
			$vars['paging']=$class->MakePaging($this->params['page'],$rows,$size_page);//вызов шаблона для постраничной навигации
		}
		$vars['list']=$this->db->rows("SELECT * FROM `sender_history` WHERE 1 ORDER BY id ASC LIMIT {$page},{$limit}");
		return $vars;
	}
}