<?php

    class EditorialController extends BaseController
    {
	   protected $params;
	   protected $editorial;
	   private $left_menu = [['title' => '.htaccess', 'url' => '/admin/editorial/act/htaccess', 'name' => 'htaccess'], ['title' => 'Темы', 'url' => '/admin/editorial/act/themes', 'name' => 'themes'], ['title' => 'Водяной знак', 'url' => '/admin/editorial/act/watermark', 'name' => 'watermark']];

	   function __construct($registry, $params)
	   {
		  parent::__construct($registry, $params);
		  $this->tb = "editorial";
		  $this->name = "Редактирование";
		  $this->registry = $registry;
		  $this->editorial = new Editorial($this->sets);
	   }

	   public function indexAction()
	   {
		  $vars['message'] = '';
		  $vars['name'] = $this->name;
		  if (isset($this->params['act'])) {
			 $act = $this->params['act'] . 'Action';
			 return $this->Index($this->$act());
		  }
		  if (isset($this->params['subsystem'])) return $this->Index($this->editorial->subsystemAction($this->left_menu));
		  if (isset($this->registry['access'])) $vars['message'] = $this->registry['access'];
		  elseif (isset($_POST['update'])) $vars['message'] = $this->editorial->save();
		  elseif (isset($_POST['update_close'])) $vars['message'] = $this->editorial->save();
		  $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->editorial->listView()]);
		  $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name, 'menu2' => $this->left_menu]);
		  $data['content'] = $this->view->Render('list.phtml', $vars);
		  return $this->Index($data);
	   }

	   public function editAction()
	   {
		  $vars['message'] = '';
		  if (isset($_POST['update'])) $vars['message'] = $this->editorial->save();
		  $vars['edit'] = $this->editorial->find($this->params['edit']);
		  $data['styles'] = ['codemirror.css'];
		  $data['scripts'] = ['codemirror.js', 'css.js', 'matchbrackets.js', 'active-line.js', 'htmlmixed.js', 'closetag.js', 'xml.js', 'php.js', 'clike.js'];
		  $data['content'] = $this->view->Render('edit.phtml', $vars);
		  return $this->Index($data);
	   }

	   public function htaccessAction()
	   {
		  $vars['message'] = '';
		  if (isset($_POST['update'])) $vars['message'] = $this->editorial->save();
		  $vars['edit'] = $this->editorial->find('.htaccess');
		  $data['styles'] = ['codemirror.css'];
		  $data['scripts'] = ['codemirror.js', 'css.js', 'active-line.js'];
		  $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name, 'sub' => 'htaccess', 'menu2' => $this->left_menu]);
		  $data['content'] = $this->view->Render('htaccess.phtml', $vars);
		  return $data;
	   }

	   public function themesAction()
	   {
		  $vars['message'] = '';
		  if (isset($_POST['update'])) $vars['message'] = $this->editorial->save_theme();
		  $vars['edit'] = Dir::get_directory_list("tpl/", ["..", ".", "admin"]);
		  sort($vars['edit']);
		  foreach ($vars['edit'] as $key => $value) {
			 $vars['colors'][$value] = Dir::get_directory_list('tpl/' . $value . '/colors/', ['..', '.']);
		  }
		  $vars['theme'] = $this->db->row("SELECT * FROM `config` WHERE `name`='theme'");
		  $vars['theme_color'] = $this->db->row("SELECT * FROM `config` WHERE `name`='theme_color'");
		  $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name, 'sub' => 'themes', 'menu2' => $this->left_menu]);
		  $data['content'] = $this->view->Render('themes.phtml', $vars);
		  return $data;
	   }

	   public function fixAction()
	   {
		  $vars['message'] = '';
		  $vars['name'] = $this->name;
		  $db = $this->registry['db_settings']['name'];
		  if ($this->db->query("/*FIX TABLES REFERENCES*/
/*brend*/
DELETE FROM `ru_brend` WHERE (`ru_brend`.`brend_id` NOT IN(SELECT `tb`.`id` FROM `brend` `tb` WHERE 1));
ALTER TABLE `ru_brend` ADD FOREIGN KEY (`brend_id`) REFERENCES `{$db}`.`brend` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*catalog*/
DELETE FROM `ru_catalog` WHERE (`ru_catalog`.`catalog_id` NOT IN(SELECT `tb`.`id` FROM `catalog` `tb` WHERE 1));
ALTER TABLE `ru_catalog` ADD FOREIGN KEY (`catalog_id`) REFERENCES `{$db}`.`catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*infoblocks*/
DELETE FROM `ru_infoblocks` WHERE (`ru_infoblocks`.`infoblocks_id` NOT IN(SELECT `tb`.`id` FROM `infoblocks` `tb` WHERE 1));
ALTER TABLE `ru_infoblocks` ADD FOREIGN KEY (`infoblocks_id`) REFERENCES `{$db}`.`infoblocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*menu*/
DELETE FROM `ru_menu` WHERE (`ru_menu`.`menu_id` NOT IN(SELECT `tb`.`id` FROM `menu` `tb` WHERE 1));
ALTER TABLE `ru_menu` ADD FOREIGN KEY (`menu_id`) REFERENCES `{$db}`.`menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*meta*/
DELETE FROM `ru_meta` WHERE (`ru_meta`.`meta_id` NOT IN(SELECT `tb`.`id` FROM `meta` `tb` WHERE 1));
ALTER TABLE `ru_meta` ADD FOREIGN KEY (`meta_id`) REFERENCES `{$db}`.`meta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*payment*/
DELETE FROM `ru_payment` WHERE (`ru_payment`.`payment_id` NOT IN(SELECT `tb`.`id` FROM `payment` `tb` WHERE 1));
ALTER TABLE `ru_payment` ADD FOREIGN KEY (`payment_id`) REFERENCES `{$db}`.`payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*delivery*/
DELETE FROM `ru_delivery` WHERE (`ru_delivery`.`delivery_id` NOT IN(SELECT `tb`.`id` FROM `delivery` `tb` WHERE 1));
ALTER TABLE `ru_delivery` ADD FOREIGN KEY (`delivery_id`) REFERENCES `{$db}`.`delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*news*/
DELETE FROM `ru_news` WHERE (`ru_news`.`news_id` NOT IN(SELECT `tb`.`id` FROM `news` `tb` WHERE 1));
ALTER TABLE `ru_news` ADD FOREIGN KEY (`news_id`) REFERENCES `{$db}`.`news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*pages*/
DELETE FROM `ru_pages` WHERE (`ru_pages`.`pages_id` NOT IN(SELECT `tb`.`id` FROM `pages` `tb` WHERE 1));
ALTER TABLE `ru_pages` ADD FOREIGN KEY (`pages_id`) REFERENCES `{$db}`.`pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*params*/
DELETE FROM `ru_params` WHERE (`ru_params`.`params_id` NOT IN(SELECT `tb`.`id` FROM `params` `tb` WHERE 1));
ALTER TABLE `ru_params` ADD FOREIGN KEY (`params_id`) REFERENCES `{$db}`.`params` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*product*/
DELETE FROM `ru_product` WHERE (`ru_product`.`product_id` NOT IN(SELECT `tb`.`id` FROM `product` `tb` WHERE 1));
ALTER TABLE `ru_product` ADD FOREIGN KEY (`product_id`) REFERENCES `{$db}`.`product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*product_photo*/
DELETE FROM `ru_product_photo` WHERE (`ru_product_photo`.`product_photo_id` NOT IN(SELECT `tb`.`id` FROM `product_photo` `tb` WHERE 1));
ALTER TABLE `ru_product_photo` ADD FOREIGN KEY (`product_photo_id`) REFERENCES `{$db}`.`product_photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*product_status*/
DELETE FROM `ru_product_status` WHERE (`ru_product_status`.`product_status_id` NOT IN(SELECT `tb`.`id` FROM `product_status` `tb` WHERE 1));
ALTER TABLE `ru_product_status` ADD FOREIGN KEY (`product_status_id`) REFERENCES `{$db}`.`product_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*slider*/
DELETE FROM `ru_slider` WHERE (`ru_slider`.`slider_id` NOT IN(SELECT `tb`.`id` FROM `slider` `tb` WHERE 1));
ALTER TABLE `ru_slider` ADD FOREIGN KEY (`slider_id`) REFERENCES `{$db}`.`slider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*translate*/
DELETE FROM `ru_translate` WHERE (`ru_translate`.`translate_id` NOT IN(SELECT `tb`.`id` FROM `translate` `tb` WHERE 1));
ALTER TABLE `ru_translate` ADD FOREIGN KEY (`translate_id`) REFERENCES `{$db}`.`translate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*product_catalog*/
DELETE FROM `product_catalog` WHERE (`product_catalog`.`catalog_id` NOT IN(SELECT `tb`.`id` FROM `catalog` `tb` WHERE 1));
ALTER TABLE `product_catalog` ADD FOREIGN KEY (`catalog_id`) REFERENCES `{$db}`.`catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
DELETE FROM `product_catalog` WHERE (`product_catalog`.`product_id` NOT IN(SELECT `tb`.`id` FROM `product` `tb` WHERE 1));
ALTER TABLE `product_catalog` ADD FOREIGN KEY (`product_id`) REFERENCES `{$db}`.`product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*params_catalog*/
DELETE FROM `params_catalog` WHERE (`params_catalog`.`params_id` NOT IN(SELECT `tb`.`id` FROM `params` `tb` WHERE 1));
ALTER TABLE `params_catalog` ADD FOREIGN KEY (`params_id`) REFERENCES `{$db}`.`params` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
DELETE FROM `params_catalog` WHERE (`params_catalog`.`catalog_id` NOT IN(SELECT `tb`.`id` FROM `catalog` `tb` WHERE 1));
ALTER TABLE `params_catalog` ADD FOREIGN KEY (`catalog_id`) REFERENCES `{$db}`.`catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*params_product*/
DELETE FROM `params_product` WHERE (`params_product`.`params_id` NOT IN(SELECT `tb`.`id` FROM `params` `tb` WHERE 1));
ALTER TABLE `params_product` ADD FOREIGN KEY (`params_id`) REFERENCES `{$db}`.`params` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
DELETE FROM `params_product` WHERE (`params_product`.`product_id` NOT IN(SELECT `tb`.`id` FROM `product` `tb` WHERE 1));
ALTER TABLE `params_product` ADD FOREIGN KEY (`product_id`) REFERENCES `{$db}`.`product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*product_status_set*/
DELETE FROM `product_status_set` WHERE (`product_status_set`.`status_id` NOT IN(SELECT `tb`.`id` FROM `product_status` `tb` WHERE 1));
ALTER TABLE `product_status_set` ADD FOREIGN KEY (`status_id`) REFERENCES `{$db}`.`product_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
DELETE FROM `product_status_set` WHERE (`product_status_set`.`product_id` NOT IN(SELECT `tb`.`id` FROM `product` `tb` WHERE 1));
ALTER TABLE `product_status_set` ADD FOREIGN KEY (`product_id`) REFERENCES `{$db}`.`product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*product_other*/
DELETE FROM `product_other` WHERE (`product_other`.`product_id` NOT IN(SELECT `tb`.`id` FROM `product` `tb` WHERE 1));
ALTER TABLE `product_other` ADD FOREIGN KEY (`product_id`) REFERENCES `{$db}`.`product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
DELETE FROM `product_other` WHERE (`product_other`.`product_id2` NOT IN(SELECT `tb`.`id` FROM `product` `tb` WHERE 1));
ALTER TABLE `product_other` ADD FOREIGN KEY (`product_id2`) REFERENCES `{$db}`.`product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;
/*FIX TABLE REFERENCES END*/")) $vars['message'] = messageAdmin('Связи таблиц исправлены. Внешние ключи перенастроены.');
		  $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->editorial->listView()]);
		  $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name, 'menu2' => $this->left_menu]);
		  $data['content'] = $this->view->Render('list.phtml', $vars);
		  return $data;
	   }

	   public function watermarkAction()
	   {
		  $vars['message'] = '';
		  if (isset($_POST['update'])) $vars['message'] = $this->editorial->save_watermark();
		  $row = $this->db->row("SELECT * FROM `config` WHERE `name`='watermark'");
		  $vars['edit'] = json_decode($row['value'], true);
		  $vars['modules'] = $this->db->rows("SELECT id,name,controller FROM `modules` WHERE `photo`='1' ORDER BY name ASC");
		  $data['scripts'] = ['colorpicker.js', 'eye.js', 'utils.js', 'layout.js'];
		  $data['styles'] = ['colorpicker.css'];
		  $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name, 'sub' => 'watermark', 'menu2' => $this->left_menu]);
		  $data['content'] = $this->view->Render('watermark.phtml', $vars);
		  return $data;
	   }
    }