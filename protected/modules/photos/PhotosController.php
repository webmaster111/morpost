<?php

/*
 * вывод каталога компаний и их данных
 */

class PhotosController extends BaseController
{

  protected $params;
  protected $db;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = "photos";
    $this->registry = $registry;
    $this->photos = new Photos($this->sets);
  }

  public function indexAction()
  {
    return Router::act('error', $this->registry);
    $vars['translate'] = $this->translation;
    if ($this->params[$this->tb] == 'all') {
      $vars['list'] = $this->photos->find(array('where' => "tb.active='1'", 'order' => 'tb.sort ASC'));
      $data['breadcrumbs'] = [['url'=>'photos/all','name' => $this->translation[$this->tb]]];
    } else return Router::act('error', $this->registry);
    $data['content'] = $this->view->Render($this->tb . '/list.phtml', $vars);
    return $this->Index($data);
  }
}