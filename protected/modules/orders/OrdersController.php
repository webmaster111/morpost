<?

class OrdersController extends BaseController
{

    protected $orders, $users, $product;

    function __construct($registry, $params)
    {
        parent::__construct($registry, $params);
        $this->tb = Orders::$table;
        $this->orders = new Orders($this->sets);
        $this->users = new Users($this->sets);
    }

    public function indexAction()
    {
        $template = 'checkout';
        switch ($this->params[$this->tb]) {

            // preview Order page
            case 'preview':
                $data['breadcrumbs'] = [['url' => 'orders/preview', 'name' => $this->translation['order_text']]];

                $vars = $this->orders->getBasket();
                $vars['thumbs_show-counter'] = true;
                $vars['product-thumbs'] = $this->view->Render('product/for-basket.phtml', $vars);
                $template = 'preview';
                break;

            // Checkout from page
            case 'checkout':

                $data['breadcrumbs'] = [['url' => 'orders/checkout', 'name' => $this->translation['in_cart']]];
                $vars = $this->orders->getBasket();
                if (empty($vars['products'])) redirect301('/orders/preview');
                $vars['products-table'] = $this->view->Render('orders/products-table.phtml', $vars);
                $vars['payments'] = Payment::getObject($this->sets)->getAll();
                $vars['delivery'] = makeRowsKey(Delivery::getObject($this->sets)->getAll(), 'id', 'name');
                $vars['info-block'] = $this->model->getBlock(16);
                // Обычная отправка заказа
                if (isset($_POST['order_action'])) {
                    $result = $this->Buy($vars['products']);
                    $vars = array_merge($vars, $result);
                    if ($vars['status']) $vars['products'] = null;
                    $vars['message'] = '<script>' . $result['uni_analytics_code'] . '</script>' . $result['message'];
                }
                break;

            default:
                return Router::act('error', $this->registry);
        }
        $data['meta']['name'] = $this->translation['in_cart'];
        $data['content'] = $this->view->Render('orders/' . $template . '.phtml', $vars);
        return $this->Index($data);
    }


    private function Buy($products)
    {
		$secretkey =  $this->settings["recaptcha_secretkey"];
    	$error = !GoogleCaptcha::check($secretkey) && false /* deactivation captcha */
            ? "<div class='alert alert-danger'>" . $this->translation['wrong-recaptcha'] . "</div>"
            : "";
        $error .= Validate::check(html_entity_decode($_POST['phone']), $this->translation, 'phone');
        $error .= Validate::check($_POST['name'], $this->translation);

        if (!empty($_POST['delivery_department']) || !empty($_POST['delivery_city']))
            $_POST['address'] = $_POST['delivery_city'] . ', ' . $_POST['delivery_department'];

        $data = array('status' => false, 'message' => $error);
        if ($error == "") {
            $data['uni_analytics_code'] = $this->orders->sendOrder($products, strip_post());
            $body = $this->model->getBlock(17);
            $data['status'] = true;
            $data['message'] = htmlspecialchars_decode($body['body']);
        }
        return $data;
    }

    // get basket info
    function getBasketAction()
    {
        return json_encode($this->orders->getBasket());
    }

    // delete product from basket
    function deleteproductAction()
    {
        return json_encode($this->orders->removeFromBasket($_POST['id_basket']));
    }

    // update quantity basket
    function quantityAction()
    {
        return json_encode($this->orders->refreshBasket($_POST['quantity'], $_POST['cat_id']));
    }

    // put in shop cart
    function incartAction()
    {
        $result = array('status' => 'error', 'message' => 'No-data');
        if (isset($_POST['product_id'], $_POST['amount'])) {
            $price_id = $_POST['price_id'];
            $amount = $_POST['amount'];
            $where = "AND " . $this->orders->where_user();
            $basket = $this->db->row("SELECT `id`,`amount`,`price_id` FROM `bascket` b WHERE `price_id` IN (?) " . $where, array($price_id));
            $result['status'] = 'success';
            if (!$basket) {
                // добавляем новый товар в корзину
                $this->orders->addBasket($price_id, $amount);
                $result['message'] = $this->translation['cart_add'];
            } else if ($amount != $basket['amount']) {
                // если товар добален в корзину но другое кол-во
                $this->db->query("UPDATE bascket b SET amount=? WHERE price_id=? " . $where, array($amount, $basket['price_id']));
                $result['message'] = $this->translation['cart_add'] . '(' . $amount . ')';
            } else {
                // если товар добален
                $result['message'] = $this->translation['products_added'];
            }
        }
        return json_encode($result);
    }

    public function countOrderPrices($products = [])
    {
        $sums = [
            'orderSum' => 0,
            'basePrice' => 0
        ];
        foreach ($products as $product) {
            $price = Numeric::viewPrice($product['price'], $product['basePrice']);
            $sums['orderSum'] += ($product['amount'] * (float)$product['price']);
            $sums['basePrice'] += ($product['amount'] * (float)$price['old_price']);
        }
        return $sums;
    }
}










