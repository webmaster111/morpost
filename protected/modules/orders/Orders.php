<?

    class Orders extends Model
    {
	   static $table = 'orders';
	   static $name = 'Заказы';

	   public function __construct($registry)
	   {
		  parent::getInstance($registry);
		  $this->view = new View($registry);
	   }

	   public static function getObject($registry)
	   {
		  return new self::$table($registry);
	   }

	   public function add()
	   {
		  $message = messageAdmin('Заполнены не все обязательные поля', 'error');
		  if (isset($_POST['email'], $_POST['phone']) && !empty($_POST['username'])) {
			 $id = $this->insert(array(
				'username' => $_POST['username'],
				'email' => $_POST['email'],
				'phone' => $_POST['phone'],
				'comment' => $_POST['comment'],
				'date_add' => date("Y-m-d H:i:s"),
				'payment_id' => $_POST['payment'],
				'delivery_id' => $_POST['delivery'],
				'status_id' => 1
			 ));
			 $message = messageAdmin('Данные успешно добавлены');
		  }
		  return $message;
	   }

	   public function save()
	   {
		  if (isset($this->registry['access'])) return $this->registry['access'];
		  $message = messageAdmin('При сохранение произошли ошибки', 'error');
		  if (!empty($_POST['status_id']) && !empty($_POST['save_id']) && is_array($_POST['save_id'])) {
			 foreach ($_POST['save_id'] as $key => $id) {
				$fields_update = ['status_id' => $_POST['status_id'][$key]];
				if (intval($_POST['status_id'][$key]) != 1) $fields_update['viewed'] = 1;
				$this->update($fields_update, [['id', '=', $id]]);
			 }
			 $message = messageAdmin('Данные успешно сохранены');
		  } else if (isset($_POST['status'], $_POST['email'], $_POST['username'], $_POST['phone'])) {
			 // Update orders product
			 $total = 0;
			 $amount = 0;
			 foreach ($_POST['product_id'] as $i => $product_id) {
				$sum = ($_POST['price'][$i] * $_POST['packaged'][$i]) * $_POST['amount'][$i];
				$total += $sum;
				$amount += $_POST['amount'][$i];
				$this->update(array(
				    'name' => $_POST['name'][$i],
				    'price' => $_POST['price'][$i],
				    'sum' => $sum,
				    'discount' => $_POST['discount'][$i],
				    'amount' => $_POST['amount'][$i],
				), [['id', '=', $product_id]], 'orders_product');
			 }

			 if (isset($_POST['delivery'])) {
				$row = $this->db->row("SELECT * FROM delivery WHERE id=?", [$_POST['delivery']]);
				$total += $row['price'];
				$this->db->query("UPDATE orders SET delivery_id=? WHERE id=?", [$_POST['delivery'], $_POST['id']]);
			 }

			 $this->update(array(
				'status_id' => $_POST['status'],
				'responsible' => $_POST['responsible'],
				'admin_comment' => $_POST['admin_comment'],
				'payment_id' => $_POST['payment'],
				'username' => $_POST['username'],
				'email' => $_POST['email'],
				'phone' => $_POST['phone'],
				'address' => $_POST['address'],
				'code_discount' => $_POST['code_discount'],
				'comment' => $_POST['comment'],
				'sum' => $total,
				'amount' => $amount
			 ), [['id', '=', $_POST['id']]]);

			 $message = messageAdmin('Данные успешно сохранены');
		  }
		  return $message;
	   }

	   function orderProduct()
	   {
		  if ($_POST['id']) {
			 $data = [];
			 $data['content'] = '<option value="0">Выберите товар...</option>';
			 $res = Product::getObject($this->sets)->find(['order' => 'tb.`sort` ASC,tb.id DESC', 'select' => 'tb.*,tb_lang.name', 'where' => '__tb.active:=1__ AND __tb3.catalog_id:=' . $_POST['id'] . '__', 'join' => ' LEFT JOIN product_catalog tb3 ON tb3.product_id=tb.id', 'type' => 'rows']);
			 if (count($res) != 0) foreach ($res as $row) $data['content'] .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
			 else $data['content'] = '<option value="0">Товаров нет...</option>';
			 return json_encode($data);
		  }
	   }

	   function orderProductView()
	   {
		  if (isset($_POST['id'], $_POST['order_id'])) {
			 $data = [];
			 $row = Product::getObject($this->sets)->find([
				    'select' => 'tb.*,tb_lang.name,tb_price.price,tb_price.discount',
				    'join' => "LEFT JOIN price tb_price ON tb_price.product_id=tb.id",
				    'where' => '__tb.id:=' . $_POST['id'] . '__']
			 );
			 $row2 = $this->db->row("SELECT id FROM orders_product WHERE orders_id=? AND product_id=?", [$_POST['order_id'], $_POST['id']]);
			 if (!$row2) $this->insert(array(
				'orders_id' => $_POST['order_id'],
				'name' => $row['name'],
				'price' => $row['price'],
				'discount' => $row['discount'],
				'amount' => 1,
				'packaged' => $row['packaged'],
				'sum' => ($row['price'] * $row['packaged']),
				'product_id' => $_POST['id'],
			 ), 'orders_product');

			 $data['price_pack'] = ($row['price'] * $row['packaged']);
			 //else $this->db->query("UPDATE orders_product SET amount=amount+1,`sum`=`sum`*amount WHERE id=?", [$row2['id']]);
			 $total = 0;
			 $res = $this->db->rows("SELECT * FROM orders_product WHERE orders_id=?", [$_POST['order_id']]);
			 foreach ($res as $row) {
				$sum = ($row['price'] * $row['packaged']) * $row['amount'];
				$total += $sum;
			 }
			 $this->recalc($_POST['order_id']);
			 $data['total'] = $total;
			 $data['res'] = $this->db->rows("SELECT tb.*,p.photo,p.url FROM orders_product tb LEFT JOIN product p ON p.id=tb.product_id WHERE orders_id=?", [$_POST['order_id']]);
			 $data['currency'] = $this->db->row("SELECT icon FROM currency WHERE `base`='1'");
			 $data['total'] = 'Итого: ' . $total;
			 return $data;
		  }
	   }

	   public function recalc($order_id)
	   {
		  $total = 0;
		  $res = $this->db->rows("SELECT * FROM orders_product WHERE orders_id=?", [$order_id]);
		  foreach ($res as $row) {
			 $sum = ($row['price'] * $row['packaged']) * $row['amount'];
			 $total += $sum;
			 $this->db->query("UPDATE `orders_product` SET `sum`=? WHERE `id`=?", [$sum, $row['id']]);
		  }
		  $this->db->query("UPDATE `orders` SET `amount`=?,`sum`=? WHERE `id`=?", [$res[0]['amount'], $total, $order_id]);
		  return $total;
	   }

	   function del_product($id)
	   {
		  $row = $this->db->row("SELECT orders_id FROM orders_product WHERE id=?", [$id]);
		  $this->db->query("DELETE FROM `orders_product` WHERE id=?", [$id]);
		  $this->recalc($row['orders_id']);
	   }

	   // user info into Letter

	   public function sendOrder($products, $info): string
	   {
		  $amount = 0;
		  $summa_all = 0;
		  $uni_analytics = '';
		  $query = [
			 'user_id' => $info['user_id'] === '' ? 0 : $info['user_id'],
			 'username' => $info['name'],
			 'email' => $info['email'],
			 'phone' => $info['phone'],
			 'delivery_id' => $info['delivery_id'],
			 'payment_id' => $info['payment_id'],
			 'status_id' => 1,
			 'date_add' => date("Y-m-d H:i:s"),
			 'currency' => $_SESSION['currency']['id'],
			 'rate' => $_SESSION['currency']['rate']
		  ];

		  $result = $this->buildLetter($info);

		  $_SESSION['order_id'] = $this->insert(array_merge($query, $result['query']));

		  foreach ($products as $product) {
			 $summa = $product['price_pack'] * $product['amount'];
			 $amount += $product['amount'];
			 $summa_all += $summa;

			 // Add orders products
			 $this->insert([
				'product_id' => $product['id'],
				'code' => $product['code'],
				'name' => $product['name'],
				'price' => $product['price'],
				'sum' => $summa,
				'discount' => !empty($product['discount']) ? $product['discount'] : 0,
				'packaged' => $product['packaged'],
				'amount' => $product['amount'],
				'photo' => $product['photo'],
				'orders_id' => $_SESSION['order_id']
			 ], 'orders_product');


			 if (isset($this->settings['uni_analytics']) && $this->settings['uni_analytics'] == 1) {
				$uni_analytics .= "{
            'id': '" . $product['id'] . "',
            'name': '" . $product['name'] . "',
            'quantity': " . $product['amount'] . ",
            'price': '" . $product['price_pack'] . "'
          },";
			 }
		  }

		  if (isset($this->settings['uni_analytics']) && $this->settings['uni_analytics'] == 1) {
			 $uni_analytics = "gtag('event','send',{'event_category':'zakaz','event_action':'send'});
            gtag('event', 'purchase', {
            'transaction_id': '" . $_SESSION['order_id'] . "',
            'affiliation': '" . $_SERVER['HTTP_HOST'] . "',
            'value': " . $summa_all . ",
            'currency': 'UAH',
            'items': [ " . $uni_analytics . " ]
          });";
		  }

		  $result['letter'] .= $this->view->Render(
			 'products-mail.phtml',
			 array('module' => self::$table, 'products' => $products)
		  );
		  $this->db->query("UPDATE `orders` SET `sum`=?,`amount`=? where `id`=?", [$summa_all, $amount, $_SESSION['order_id']]);

		  // Send Mails --
		  $template = $this->getBlock(18); // info block with TAG {{content}} for letter insert
		  $result['letter'] = str_replace('{{content}}', $result['letter'], htmlspecialchars_decode($template['body']));
		  if (isset($this->settings['notify_orders']) && $this->settings['notify_orders'] == 1) {
			 Mailer::SendToAdmin('Новый заказ на сайте', $result['letter']);
		  }
		  if (isset($info['email'])) {
			 Mailer::SendToUser($info['email'], $info['name'], 'Вы оформили заказ на сайте', $result['letter']);
		  }
		  //delete from Basket
		  $this->db->query('DELETE FROM `bascket` WHERE ' . $this->where_user('bascket'));
		  return $uni_analytics;
	   }

	   function buildLetter($info)
	   {
		  $query = array();
		  $letter = "Дата : " . date("Y-m-d H:i:s");
		  $letter .= "<br/><b>Информация о пользователе</b><br/>";
		  $letter .= "<br/>ФИО : " . $info['name'];

		  if (isset($info['email'])) {
			 $letter .= "<br/>E-mail : " . $info['email'];
		  }
		  if (isset($info['phone'])) {
			 $letter .= "<br/>Телефон : <a href='tel:" . $info['phone'] . "'>" . $info['phone'] . "</a>";
		  }

		  if (isset($info['address'])) {
			 $query['address'] = $info['address'];
			 $letter .= "<br/>Адрес : " . $info['address'];
		  }

		  if (isset($info['comment'])) {
			 $query['comment'] = $info['comment'];
			 $letter .= "<br/>Комментарий : " . $info['comment'];
		  }
		  return array(
			 'letter' => $letter,
			 'query' => $query
		  );
	   }

	   function where_user($tb = 'b')
	   {
		  if (isset($_SESSION['user_id'])) $ssid = "($tb.user_id='" . $_SESSION['user_id'] . "' OR $tb.session_id='" . session_id() . "')";
		  else $ssid = "$tb.session_id='" . session_id() . "'";
		  return $ssid;
	   }

	   // add to Basket

	   function sendOrder2($order_id, $type = 0)
	   {
		  $info = $this->db->row(
			 "SELECT 
       			orders.*,
			    	d.name AS delivery,
			    	p.name AS payment 
			 FROM orders 
			 	LEFT JOIN ru_delivery d ON d.delivery_id=orders.delivery_id 
			 	LEFT JOIN ru_payment p ON p.payment_id=orders.payment_id 
			 WHERE id='$order_id'");
		  $currencyOrder = Currency::getObject($this->sets)->find((int)$info['currency']);
		  $products = $this->db->rows(
			 "SELECT 
			 	op.*,
				p.photo,
				p.url 
			 FROM orders_product op 
			 	LEFT JOIN product p ON p.id=op.product_id 
			 WHERE op.orders_id='$order_id'");

		  // Add order
		  $date = date("Y-m-d H:i:s");

		  $result['letter'] = $this->view->Render(
			 'products-mail.phtml',
			 array('module' => self::$table, 'products' => $products)
		  );

		  $letter_admin = htmlspecialchars_decode($this->getBlock(18)['body']);
		  $letter_admin = str_replace('{{content}}', $result['letter'], htmlspecialchars_decode($letter_admin));

		  $letter = $letter_admin;

		  if (isset($info['email']) && $info['email'] != '') {
			 $letter_admin = str_replace('{{email}}', $info['email'], $letter_admin);
			 $letter = str_replace('{{email}}', $info['email'], $letter);
		  }
		  if (isset($info['phone']) && $info['phone'] != '') {
			 $letter_admin = str_replace('{{phone}}', $info['phone'], $letter_admin);
			 $letter = str_replace('{{phone}}', $info['phone'], $letter);
		  }
		  if (isset($info['code_discount']) && $info['code_discount'] != '') {
			 $letter_admin = str_replace('{{code_discount}}', $info['code_discount'], $letter_admin);
			 $letter = str_replace('{{code_discount}}', $info['code_discount'], $letter);
		  }

		  // Destination info
		  $d_info = '';
		  if (isset($info['destination_name']) && $info['destination_name'] != '') {
			 $d_info .= "ФИО получателя: {$info['destination_name']}<br>";
		  }
		  if (isset($info['destination_phone']) && $info['destination_phone'] != '') {
			 $d_info .= "Телефон получателя: {$info['destination_phone']}<br>";
		  }
		  if (isset($info['datetime']) && $info['datetime'] != '') {
			 $d_info .= "Дата и время доставки: {$info['datetime']}<br>";
		  }
		  if (isset($info['address']) && $info['address'] != '') {
			 $d_info .= "Адрес: {$info['address']}<br>";
		  }
		  if (isset($info['city']) && $info['city'] != '') {
			 $d_info .= "Город: {$info['city']}<br>";
		  }
		  if (isset($info['text']) && $info['text'] != '') {
			 $d_info .= "Примечание: <br>{$info['text']}<br>";
		  }

		  if ($d_info != '') {
			 $d_info = "<h4>Информация о получателе</h4>" . $d_info;
			 $letter_admin = str_replace('{{d_info}}', $d_info, $letter_admin);
			 $letter = str_replace('{{d_info}}', $d_info, $letter);
		  } else {
			 $letter_admin = str_replace('{{d_info}}', '', $letter_admin);
			 $letter = str_replace('{{d_info}}', '', $letter);
		  }

		  $order_id = $info['id'];
		  $path = $_SERVER['HTTP_HOST'];

		  if (isset($info['delivery']) && $info['delivery'] != '') {
			 $letter_admin = str_replace('{{delivery}}', 'Способ доставки: ' . $info['delivery'], $letter_admin);
			 $letter = str_replace('{{delivery}}', 'Способ доставки: ' . $info['delivery'], $letter);
		  } else {
			 $letter_admin = str_replace('{{delivery}}', '', $letter_admin);
			 $letter = str_replace('{{delivery}}', '', $letter);
		  }

		  if (isset($info['payment']) && $info['payment'] != '') {
			 $letter_admin = str_replace('{{payment}}', 'Способ оплаты: ' . $info['payment'], $letter_admin);
			 $letter = str_replace('{{payment}}', 'Способ оплаты: ' . $info['payment'], $letter);
		  } else {
			 $letter_admin = str_replace('{{payment}}', '', $letter_admin);
			 $letter = str_replace('{{payment}}', '', $letter);
		  }

		  $discount_text = '';
		  if (isset($info['discount']) && $info['discount'] > 0) $discount_text = '<br><span style="color:red;">Скидка: ' . $info['discount'] . '%</span>';
		  if (isset($info['discount2']) && $info['discount2'] > 0) $discount_text .= '<br><span style="color:red;">Скидка накопительная: ' . $info['discount2'] . '%</span>';

		  $text = "<br><br>Товары:<br><table border='1' cellpadding='0' cellspacing='0' width='700' style='border-collapse:collapse;'><tr><th width='60px' style='text-align:center;border:1px solid #cccccc;padding:10px;'>Артикул</th><th style='border:1px solid #cccccc;' width='100'>Фото</th><th style='border:1px solid #cccccc;'>Название товара</th><th width='60px' style='text-align:center;border:1px solid #cccccc;padding:10px;'>Кол-во</th><th style='border:1px solid #cccccc;' width='100'>Цена</th><th width='100px' style='text-align:center;border:1px solid #cccccc;padding:10px;'>Сумма</th></tr>";
		  $i = 0;
		  $total = 0;
		  foreach ($products as $row) {
			 $price = $row['price'];
			 $summa = $row['sum'];
			 $total += $row['sum'];
			 $src = "/" . $row['photo'];
			 $status = '';
			 $name = $row['name'];
			 if ($row['brend_id'] == 4) $status = '<div style="color: #317edd;font-size:10px;font-style:italic;margin-top:10px;">Под заказ</div>';
			 elseif ($row['brend_id'] == 3) {
				$status = '<div style="color: #317edd;font-size:10px;font-style:italic;margin-top:10px;">Сообщить цену клиенту </div>';
				$name .= ' (Сообщить цену клиенту)';
			 }
			 $text .= "<tr><td style='text-align:center;border:1px solid #cccccc;padding:10px;'>" . $row['code'] . "</td><td style='text-align:center;border:1px solid #cccccc;padding:10px;'><a href='http://" . $path . "/product" . $row['url'] . "'><img src='http://" . $path . $src . "' width='100'/></a></td><td style='text-align:center;border:1px solid #cccccc;padding:10px;'><a href='http://" . $path . "/product/" . $row['url'] . "'>" . $row['name'] . "</a> $status</td><td style='text-align:center;border:1px solid #cccccc;padding:10px;'>" . $row['amount'] . "</td><td style='text-align:center;border:1px solid #cccccc;padding:10px;'>" . $price . "</td><td style='text-align:center;border:1px solid #cccccc;padding:10px;'>" . $summa . "</td></tr>";
			 $i += $row['amount'];
		  }

		  if ($discount_text != '') $discount_text .= '<br><b>Итого к оплате: ' . Numeric::formatPrice($info['sum'], $currencyOrder) . '</b>';
		  $text .= "<tr><td colspan='7' align='right' style='border:1px solid #fff;'><div style='font-size:15px;'>" . $this->translation['all2'] . ": " . Numeric::formatPrice($total, $currencyOrder) . "{$discount_text}</div></td></tr></table>";

		  if ($type == 0) {
			 $subject = 'Ваш заказ №' . $order_id . ' изменен на сайте ' . $_SERVER['HTTP_HOST'];
		  } else {
			 $subject = 'Выписан счет на сайте ' . $_SERVER['HTTP_HOST'];
			 $letter = $this->getBlock(18);
		  }

		  $letter = str_replace('{{order_id}}', $order_id, $letter);
		  $letter = str_replace('{{products}}', $text, $letter);
		  $link = 'http://' . $_SERVER['HTTP_HOST'] . '/orders/payment/orderid/' . $order_id;
		  $letter = str_replace('{{link}}', '<a href="' . $link . '" target="_blank">' . $link . '</a>', $letter);

		  // Send to admin
		  if (isset($this->settings['notsify_orders']) && $this->settings['notify_orders'] == 1) {
			 $letter_admin = str_replace(array('{{order_id}}', '{{products}}'), array($order_id, $text), $letter_admin);
			 Mail::send('Заказ №' . $order_id . ' был изменен на сайте ' . $_SERVER['HTTP_HOST'], $letter_admin, $this->settings['email'], $this->settings['sitename']);
		  }

		  // Send to the client
		  if (isset($info['email'])) {
			 Mail::send($subject, $letter, $info['email'], $info['username']);
		  }
	   }

	   // Bascket shop cart

	   function addBasket($price_id, $amount)
	   {
		  if ($amount <= 0) $amount = 1;
		  $price = $this->db->row(" 
              SELECT pr.id, pr.product_id, pr.price, 
              pr.code, pr.discount, (pr.price * p.packaged) as  price_pack  FROM price pr
              LEFT JOIN product p on pr.product_id = p.id
              WHERE pr.id=? ",
			 array($price_id));
		  $params = array(
			 'price' => $price['price'],
			 'price_pack' => $price['price_pack'],
			 'session_id' => session_id(),
			 'product_id' => $price['product_id'],
			 'price_id' => $price['id'],
			 'code' => $price['code'],
			 'discount' => $price['discount'],
			 'date' => date("Y-m-d H:i:s"),
			 'amount' => $amount
		  );
		  if (isset($_SESSION['user_id'])) $params['user_id'] = $_SESSION['user_id'];
		  $this->insert($params, 'bascket');
	   }

	   function removeFromBasket($id)
	   {
		  $where = "AND " . $this->where_user('bascket');
		  $this->db->query("DELETE FROM `bascket` WHERE `id`=? $where", [$id]);
		  return $this->getBasket();
	   }

	   function getBasket()
	   {
		  $vars = array('products' => $this->query_basket());
		  $vars['count'] = count($vars['products']);
		  $vars['count_format'] = $vars['count'] ? ruCountProducts($vars['count']) . ' - ' : $this->translation['cart_empty'];
		  $sum = 0;
		  foreach ($vars['products'] as $product) {
			 $price = Numeric::getPrice($product);
			 $sum += $price['price_pack'] * $product['amount'];
		  }

		  $vars['total'] = Numeric::formatPrice($sum);
		  return $vars;
	   }

	   function query_basket()
	   {
		  $where = "WHERE " . $this->where_user();
		  return $this->db->rows("
                      SELECT 
                        b.amount,
                        b.id as cart_id,
                        b.code,
                        b.photo AS photo_basket,
                        p.id,
                        p.url,
                        p.photo,
                        p.sizes,
                        p2.material,
                        p.packaged,
                        p2.name,
                        price.price,
                        b.price_pack,
                        price.basePrice,
                        price.discount
                      FROM `bascket` b 
                      LEFT JOIN product p ON p.id=b.product_id 
                      LEFT JOIN `price` price ON price.id=b.price_id 
                      LEFT JOIN " . $this->registry['key_lang'] . "_product p2 ON p.id = p2.product_id 
                      {$where}
                      GROUP BY b.id");
	   }

	   function refreshBasket($count, $cart_id)
	   {
		  if (!empty($count) && !empty($cart_id)) {
			 $where = "AND " . $this->where_user('bascket');
			 $this->db->query("UPDATE `bascket` SET amount=? WHERE id=? " . $where, [$count, $cart_id]);
			 $row = $this->db->row("SELECT price_pack,amount FROM bascket WHERE id='{$cart_id}' " . $where);
			 return array('sum' => Numeric::formatPrice($row['price_pack'] * $row['amount']));
		  }
		  return null;
	   }

	   function getOrder($id)
	   {
		  $vars['order'] = $this->db->row("SELECT tb.*,tb2.name,tb3.name as delivery FROM orders tb LEFT JOIN orders_status tb2 ON tb.status_id=tb2.id LEFT JOIN " . $this->registry['key_lang'] . "_delivery tb3 ON tb.delivery_id=tb3.delivery_id WHERE tb.id=? AND tb.user_id=?", [$id, $_SESSION['user_id']]);
		  if (isset($vars['order']['id'])) {
			 $vars['translate'] = $this->translation;
			 $vars['product'] = $this->db->rows("SELECT * FROM orders_product WHERE orders_id=?", [$vars['order']['id']]);
			 return $vars;
		  }
	   }

	   function get_user_id($tb = 'b')
	   {
		  if (isset($_SESSION['user_id'])) {
			 $ssid = "($tb.user_id='" . $_SESSION['user_id'] . "' OR $tb.session_id='" . session_id() . "')";
		  } else {
			 $ssid = "$tb.session_id='" . session_id() . "'";
		  }
		  return $ssid;
	   }

	   function incomplete($user_id = 0)
	   {
		  ## Start Statistics
		  $cur_start_date = getdate(mktime(0, 0, 0, date("m") - 3, date("d"), date("Y")));
		  $cur_end_date = date("Y-m-d H:i:s");
		  if (strlen($cur_start_date['mon']) == 1) $cur_start_date['mon'] = '0' . $cur_start_date['mon'];
		  if (strlen($cur_start_date['mday']) == 1) $cur_start_date['mday'] = '0' . $cur_start_date['mday'];
		  $cur_start_date = $cur_start_date['year'] . '-' . $cur_start_date['mon'] . '-' . $cur_start_date['mday'];
		  if (isset($_POST['start'])) {
			 $_SESSION['date_start'] = $_POST['start'];
			 $_SESSION['date_end'] = $_POST['end'];
		  } elseif (!isset($_SESSION['date_start'])) {
			 $_SESSION['date_start'] = $cur_start_date;
			 $_SESSION['date_end'] = $cur_end_date;
		  }
		  $vars['message'] = '';
		  $vars['name'] = 'Незавершенные заказы';
		  if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->delete('bascket');
		  $where = '';
		  if ($user_id != 0) $where = "AND b.user_id='{$user_id}'";
		  $vars['product'] = $this->db->rows("SELECT p.*,p2.name,b.*,u.name AS username,u.email FROM `bascket` b LEFT JOIN product p ON p.id=b.product_id LEFT JOIN " . $this->registry['key_lang_admin'] . "_product p2 ON p.id=p2.product_id LEFT JOIN `price` tb_price ON `tb_price`.id=b.price_id LEFT JOIN users u ON u.id=b.user_id WHERE b.`date` BETWEEN '{$_SESSION['date_start']}' AND '{$_SESSION['date_end']}' $where GROUP BY b.id ORDER BY session_id DESC,b.date DESC");
		  return $vars;
	   }

	   public function add_orders_status()
	   {
		  if (isset($this->registry['access'])) return $this->registry['access'];
		  $this->db->query("INSERT INTO `orders_status` SET `name`='Новый статус'");
		  return messageAdmin('Данные успешно сохранены');
	   }

	   public function save_orders_status()
	   {
		  if (isset($this->registry['access'])) return $this->registry['access'];
		  $message = messageAdmin('При сохранение произошли ошибки', 'error');
		  if (isset($_POST['name'], $_POST['id'])) {
			 foreach ($_POST['id'] as $i => $id)
				$this->db->query("UPDATE `orders_status` SET `name`=? WHERE id=?", [$_POST['name'][$i], $id]);
			 $message = messageAdmin('Данные успешно сохранены');
		  }
		  return $message;
	   }

	   public function orderInXml($order_id)
	   {
		  sleep(3);// 3 секунды, чтобы админ смог прочитать сообщение до загрузки
		  $res = $this->db->rows("SELECT tb.*,tb2.*,d.name AS delivery,p.name AS payment FROM `orders_product` tb LEFT JOIN orders tb2 ON tb2.id=tb.orders_id LEFT JOIN ru_delivery d ON d.delivery_id=tb2.delivery_id LEFT JOIN ru_payment p ON p.payment_id=tb2.payment_id WHERE orders_id='$order_id' GROUP BY tb.id ORDER BY name ASC");
		  $xml = new DomDocument('1.0', 'utf-8');
		  $order = $xml->appendChild($xml->createElement('order'));
		  $xml_order_pid = $order->appendChild($xml->createElement('id'));
		  $xml_order_pid->appendChild($xml->createTextNode($order_id));
		  $xml_date = $order->appendChild($xml->createElement('date'));
		  $xml_date->appendChild($xml->createTextNode($res[0]['date_add']));
		  $xml_username = $order->appendChild($xml->createElement('username'));
		  $xml_username->appendChild($xml->createTextNode($res[0]['username']));
		  $xml_userid = $order->appendChild($xml->createElement('userid'));
		  $xml_userid->appendChild($xml->createTextNode($res[0]['user_id']));
		  $xml_email = $order->appendChild($xml->createElement('email'));
		  $xml_email->appendChild($xml->createTextNode($res[0]['email']));
		  $xml_phone = $order->appendChild($xml->createElement('phone'));
		  $xml_phone->appendChild($xml->createTextNode($res[0]['phone']));
		  $xml_city = $order->appendChild($xml->createElement('city'));
		  $xml_city->appendChild($xml->createTextNode($res[0]['city']));
		  $xml_address = $order->appendChild($xml->createElement('address'));
		  $xml_address->appendChild($xml->createTextNode($res[0]['address']));
		  $xml_delivery = $order->appendChild($xml->createElement('delivery'));
		  $xml_delivery->appendChild($xml->createTextNode($res[0]['delivery']));
		  $xml_payment = $order->appendChild($xml->createElement('payment'));
		  $xml_payment->appendChild($xml->createTextNode($res[0]['payment']));
		  $products = $order->appendChild($xml->createElement('products'));
		  foreach ($res as $row) {
			 // Adding data to XML
			 $product = $products->appendChild($xml->createElement('product'));
			 $xml_product_id = $product->appendChild($xml->createElement('id'));
			 $xml_product_id->appendChild($xml->createTextNode($row['product_id']));
			 $xml_product_name = $product->appendChild($xml->createElement('name'));
			 $xml_product_name->appendChild($xml->createTextNode($row['name']));
			 $xml_product_ones = $product->appendChild($xml->createElement('code'));
			 $xml_product_ones->appendChild($xml->createTextNode($row['code']));
			 $xml_product_price = $product->appendChild($xml->createElement('price'));
			 $xml_product_price->appendChild($xml->createTextNode($row['price']));
			 $xml_product_amount = $product->appendChild($xml->createElement('amount'));
			 $xml_product_amount->appendChild($xml->createTextNode($row['amount']));
			 $xml_product_sum = $product->appendChild($xml->createElement('sum'));
			 $xml_product_sum->appendChild($xml->createTextNode($row['price'] * $row['amount']));
			 $xml_product_size = $product->appendChild($xml->createElement('size'));
			 $xml_product_size->appendChild($xml->createTextNode($row['size']));
			 $xml_product_color = $product->appendChild($xml->createElement('color'));
			 $xml_product_color->appendChild($xml->createTextNode($row['color']));
		  }
		  $xml->formatOutput = true;
		  $xml = $xml->saveXML();
		  header('Content-Type: text/csv; charset=utf-8');
		  header('Content-Disposition: attachment; filename=order_' . $order_id . '.xml');
		  echo $xml;
		  exit();
	   }

    }