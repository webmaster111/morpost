<?

class ImportexelController extends BaseController
{

  protected $importexel, $path;

  function __construct($registry, $package)
  {
    parent::__construct($registry, $package);
    $this->tb = Importexel::$table;
    $this->name = Importexel::$name;
    $this->importexel = new Importexel($this->sets);
    $this->path = SITE_PATH . 'protected/cron/import/products/';
  }

  public function indexAction()
  {
    $vars['name'] = $this->name;
    $vars['message'] = isset($_POST['upload'], $_POST['link_import']) ? $this->save() : '';
    $vars['files'] = scandir($this->path);
    $this->getDirFiles($this->path, $vars['files']);
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }

  public function getDirFiles($path, &$files)
  {
    if (!$files) return $files;
    foreach ($files as $k => $file) {
      if (!is_file($path . $file)) unset($files[$k]);
    }
    return true;
  }

  function save()
  {
    if (!empty($_POST['link_import'])) {
      ini_set('max_execution_time',0);
      set_time_limit(0);
      $file = file_get_contents($_POST['link_import']);
      if (!is_dir($this->path)) mkdir($this->path, 0755, true);
      file_put_contents($this->path . 'import_' . time() . '.xml', $file);
    }
    return messageAdmin('Файл загружен');
  }

}