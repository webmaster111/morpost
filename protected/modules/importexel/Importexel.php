<?

class Importexel extends Model
{
  static $table = 'importexel';
  static $name = 'Выгрузка';

  private $catalog;
    protected $product;
    private $_log = '';

  public function __construct($registry)
  {
    parent::getInstance($registry);
    $this->catalog = new Catalog($registry);
    $this->product = new Product($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  //to do Сделать выгрузку до конца


  function import_url($url)
  {
    //$context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
    //$xml = file_get_contents($url, false, $context);
    //$xml = simplexml_load_string($xml);
    $xml = file_get_contents('import.xml');
    $xml = simplexml_load_string($xml);
    $xml = $xml->shop->children();
    $this->saveCategories($xml->categories->children());
    $this->saveProducts($xml->offers->children());

    return $this->_log;
  }

  private function saveProducts($products)
  {
    $i = 0;
    foreach ($products as $key => $product) {
      if ($i > 100) break;
      $RU = [
          'name' => (string)$product->name,
          'body' => (string)$product->description,
          'body_m' => ''
      ];
      $packaged = (string)$product->sales_notes;
      if (!empty($packaged)) {
        preg_match('/^.*(.\d).*$/m', $packaged, $matches);
        $packaged = intval($matches[1]);
      } else $packaged = 1;
      $mainArr = [
          'active' => 1,
          'code' => (string)$product->vendorCode,
          'id1c' => (string)$product['id'][0],
          'packaged' => $packaged,
          'date_edit' => date("Y-m-d H:i:s"),
          'date_add' => date("Y-m-d H:i:s")
      ];

      $productId = $this->db->cell("SELECT id FROM product WHERE id1c=?", array($mainArr['id1c']));
      if ($productId) {
        $this->product->update($mainArr, " `id`='" . $productId . "'", Product::$table, false);
      } else $productId = $this->product->insert($mainArr, Product::$table, false);

      if ($productId) {
        $RU['product_id'] = $productId;
          $url = StringLibrary::translit($RU['name']);
        $this->product->checkUrl(Product::$table, $url, $productId);
        foreach ($this->language as $lang) {
          $this->db->query("DELETE FROM `" . $lang['language'] . "_product` WHERE `product_id`=?", [$productId]);
          $this->product->insert($RU, $lang['language'] . '_product', false);
        }

        // закрепление за каталогами
        if (!empty($product->categoryId)) {
          $this->db->query("DELETE FROM `product_catalog` WHERE `product_id`=?", [$productId]);
          $catalog = $this->db->row("SELECT id,sub FROM catalog WHERE old_id=?", array($product->categoryId));
          if ($catalog) {
            $catalogsArr = array();
            $this->getParentsByArray($catalog['id'], $catalogsArr);
            foreach ($catalogsArr as $item)
              $this->db->query("INSERT INTO `product_catalog` SET `product_id`=?, `catalog_id`=?", [$productId, $item]);
          }
        }


        // Сохраняем фото
        if (!empty($product->picture)) {
          $this->db->query("DELETE FROM product_photo WHERE product_id={$productId}");
          if (!is_dir(SITE_PATH . "tmp/")) mkdir(SITE_PATH . "tmp/", 0755, true);
          // lol Rm  =)
          $dir = Dir::createDir($productId, SITE_PATH);
          recurseRmdir(SITE_PATH . $dir[0]);
          $dir = Dir::createDir($productId, SITE_PATH);
          // -----------
          $key_photo = 0;
          foreach ($product->picture as $photo_url) {
            $file = explode('/', $photo_url);
            $file_name = $file[count($file) - 1];
            $file = SITE_PATH . "tmp/" . $file_name;
            copy($photo_url, $file);
            if ($key_photo == 0) {
              $photo = Images::loadOriginalImage($file, SITE_PATH . $dir[0], $productId);
              $photo = explode('/files/', $photo);
              $this->db->query("UPDATE `product` SET `photo`=? WHERE `id`=?", ['files/' . $photo[1], $productId]);
            } else {
              $id = $this->db->insert_id("INSERT INTO `product_photo` SET `product_id`=? ,`texture`='0', `active`='1' , `sort`='0'", array($productId));
              $this->db->query("INSERT INTO `ru_product_photo` SET `product_photo_id`=? ,`name`=?", array($id, $file_name));
              $photo = Images::loadOriginalImage($file, SITE_PATH . $dir[1], $id);
              $photo = explode('/files/', $photo);
              $this->db->query("UPDATE `product_photo` SET `photo`=? WHERE `id`=?", ['files/' . $photo[1], $id]);
            }
            $key_photo++;
          }

        }

        if (!empty($product->price)) {
          $this->db->query("DELETE FROM price WHERE product_id='{$productId}'");
          $price = (string)$product->price;
          $this->insert(array(
              'code' => (string)$product->vendorCode,
              'basePrice' => $price,
              'price' => $price,
              'stock' => 10,
              'discount' => 0,
              'product_id' => $productId,
          ), 'price');
        }

        $i++;
      }
    }
  }

  private function getParentsByArray($id, &$array)
  {
    if ($id) {
      $array[] = $id;
      $id_parent = $this->db->cell("SELECT sub FROM catalog WHERE id=?", array($id));
      $this->getParentsByArray($id_parent, $array);
    } else return null;
  }

  private function saveCategories($catalogs)
  {
    if (!empty($catalogs)) {
      foreach ($catalogs as $key => $catalog) {
        $saved = $this->saveCatalog($catalog);
      }

      // Сохраняем вложености
      foreach ($catalogs as $key => $catalog) {
        $parent_id = (string)$catalog['parentId'][0];
        if (!empty($parent_id)) {
          $sub = $this->db->cell("SELECT id FROM `catalog` WHERE `old_id`=?", array($parent_id));
          if ($sub) $this->db->query("UPDATE `catalog` SET `sub`=? WHERE `old_id`=?", array($sub, (string)$catalog['id'][0]));
        }
      }
    }
  }

  private function saveCatalog($catalog)
  {
    $RU = [
        'name' => (string)$catalog[0],
        'body' => '',
    ];
    $mainArr = [
        'active' => 1,
        'sub' => null,
        'old_id' => (string)$catalog['id'][0]
    ];

    $catalogId = $this->db->cell("SELECT id FROM `catalog` WHERE `old_id`=?", array($mainArr['old_id']));
    if ($catalogId) {
      $this->catalog->update($mainArr, " `id`='" . $catalogId . "'", Catalog::$table, false);
    } else {
      $catalogId = $this->catalog->insert($mainArr, Catalog::$table, false);
    }


    if ($catalogId) {
      $RU['catalog_id'] = $catalogId;
        $url = StringLibrary::translit($RU['name']);
      $this->catalog->checkUrl(Catalog::$table, $url, $catalogId);
      foreach ($this->language as $lang) {
        $this->db->query("DELETE FROM `" . $lang['language'] . "_catalog` WHERE `catalog_id`=?", [$catalogId]);
        $this->catalog->insert($RU, $lang['language'] . '_catalog', false);
      }
      return array('id' => $catalogId, 'old_id' => $mainArr['old_id']);
    }
    return false;
  }

}