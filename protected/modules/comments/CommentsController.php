<?

class CommentsController extends BaseController
{
  protected $comments;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Comments::$table;
    $this->comments = new Comments($this->sets);
  }

  function indexAction()
  {
    $data['breadcrumbs'] = [['name' => $this->translation[$this->tb], 'url' => $this->tb . '/all']];
    switch ($this->params[$this->tb]) {
      case 'all' :
        $vars['template'] = 'page-all';
        $vars = $this->RenderSiteComments();
        break;
      default :
        return Router::act('error', $this->registry);
    }
    $data['scripts'] = array('jquery.fancybox.min.js');
    $data['styles'] = array('jquery.fancybox.min.css');
    $menu_catalogs = getTree(containArrayInHisId($this->catalog->getAll(" AND tb.active='1'")));
    $vars['menu_catalogs'] = $this->view->Render('layout/menu.phtml', array('catalogs' => $menu_catalogs));
    if (isset($data['breadcrumbs'])) $vars['breadcrumbs'] = $this->model->breadcrumbs($data['breadcrumbs'], $this->view);
    $data['content'] = $this->view->Render($this->tb . '/' . $vars['template'] . '.phtml', $vars);
    return $this->Index($data);
  }

  private function RenderSiteComments()
  {
    $vars = array(
        'template' => 'list',
        'title' => $this->translation['all_reviews'],
        'form' => $this->view->Render('forms/add_comment.phtml'),
        'table' => $this->tb
    );
    $reviews = $this->comments->find(array(
        'type' => 'rows',
        'where' => 'tb.active = "1"',
        'limit' => $this->settings['paging_comment']
    ));
    $vars['no-load'] = !($this->comments->count("active='1'") == count($reviews));
    $vars['list'] = $this->view->Render($this->tb . '/thumb.phtml', array('reviews' => $reviews));
    return $vars;
  }

  private function RenderGalleryComments()
  {
    $gallery = new Gallery($this->sets);
    $vars = array(
        'template' => 'list',
        'title' => $this->translation['instagram_reviews'],
        'table' => Gallery::$table
    );
    $photos = $gallery->find(array(
        'type' => 'rows',
        'where' => 'tb.active = "1"',
        'order' => 'tb.sort',
        'limit' => $this->settings['gallery_paging']
    ));
    $vars['no-load'] = !($gallery->count("active='1'") == count($photos));
    $vars['list'] = $this->view->Render($vars['table'] . '/thumb.phtml', array('photos' => $photos));
    return $vars;
  }

  public function getMoreAction()
  {
    if (isset($_POST['offset'])) {
      $offset = $_POST['offset'];
      $on_page = $this->settings['paging_comment'];
      $result = array('offset' => $offset + 1);
      $all = $this->comments->count("active='1'");
      $list = $this->comments->find(array(
          'type' => 'rows',
          'where' => 'tb.active = "1"',
          'limit' => $on_page . ' OFFSET ' . ($offset * $on_page)
      ));
      $result['thumbs'] = $this->view->Render($this->tb . '/thumb.phtml', array('reviews' => $list, 'slide_down' => true));
      if (($all - (count($list) + $offset * $on_page)) <= 0) $result['offset'] = 0;
      return json_encode($result);
    }
    return false;
  }

  // Add comments
  function addAction()
  {
    return json_encode($this->comments->add());
  }
}