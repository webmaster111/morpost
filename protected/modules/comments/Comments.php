<?

class Comments extends Model
{
    static $table = 'comments';
    static $name = 'Комментарии';

    public function __construct($registry)
    {
        parent::getInstance($registry);
        $this->users = new Users($this->sets);
    }

    public static function getObject($registry)
    {
        return new self::$table($registry);
    }

    public function add()
    {
        $data = array('status' => 'error', 'message' => $this->translation['sth_wrong']);
        strip_post();
		$secretkey =  $this->settings["recaptcha_secretkey"];
        if (!GoogleCaptcha::check($secretkey)) {
            $data['message'] = $this->translation['wrong-recaptcha'];
            return $data;
        }
        if (isset($_POST['name'], $_POST['text'], $_POST['type_id']) && $_POST['name'] != "" && $_POST['text'] != "") {
            $insert_id = $this->insert(array(
                'author' => $_POST['name'],
                'date' => date("Y-m-d H:i:s"),
                'text' => $_POST['text'],
                'type_id' => $_POST['type_id'],
                'active' => 0,
            ));
            $data['file_upload'] = $this->savePostPhoto($_FILES['photo'], $insert_id);
            $data['status'] = 'success';
            $data['message'] = $this->translation['comment_add'];
        }
        return $data;
    }

    public function save()
    {
        $message = messageAdmin('Заполнены не все обязательные поля', 'error');
        if (isset($this->registry['access'])) $message = $this->registry['access'];
        else if (isset($_POST['active'], $_POST['name'], $_POST['text'])) {
            $this->update(array(
                'author' => $_POST['name'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone'],
                'author2' => $_POST['name2'],
                'text' => $_POST['text'],
                'text2' => $_POST['text2'],
                'active' => $_POST['active'],
                'moderator_id' => $_SESSION['admin']['id']
            ), "id={$_POST['id']}");
            $message = messageAdmin('Данные успешно сохранены');
        }
        return $message;
    }

    public function list_comments_admin($id, $type = 'product')
    {
        $vars = [];
        $vars['type'] = $type;
        $vars['id'] = $id;
        $vars['comments'] = $this->db->rows("SELECT * FROM `comments` WHERE content_id=? AND type=? ORDER BY date DESC", [$id, $type]);
        if (isset($vars['comments'][0]['id'])) {
            $view = new View($this->registry);
            $vars['module'] = 'comments/admin';
            return $view->Render('comments_list.phtml', $vars);
        } else return '';
    }
}