<?

class CommentsController extends BaseController
{
  private $comments;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Comments::$table;
    $this->name = "Комментарии";
    $this->comments = new Comments($this->sets);
  }

  public function indexAction()
  {
    $vars['name'] = $this->name;
    if (isset($this->params['subsystem'])) return $this->Index($this->comments->subsystemAction());
    $vars['message'] = isset($this->registry['access']) ? $this->registry['access'] : '';
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->comments->delete($this->tb);
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->comments->save();
    $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->comments->find(['type' => 'rows', 'order' => 'tb.date DESC', 'paging' => $this->settings['paging_comment_admin']])]);
    $data['content'] = $this->view->Render('list.phtml', $vars);
    $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name]);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = isset($_POST['update']) ? $this->comments->save() : '';
    $vars['edit'] = $this->comments->find($this->params['edit']);
    if (isset($this->params['del-photo'])) {
      if (file_exists($vars['edit']['photo'])) unlink($vars['edit']['photo']);
      $vars['edit']['photo'] = NULL;
      $this->comments->update(array('photo' => $vars['edit']['photo']), array(['id', '=', $vars['edit']['id']]));
      $vars['message'] = messageAdmin('Фото успешно удалено !');
    } else if (isset($this->params['del-video'])) {
      $vars['edit']['video'] = NULL;
      $this->comments->update(array('video' => $vars['edit']['video']), array(['id', '=', $vars['edit']['id']]));
      $vars['message'] = messageAdmin('Видео успешно удалено !');
    }
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }

}