<?php

    class Novaposhta extends Model
    {
	   static $table = 'np_city';
	   static $name = 'Новая почта';

	   private $NP;

	   public function __construct($registry)
	   {
		  parent::getInstance($registry);
		  $this->NP = new \LisDev\Delivery\NovaPoshtaApi2('5e5c29cb397ef484df6f5c8bc429f69c', 'ua');
	   }

	   public static function getObject($registry)
	   {
		  return new self::$table($registry);
	   }

	   public function save()
	   {
		  if (isset($this->registry['access'])) return $this->registry['access'];
		  $message = messageAdmin('При сохранение произошли ошибки', 'error');
		  if (isset($_POST['name'], $_POST['id'])) {
			 foreach ($this->language as $lang)
				$this->update(['name' => $_POST['name']], [[self::$table . '_id', '=', $_POST['id']]], $lang['language'] . "_" . self::$table);
			 $message = messageAdmin('Данные успешно сохранены');
		  }
		  return $message;
	   }

	   public function getFromApi(): string
	   {
		  ini_set('max_execution_time', 0);
		  set_time_limit(0);
		  $cities = $this->NP->getCities();
		  if (!empty($cities['data'])) {

			 $this->clearTableNP();

			 foreach ($cities['data'] as $city) {
				$id = $this->insert(array(
				    'api_id' => $city['CityID'],
				    'ref' => $city['Ref'],
				    'area' => $city['Area']
				));
				$this->insert(array(
				    'name' => $city['DescriptionRu'],
				    'np_city_id' => $id
				), 'ru_np_city');

				$this->departments($city['Ref']);
			 }

			 $this->cloneTableAnotherLanguage();

			 return messageAdmin('Данные успешно обновлены');
		  }
		  return messageAdmin('Ошибка API', 'error');
	   }

	   /**
	    * @description TODO clear all data in tables Nova Poshta
	    * @return void
	    */
	   private function clearTableNP(): void
	   {
		  $tables = ['np_city', 'np_department'];
		  $this->getTableLanguage($tables);
		  foreach ($tables as $key => $item) {
			 $this->query("DELETE FROM `$item`");
			 $this->query("ALTER TABLE `$item` AUTO_INCREMENT=1;");
		  }
	   }

	   /**
	    * @description TODO add in table array language table
	    * @param array $tables
	    * @return  void
	    */
	   private function getTableLanguage(array &$tables): void
	   {
		  $length = count($tables);
		  foreach ($this->language as $key => $item) {
			 foreach ($tables as $key2 => $item2) {
				if ($key2 >= $length) {
				    break;
				}
				$tables[] = $item['language'] . '_' . $item2;
			 }
		  }
	   }

	   /**
	    * @description TODO adding in DB departments Nova Poshta in city
	    * @param string $cityRef
	    * @return void
	    */
	   private function departments(string $cityRef): void
	   {
		  $departments = $this->NP->getWarehouses($cityRef);
		  if (!empty($departments['data'])) {
			 foreach ($departments['data'] as $department) {
				$dep_id = $this->insert(array(
				    'api_id' => $department['SiteKey'],
				    'ref' => $department['Ref'],
				    'city_ref' => $department['CityRef']
				), 'np_department');
				$this->insert(array(
				    'name' => $department['DescriptionRu'],
				    'short_name' => $department['ShortAddressRu'],
				    'np_department_id' => $dep_id
				), 'ru_np_department');
			 }
		  }
	   }

	   /**
	    * @description TODO create russian table clone for another languages
	    * @return void
	    */
	   private function cloneTableAnotherLanguage(): void
	   {
		  foreach ($this->language as $key => $item) {
			 if ($item['language'] === 'ru') {
				continue;
			 }
			 $this->query("INSERT INTO `{$item['language']}_np_city` SELECT * FROM `ru_np_city`");
			 $this->query("INSERT INTO `{$item['language']}_np_department` SELECT * FROM `ru_np_department`");
		  }
	   }

    }