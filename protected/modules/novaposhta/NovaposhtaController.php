<?

class NovaposhtaController extends BaseController
{
  protected $cities;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Novaposhta::$table;
    $this->name = Novaposhta::$name;
    $this->cities = new Novaposhta($this->sets);
  }

  public function getcitiesAction()
  {
    if (isset($_POST['name'], $_POST['required'])) {
      return json_encode(array(
          'html' => $this->view->Render('delivery/cities-list.phtml', array(
              'name' => $_POST['name'],
              'required' => $_POST['required'],
              'cities' => $this->cities->getAll()
          ))
      ));
    } else return false;
  }

  public function getdepartmentsAction()
  {
    if (isset($_POST['name'], $_POST['required']) && !empty($_POST['ref'])) {
      return json_encode(array(
          'html' => $this->view->Render('delivery/departments-list.phtml', array(
              'name' => $_POST['name'],
              'required' => $_POST['required'],
              'departments' => $this->cities->find(array(
                  'table' => 'np_department',
                  'type' => 'rows',
                  'where' => "tb.city_ref='{$_POST['ref']}'"
              ))
          ))
      ));
    } else return false;
  }

}