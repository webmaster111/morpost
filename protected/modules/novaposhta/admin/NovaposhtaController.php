<?

class NovaposhtaController extends BaseController
{
  protected $cities;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Novaposhta::$table;
    $this->name = Novaposhta::$name;
    $this->cities = new Novaposhta($this->sets);
  }

  public function indexAction()
  {
    $vars['name'] = $this->name;
    if (isset($this->registry['access'])) return $this->registry['access'];
    if (isset($this->params['subsystem'])) return $this->Index($this->cities->subsystemAction());

    $vars['message'] = isset($this->params['delete']) ? $this->cities->delete($this->tb) : '';
    if (isset($this->params['update-list'])) $vars['message'] .= $this->cities->getFromApi();
    if (isset($_POST['update'])) $vars['message'] = $this->cities->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->cities->save();
    $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->cities->find(['type' => 'rows'])]);
	 $data['left_menu'] = $this->model->left_menu_admin(['action' => 'novaposhta', 'name' => $this->name]);
	 $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = isset($_POST['update']) ? $this->cities->save() : '';
    $vars['edit'] = $this->cities->find((int)$this->params['edit']);
    $vars['departments'] = $this->cities->find(array(
        'table' => 'np_department',
        'type' => 'rows',
        'where' => "city_ref='{$vars['edit']['ref']}'"
    ));
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }
}