<?

class Menu extends Model
{
  static $table = 'menu';
  static $name = "Меню";

  public function __construct($registry)
  {
    parent::getInstance($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  public function add()
  {
    $message = '';
    if (isset($_POST['sub'], $_POST['active'], $_POST['url'], $_POST['name'], $_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body']) && $_POST['name'] != "") {
        if ($_POST['url'] == '') $url = StringLibrary::translit($_POST['name'], true);
        elseif (!strpos($_POST['url'], '://')) $url = StringLibrary::translit($_POST['url'], true);
      else $url = $_POST['url'];
      // Save meta data
      $meta = new Meta($this->sets);
      $meta->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);
      $sub = $_POST['sub'] == 0 ? NULL : $_POST['sub'];
      $insert_id = $this->db->insert_id("INSERT INTO `" . self::$table . "` SET `sub`=?,`active`=?",
          [$sub, $_POST['active']]);
      $this->checkUrl(self::$table, $url, $insert_id);
      foreach ($this->language as $lang)
        $this->db->query("INSERT INTO `" . $lang['language'] . "_" . self::$table . "` SET `name`=?,`body`=?,`body_2`=? , `" . self::$table . "_id`=?",
            [$_POST['name'], $_POST['body'], $_POST['body_2'], $insert_id]);
      $this->savePhoto($insert_id, $_POST['tmp_image'], self::$table);
      $message .= messageAdmin('Данные успешно добавлены');
    } else $message .= messageAdmin('Заполнены не все обязательные поля', 'error');
    return $message;
  }

  public function save()
  {
    $message = '';
    $infoblocks = [];

    if(!empty($_POST['info_block_id']) && !empty($_POST['sort']) && !empty($_POST['info_block_group'])){
      for ($i=0; $i < count($_POST['info_block_id']); $i++){
          $infoblocks[] = array(
              'iblock' => $_POST['info_block_id'][$i],
              'sort'   => $_POST['sort'][$i],
              'col_lg' => $_POST['col_lg'][$i],
              'col_md' => $_POST['col_md'][$i],
              'col_sm' => $_POST['col_sm'][$i],
              'col_xs' => $_POST['col_xs'][$i],
              'group'  => $_POST['info_block_group'][$i],
              'type'   => $_POST['info_block_type'][$i],
          );
      }
    }
    if (isset($this->registry['access'])) $message = $this->registry['access'];
    else {
      if (isset($_POST['save_id']) && is_array($_POST['save_id'])) {
        if (isset($_POST['save_id'], $_POST['name'], $_POST['url'])) {
          for ($i = 0; $i < count($_POST['save_id']); $i++) {
              if ($_POST['url'][$i] == '') $url = StringLibrary::translit($_POST['name'][$i], true);
              elseif (!strpos($_POST['url'], '://')) $url = StringLibrary::translit($_POST['url'][$i], true);
            else $url = $_POST['url'];
            $this->checkUrl(self::$table, $url, $_POST['save_id'][$i]);
            $param = array($_POST['name'][$i], $_POST['save_id'][$i]);
            $this->db->query("UPDATE `" . $this->registry['key_lang_admin'] . "_" . self::$table . "` SET `name`=? WHERE " . self::$table . "_id=?", $param);
          }
          $message .= messageAdmin('Данные успешно сохранены');
        } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
      } else {
        if (isset($_POST['sub'], $_POST['active'], $_POST['url'], $_POST['id'], $_POST['name'], $_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body'])) {
            if ($_POST['url'] == '') $url = StringLibrary::translit($_POST['name'], true);
            elseif (!strpos($_POST['url'], '://')) $url = StringLibrary::translit($_POST['url'], true);
          else $url = $_POST['url'];
          // Save meta data
          $meta = new Meta($this->sets);
          $meta->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);
          $this->checkUrl(self::$table, $url, $_POST['id']);
          $sub = $_POST['sub'] == 0 ? NULL : $_POST['sub'];
          $param = array($sub, $_POST['active'], json_encode($infoblocks), $_POST['id']);
          $this->update(
            array('sub' => $sub, 'infoblocks' => json_encode($infoblocks), 'active' => $_POST['active']),
            [['id', '=', $_POST['id']]]
          );
          //$this->db->query("UPDATE `" . self::$table . "` SET `sub`=?,`active`=?,`infoblocks`=? WHERE id=?", $param);
          $param = array($_POST['name'], $_POST['body'], $_POST['body_2'], $_POST['id']);
          $this->db->query("UPDATE `" . $this->registry['key_lang_admin'] . "_" . self::$table . "` SET `name`=?,`body`=?,`body_2`=? WHERE `" . self::$table . "_id`=?", $param);
          $message .= messageAdmin('Данные успешно сохранены');
        } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
      }
    }
    return $message;
  }
}