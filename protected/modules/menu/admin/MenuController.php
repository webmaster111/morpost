<?

class MenuController extends BaseController
{
  protected $menu;
  protected $infoblocks;
  protected $iblocks_types = ['column' => 'Колонка', 'tab' => 'Таб', 'anchor' => 'Якорьная ссылка', 'link' => 'Страничная ссылка'];

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->name = "Меню";
    $this->tb = Menu::$table;
    $this->menu = new Menu($this->sets);
    $this->infoblocks = new Infoblocks($this->sets);
  }

  public function indexAction()
  {
    if (isset($this->params['subsystem'])) return $this->Index($this->menu->subsystemAction());
    $vars['name'] = $this->name;
    $vars['message'] = isset($this->registry['access']) ? $this->registry['access'] : '';
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->menu->delete('menu');
    elseif (isset($_POST['update'])) $vars['message'] = $this->menu->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->menu->save();
    elseif (isset($_POST['add_close'])) $vars['message'] = $this->menu->add();
    $where = "tb.sub is NULL";
    $vars['url'] = '/admin/' . $this->tb;
    if (isset($this->params['cat'])) {
//      $where = "tb.sub='{$this->params['cat']}' ";
      $where = '';
      $_SESSION['sort_menu'] = $this->params['cat'];
      $vars['url'] .= '/cat/' . $this->params['cat'];
    } else $_SESSION['sort_menu'] = 0;
    $vars['select_tree'] = $this->model->select_tree($this->tb, $_SESSION['sort_menu']);
    $vars['menu'] = $this->menu->find(array('type' => 'rows', 'order' => 'tb.sort ASC,tb.id ASC'));
    $vars['list'] = $this->view->Render('view.phtml', $this->menu->find(array('paging' => true, 'where' => $where, 'order' => 'tb.sort ASC,tb.id ASC')));
    foreach ($vars['menu'] as $key => $value) $vars['menu'][$key]['url'] = $vars['menu'][$key]['id'];
    $settings = array('arr' => $vars['menu'], 'link' => '/admin/menu/cat/', 'id' => 'tree');
    $data['left_menu'] = $this->view->Render('cat_menu.phtml', array('cat_menu' => Arr::treeview($settings)));
    $data['left_menu'] .= $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name));
    $data['content'] = $this->view->Render('list.phtml', $vars);
    $data['styles'] = array('jquery.treeview.css');
    $data['scripts'] = array('jquery.treeview.js');
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = isset($_POST['add']) ? $this->menu->add() : '';
    $vars['width'] = 600;
    $vars['height'] = 400;
    $vars['catalog'] = $this->menu->getAll(" AND sub IS NULL AND tb.id!='" . $vars['edit']['id'] . "'");
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = isset($_POST['update']) ? $this->menu->save() : '';
    $vars['edit'] = $this->menu->find((int)$this->params['edit']);
    $dir = Dir::createDir($this->params['edit'], '', $this->tb);
    $vars['path'] = $dir[0];
    $vars['width'] = 600;
    $vars['height'] = 400;

    // Load meta
    $row = $this->meta->load_meta($this->tb, $vars['edit']['url']);
    if ($row) {
      $vars['edit']['title'] = $row['title'];
      $vars['edit']['keywords'] = $row['keywords'];
      $vars['edit']['description'] = $row['description'];
    }

    $vars['infoblocks'] = $this->view->Render('iBlocks_list.phtml', [
      'iblocks'       => json_decode($vars['edit']['infoblocks']),
      'iblocks_types' => $this->iblocks_types,
      'infoblocks'    => $this->infoblocks->getAll()
    ]);

    $vars['catalog'] = $this->menu->getAll(" AND sub IS NULL AND tb.id!='" . $vars['edit']['id'] . "'");
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }

  public function addIBlockRowAction()
  {
    $data = ['success' => false, 'message' => 'Возникла ошибка при добавлении информационного блока'];
    if(!empty($_POST['id'])){
        $this->registry::set('admin', 'menu');
        $iblocks = array();
        $cur_page = $this->menu->find($_POST['id']);
        $infoblocks = [
            'iblock' => 0,
            'sort'   => 0,
            'col_lg' => 12,
            'col_md' => 12,
            'col_sm' => 12,
            'col_xs' => 12,
            'group'  => 1,
            'type'   => 'column',
        ];
        $vars['row_id'] = 1;
        if(!empty($cur_page['infoblocks'])){
            $added_iblocks = json_decode($cur_page['infoblocks']);
            foreach ($added_iblocks as $added_iblock){
                $iblocks[] = $added_iblock;
            }
            $vars['row_id'] = count($added_iblocks);
        }
        $iblocks[] = $infoblocks;
        $this->menu->update(['infoblocks' => json_encode($iblocks)], [['id', '=', $_POST['id']]]);
        $vars['iblocks'] = $iblocks;
        $vars['iblocks_types'] = $this->iblocks_types;
        $vars['infoblocks'] = $this->infoblocks->getAll();
        $data['output'] = $this->view->Render('iBlocks_list.phtml', $vars);
        $data['success'] = true;
        $data['message'] = 'Инфоблок удален';
        return json_encode($data);
    }
    return false;
  }

  public function delIBlockRowAction()
  {
      $data = ['success' => false, 'message' => 'Возникла ошибка при удалении информационного блока'];
      if(!empty($_POST['id'])){
          $this->registry::set('admin', 'menu');
          $iblocks = array();
          $i = 0;
          $cur_page = $this->menu->find($_POST['id']);
          $added_iblocks = json_decode($cur_page['infoblocks']);
          if(count($added_iblocks) > 1){
              foreach ($added_iblocks as $added_iblock){
                  $i++;
                  if($i == $_POST['row_id']){
                      continue;
                  }else{
                      $iblocks[] = $added_iblock;
                  }
              }
              $this->menu->update(['infoblocks' => json_encode($iblocks)], [['id', '=', $_POST['id']]]);
          }else{
              $this->menu->update(['infoblocks' => NULL], [['id', '=', $_POST['id']]]);
          }
          $vars['iblocks'] = $iblocks;
          $vars['iblocks_types'] = $this->iblocks_types;
          $vars['infoblocks'] = $this->infoblocks->getAll();
          $data['output'] = $this->view->Render('iBlocks_list.phtml', $vars);
          $data['success'] = true;
          $data['message'] = 'Инфоблок удален';
          return json_encode($data);
      }
      return json_encode($data);
  }
}