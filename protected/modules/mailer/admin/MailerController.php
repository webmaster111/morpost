<?

    class MailerController extends BaseController
    {
	   public $left_menu;
	   private $mailer;

	   function __construct($registry, $params)
	   {
		  parent::__construct($registry, $params);
		  $this->tb = Mailer::$table;
		  $this->name = "Модуль рассылки";
		  $this->queue = "mail_queue";
		  $this->mailer = new Mailer($this->sets);
	   }

	   public function indexAction()
	   {
		  $vars['message'] = '';
		  if (isset($this->params['act'])) {
			 $act = $this->params['act'] . 'Action';
			 return $this->Index($this->$act());
		  }
		  $this->checkEmptySubscribers();

		  $vars['name'] = 'Подписчики';
		  $vars['action'] = $this->tb;
		  $vars['path'] = '/act/subscribers';
		  $vars['list'] = $this->mailer->find(array('table' => 'subscribers', 'order' => 'date_add ASC', 'type' => 'rows', 'paging' => true));
		  $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'menu2' => $this->left_menu));
		  $data['content'] = $this->view->Render('subscribers.phtml', $vars);
		  return $this->Index($data);
	   }

	   public function addAction()
	   {
		  $vars['message'] = isset($_POST['add']) ? $this->mailer->add($this->queue) : '';
		  $vars['users'] = $this->db->rows("SELECT `mailer`,`email`,`id` FROM `users`");
		  $data['content'] = $this->view->Render('add.phtml', $vars);
		  return $this->Index($data);
	   }

	   public function editAction()
	   {
		  $vars['message'] = isset($_POST['update']) ? $this->mailer->save() : '';
		  $vars['edit'] = $this->mailer->find((int)$this->params['edit']);
		  $vars['list'] = $this->mailer->listUsersView($this->params['edit']);
		  $vars['users'] = $this->db->rows("SELECT `mailer`,`email`,`id` FROM `users`");
		  $data['content'] = $this->view->Render('edit.phtml', $vars);
		  return $this->Index($data);
	   }

	   public function subscribersAction()
	   {
		  $vars['message'] = '';
		  if (isset($_POST['update'])) $vars['message'] = $this->mailer->save_subscribers();
		  elseif (isset($this->params['addsubscribers'])) $vars['message'] = $this->mailer->add_subscribers();
		  elseif (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->mailer->delete('subscribers');
		  $vars['name'] = 'Подписчики';
		  $vars['action'] = $this->tb;
		  $vars['path'] = '/act/subscribers';
		  $vars['list'] = $this->mailer->find(array('table' => 'subscribers', 'order' => 'date_add ASC', 'type' => 'rows', 'paging' => true));
		  $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'sub' => 'subscribers', 'menu2' => $this->left_menu));
		  $data['content'] = $this->view->Render('subscribers.phtml', $vars);
		  return $data;
	   }

	   /**
	    * @description TODO finds empty fields in subscribers and deleting this subscribers
	    * @return void
	    */
	   private function checkEmptySubscribers(): void
	   {
		  $this->db->query('DELETE FROM `subscribers` WHERE `name` IS NULL OR `email` IS NULL');
	   }
    }