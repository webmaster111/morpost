<?

class MailerController extends BaseController
{

  protected $mailer;
  static $left_menu;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Mailer::$table;
    $this->mailer = new Mailer($this->sets);
  }

  public function indexAction()
  {
    if (isset($this->params[$this->tb], $this->params['code']) && $this->params[$this->tb] == 'unsubscribe' && $this->params['code'] != '') {
      $vars['message'] = $this->mailer->unsubscribe($this->params['code']);
    }
    $data['content'] = $this->view->Render('message.phtml', $vars);
    return $this->Index($data);
  }

  // Subscribers
  function subscribersAction()
  {
    return json_encode($this->mailer->subscriber(($_POST['email'] ? $_POST['email'] : 'no-email')));
  }

  // Add email
  function mailtoAction()
  {
    if (isset($_POST['email'])) {
      $message = Validate::check($_POST['email'], $this->translation, 'email');
      if ($this->db->row("SELECT `id` FROM `email` WHERE `email`=?", array($_POST['email'])))
        $message = "<div class='err'>" . $this->translation['email_exists'] . "</div>";
      if ($message == "") {
        $query = "INSERT INTO `email` SET `email`=?, `date`=?";
        $this->db->query($query, array($_POST['email'], date("Y-m-d H:i:s")));
        $message = "<div class='done'>" . $this->translation['email_added'] . "!</div>";
      }
      return $message;
    }
    return false;
  }
}
