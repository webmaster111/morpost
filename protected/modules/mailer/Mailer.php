<?

class Mailer extends Model
{
	static $table = 'mailer';
	static $name = 'Модуль рассылки';

	public function __construct($registry)
	{
		parent::getInstance($registry);
	}

	public static function getObject($registry)
	{
		return new self::$table($registry);
	}


	public static function SendToAdmin($subject, $letter): void
	{
		Mail::send(
			$subject,
			$letter . "<br/> ---------------------------------------" .
			"<br/>REFERER : " . LINK . $_SERVER['HTTP_REFERER'] .
			"<br/>Страница : " . LINK . $_SERVER['REQUEST_URI']
		);
	}

	public static function SendToUser($email, $name, $subject, $letter): void
	{
		Mail::send($subject, $letter, $email, $name);
	}

	public function add($queue)
	{
		$message = messageAdmin('При добавление произошли ошибки', 'error');
		if (isset($_POST['name'], $_POST['body']) && $_POST['name'] != "") {
			$reset = isset($_POST['reset']) ? 1 : 0;
			$param = array($_POST['active'], $reset);
			$insert_id = $this->db->insert_id(
				"INSERT INTO `" . self::$table . "` SET `active`=?,`reset_pass`=?",
				$param
			);
			foreach ($this->language as $lang) {
				$tb = $lang['language'] . "_" . self::$table;
				$param = [$_POST['name'], $_POST['body'], $insert_id];
				$this->db->query("INSERT INTO `$tb` SET `name`=?,`text`=?,`mailer_id`=?", $param);
			}
			$message = messageAdmin('Данные успешно добавлены');
		}

		return $message;
	}

	public function save()
	{
		$message = '';
		if (isset($this->registry['access'])) {
			$message = $this->registry['access'];
		} else {
			if (isset($_POST['save_id']) && is_array($_POST['save_id'])) {
				if (isset($_POST['save_id'], $_POST['name'])) {
					for ($i = 0; $i <= count($_POST['save_id']) - 1; $i++) {
						$param = array($_POST['name'][$i], $_POST['save_id'][$i]);
						$this->db->query(
							"UPDATE `" . $this->registry['key_lang_admin'] . "_" . self::$table . "` SET `name`=? WHERE mailer_id=?",
							$param
						);
					}
					$message .= messageAdmin('Данные успешно сохранены');
				} else {
					$message .= messageAdmin('При сохранение произошли ошибки', 'error');
				}
			} else {
				if (isset($_POST['active'], $_POST['id'], $_POST['name'], $_POST['body'])) {
					$reset = isset($_POST['reset']) ? 1 : 0;
					$param = [$_POST['active'], $reset, $_POST['id']];
					$this->db->query("UPDATE `" . self::$table . "` SET `active`=?,`reset_pass`=? WHERE id=?", $param);
					$param = [$_POST['name'], $_POST['body'], $_POST['id']];
					$this->db->query(
						"UPDATE `" . $this->registry['key_lang_admin'] . "_" . self::$table . "` SET `name`=?,`text`=? WHERE `mailer_id`=?",
						$param
					);
					$vars['users'] = $this->db->rows("SELECT `mailer`,`email`,`id` FROM `users`");
					$message .= messageAdmin('Данные успешно сохранены');
				} else {
					$message .= messageAdmin('При сохранение произошли ошибки', 'error');
				}
			}
		}
		return $message;
	}

	public function listView()
	{
		$vars['list'] = $this->db->rows(
			"SELECT tb.*,tb2.name,(SELECT COUNT(*) FROM `mail_queue` WHERE mailbody_id=tb.id) as total,(SELECT COUNT(*) FROM `mail_queue` WHERE mailbody_id=tb.id AND `delivered`='1') as sent FROM " . self::$table . " tb LEFT JOIN " . $this->registry['key_lang_admin'] . "_" . self::$table . " tb2 ON tb.id=tb2.mailer_id LEFT JOIN `mail_queue` tb3 ON tb.id=tb3.mailbody_id GROUP BY tb.id ORDER BY tb.`sort` ASC"
		);
		return $vars;
	}

	public function listUsersView($id)
	{
		$vars['list'] = $this->db->rows(
			"SELECT tb.*,tb2.email,tb2.name FROM `mail_queue` tb LEFT JOIN `users` tb2 ON tb.`user_id`=tb2.`id` WHERE tb.`mailbody_id`='" . $id . "'"
		);
		return $vars['list'];
	}

	/**
	 * @description Подписка на рассылку
	 * @param string $email - емейл подписчика
	 * @param string $name_user - имя подписчика
	 * @param bool   $mail_user - отправлять ли подписчику емейл с уведомлением
	 * @return array $data
	 */
	public function subscriber(string $email, string $name_user = 'Не указан', bool $mail_user = true): array
	{
		$data = [
			'status' => 'error',
			'message ' => Validate::check($email, $this->translation, 'email')
		];
		$row = $this->db->row('SELECT `id`,active FROM `subscribers` WHERE `email`=?', [$email]);
		if (isset($row['active']) && $row['active'] = 1) {
			$data['message'] = $this->translation['email_exists'];
		}
		if (empty($data['message'])) {
			$code = md5(mktime()); //уникальный хеш подписчика
			if (isset($row['active'])) {
				$this->db->query('UPDATE `subscribers` SET active=1, code=? WHERE email=?', [$code, $email]);
			} else {
				$this->db->query(
					"INSERT INTO `subscribers` SET name=?,email=?,date_add=?,active='1',code=?",
					[$name_user, $email, date('Y-m-d H:i:s'),$code ]
				);
			}
			if ($mail_user) {
				$text = 'Вы успешно подписались на рассылку на сайта ' . $_SERVER['HTTP_HOST'] . "
						<div style='margin:35px 0;'>
							<a href='http://" . $_SERVER['HTTP_HOST'] . "/mailer/unsubscribe/code/$code' style='font-size:11px;'>
								Отписаться
							</a>
						</div>";
				self::SendToUser($email, $name_user, 'Подписка на рассылку на сайте ', $text);
			}
			$data['status'] = 'success';
			$data['message'] = $this->translation['email_added'];
		}
		return $data;
	}

	public function unsubscribe($code)
	{
		$err = '';
		$row = $this->db->row("SELECT `id` FROM `subscribers` WHERE `code`=?", [$code]);
		if (!$row) {
			$err .= $this->translation['email_exists2'];
		}
		if ($err == "") {
			$this->db->query("UPDATE `subscribers` SET active='0' WHERE code=?", [$code]);
			$message = '<div class="done">' . $this->translation['email_deleted'] . '</div>';
		} else {
			$message = '<div class="err">' . $err . '</div>';
		}
		return $message;
	}

	public function add_subscribers()
	{
		$this->db->query("INSERT INTO `subscribers` SET active='0'");
		return messageAdmin('Данные успешно сохранены');
	}

	public function save_subscribers()
	{
		if (isset($_POST['save_id'], $_POST['email'], $_POST['name']) && is_array($_POST['save_id'])) {
			$count = count($_POST['save_id']) - 1;
			for ($i = 0; $i <= $count; $i++) {
				$this->db->query(
					"UPDATE `subscribers` SET name=?,email=?, `code`=? WHERE `id`=?",
					array(
						$_POST['name'][$i],
						$_POST['email'][$i],
						md5(date('Y.m.d') + $_POST['save_id'][$i]),
						$_POST['save_id'][$i]
					)
				);
			}
			return messageAdmin('Записи успешно удалены');
		}
	}
}
