<?

class News extends Model
{
    static $table = 'news';
    static $name = 'Новости';

    public function __construct($registry)
    {
        parent::getInstance($registry);
    }

    public static function getObject($registry)
    {
        return new self::$table($registry);
    }

    public function add()
    {
        $message =  messageAdmin('Заполнены не все обязательные поля', 'error');
        if (isset($_POST['active'], $_POST['date'], $_POST['url'], $_POST['name'], $_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body'], $_POST['body_m']) && $_POST['name'] != "") {
            if ($_POST['date'] == '0000-00-00 00:00:00') $_POST['date'] = date("Y-m-d H:i:s");
            $insert_id = $this->db->insert_id("INSERT INTO `" . self::$table . "` SET `date_add`=?,`active`=?", [$_POST['date'], $_POST['active']]);
            foreach ($this->db->rows("SELECT * FROM language") as $lang)
                $this->db->query("INSERT INTO `" . $lang['language'] . "_" . self::$table . "` SET `name`=?,`body`=?,`body_m`=?,`" . self::$table . "_id`=?", [$_POST['name'], $_POST['body'], $_POST['body_m'], $insert_id]);
            if ($_POST['url'] == '') $url = StringLibrary::translit($_POST['name']);
            else $url = StringLibrary::translit($_POST['url']);
            // Save meta data
            $meta = new Meta($this->sets);
            $meta->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);
            $this->checkUrl(self::$table, $url, $insert_id);
            // Photo
            $this->savePhoto($insert_id, $_POST['tmp_image'], self::$table);
            $message = messageAdmin('Данные успешно добавлены');
        }
        return $message;
    }

    public function save()
    {
        $message = '';
        if (isset($this->registry['access'])) $message = $this->registry['access'];
        else {
            if (isset($_POST['save_id']) && is_array($_POST['save_id'])) {
                if (isset($_POST['save_id'], $_POST['name'])) {
                    for ($i = 0; $i <= count($_POST['save_id']) - 1; $i++) {
                        if ($_POST['date'][$i] == '0000-00-00 00:00:00') $_POST['date'][$i] = date("Y-m-d H:i:s");
                        $this->db->query("UPDATE `" . self::$table . "` SET `date_add`=? WHERE id=?", [$_POST['date'][$i], $_POST['save_id'][$i]]);
                        $this->db->query("UPDATE `" . $this->registry['key_lang_admin'] . "_" . self::$table . "` SET `name`=? WHERE " . self::$table . "_id=?", [$_POST['name'][$i], $_POST['save_id'][$i]]);
                    }
                    $message .= messageAdmin('Данные успешно сохранены');
                } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
            } else {
                if (isset($_POST['active'], $_POST['url'], $_POST['id'], $_POST['name'], $_POST['body_m'], $_POST['body'])) {
                    if ($_POST['url'] == '') $url = StringLibrary::translit($_POST['name']);
                    else $url = StringLibrary::translit($_POST['url']);
                    // Save meta data
                    $meta = new Meta($this->sets);
                    $meta->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);
                    if ($_POST['date'] == '0000-00-00 00:00:00') $date = date("Y-m-d H:i:s");
                    else $date = $_POST['date'];
                    $this->checkUrl(self::$table, $url, $_POST['id']);
                    $this->db->query("UPDATE `" . self::$table . "` SET `active`=?,`date_add`=? WHERE id=?", [$_POST['active'], $date, $_POST['id']]);
                    $this->db->query("UPDATE `" . $this->registry['key_lang_admin'] . "_" . self::$table . "` SET `name`=?,`body_m`=?,`body`=? WHERE `" . self::$table . "_id`=?", [$_POST['name'], $_POST['body_m'], $_POST['body'], $_POST['id']]);
                    $message .= messageAdmin('Данные успешно сохранены');
                } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
            }
        }
        return $message;
    }

    public function getArchive()
    {
        $r = [];
        foreach (self::find(['select' => 'date_add', 'order' => 'date_add desc', 'type' => 'rows', 'where' => 'active="1"']) as $d) $r = array_merge($r, [Date::date_view($d['date_add'], 'YY-mm') => Date::date_view($d['date_add'], 'MM YY')]);
        return array_unique($r);
    }

    public function getLast($limit, $where = "tb.active='1'")
    {
        return self::find(['where' => $where, 'limit' => $limit, 'order' => 'date_add desc', 'type' => 'rows']);
    }

    public function getOne($url)
    {
        return self::find(['where' => 'url="' . $url . '"', 'select' => 'id,url,photo,date_add,name,body']);
    }

    public function getOther($id, $limit)
    {
        return self::find(['where' => 'not id="' . $id . '" and active="1"', 'select' => 'url,photo,date_add,name,body_m', 'type' => 'rows', 'limit' => $limit]);
    }

    public function getByDate($date)
    {
        return self::find(['where' => 'active="1" and date_add like "' . $date . '-%"', 'select' => 'url,photo,date_add,name,body_m', 'type' => 'rows']);
    }
}