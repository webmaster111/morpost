<?

class NewsController extends BaseController
{
    protected $news;

    function __construct($registry, $params)
    {
        parent::__construct($registry, $params);
        $this->tb = News::$table;
        $this->news = new News($this->sets);
    }

    public function indexAction()
    {
        $template = 'list';
        if (!isset($this->params[$this->tb]) || $this->params[$this->tb] == 'all') {
            $count = $this->settings['paging_news'];
            $vars['news'] = $this->news->find(array('type' => 'rows', 'where' => "tb.active = '1'", 'limit' => $count, 'order' => 'tb.date_add DESC'));
            if (count($vars['news']) < $count) $vars['no-load'] = true;
            $data['breadcrumbs'] = [['url' => '#', 'name' => $this->translation[$this->tb]]];
        } else {
            $template = 'item';
            $vars['news'] = $this->news->getOne($this->params[$this->tb]);
            if (!isset($vars['news']['id'])) return Router::act('error', $this->registry);
            $data['meta'] = $vars['news'];
            $vars['list'] = $this->news->getOther($vars['news']['id'], $this->settings['limit_news_block']);
            $vars['next'] = $this->getNextPost($vars['news']['id']);
            $data['breadcrumbs'] = [
                ['name' => $this->translation[$this->tb], 'url' => $this->tb . '/all'],
                ['name' => $vars['news']['name'], 'url' => 'news/' . $vars['news']['url']]
            ];
        }
        $menu_catalogs = getTree(containArrayInHisId($this->catalog->getAll(" AND tb.active='1'")));
        $vars['menu_catalogs'] = $this->view->Render('layout/menu.phtml', array('catalogs' => $menu_catalogs));
        if (isset($data['breadcrumbs'])) $vars['breadcrumbs'] = $this->model->breadcrumbs($data['breadcrumbs'], $this->view);
        $data['content'] = $this->view->Render($this->tb . '/' . $template . '.phtml', $vars);
        return $this->Index($data);
    }

    function getNextPost($id)
    {
        $return_next = false;
        $_all = $this->news->find(array('type' => 'rows', 'where' => "tb.active = '1'", 'order' => 'tb.date_add DESC'));
        foreach ($_all as $key => $item) {
            if ($return_next) {
                $item['header'] = $this->translation['next_news'];
                $item['url'] = 'news/' . $item['url'];
                return $item;
            }
            if ($item['id'] == $id) $return_next = true;
        }
        return array('name' => $this->translation[$this->tb], 'url' => 'news/all', 'header' => $this->translation['all_news'],);
    }

    public function getMoreAction()
    {
        if (isset($_POST['offset'])) {
            $offset = $_POST['offset'];
            $on_page = $this->settings['paging_news'];
            $result = array('offset' => $offset + 1);
            $all = $this->news->count("active='1'");
            $news = $this->news->find(array('type' => 'rows',
                'where' => "tb.active = '1'",
                'limit' => $on_page . ' OFFSET ' . $offset * $on_page,
                'order' => 'tb.date_add DESC'));
            $result['thumbs'] = $this->view->Render('news/thumb.phtml', array('news' => $news));
            if (($all - (count($news) + $offset * $on_page)) <= 0) $result['offset'] = 0;
            return json_encode($result);
        }
        return false;
    }
}