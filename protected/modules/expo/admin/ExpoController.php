<?

class ExpoController extends BaseController
{
  protected $params;
  protected $db;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = "expo";
    $this->name = "Фото выставок и ссылки на них";
    $this->registry = $registry;
    $this->expo = new Expo($this->sets);
  }

  public function indexAction()
  {
    $vars['message'] = '';
    $vars['name'] = $this->name;
    if (isset($this->params['subsystem'])) return $this->Index($this->expo->subsystemAction());
    if (isset($this->registry['access'])) $vars['message'] = $this->registry['access'];
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->expo->delete($this->tb);
    elseif (isset($_POST['update'])) $vars['message'] = $this->expo->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->expo->save();
    elseif (isset($_POST['add_close'])) $vars['message'] = $this->expo->add();
    $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->expo->find(['type' => 'rows', 'order' => 'tb.sort'])]);
    $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name]);
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = '';
    $vars['width'] = $this->settings['expo_width'];
    $vars['height'] = $this->settings['expo_height'];
    if (isset($_POST['add'])) $vars['message'] = $this->expo->add();
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = '';
    $vars['width'] = $this->settings['expo_width'];
    $vars['height'] = $this->settings['expo_height'];
    if (isset($_POST['update'])) $vars['message'] = $this->expo->save();
    $vars['edit'] = $this->expo->find((int)$this->params['edit']);
    $dir = Dir::createDir($this->params['edit'],'', $this->tb);
    $vars['path'] = $dir[0];
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }
}