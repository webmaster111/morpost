<?

class Expo extends Model
{
  static $table = 'expo';
  static $name = 'Выставки';

  public function __construct($registry)
  {
    parent::getInstance($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  public function add()
  {
    $message = '';
    if (isset($_POST['active'], $_POST['url'])) {
      $sort = $this->db->cell("SELECT MAX(sort)+1 FROM " . self::$table . "`");
      $sort = $sort ? $sort : 1;
      $id = $this->db->insert_id("INSERT INTO `" . self::$table . "` SET active=?,`url`=?,`sort`=?", array($_POST['active'], $_POST['url'],$sort));
      $this->savePhoto($id, $_POST['tmp_image'], self::$table);
      $message .= messageAdmin('Данные успешно добавлены');
    } else $message .= messageAdmin('Заполнены не все обязательные поля', 'error');
    return $message;
  }

  public function save()
  {
    $message = '';
    if (isset($this->registry['access'])) return $this->registry['access'];
    if (!is_array($_POST['save_id']) && isset($_POST['active'], $_POST['id'])) {
      $this->db->query("UPDATE `" . self::$table . "` SET `active`=?,`url`=? WHERE id=?", [$_POST['active'], $_POST['url'], $_POST['id']]);
      $message .= messageAdmin('Данные успешно сохранены');
    } else if (isset($_POST['save_id']) && is_array($_POST['save_id'])) {
      if (isset($_POST['save_id'], $_POST['sort'])) {
        for ($i = 0; $i <= count($_POST['save_id']) - 1; $i++)
          $this->db->query("UPDATE `" . self::$table . "` SET `sort`=? WHERE id=?", [$_POST['sort'][$i], $_POST['save_id'][$i]]);
        $message .= messageAdmin('Данные успешно сохранены');
      } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
    } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
    return $message;
  }

}