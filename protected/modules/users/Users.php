<?

class Users extends Model
{
	static $table = 'users';
	static $name = 'Пользователи';

	public function __construct($registry)
	{
		parent::getInstance($registry);
	}

	public static function getObject($registry)
	{
		return new self::$table($registry);
	}

	public function add($open = false)
	{
		$message = messageAdmin('При добавление произошли ошибки', 'error');
		if (isset($_POST['active'], $_POST['email'], $_POST['password'], $_POST['password2'])) {
			$error = Validate::checkPass($_POST['password'], $_POST['password2']);
			if ($this->getByEmail($_POST['email'])) {
				$error = "Данный E-mail уже зарегистрирован!";
			}
			if ($error == "") {
				$id = $this->insert(
					array(
						'name' => $_POST['name'],
						'surname' => $_POST['surname'],
						'patronymic' => $_POST['patronymic'],
						'email' => $_POST['email'],
						'pass' => md5($_POST['password']),
						'discount' => $_POST['discount'],
						'phone' => $_POST['phone'],
						'skype' => $_POST['skype'],
						'city' => $_POST['city'],
						'address' => $_POST['address'],
						'post_index' => $_POST['post_index'],
						'start_date' => date("Y-m-d H:i:s"),
						'info' => $_POST['info'],
						'status_id' => $_POST['status_id'],
						'active' => 1
					)
				);
				$this->savePhoto($id, $_POST['tmp_image'], self::$table);
				$message = messageAdmin('Данные успешно добавлены');
				if ($open) {
					header('Location: /admin/' . self::$table . '/edit/' . $id);
					exit();
				}
			} else {
				$message = messageAdmin($error, 'error');
			}
		}
		return $message;
	}

	public function save()
	{
		$message = '';
		if (isset($this->registry['access'])) {
			$message = $this->registry['access'];
		} else {
			if (isset($_POST['active'], $_POST['email'], $_POST['password'], $_POST['password2'])) {
				$err = '';
				foreach ($_POST as $name => $value) {
					$_POST[$name] = trim($value);
				}

				if ($this->getByEmail($_POST['email'], $_POST['id'])) {
					$message .= messageAdmin(
						'<div class="alert alert-danger">' . $this->translation['email_exists'] . '</div>',
						'error'
					);
				} elseif ($this->getByPhone($_POST['phone'], $_POST['id'])) {
					$message .= messageAdmin(
						'<div class="alert alert-danger">' . $this->translation['phone_exists'] . '</div>',
						'error'
					);
				} else {
					if ($_POST['password'] != "" && $_POST['password2'] != "") {
						$err = Validate::checkPass($_POST['password'], $_POST['password2']);
					}
					if ($err == "") {
						$pass = [];
						if ($_POST['password'] != "" && $_POST['password2'] != "") {
							$pass = ['pass' => md5($_POST['password'])];
							$prot = getProtocol();
							// Отправка письма об изменении пароля пользователю

							$text = 'Ваш новый пароль: ' . $_POST['password'] . "<br>Вы можете сменить этот пароль в Вашем кабинете.<br>Для входа в кабинет перейдите по <a href='" . $prot . '://' . $_SERVER['HTTP_HOST'] . "/users/cabinet'>этой ссылке</a>";
							Mail::send(
								'Измение пароля на сайте ' . $this->settings['sitename'],
								$text,
								$_POST['email'],
								$_POST['name'] . ' ' . $_POST['surname']
							);
							$message .= messageAdmin('Письмо с новым паролем отправлено пользователю!');
						}


						$row = $this->find((int)$_POST['id']);
						if ($_POST['status_id'] === '2' && $row['status_id'] !== $_POST['status_id'] && $_POST['notice_user'] === 'on') {
							$letter = $this->getBlock(18);
							$letter['body'] = str_replace(
								'{{content}}',
								$this->translation['change_status_user'],
								htmlspecialchars_decode($letter['body'])
							);

							Mail::send(
								'Вы активированы как оптовик',
								htmlspecialchars_decode($letter['body']),
								$_POST['email'],
								$_POST['name']
							);
						}

						if (!isset($_POST['mailer'])) {
							$_POST['mailer'] = 0;
						}

						$this->update(
							array_merge(
								[
									'name' => $_POST['name'],
									'surname' => $_POST['surname'],
									'patronymic' => $_POST['patronymic'],
									'email' => $_POST['email'],
									'phone' => $_POST['phone'],
									'skype' => $_POST['skype'],
									'city' => $_POST['city'],
									'code_discount' => $_POST['code_discount'],
									'mailer' => $_POST['mailer'],
									'address' => $_POST['address'],
									'post_index' => $_POST['post_index'],
									'info' => $_POST['info'],
									'discount' => $_POST['discount'],
									'active' => $_POST['active'],
									'status_id' => $_POST['status_id'],
									'delivery_id' => $_POST['delivery_id'],
									'payment_id' => $_POST['payment_id']
								],
								$pass
							),
							[
								['id', '=', $_POST['id']]
							]
						);

						$dir = "files/users/";
						if (isset($_FILES['photo']['tmp_name']) && $_FILES['photo']['tmp_name'] != "") {
							Images::resizeImage(
								$_FILES['photo']['tmp_name'],
								$dir . $_POST['id'] . ".jpg",
								$dir . $_POST['id'] . "_s.jpg",
								214,
								159
							);
						}
						$message .= messageAdmin('Данные успешно сохранены');
					} else {
						$message .= messageAdmin($err, 'error');
					}
				}
			} else {
				$message .= messageAdmin('Заполнены не все обязательные поля', 'error');
			}
		}
		return $message;
	}

	public function updatePassword($password, $password_repeat, $user_id)
	{
		$message = messageAdmin('Неизвестный пользователь', 'error');
		$user = $this->find($user_id);
		if ($user) {
			$message = Validate::checkPass($password, $password_repeat);
			if (empty($message) && $this->update(array('pass' => md5($password)), [['id', '=', $user_id]])) {
				$message = $this->translation['save_data'];
				Mailer::SendToUser(
					$user['email'],
					$user['name'],
					"Измение пароля на сайте " . $this->settings['sitename'],
					"Ваш новый пароль: " . $_POST['password'] . "<br>
                   Вы можете сменить этот пароль в Вашем кабинете.<br>
                   Для входа в кабинет перейдите по <a href='" . PROTOCOL . "://" . $_SERVER['HTTP_HOST'] . "/users/cabinet'>этой ссылке</a>"
				);
			}
		}
		return $message;
	}

	public function listView()
	{
		$size_page = 30;
		$start_page = 0;
		$cur_page = 0;
		$vars['paging'] = '';
		if (isset($this->params['page'])) {
			$cur_page = $this->params['page'];
			$start_page = ($cur_page - 1) * $size_page;//номер начального элемента
		}
		if (isset($_POST['word'])) {
			$_SESSION['search_user']['status'] = $_POST['status'];
			$_SESSION['search_user']['sort'] = $_POST['sort'];
			$_SESSION['search_user']['word'] = trim($_POST['word']);
		}
		$where = "";
		if (!isset($_SESSION['search_user']) || isset($_POST['clear'])) {
			$_SESSION['search_user']['status'] = '';
			$_SESSION['search_user']['cat_id'] = '';
			$_SESSION['search_user']['word'] = '';
			$_SESSION['search_user']['sort'] = 'ORDER BY tb.`start_date` DESC';
		}
		if ($_SESSION['search_user']['status'] != '') {
			$where .= " AND tb.status_id='{$_SESSION['search_user']['status']}'";
		}
		/*if($_SESSION['search_user']['rating']!='')$where.=" AND tb.rating='{$_SESSION['search_user']['rating']}'";*/
		if ($_SESSION['search_user']['word'] != "") {
			$where .= "AND (tb.surname LIKE '%{$_SESSION['search_user']['word']}%' OR tb.name LIKE '%{$_SESSION['search_user']['word']}%' OR tb.email LIKE '%{$_SESSION['search_user']['word']}%' OR tb.phone LIKE '%{$_SESSION['search_user']['word']}%')";
		}
		$sort = 'tb.`start_date` DESC';
		if (isset($_SESSION['search_user']['sort']) && $_SESSION['search_user']['sort'] != '') {
			if ($_SESSION['search_user']['sort'] == 'name asc') {
				$sort = "tb.name ASC,tb.id DESC";
			} elseif ($_SESSION['search_user']['sort'] == 'name desc') {
				$sort = "tb.name DESC,tb.id DESC";
			} elseif ($_SESSION['search_user']['sort'] == 'email asc') {
				$sort = "tb.email ASC,tb.id DESC";
			} elseif ($_SESSION['search_user']['sort'] == 'email desc') {
				$sort = "tb.email DESC,tb.id DESC";
			} elseif ($_SESSION['search_user']['sort'] == 'date asc') {
				$sort = "tb.start_date ASC,tb.id DESC";
			} elseif ($_SESSION['search_user']['sort'] == 'price asc') {
				$sort = "total ASC,tb.id DESC";
			} elseif ($_SESSION['search_user']['sort'] == 'price desc') {
				$sort = "total DESC,tb.id DESC";
			}
		}
		$join = "";
		if ($_SESSION['search_user']['cat_id'] != '') {
			$where .= " AND cat.catalog_id='{$_SESSION['search_user']['cat_id']}'";
			$join = "LEFT JOIN orders_product orders_p ON orders_p.orders_id=orders.id LEFT JOIN product_catalog cat ON cat.product_id=orders_p.product_id";
		}
		$vars['list'] = $this->find(
			[
				'paging' => true,
				'where' => "tb.id!='0' $where",
				'group' => 'tb.id',
				'order' => $sort,
				'select' => 'tb.*,tb2.name as status',
				'join' => 'LEFT JOIN user_status tb2 ON tb.status_id=tb2.id LEFT JOIN `orders` ON orders.user_id=tb.id' . $join
			]
		);
		return $vars;
	}

	public function addusertype()
	{
		$message = '';
		if (isset($this->registry['access'])) {
			$message = $this->registry['access'];
		} else {
			$this->db->query(
				"INSERT INTO `user_status` SET `name`='New user status',price_type_id='" . Product::getObject(
					$this->sets
				)->default_price_type() . "'"
			);
			$message .= messageAdmin('Данные успешно сохранены');
		}
		return $message;
	}

	public function save_usertype()
	{
		$message = '';
		if (isset($this->registry['access'])) {
			$message = $this->registry['access'];
		} else {
			if (isset($_POST['name'], $_POST['id'])) {
				$count = count($_POST['id']) - 1;
				if (!isset($_POST['default'])) {
					$_POST['default'] = 1;
				}
				for ($i = 0; $i <= $count; $i++) {
					$default = 0;
					if ($_POST['default'] == $_POST['id'][$i]) {
						$default = 1;
					}
					$param = [$_POST['name'][$i], $_POST['price_type'][$i], $default, $_POST['id'][$i]];
					$this->db->query(
						"UPDATE `user_status` SET `name`=?,`price_type_id`=?,`default`=? WHERE id=?",
						$param
					);
				}
				$message .= messageAdmin('Данные успешно сохранены');
			} else {
				$message .= messageAdmin('Заполнены не все обязательные поля', 'error');
			}
		}
		return $message;
	}

	public function signUp()
	{
		$result = array('message' => 'no-data', 'status' => false);
		if (isset($_POST['email'], $_POST['phone'], $_POST['surname'], $_POST['name'], $_POST['password'])) {
			$secretkey = $this->settings["recaptcha_secretkey"];
			$message = !GoogleCaptcha::check($secretkey) ? "<div class='alert alert-danger'>" . $this->translation['wrong-recaptcha'] . "</div>" : "";
			$message .= Validate::check($_POST['email'], $this->translation, 'email');
			$_POST['phone'] = html_entity_decode($_POST['phone']);
			$message .= Validate::check($_POST['phone'], $this->translation, 'phone');
			$message .= Validate::checkPass($_POST['password'], $_POST['password_repeat']);

			if ($this->db->row("SELECT id FROM users WHERE email='{$_POST['email']}'")) {
				$message .= "<div class='alert alert-danger'>" . $this->translation['email_exists'] . "</div>";
			}
			if ($message === '') {
				$result['user_id'] = $this->insert(
					array(
						'active' => 1,
						'name' => $_POST['name'],
						'surname' => $_POST['surname'],
						'email' => $_POST['email'],
						'phone' => $_POST['phone'],
						'pass' => md5($_POST['password']),
						'status_id' => 1
					)
				);
				Mailer::SendToAdmin('Новый пользователь на сайте', $this->emailText('admin', $_POST));
				Mailer::SendToUser(
					$_POST['email'],
					$_POST['surname'] . ' ' . $_POST['name'],
					'Вы зарегистрировались на сайте ',
					$this->emailText('user', $_POST)
				);
				Mailer::getObject($this->sets)->subscriber(
					$_POST['email'],
					$_POST['surname'] . ' ' . $_POST['name'],
					false
				);
				$result['status'] = true;
				$letter = $this->getBlock(11);
				$message = '<div>' . htmlspecialchars_decode($letter['body']) . '</div>';
				unset($_POST);
			}
			$result['message'] = $message;
		}
		return $result;
	}

	private function emailText($userType, $data)
	{
		$text = '';

		switch ($userType) {
			case 'user':
				$text = $this->makeEmailTextUser($data);
				break;

			case 'admin':
				$text = $this->makeEmailTextAdmin($data);
				break;
		}

		return $text;
	}

	private function makeEmailTextAdmin($data)
	{
		$text = "<br>Дата: " . date("d/m/Y H:i") . "
               <br>ФИО: {$data['surname']} {$data['name']}
               <br>Телефон: {$data['phone']}
               <br>E-mail: {$data['email']}";

		return htmlspecialchars_decode($text);
	}

	private function makeEmailTextUser($data)
	{
		$text = $this->getBlock(19)['body'];

		$text = str_replace('{{date}}', date('d-m-Y'), $text);
		foreach ($data as $key => $item) {
			$text = str_replace('{{' . $key . '}}', $data[$key], $text);
		}

		return htmlspecialchars_decode($text);
	}

	public function auth($email, $pass, $md5_hash = true)
	{
		$response = array('status' => 'error');
		$message = Validate::check($email, $this->translation, 'email');
		$message .= Validate::check($pass, $this->translation);
		if ($md5_hash) {
			$pass = md5($pass);
		}

		$user = $this->db->row("SELECT * FROM users WHERE email=? AND pass=?", [$email, $pass]);
		if (!$user) {
			$message .= "<div class='alert alert-danger'>" . $this->translation['wrong_pass_or_email'] . "</div>";
		} else {
			if (!$user['active']) {
				$message .= "<div class='alert alert-danger'>" . $this->translation['no_active'] . "</div>";
			}
		}

		if ($message == "") {
			Users::setSessionUserInfo($user);
			$_SESSION['user_id'] = $user['id'];
			$_SESSION['discount'] = $user['discount'];

			$this->recalcPrice($user['id']);
			$response['status'] = 'success';
			$message = "<strong>" . $this->translation['auth_yes'] . "</strong>";
		}
		$response['message'] = $message;
		return $response;
	}

	public function logout()
	{
		$this->recalcPrice2();
		unset($_SESSION['user_id'], $_SESSION['price_type_id'], $_SESSION['user_info']);
		header("Location: /users/login");
	}

	public function active($id, $tb, $tb2)
	{
		$row = $this->db->row("SELECT id,email,pass FROM users WHERE active_email=?", [$this->params['code']]);
		if ($row) {
			$code = md5(mktime());
			$this->db->query("UPDATE users SET active=?,active_email=? WHERE id=?", [1, $code, $row['id']]);
			$this->auth($row['email'], $row['pass'], false);
			$response = [
				'status' => true,
				'message' => "<div class='alert alert-success text-center'>" . $this->translation['you_active'] . "</div>"
			];
		} else {
			$response = [
				'status' => false,
				'message' => "<div class='alert alert-danger text-center'>" . $this->translation['wrong_active'] . "</div>"
			];
		}
		return $response;
	}

	function recalcPrice($id)
	{
		$row7 = $this->db->row(
			"SELECT us.price_type_id,pt.default FROM users LEFT JOIN user_status us ON us.id=users.status_id LEFT JOIN price_type pt ON pt.id=us.price_type_id WHERE users.id=?",
			[$id]
		);
		if ($row7['default'] != 1) {
			$res = $this->db->rows("SELECT * FROM bascket WHERE session_id='" . session_id() . "'");
			foreach ($res as $row) {
				$row2 = $this->db->row(
					"SELECT id,price FROM price WHERE product_id='{$row['product_id']}' AND price_type_id='{$row7['price_type_id']}'"
				);
				$this->db->query(
					"UPDATE bascket SET user_id='$id',price='{$row2['price']}',price_id='{$row2['id']}' WHERE id='{$row['id']}'"
				);
			}
		}
	}

	function recalcPrice2()
	{
		if ($_SESSION['price_type_id'] == 2) {
			$this->db->query("DELETE FROM bascket WHERE session_id='" . session_id() . "'");
		} else {
			$res = $this->db->rows("SELECT * FROM bascket WHERE session_id='" . session_id() . "'");
			foreach ($res as $row) {
				$row2 = $this->db->row(
					"SELECT price.* FROM price LEFT JOIN price_type pt ON pt.id=price.price_type_id WHERE product_id='{$row['product_id']}' AND pt.default='1'"
				);
				if ($row2) {
					$this->db->query(
						"UPDATE bascket SET price='{$row2['price']}',price_id='{$row2['id']}' WHERE id='{$row['id']}'"
					);
				}
			}
		}
	}

	/*
	 * Find by Email
	 * $id -> if you need to exclude some user
	 */
	public function getByEmail($email, $id = null)
	{
		$query = ['where' => 'email="' . $email . '"'];
		if ($id) {
			$query['where'] .= ' AND id <> "' . $id . '"';
		}
		return self::find($query);
	}

	/*
	 * Find by Phone
	 * $id -> if you need to exclude some user
	 */
	public function getByPhone($phone, $id = null)
	{
		$query = ['where' => 'phone="' . $phone . '"'];
		if ($id) {
			$query['where'] .= ' AND id <> "' . $id . '"';
		}
		return self::find($query);
	}

	public static function setSessionUserInfo($user)
	{
		$_SESSION['user_info'] = array(
			'agent' => $_SERVER['HTTP_USER_AGENT'],
			'referer' => $_SERVER['HTTP_REFERER'],
			'ip' => $_SERVER['REMOTE_ADDR'],
			'id' => $user['id'],
			'status' => $user['status_id'],
			'email' => $user['email'],
			'name' => $user['name'],
			'sname' => $user['surname'],
			'phone' => $user['phone'],
			'address' => $user['address'],
			'delivery_id' => $user['delivery_id'],
			'payment_id' => $user['payment_id']
		);
	}
}