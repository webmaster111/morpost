<?

class UsersController extends BaseController
{
  protected $params;
  protected $db;
  private $left_menu = [['title' => 'Статусы пользователей', 'url' => '/admin/users/act/usertype', 'name' => 'usertype'], ['title' => 'Экспорт данных', 'url' => '/admin/users/act/export', 'name' => 'export']];

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = "users";
    $this->name = "Покупатели";
    $this->registry = $registry;
    $this->users = new Users($this->sets);
    $this->orders = new Orders($this->sets);
  }

  public function indexAction()
  {
    $vars['message'] = '';
    $vars['name'] = $this->name;
    if (isset($this->params['act'])) {
      $act = $this->params['act'] . 'Action';
      return $this->Index($this->$act());
    }
    if (isset($this->params['subsystem'])) return $this->Index($this->users->subsystemAction($this->left_menu));
    if (isset($this->registry['access'])) $vars['message'] = $this->registry['access'];
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->users->delete();
    elseif (isset($_POST['update'])) $vars['message'] = $this->users->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->users->save();
    elseif (isset($_POST['add_open'])) $vars['message'] = $this->users->add(true);
    $vars['list'] = $this->view->Render('view.phtml', $this->users->listView());
    $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'menu2' => $this->left_menu));
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = '';
    if (isset($_POST['add'])) $vars['message'] = $this->users->add();
    $vars['price_type'] = $this->db->row("SELECT id FROM user_status WHERE `default`='1'");
    $vars['status'] = $this->db->rows("SELECT * FROM user_status ORDER BY id ASC");
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    if (isset($this->params['del']) || isset($_POST['del'])) {
      $table = 'bascket';
      if (isset($_POST['del']) && is_array($_POST['bascket_id'])) {
        $count = count($_POST['bascket_id']) - 1;
        for ($i = 0; $i <= $count; $i++) $this->db->query("DELETE FROM `" . $table . "` WHERE `id`=?", [$_POST['bascket_id'][$i]]);
        $message = messageAdmin('Запись успешно удалена');
      } elseif (isset($this->params['del']) && $this->params['del'] != '') {
        $id = $this->params['del'];
        if ($this->db->query("DELETE FROM `" . $table . "` WHERE `id`=?", [$id])) $message = messageAdmin('Запись успешно удалена');
      }
    }
    if (isset($this->params['delorder']) || isset($_POST['del_orders'])) {
      $table = 'orders';
      if (isset($_POST['del_orders']) && is_array($_POST['order_id'])) {
        $count = count($_POST['order_id']) - 1;
        for ($i = 0; $i <= $count; $i++) $this->db->query("DELETE FROM `" . $table . "` WHERE `id`=?", [$_POST['order_id'][$i]]);
        $message = messageAdmin('Запись успешно удалена');
      } elseif (isset($this->params['delorder']) && $this->params['delorder'] != '') {
        $id = $this->params['delorder'];
        if ($this->db->query("DELETE FROM `" . $table . "` WHERE `id`=?", [$id])) $message = messageAdmin('Запись успешно удалена');
      }
    }
    $vars = $this->orders->incomplete($this->params['edit']);
    $data['styles'] = ['jquery.simple-dtpicker.css'];
    $data['scripts'] = ['jquery.simple-dtpicker.js'];
    $vars['incomplete'] = $this->view->Render('incomplete.phtml', $vars);
    $vars['message'] = '';
    if (isset($_POST['update'])) $vars['message'] = $this->users->save();
    $vars['currency'] = $this->db->row("SELECT icon FROM currency WHERE `base`='1'");
    $vars['list']['list'] = $this->orders->find(['type' => 'rows', 'where' => "tb.user_id='" . $this->params['edit'] . "'", 'order' => 'tb.date_add DESC', 'select' => 'tb.*,tb2.name as status', 'join' => 'LEFT JOIN orders_status tb2 ON tb.status_id=tb2.id']);
    $vars['orders'] = $this->view->Render('orders.phtml', $vars);
    $vars['edit'] = $this->users->find(array('where' => '__tb.id:=' . $this->params['edit'] . '__', 'select' => 'tb.*,tb2.name as status', 'join' => ' LEFT JOIN user_status tb2 ON tb.status_id=tb2.id'));
    $vars['status'] = $this->db->rows("SELECT * FROM user_status ORDER BY id ASC");
    $vars['sum'] = $this->db->row("
      SELECT 
        SUM(tb2.`sum`) as total,
        COUNT(*) as cnt 
      FROM `orders` 
      LEFT JOIN orders_product tb2 ON tb2.orders_id=orders.id 
      WHERE orders.user_id=? AND (orders.status_id='4' OR orders.status_id='5') 
      ORDER BY orders.id ",
        [$vars['edit']['id']]
    );
    #events
    $vars['events'] = '';
    $events = $this->db->rows("SELECT * FROM `user_events` WHERE `user_id`=? ORDER BY `date_add` DESC", [$vars['edit']['id']]);
    if ($events != false) {
      $vars['events'] .= '<ul>';
      foreach ($events as $row) $vars['events'] .= '<li><br>Дата: ' . $row['date_add'] . '<br>Событие: ' . $row['text'] . '<br></li>';
      $vars['events'] .= '</ul>';
    }

    $vars['delivery'] = Delivery::getObject($this->sets)->find([
        'type' => 'rows',
        'select' => 'tb.id, tb_lang.name'
    ]);

    $vars['payment'] = Payment::getObject($this->sets)->find([
        'type' => 'rows',
        'select' => 'tb.id, tb_lang.name'
    ]);

    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }

  function addeventAction()
  {
    if (isset($_POST['id'], $_POST['text']) && is_numeric($_POST['id']) && $_POST['text'] != '') {
      $date = date('Y-m-d H:i:s');
      $this->db->insert_id("INSERT INTO `user_events` SET user_id=?,date_add=?,text=?", [$_POST['id'], $date, $_POST['text']]);
    }
    $html = '';
    $events = $this->db->rows("SELECT * FROM `user_events` WHERE `user_id`=? ORDER BY `date_add` DESC", [$_POST['id']]);
    if ($events != false) {
      $html .= '<ul>';
      foreach ($events as $row) $html .= '<li><br>Дата: ' . $row['date_add'] . '<br>Событие: ' . $row['text'] . '<br></li>';
      $html .= '</ul>';
    }
    return $html;
  }

  public function usertypeAction()
  {
    $vars['message'] = '';
    if (isset($_POST['update'])) $vars['message'] = $this->users->save_usertype();
    elseif (isset($this->params['addusertype'])) $vars['message'] = $this->users->addusertype();
    elseif (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->users->delete('user_status');
    $vars['name'] = 'Статусы пользователей';
    $vars['action'] = $this->tb;
    $vars['path'] = '/act/usertype';
    $vars['price_type'] = $this->db->rows("SELECT * FROM `price_type` ORDER BY id ASC");
    $vars['list'] = $this->db->rows("SELECT * FROM `user_status` ORDER BY id ASC");
    $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'sub' => 'usertype', 'menu2' => $this->left_menu));
    $data['content'] = $this->view->Render('user_type.phtml', $vars);
    return $data;
  }

  public function exportAction()
  {
    if (isset($_POST['download'])) {
      $selectedColumns = implode(',', $_POST['columns']);
      $dataSet = $this->users->find(['select' => $selectedColumns, 'type' => 'rows']);
      $save = 'export' . strtoupper($_POST['file_format']);
      Export::$save($dataSet);
    }
    $vars['name'] = 'Экспорт - ' . $this->name;
    $vars['action'] = $this->tb;
    $vars['path'] = '/act/export';
    $vars['columns'] = $this->db->rows("SHOW COLUMNS FROM " . $this->tb);
    $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name, 'sub' => 'export', 'menu2' => $this->left_menu]);
    $data['content'] = $this->view->Render('export.phtml', $vars);
    return $data;
  }
}