<?

class UsersController extends BaseController
{

  protected $users, $orders;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Users::$table;
    $this->users = new Users($this->sets);
    $this->orders = new Orders($this->sets);
  }

  public function indexAction()
  {
    strip_post();
    $method = strtolower(str_replace('-', '_', $this->params['users'])) . 'Action';
    if (method_exists($this, $method)) {
      $result = $this->$method();
      if ($result !== false) {
        $result['meta']['name'] = $result['page']['name'];
        $result['content'] = $this->view->Render($this->tb . '/' . strtolower($this->params['users']) . '.phtml', $result);
        return $this->Index($result);
      }
    }
    return Router::act('error', $this->registry);
  }

  // user cabinet Page
  private function cabinetAction()
  {
    if (!isset($_SESSION['user_id'])) header("Location: /users/login");
    $data['user_info'] = $this->users->find((int)$_SESSION['user_id']);
    $data['page']['name'] = $this->translation['cabinet'];

    // update user info
    if (!empty($_POST['save_user_info'])) {
      $_POST['phone'] = html_entity_decode($_POST['phone']);
      $data['info-message'] = Validate::check($_POST['phone'], $this->translation, 'phone');
      $data['info-message'] .= Validate::check($_POST['name'], $this->translation, 'text');
      if (empty($data['info-message'])) {
        $this->users->update([
            'name' => $_POST['name'],
            'surname' => $_POST['surname'],
            'phone' => $_POST['phone']
        ], [['id', '=', $_SESSION['user_id']]]);
        Users::setSessionUserInfo($this->users->find($_SESSION['user_id']));
        $data['info-message'] = $this->translation['save_data'];
      }
    } else if (!empty($_POST['save_new_password']))
      $data['info-message'] = $this->users->updatePassword($_POST['password'], $_POST['password_repeat'], $_SESSION['user_id']); // update password

    //tabs
    $data['show-tab'] = 'user_info';
    $data['nav-tabs'] = $this->get_all_tabs();

    $data['breadcrumbs'] = [['url' => '#', 'name' => $data['page']['name']]];
    return $data;

  }


  // initializing  NAVIgation tabs =>  с детвства за #START Na'Vi

  function get_all_tabs()
  {
    $tabs = array();
    $tabs_keys = array(
        'user_info',
      //'password',
      //'addresses',
      //'favorites',
        'orders',
      //'bonuses'
    );;
    foreach ($tabs_keys as $item) {
      $trust_path = 'get_tab_' . $item;
      if (method_exists($this, $trust_path)) $tabs[$item] = $this->$trust_path();
      else $tabs[$item] = $this->get_one_tab($item);
    }
    return $tabs;
  }

  // only tabs with translate and simple view
  function get_one_tab($tab_key)
  {
    return array(
        'name' => $this->translation['tab_' . $tab_key],
        'body' => $this->view->Render($this->tb . '/cabinet-tabs/' . $tab_key . '.phtml')
    );
  }

  // get orders of current user
  function get_tab_orders()
  {
    return array(
        'name' => $this->translation['tab_orders'],
        'body' => $this->view->Render($this->tb . '/cabinet-tabs/orders.phtml', array('orders' => $this->orders->find([
            'type' => 'rows',
            'select' => 'tb.id, tb.date_edit, tb.sum, tb.amount, os.name as status ',
            'join' => 'LEFT JOIN orders_status os ON tb.status_id = os.id',
            'where' => 'tb.user_id = "' . $_SESSION['user_id'] . '"',
            'order' => 'tb.date_edit DESC'])
        ))
    );
  }

  // end Tabs

  //registration of user
  private function registrationAction()
  {
    if (isset($_SESSION['user_id'])) header("Location: /users/cabinet");
    else if (isset($_POST['email'])) $data['message'] = $this->users->signUp();
    $data['page']['name'] = $this->translation['sign_up'];
    $data['breadcrumbs'] = [['url' => '#', 'name' => $data['page']['name']]];
    return $data;
  }

  //auth Page
  private function loginAction()
  {
    if (isset($_SESSION['user_id'])) header("Location: /users/cabinet");
    else if (isset($_POST['email'])) $data['message'] = $this->users->auth($_POST['email'], $_POST['password']);
    $data['page']['name'] = $this->translation['auth'];
    $data['breadcrumbs'] = [['url' => '#', 'name' => $data['page']['name']]];
    return $data;
  }

  // show just one order
  function getOrderInfo($order_id)
  {
    $vars['order'] = Orders::getObject($this->sets)->find([
        'select' => 'tb.*, 
                      delivery_lang.name as delivery, 
                      payment_lang.name as payment, 
                      os.name as status',
        'join' => 'LEFT JOIN orders_status os ON tb.status_id = os.id
                      LEFT JOIN ' . $this->registry['key_lang'] . '_payment payment_lang ON tb.payment_id=payment_lang.payment_id
                      LEFT JOIN ' . $this->registry['key_lang'] . '_delivery delivery_lang ON tb.delivery_id=delivery_lang.delivery_id',
        'where' => 'tb.id = "' . $order_id . '" AND tb.user_id = "' . $_SESSION['user_id'] . '"'
    ]);

    if (isset($vars['order']['id'])) {
      $vars['products'] = Product::getObject($this->sets)->find([
          'table' => 'orders_product',
          'type' => 'rows',
          'select' => 'tb.*, product.url',
          'join' => 'LEFT JOIN product ON product.id = tb.product_id',
          'where' => 'orders_id = "' . $vars['order']['id'] . '"'
      ]);
    }

    $vars['translate'] = $this->translation;
    $data['content'] = $this->view->Render($this->tb . '/order.phtml', $vars);

    return json_encode([
        'status' => true,
        'content' => $data['content']
    ]);
  }

  function authAction()
  {
    return json_encode($this->users->auth($_POST['email'], $_POST['password']));
  }

  function logoutAction()
  {
    $this->users->logout();
  }
}