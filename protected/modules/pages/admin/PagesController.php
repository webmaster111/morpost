<?

class PagesController extends BaseController
{
  protected $pages;
  protected $infoblocks;
  protected $iblocks_types = ['column' => 'Колонка', 'tab' => 'Таб', 'anchor' => 'Якорьная ссылка', 'link' => 'Страничная ссылка'];

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Pages::$table;
    $this->name = "Инфо страницы";
    $this->pages = new Pages($this->sets);
    $this->infoblocks = new Infoblocks($this->sets);
  }

  public function indexAction()
  {
    if (isset($this->params['subsystem'])) return $this->Index($this->pages->subsystemAction());
    $vars['name'] = $this->name;
    $vars['message'] = isset($this->registry['access']) ? $this->pages->registry['access'] : '';
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->pages->delete($this->tb);
    elseif (isset($_POST['update'])) $vars['message'] = $this->pages->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->pages->save();
    elseif (isset($_POST['add_close'])) $vars['message'] = $this->pages->add();
    $vars['list'] = $this->view->Render('view.phtml', array('list' => $this->pages->find(array('type' => 'rows', 'order' => 'tb.sort ASC,tb.id DESC'))));
    $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name));
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = isset($_POST['add']) ? $this->pages->add() : '';
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = isset($_POST['update']) ? $this->pages->save() : '';
    if (isset($this->params['duplicate'])) $vars['message'] = $this->pages->duplicate($vars['edit'], $this->tb);
    $vars['edit'] = $this->pages->find((int)$this->params['edit']);

    // Load meta
    $row = $this->meta->load_meta($this->tb, $vars['edit']['url']);
    if ($row) {
      $vars['edit']['title'] = $row['title'];
      $vars['edit']['keywords'] = $row['keywords'];
      $vars['edit']['description'] = $row['description'];
    }

    $vars['infoblocks'] = $this->view->Render('iBlocks_list.phtml', [
      'iblocks'    => json_decode($vars['edit']['infoblocks']),
      'iblocks_types' => $this->iblocks_types,
      'infoblocks' => $this->infoblocks->getAll()
    ]);

    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }

  public function addIBlockRowAction()
  {
    $data = ['success' => false, 'message' => 'Возникла ошибка при добавлении информационного блока'];
    if(!empty($_POST['id'])){
        $this->registry::set('admin', 'pages');
        $iblocks = array();
        $cur_page = $this->pages->find($_POST['id']);
        $infoblocks = [
            'iblock' => 0,
            'sort'   => 0,
            'col_lg' => 12,
            'col_md' => 12,
            'col_sm' => 12,
            'col_xs' => 12,
            'group'  => 1,
            'type'   => 'column',
        ];
        $vars['row_id'] = 1;
        if(!empty($cur_page['infoblocks'])){
            $added_iblocks = json_decode($cur_page['infoblocks']);
            foreach ($added_iblocks as $added_iblock){
                $iblocks[] = $added_iblock;
            }
            $vars['row_id'] = count($added_iblocks);
        }
        $iblocks[] = $infoblocks;
        $this->pages->update(['infoblocks' => json_encode($iblocks)], [['id', '=', $_POST['id']]]);
        $vars['iblocks'] = $iblocks;
        $vars['iblocks_types'] = $this->iblocks_types;
        $vars['infoblocks'] = $this->infoblocks->getAll();
        $data['output'] = $this->view->Render('iBlocks_list.phtml', $vars);
        $data['success'] = true;
        $data['message'] = 'Инфоблок удален';
        return json_encode($data);
    }
    return false;
  }

  public function delIBlockRowAction(){
    $data = ['success' => false, 'message' => 'Возникла ошибка при удалении информационного блока'];
    if(!empty($_POST['id'])){
        $this->registry::set('admin', 'pages');
        $iblocks = array();
        $i = 0;
        $cur_page = $this->pages->find($_POST['id']);
        $added_iblocks = json_decode($cur_page['infoblocks']);
        if(count($added_iblocks) > 1){
            foreach ($added_iblocks as $added_iblock){
                $i++;
                if($i == $_POST['row_id']){
                    continue;
                }else{
                    $iblocks[] = $added_iblock;
                }
            }
            $this->pages->update(['infoblocks' => json_encode($iblocks)], [['id', '=', $_POST['id']]]);
        }else{
            $this->pages->update(['infoblocks' => NULL], [['id', '=', $_POST['id']]]);
        }
        $vars['iblocks'] = $iblocks;
        $vars['iblocks_types'] = $this->iblocks_types;
        $vars['infoblocks'] = $this->infoblocks->getAll();
        $data['output'] = $this->view->Render('iBlocks_list.phtml', $vars);
        $data['success'] = true;
        $data['message'] = 'Инфоблок удален';
        return json_encode($data);
    }
    return json_encode($data);
  }
}