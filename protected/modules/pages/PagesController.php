<?

class PagesController extends BaseController
{
  protected $pages;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Pages::$table;
    $this->pages = new Pages($this->sets);
  }

  public function indexAction()
  {

    $vars['page'] = $this->model->getPage($this->params['pages']);

      if (!isset($vars['page']['url'])) return Router::act('error', $this->registry);

    // шаблон по-умолчанию
    $template = 'layout/body.phtml';
    if (file_exists('tpl/default/pages/' . $this->params['pages'] . '.phtml')) {
      $template = 'pages/' . $this->params['pages'] . '.phtml';
    }

    if ($this->params['pages'] == 'contacts') {
      $vars['contact-form'] = $this->view->Render('forms/contact.phtml', $vars);
      $vars['contacts'] = $this->getContacts();
    }


      $data['breadcrumbs'] = array(['url' => $vars['page']['url'], 'name' => $vars['page']['name']]);
    $data['meta'] = $vars['page'];

    $menu_catalogs = getTree(containArrayInHisId($this->catalog->getAll(" AND tb.active='1'")));
    $vars['menu_catalogs'] = $this->view->Render('layout/menu.phtml', array('catalogs' => $menu_catalogs));
    if (isset($data['breadcrumbs'])) $vars['breadcrumbs'] = $this->model->breadcrumbs($data['breadcrumbs'], $this->view);

    $infoblocks = [];
    if(!empty($vars['page']['infoblocks'])){
      $iblocks = json_decode($vars['page']['infoblocks']);
      foreach ($iblocks as $iblock){
          $infoblocks[] = array(
              'sort'   => $iblock->sort,
              'group'  => $iblock->group,
              'type'   => $iblock->type,
              'col_lg' => $iblock->col_lg,
              'col_md' => $iblock->col_md,
              'col_sm' => $iblock->col_sm,
              'col_xs' => $iblock->col_xs,
              'iblock' => $this->model->getBlock((int)$iblock->iblock),
          );
      }
    }
    usort($infoblocks, function($a, $b){
      if ($a['group'] > $b['group']) { return 1; }
      if ($a['group'] < $b['group']) { return -1; }
      if ($a['group'] == $b['group']) {
          if ($a['sort'] == $b['sort']) return 0;
          return ($a['sort'] > $b['sort']) ? 1 : -1;
      }
    });

    $vars['infoblocks'] = $infoblocks;
    $data['content'] = $this->view->Render($template, $vars);
    return $this->Index($data);
  }

}