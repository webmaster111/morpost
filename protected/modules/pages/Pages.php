<?

class Pages extends Model
{
  static $table = 'pages';
  static $name = 'Содержимое';

  public function __construct($registry)
  {
    parent::getInstance($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  public function add()
  {
    if (isset($_POST['active'], $_POST['url'], $_POST['name'], $_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body']) && $_POST['name'] != "") {

      $insert_id = $this->insert(array(
          'active' => $_POST['active'],
          'form' => $_POST['form'],
      ));

      // Translate
      foreach ($this->db->rows("SELECT * FROM language") as $lang) {
        $this->insert(array(
            'name' => $_POST['name'],
            'body' => $_POST['body'],
            self::$table . '_id' => $insert_id
        ), $lang['language'] . "_" . self::$table);
      }
      // Set unique URL
        $url = !empty($_POST['url']) ? StringLibrary::translit($_POST['url']) : StringLibrary::translit($_POST['name']);
      $this->checkUrl(self::$table, $url, $insert_id);

      // Save meta data
      Meta::getObject($this->sets)->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);

      return messageAdmin('Данные успешно добавлены');
    }
    return messageAdmin('Заполнены не все обязательные поля', 'error');
  }

  public function save()
  {
    if (isset($this->registry['access'])) return $this->registry['access'];
    $message = messageAdmin('При сохранение произошли ошибки', 'error');

    $infoblocks = [];

    if(!empty($_POST['info_block_id']) && !empty($_POST['sort']) && !empty($_POST['info_block_group'])){
      for ($i=0; $i < count($_POST['info_block_id']); $i++){
          $infoblocks[] = array(
              'iblock' => $_POST['info_block_id'][$i],
              'sort'   => $_POST['sort'][$i],
              'col_lg' => $_POST['col_lg'][$i],
              'col_md' => $_POST['col_md'][$i],
              'col_sm' => $_POST['col_sm'][$i],
              'col_xs' => $_POST['col_xs'][$i],
              'group'  => $_POST['info_block_group'][$i],
              'type'   => $_POST['info_block_type'][$i],
          );
      }
    }

    if (isset($_POST['save_id'], $_POST['name'], $_POST['url']) && is_array($_POST['save_id'])) {
      foreach ($_POST['save_id'] as $i => $id) {
          $url = !empty($_POST['url'][$i]) ? StringLibrary::translit($_POST['url'][$i]) : StringLibrary::translit($_POST['name'][$i]);
        $this->checkUrl(self::$table, $url, $id);
        $this->update(array('name' => $_POST['name'][$i]), [[self::$table . '_id', '=', $id]],
            $this->registry['key_lang_admin'] . "_" . self::$table);
      }
      $message = messageAdmin('Данные успешно сохранены');
    } else if (isset($_POST['active'], $_POST['url'], $_POST['id'], $_POST['name'], $_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body'])) {

      $this->update(
          array('active' => $_POST['active'], 'infoblocks' => json_encode($infoblocks), 'form' => $_POST['form']),
          [['id', '=', $_POST['id']]]);

      $this->update(
          array('name' => $_POST['name'], 'body' => $_POST['body']),
          [[self::$table . '_id', '=', $_POST['id']]],
          $this->registry['key_lang_admin'] . "_" . self::$table);

        $url = !empty($_POST['url']) ? StringLibrary::translit($_POST['url']) : StringLibrary::translit($_POST['name']);
      $this->checkUrl(self::$table, $url, $_POST['id']);
      // Save meta data
      Meta::getObject($this->sets)->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);

      $message = messageAdmin('Данные успешно сохранены');
    }

    return $message;
  }

}