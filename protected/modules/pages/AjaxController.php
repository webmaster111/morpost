<?
class AjaxController extends BaseController
{
	function __construct($registry,$params)
	{
		$this->registry=$registry;
		parent::__construct($registry,$params);
	}

	function indexAction(){}

	function activeAction()
	{
		if(isset($_POST['id'],$_POST['tb'])){
			$data=[];
			//$data['message']=$this->checkAccess('edit');
			if(true){
				$_POST['id']=str_replace("active","",$_POST['id']);
				$tb=$_POST['tb'];
				$row=$this->db->row("SELECT `active` FROM `$tb` WHERE `id`=?",array($_POST['id']));
				if($row['active']==1){
					$this->db->query("UPDATE `$tb` SET `active`=? WHERE `id`=?",array(0,$_POST['id']));
					$data['active']='<div class="selected-status status-d"><a> Выкл. </a></div>';
				}else{
					$this->db->query("UPDATE `$tb` SET `active`=? WHERE `id`=?",array(1,$_POST['id']));
					$data['active']='<div class="selected-status status-a"><a> Вкл. </a></div>';
				}
				$data['message']=messageAdmin('Данные успешно сохранены');
			}
			echo json_encode($data);
		}
	}

	function sortAction()
	{
		if(isset($_POST['arr'],$_POST['tb'])){
			$data=[];
			$data['message']=$this->checkAccess('edit');
			if($data['message']==''){
				$tb=$_POST['tb'];
				$_POST['arr']=str_replace("sort","",$_POST['arr']);
				preg_match_all("/=(\d+)/",$_POST['arr'],$a);
				foreach($a[1] as $pos=>$id){
					$pos2=$pos + 1;
					$this->db->query("update `$tb` set `sort`=? WHERE `id`=?",[$pos2,$id]);
				}
				$data['message']=messageAdmin('Данные успешно сохранены');
			}
			echo json_encode($data);
		}
	}

	function uploadifyAction()
	{
		$tempFile=$_FILES['Filedata']['tmp_name'];
		$name=str_replace(strchr($_FILES['Filedata']['name'],"."),"",$_FILES['Filedata']['name']);
		$insert_id=$this->db->insert_id("insert into photo set photos_id=?,active=?",[$_REQUEST['id'],1]);
		foreach($this->language as $lang){
			$tb=$lang['language']."_photo";
			$this->db->query("INSERT INTO `$tb` SET `name`=?,`photo_id`=?",[$name,$insert_id]);
		}
		$dir="files/photos/{$_REQUEST['id']}/";
		if(!is_dir($dir))mkdir($dir,0755);
		resizeImage($tempFile,$dir.$insert_id.".jpg",$dir.$insert_id."_s.jpg",214,145);
		$tempFile=$_FILES['Filedata']['tmp_name'];
		$targetPath=$_SERVER['DOCUMENT_ROOT'].$_REQUEST['folder'].'/';
		$targetFile=str_replace('//','/',$targetPath).$_FILES['Filedata']['name'];
		echo str_replace($_SERVER['DOCUMENT_ROOT'],'',$targetFile);
	}

	function photosAction()
	{
		$res=$this->db->rows("SELECT * FROM `photo` tb LEFT JOIN `".$this->key_lang."_photo` tb2 ON tb.id=tb2.photo_id WHERE photos_id=? ORDER BY sort ASC",[$_REQUEST['id']]);
		?><tr class="noDrop"><th width="25"></th><th width="1%" class="center"><input type="checkbox" class="check_all2" title="Отметить/снять все" value="Y" name="check_all"></th><th width="2%">ID</th><th width="50">Фото</th><th>Название</th><th width="20%">Статус</th><th width="15%">&nbsp;</th></tr><?
		foreach($res as $row){
			if($row['active']==1) $active='<div class="selected-status status-a"><a> Вкл. </a></div>';
			else $active='<div class="selected-status status-d"><a> Выкл. </a></div>';
			echo '<tr id="sort'.$row['id'].'"><td class="move"></td><td class="center"><input type="checkbox" class="check-item" value="'.$row['id'].'" name="photo_id[]"/></td><td><span>'.$row['id'].'</span></td><td><img src="/files/photos/'.$row['photos_id'].'/'.$row['id'].'_s.jpg" alt="" width="50"/></td><td><input type="text" value="'.$row['name'].'" name="photo_name[]" class="input-text"></td><td><input type="hidden" value="'.$row['id'].'" name="save_photo_id[]"/><div class="select-popup-container active_status" id="active'.$row['id'].'">'.$active.' </div></td><td width="10%"><ul class="cm-tools-list tools-list"><li><a href="/admin/photos/edit/'.$row['photos_id'].'/delete/'.$row['id'].'" class="cm-confirm">Удалить</a></li></ul></td></tr>';
		}
	}

	function addcommentAction()
	{
		$name=$_POST['name'];
		$pos=strpos($name,"<a");
		if($pos===false&&$_POST['name']!=""&&$_POST['comment']!=""){
			$date=date("Y-m-d H:i:s");
			$this->db->query("INSERT INTO `comments` SET `author`=?,`text`=?,`content_id`=?,`date`=?,`session_id`=?,`language`=?",[$_POST['name'],$_POST['comment'],$_POST['id'],$date,session_id(),$this->key_lang]);
			echo "<div class='message'>".$this->translation['comment_add']."!</div>";
		}
	}

	function feedbackAction()
	{
		$send=0;
		if(strlen($_POST['message'])<5||strlen($_POST['name'])<3) $message=$this->translation['required'];
		elseif(!preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is',$_POST['email'])) $message=$this->translation['wrong_email'];
		else{
			$settings=Registry::get('user_settings');
			send_mime_mail($settings['sitename'],"info@".$_SERVER['HTTP_HOST'],$settings['sitename'],$settings['email'],"utf-8","utf-8","Обратная связь: ".$_SERVER['HTTP_HOST'],"Имя: {$_POST['name']}<br>E-mail: {$_POST['email']}<br><br>Сообщение:{$_POST['message']}");
			$send=1;
			$message="<div class='alert alert-success'>".$this->translation['message_sent']."</div>";
		}
		echo json_encode([$send,$message]);
	}

	function addModuleAction()
	{
		$dir=MODULES.$_POST['id']."/admin/data/info.txt";
		if(file_exists($dir)){
			$lines=file($dir);
			$i=0;
			$data=[];
			foreach($lines as $line_num=>$line){
				if($i==0) $data['name']=$line;
				elseif($i==1) $data['comment']=$line;
				else $data['tables']=$line;
				$i++;
			}
			return json_encode($data);
		}
	}
}