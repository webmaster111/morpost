<?php

	/*
	 * Модуль автоматической публикации постовок вконтакте
	 */

	class VkontakteController extends BaseController
	{

		protected $params;
		protected $db;
		private $left_menu = array(array('title' => 'Очередь публикаций',
										 'url'   => '/admin/vkontakte/act/queue',
										 'name'  => 'vkontakte'),
			array('title' => 'История публикаций',
				  'url'   => '/admin/vkontakte/act/history',
				  'name'  => 'vkontakte'));

		function  __construct($registry, $params)
		{
			parent::__construct($registry, $params);
			$this->tb = "vkontakte";
			$this->name = "Автопостинг контента в vk";
			$this->registry = $registry;
			$this->vk = new Vkontakte($this->sets);
		}

		public function indexAction()
		{
			$vars['message'] = '';
			$vars['name'] = $this->name;
			if (isset($this->registry['access'])) $vars['message'] = $this->registry['access'];
			if (isset($this->params['act'])) {
				$act = $this->params['act'] . 'Action';
				return $this->Index($this->$act());
			}
			if (isset($this->params['subsystem'])) return $this->Index($this->vk->subsystemAction($this->left_menu));
			$vars['client_id'] = $this->settings['client_id'];
			$vars['access_token'] = $this->settings['access_token'];
			$vars['client_secret'] = $this->settings['client_secret'];
			$vars['group_id'] = $this->settings['group_id'];
			$vars['albums'] = $this->vk->getAlbums($this->settings);
			$data['styles'] = array($this->tb . '.css', 'jquery.simple-dtpicker.css');
			$data['scripts'] = array($this->tb . '.js', 'jquery.searchabledropdown.js', 'jquery.simple-dtpicker.js');
			$data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'menu2' => $this->left_menu));
			$data['content'] = $this->view->Render('start.phtml', $vars);
			return $this->Index($data);
		}

		public function getContentAction()
		{
			if (isset($_POST['content_type']) && $_POST['content_type'] != NULL) {
				return json_encode($this->vk->getContent($_POST['content_type'], $_POST['date_from'], $_POST['date_to']));
			} else return false;
		}

		public function publishContentAction()
		{
			/*Base actions*/
			$this->vars = $_POST;
			require CLASSES . 'Vk.php';
			require CLASSES . 'VkPost.php';
			$where = '0';
			$vk = \vkApi\vk::create($this->settings['access_token']);
			$post = new \vkApi\post($vk, NULL, $this->settings['group_id']);
			/*Select API query type*/
			if ($this->vars['target'] == 'wall') {
				/*Prepare content*/
				if (is_array($this->vars['content'])) {
					foreach ($this->vars['content'] as $row) {
						$where .= ',' . $row;
					}
				} else $where .= ',' . $this->vars['content'];
				$table = $this->vars['type'];
				$lang_table = 'ru_' . $table;
				$content = $this->db->rows("SELECT * FROM {$table}
										LEFT JOIN {$lang_table} ON {$lang_table}.{$table}_id = {$table}.id
										WHERE {$table}.id IN({$where}) ");
				/*Sending API queries*/
				$data = array();
				$i = 0;
				foreach ($content as $c) {
					$text = $c['name'] . "\r\n" . $c['body_m'];
					$image = 'http://' . $_SERVER['HTTP_HOST'] . '/' . str_replace('_s.jpg', '.jpg', $c['photo']);
					$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $this->vars['type'] . '/' . $c['url'];
					$data [] = $post->post($text, $image, $url);
					/*History push*/
					if (isset($data[$i])) {
						$now = date('Y-m-d H:i:s');
						$this->db->query("INSERT INTO {$this->tb} SET `date`=?, `message`=?, `where`=?, `status`=?",
							array($now, $text . "\r\n" . $image . "\r\n" . $url, 'http://vk.com/club' . $this->settings['group_id'] . '?w=wall-' . $this->settings['group_id'] . '_' . $data[$i]->response->post_id . '', 'sent'));
					}
					$i++;
				}
				return json_encode($data);
			} elseif ($this->vars['target'] == 'album') {
				/*Prepare content*/
				if (is_array($this->vars['content'])) {
					foreach ($this->vars['content'] as $row) {
						$where .= ',' . $row;
					}
				} else $where .= ',' . $this->vars['content'];
				$table = $this->vars['type'];
				$lang_table = 'ru_' . $table;
				$content = $this->db->rows("SELECT * FROM {$table}
										LEFT JOIN {$lang_table} ON {$lang_table}.{$table}_id = {$table}.id
										WHERE {$table}.id IN({$where}) ");
				/*Sending API queries*/
				$data = array();
				$i = 0;
				foreach ($content as $c) {
					$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $this->vars['type'] . '/' . $c['url'];
					$text = $c['name'] . "\r\n" . $c['body_m'] . "\r\n" . $url;
					$image = 'http://' . $_SERVER['HTTP_HOST'] . '/' . str_replace('_s.jpg', '.jpg', $c['photo']);
					$data [] = $post->post_album($text, $image, $this->vars['album']);
					/*History push*/
					if (isset($data[$i])) {
						$now = date('Y-m-d H:i:s');
						$this->db->query("INSERT INTO {$this->tb} SET `date`=?, `message`=?, `where`=?, `status`=?",
							array($now, $text . "\r\n" . $image . "\r\n" . $url, 'http://vk.com/photo-' . $this->settings['group_id'] . '_' . $data[$i]->response[0]->pid . '', 'sent'));
					}
					$i++;
				}
				return json_encode($data);
			} else return json_encode('Error');
		}

		public function queuePushAction()
		{
			/*Base actions*/
			$this->vars = $_POST;
			$where = '0';
			/*Select API query type*/
			if ($this->vars['target'] == 'wall') {
				/*Prepare content*/
				if (is_array($this->vars['content'])) {
					foreach ($this->vars['content'] as $row) {
						$where .= ',' . $row;
					}
				} else $where .= ',' . $this->vars['content'];
				$table = $this->vars['type'];
				$lang_table = 'ru_' . $table;
				$q_table = $this->tb . '_queue';
				$content = $this->db->rows("SELECT * FROM {$table}
										LEFT JOIN {$lang_table} ON {$lang_table}.{$table}_id = {$table}.id
										WHERE {$table}.id IN({$where}) ");
				/*Queue push*/
				foreach ($content as $c) {
					$text = $c['name'] . "\r\n" . $c['body_m'];
					$image = 'http://' . $_SERVER['HTTP_HOST'] . '/' . str_replace('_s.jpg', '.jpg', $c['photo']);
					$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $this->vars['type'] . '/' . $c['url'];
					$now = date('Y-m-d H:i:s');
					$this->db->query("INSERT INTO {$q_table} SET `date_add`=?, `publish_type`=?, `group_id`=?, `album_id`=?, `url`=?, `image`=?, `text`=?, `status`=?",
						array($now, $this->vars['target'], $this->settings['group_id'], 0, $url, $image, $text, 0));
				}
				return json_encode("Pushed");
			} elseif ($this->vars['target'] == 'album') {
				/*Prepare content*/
				if (is_array($this->vars['content'])) {
					foreach ($this->vars['content'] as $row) {
						$where .= ',' . $row;
					}
				} else $where .= ',' . $this->vars['content'];
				$table = $this->vars['type'];
				$lang_table = 'ru_' . $table;
				$q_table = $this->tb . '_queue';
				$content = $this->db->rows("SELECT * FROM {$table}
										LEFT JOIN {$lang_table} ON {$lang_table}.{$table}_id = {$table}.id
										WHERE {$table}.id IN({$where}) ");
				/*Queue push*/
				foreach ($content as $c) {
					$url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $this->vars['type'] . '/' . $c['url'];
					$text = $c['name'] . "\r\n" . $c['body_m'] . "\r\n" . $url;
					$image = 'http://' . $_SERVER['HTTP_HOST'] . '/' . str_replace('_s.jpg', '.jpg', $c['photo']);
					$now = date('Y-m-d H:i:s');
					$this->db->query("INSERT INTO {$q_table} SET `date_add`=?, `publish_type`=?, `group_id`=?, `album_id`=?, `url`=?, `image`=?, `text`=?, `status`=?",
						array($now, $this->vars['target'], $this->settings['group_id'], $this->vars['album'], $url, $image, $text, 0));
				}
				return json_encode("Pushed");
			} else return json_encode('Error');
		}

		public function queueAction()
		{
			$vars['message'] = '';
			if (isset($_POST['delete'])) {
				$vars['message'] = $this->model->delete('vkontakte_queue');
			}
			$vars['name'] = 'Очередь публикации';
			$tb = $this->tb . '_queue';
			$vars['action'] = $this->tb;
			$vars['path'] = '/act/queue';
			$vars['list']['list'] = $this->db->rows("SELECT * FROM `{$tb}` ORDER BY `id` DESC");
			$vars['list'] = $this->view->Render('view.phtml', $vars);
			$data['styles'] = array($this->tb . '.css', 'jquery.simple-dtpicker.css');
			$data['scripts'] = array($this->tb . '.js', 'jquery.searchabledropdown.js', 'jquery.simple-dtpicker.js');
			$data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'menu2' => $this->left_menu));
			$data['content'] = $this->view->Render('list.phtml', $vars);
			return $data;
		}

		public function historyAction()
		{
			$vars['message'] = '';
			$vars['name'] = 'История публикаций';
			$vars['action'] = $this->tb;
			$vars['path'] = '/act/queue';
			$vars['list']['list'] = $this->db->rows("SELECT * FROM `{$this->tb}` ORDER BY `id` DESC");
			$vars['list'] = $this->view->Render('view_h.phtml', $vars);
			$data['styles'] = array($this->tb . '.css', 'jquery.simple-dtpicker.css');
			$data['scripts'] = array($this->tb . '.js', 'jquery.searchabledropdown.js', 'jquery.simple-dtpicker.js');
			$data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'menu2' => $this->left_menu));
			$data['content'] = $this->view->Render('list_h.phtml', $vars);
			return $data;
		}
	}