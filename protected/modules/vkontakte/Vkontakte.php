<?

	class Vkontakte extends Model
	{
		static $table = 'vkontakte';
		static $name = 'Автопостинг контента в vk';

		public function __construct($registry)
		{
			parent::getInstance($registry);
		}

		public static function getObject($registry)
		{
			return new self::$table($registry);
		}

		public function getContent($type, $from, $to)
		{
			$table = $type;
			$lang_table = 'ru_' . $table;
			$list = $this->db->rows("SELECT * FROM {$table}
										LEFT JOIN {$lang_table} ON {$lang_table}.{$table}_id = {$table}.id
										WHERE date_add BETWEEN '{$from}:00' AND '{$to}:59'");
			return $list;
		}

		public function getAlbums($settings)
		{
			require CLASSES . 'Vk.php';
			require CLASSES . 'VkPost.php';
			try {
				$vk = \vkApi\vk::create($settings['access_token']);
				$albums = $vk->get('photos.getAlbums', array('owner_id' => '-' . $settings['group_id'], 'owner_id' => '-' . $settings['group_id'], 'need_system' => '1'));
				return $albums->response;
			} catch (Exception $e) {
				echo 'Error: <b>' . $e->getMessage() . '</b><br />';
				echo 'in file "' . $e->getFile() . '" on line ' . $e->getLine();
			}
		}

	}