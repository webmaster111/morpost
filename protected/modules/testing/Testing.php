<?
class Testing extends Model
{
	static $table='testing';
	static $name='Тикеты';

	public function __construct($registry)
	{
		parent::getInstance($registry);
	}

	public static function getObject($registry)
	{
		return new self::$table($registry);
	}
}