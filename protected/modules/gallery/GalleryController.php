<?

class GalleryController extends BaseController
{
  protected $gallery;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Gallery::$table;
    $this->gallery = new Gallery($this->sets);
  }

  public function getMoreAction()
  {
    if (isset($_POST['offset'])) {
      $offset = $_POST['offset'];
      $on_page = $this->settings['gallery_paging'];
      $result = array('offset' => $offset + 1);
      $all = $this->gallery->count("active='1'");
      $list = $this->gallery->find(array(
          'type' => 'rows',
          'where' => 'tb.active = "1"',
          'order' => 'tb.sort',
          'limit' => $on_page . ' OFFSET ' . ($offset * $on_page)
      ));
      $result['thumbs'] = $this->view->Render($this->tb . '/thumb.phtml', array('photos' => $list, 'slide_down' => true));
      if (($all - (count($list) + $offset * $on_page)) <= 0) $result['offset'] = 0;
      return json_encode($result);
    }
    return false;
  }
}