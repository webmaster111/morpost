<?

class GalleryController extends BaseController
{
  protected $gallery;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Gallery::$table;
    $this->name = "Медиа-файлы";
    $this->gallery = new Gallery($this->sets);
  }

  public function indexAction()
  {
    if (isset($this->params['subsystem'])) return $this->Index($this->gallery->subsystemAction());
    $vars['message'] = isset($this->registry['access']) ? $this->registry['access'] : '';
    $vars['name'] = $this->name;
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->gallery->delete($this->tb);
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->gallery->save();
    elseif (isset($_POST['add_close'])) $vars['message'] = $this->gallery->add();
    $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->gallery->getAll('', true)]);
    $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name]);
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = isset($_POST['add']) ? $this->gallery->add() : '';
    $vars['width'] = $this->settings['gallery_width'];
    $vars['height'] = $this->settings['gallery_height'];
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = isset($_POST['update']) ? $this->gallery->save() : '';
    $vars['edit'] = $this->gallery->find((int)$this->params['edit']);
    $dir = Dir::createDir($this->params['edit'], '', $this->tb);
    $vars['path'] = $dir[0];
    $vars['width'] = $this->settings['gallery_width'];
    $vars['height'] = $this->settings['gallery_height'];
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }
}