<?

class Gallery extends Model
{
  static $table = 'gallery';
  static $name = 'Галерея';

  public function __construct($registry)
  {
    parent::getInstance($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  public function add()
  {
    $message = messageAdmin('Заполнены не все обязательные поля', 'error');
    if (isset($_POST['type'])) {
      $id = $this->insert(array(
          'active' => $_POST['active'],
          'type' => $_POST['type'],
          'video' => $_POST['video']
      ));
      $this->savePhoto($id, $_POST['tmp_image'], self::$table);
      $message = messageAdmin('Данные успешно добавлены');
    }
    return $message;
  }

  public function save()
  {
    $message = messageAdmin('При сохранение произошли ошибки', 'error');
    if (isset($this->registry['access'])) return $this->registry['access'];
    if (!is_array($_POST['save_id']) && isset($_POST['active'], $_POST['type'], $_POST['id'])) {
      $this->update(array(
          'active' => $_POST['active'],
          'type' => $_POST['type'],
          'video' => $_POST['video']
      ), array(['id', '=', $_POST['id']]));
      $message = messageAdmin('Данные успешно сохранены');
    }
    return $message;
  }

}