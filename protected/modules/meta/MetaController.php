<?

class MetaController extends BaseController
{
    protected $meta;

    function __construct($registry, $params)
    {
        parent::__construct($registry, $params);
        $this->tb = Meta::$table;
        $this->meta = new Meta($this->sets);
    }

    function sitemapAction()
    {
        $filename = 'sitemap.xml';
        $filePath = './' . $filename;
        header("Content-type: text/xml; charset=utf-8");
        //Если включен динамический сайтмап или сайтмапа пока нету
        if ((int)$this->settings['sitemap_generation'] === 1 OR !file_exists($filePath)) {
            $sitemap = $this->meta->sitemap_generate();
        } else {
            $sitemap = file_get_contents($filePath);
        }
        echo $sitemap;
    }

    //////Yandex market xml
    function yandexAction()
    {
        if (isset($this->settings['yandex_market']) && $this->settings['yandex_market'] == 1) {
            header("Content-Type: text/xml");
            $vars = $this->meta->yandex_market();
            $vars['sitename'] = $this->settings['sitename'];

            if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/tpl/default/yandex.phtml')) {
                echo $this->view->Render('yandex.phtml', $vars);
            }
        }
    }

}