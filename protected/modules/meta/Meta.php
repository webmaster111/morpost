<?

class Meta extends Model
{
	static $table = 'meta'; //Главная талица
	static $name = 'Meta-данные'; // primary key

	public function __construct($registry)
	{
		parent::getInstance($registry);
	}

	//для доступа к классу через статичекий метод
	public static function getObject($registry)
	{
		return new self::$table($registry);
	}


	public function load_meta($tb, $url)
	{
		$url = $this->get_url($tb, $url);
		$row = $this->find( (string) $url);
		if ($row) return $row;
		return array('title' => '', 'keywords' => '', 'description' => '');

	}

	function get_url($tb, $url)
	{
//		$protocol = getProtocol();
		$row = $this->db->row("SELECT url FROM `modules` WHERE `controller`=?", array($tb));
		if ($row) {
			if ($url == '/')
				$url = '';
			if ($row['url'] != '')
				$row['url'] .= '/';
//			$url = $protocol.$_SERVER['HTTP_HOST'].'/'.$row['url'].$url;
			$url = '/'.$row['url'].$url;
			return $url;
		}
		return false;
	}

	function delete_meta($tb, $id)
	{
		if (isset($this->registry['access'])) return $this->registry['access'];

		$row = $this->db->row("SELECT * FROM `".$tb."` WHERE `id`=?", array($id));
		if (isset($row['url'])) {
			$url = $this->get_url($tb, $row['url']);
			if ($url != '')
				$this->db->query("DELETE FROM ".static::$table." WHERE url=?", array($url));
		}
	}

	public function add()
	{
		if (isset($this->registry['access'])) return $this->registry['access'];

		$message = '';
		if (isset($_POST['active'], $_POST['url'], $_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body'], $_POST['excerpt'], $_POST['expandable']) && $_POST['url'] != "") {
			$param = array($_POST['url'], $_POST['active'], $_POST['type']);
			$id = $this->db->insert_id("INSERT INTO `".static::$table."` SET `url`=?, `active`=?, `type`=?", $param);
			$param = array($_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body'], $_POST['excerpt'], $_POST['expandable'], $id);
			foreach ($this->language as $lang) {
				$tb = $lang['language']."_".static::$table;
				$this->db->query("INSERT INTO `$tb` SET `title`=?, `keywords`=?, `description`=?, `body`=?, `excerpt`=?, `expandable`=?, `meta_id`=?", $param);
			}
			$message .= messageAdmin('Данные успешно добавлены');
		}
		//else $message.= messageAdmin('При добавление произошли ошибки', 'error');
		return $message;
	}

	public function save()
	{
		if (isset($this->registry['access'])) return $this->registry['access'];
		$message = '';
		if (isset($_POST['save_id']) && is_array($_POST['save_id'])) {
			if (isset($_POST['save_id'], $_POST['name'], $_POST['url'])) {
				$count = count($_POST['save_id']) - 1;
				for ($i = 0; $i <= $count; $i++) {
					$url = $_POST['url'][$i];
					$this->checkUrl(static::$table, $url, $_POST['save_id'][$i]);
					$param = array($_POST['name'][$i], $_POST['save_id'][$i]);
					$this->db->query("UPDATE `".$this->registry['key_lang_admin']."_".static::$table."` SET `name`=? WHERE ".static::$table."_id=?", $param);
				}
				$message .= messageAdmin('Данные успешно сохранены');
			} else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
		} else {
			if (isset($_POST['active'], $_POST['url'], $_POST['id'], $_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body'], $_POST['expandable'])) {
				$param = array($_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body'], $_POST['excerpt'], $_POST['expandable'], $_POST['id']);
				$this->db->query("UPDATE `".static::$table."` SET `url`=?, `active`=?, `type`=? WHERE id=?", array($_POST['url'], $_POST['active'], $_POST['type'], $_POST['id']));
				$this->db->query("UPDATE `".$this->registry['key_lang_admin']."_".static::$table."` SET `title`=?, `keywords`=?, `description`=?, `body`=?, `excerpt`=?, `expandable`=? WHERE meta_id=?", $param);
				$message .= messageAdmin('Данные успешно сохранены');
			} else
				$message .= messageAdmin('При сохранение произошли ошибки', 'error');
		}

		return $message;
	}

	public function save_seoconfig()
	{
		if (isset($this->registry['access'])) return $this->registry['access'];
		$message = '';

		if (isset($_POST['on_sitemap'])) $value = 1;
		else $value = 0;
		$this->db->query("UPDATE `config` SET `value`='$value' WHERE name='sitemap_generation'");

		if (isset($_POST['on_yandex_market'])) $value = 1;
		else $value = 0;
		$this->db->query("UPDATE `config` SET `value`='$value' WHERE name='yandex_market'");

		if (!isset($_POST['main_domain'])) $value = 0;
		else $value = $_POST['main_domain'];
		$this->db->query("UPDATE `config` SET `value`='$value' WHERE name='main_domain'");

		$message .= messageAdmin('Данные успешно сохранены');

		return $message;
	}


	public function save_meta($tb, $url, $title, $keywords, $description)
	{
		if (isset($this->registry['access'])) return $this->registry['access'];

		$url = $this->get_url($tb, $url);
		if ($url != '') {
			$row = $this->find(array(
					'select' => 'id, body',
					'where' => "tb.url = '".$url."'"
				)
			);
			if ($row) {
				if ($title == '' && $keywords == '' && $description == '' && $row['body'] == '') {
					$this->db->query("DELETE FROM ".static::$table." WHERE id=?", array($row['id']));
				} else {
					$this->db->query("UPDATE `".$this->registry['key_lang_admin']."_meta` SET `title`=?, `keywords`=?, `description`=? WHERE meta_id=?", array($title, $keywords, $description, $row['id']));
				}
			} else {
				if ($title == '' && $keywords == '' && $description == '') {
					return false;
				} else {
					$param = array($url, 1);
					$id = $this->db->insert_id("INSERT INTO `".static::$table."` SET `url`=?, `active`=?", $param);
					$param = array($title, $keywords, $description, $id);
					foreach ($this->language as $lang) {
						$tb = $lang['language']."_".static::$table;
						$this->db->query("INSERT INTO `$tb` SET `title`=?, `keywords`=?, `description`=?, `meta_id`=?", $param);
					}
				}
			}
		}
	}

	public function addredirect()
	{
		if (isset($this->registry['access'])) return $this->registry['access'];

		$message = '';
		$this->db->query("INSERT INTO `redirects` SET `active`='0'");
		$message .= messageAdmin('Данные успешно сохранены');

		return $message;
	}

	public function save_redirects()
	{
		if (isset($this->registry['access'])) return $this->registry['access'];
		$message = '';

		if (isset($_POST['id'], $_POST['from'], $_POST['to'])) {
			$count = count($_POST['id']) - 1;
			for ($i = 0; $i <= $count; $i++) {
				$from = str_replace('http://', '', $_POST['from'][$i]);
				$to = str_replace('http://', '', $_POST['to'][$i]);
				$param = array($from, $to, $_POST['type'][$i], $_POST['id'][$i]);
				$this->db->query("UPDATE `redirects` SET `from`=?, `to`=?, `type`=? WHERE id=?", $param);
			}
			$message .= messageAdmin('Данные успешно сохранены');
		} else
			$message .= messageAdmin('При сохранение произошли ошибки', 'error');

		return $message;
	}

	function check_redirects()
	{
		$protocol = getProtocol();
		$url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$row = $this->db->row("SELECT `to`, `type` FROM `redirects` WHERE `from`=? AND `from`!='' AND `to`!='' AND active='1'", array($url));
		if ($row) {
			if ($row['type'] == '302')
				header("HTTP/1.1 302 Found");
			else
				header("HTTP/1.1 301 Moved Permanently");
			header("Location: ".$protocol.$row['to']);
			exit();
		}
		$domain = current(explode('.', $_SERVER['HTTP_HOST']));
		if (isset($this->settings['main_domain']) && $this->settings['main_domain'] == 1 && $domain != 'www') {
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: ".$protocol."www.".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			exit();
		} elseif (isset($this->settings['main_domain']) && $this->settings['main_domain'] == 0 && $domain == 'www') {
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: ".$protocol.str_replace('www.', '', $_SERVER['HTTP_HOST']).$_SERVER['REQUEST_URI']);
			exit();
		}
	}

    static function check_targets($db, $url)
	{
		//
		$row = $db->row("SELECT * FROM config WHERE name='targets_for_google'");
		if ($row && $row != '') {
			$targets = explode(',', str_replace(' ', '', $row['value']));
			//var_info($_POST);
			$countPost = count($_POST);
			$i = 0;
			foreach ($url as $row) {
				//echo $row.'<br />';
				foreach ($targets as $row2) {
					if ($row == $row2) {
						$_SERVER['REQUEST_URI'] = str_replace('/'.$row2, '', $_SERVER['REQUEST_URI']);
						if ($countPost == 0) {
							if ($_SERVER['REQUEST_URI'] == '')
								$_SERVER['REQUEST_URI'] = '/';
							//echo $link.'asdsaaaaaaaaa'.$url[$i];
							header("HTTP/1.1 301 Moved Permanently");
							header('Location: '.$_SERVER['REQUEST_URI']);
							exit();
						} else {
							unset($url[$i]);
						}
					}
				}
				$i++;
			}
			array_values($url);
		}
		//var_info($url);
		return $url;
	}

	public function search_link()
	{
		$protocol = getProtocol();
		$query = '';
		$res = $this->db->rows("SELECT controller
								FROM modules 
								WHERE controller='pages' OR 
									  controller='menu' OR 
									  controller='news' OR 
									  controller='article' OR 
									  controller='product' OR 
									  controller='catalog' OR 
									  controller='params' OR 
									  controller='brend' OR 
									  controller='photos' OR 
									  controller='meta'");
		foreach ($res as $row) {
			$where = "";
			if ($this->db->query("SHOW COLUMNS FROM `".$row['controller']."` LIKE 'body_m'"))
				$where = " OR body_m LIKE '%".$protocol."%'";
			if ($query != '')
				$query .= " UNION ";
			$query .= "(SELECT tb.id, tb.url, m.url as url2, m.controller, '".$row['controller']."' as action, tb2.body
					  FROM ".$row['controller']." tb
					
					  LEFT JOIN ru_".$row['controller']." tb2
					  ON tb.id=tb2.".$row['controller']."_id
					
					  LEFT JOIN modules m
					  ON m.controller='".$row['controller']."'
					
					  WHERE body LIKE '%".$protocol."%' $where
					  GROUP BY tb.id
					  )";
		}
		//Menu
		$result = $this->db->rows($query);
		return $result;
	}

	public function clear_from_links()
	{
		$protocol = getProtocol();
		$message = '';
		if (isset($this->registry['access'])) return $this->registry['access'];

		if (isset($_POST['id']) && is_array($_POST['id'])) {
			$parser = new Parser();
			$count = count($_POST['id']) - 1;
			for ($i = 0; $i <= $count; $i++) {
				$id = explode('-', $_POST['id'][$i]);
				$where = "";
				if ($this->db->query("SHOW COLUMNS FROM `".$id[0]."` LIKE 'body_m'"))
					$where = " OR body_m LIKE '%".$protocol."%'";
				$row = $this->db->row("SELECT * FROM `".$this->registry['key_lang_admin']."_".$id[0]."` WHERE ".$id[0]."_id=? AND (body LIKE '%".$protocol."%'$where)", array($id[1]));
				if ($row) {
					$where = "";
                    $body = StringLibrary::clear_links(htmlspecialchars_decode($row['body']));
					if (isset($row['body_m'])) {
						$where = ", body_m=?";
                        $body_m = StringLibrary::clear_links(htmlspecialchars_decode($row['body_m']));
						$param = array($body, $body_m, $id[1]);
					} else
						$param = array($body, $id[1]);
					$parser->parse_recursive_tree($param);
					//var_info($param);
					$this->db->query("UPDATE `".$this->registry['key_lang_admin']."_".$id[0]."` SET `body`=? $where WHERE ".$id[0]."_id=?", $param);
				}
			}
			$message .= messageAdmin('Данные успешно сохранены');
		} elseif (isset($this->params['clear'])) {
			$parser = new Parser();
			$id = explode('-', $this->params['clear']);
			$where = "";
			if ($this->db->query("SHOW COLUMNS FROM `".$id[0]."` LIKE 'body_m'"))
				$where = " OR body_m LIKE '%".$protocol."%'";
			$row = $this->db->row("SELECT * FROM `".$this->registry['key_lang_admin']."_".$id[0]."` WHERE ".$id[0]."_id=? AND (body LIKE '%".$protocol."%' $where)", array($id[1]));
			if ($row) {
				$where = "";
                $body = StringLibrary::clear_links(htmlspecialchars_decode($row['body']));
				if (isset($row['body_m'])) {
					$where = ", body_m=?";
                    $body_m = StringLibrary::clear_links(htmlspecialchars_decode($row['body_m']));
					$param = array($body, $body_m, $id[1]);
				} else
					$param = array($body, $id[1]); //var_info($param);
				$parser->parse_recursive_tree($param);
				//$this->db->query("UPDATE `".$this->registry['key_lang_admin']."_".$id[0]."` SET `body`=? $where WHERE ".$id[0]."_id=?", $param);
				$message .= messageAdmin('Данные успешно сохранены');
			}
		}

		return $message;
	}

    public function yandex_market()
    {
        $vars = array();
        $vars['price_retail'] = $this->db->row('SELECT config.* FROM config WHERE config.name="price_retail"');

        $vars['product'] = Product::getObject($this->sets)->find(array(
            'select' => "
            product_photo.photo,
            tb.id, 
            tb.photo, 
            price.price, 
            tb.url, 
            price.discount, 
            tb_lang.name, 
            tb_lang.body, 
            tb3.catalog_id, 
            GROUP_CONCAT(DISTINCT tb3.catalog_id ORDER BY tb3.catalog_id ASC SEPARATOR ', ') as cat_id",
            'join' => "LEFT JOIN product_catalog tb3
		            ON tb3.product_id=tb.id
		            
		            LEFT JOIN ".$_SESSION['key_lang']."_catalog cat
		            ON cat.catalog_id=tb3.catalog_id
		            
		            LEFT JOIN price
		            ON price.product_id=tb.id
		            
		            LEFT JOIN  product_photo
		            ON product_photo.product_id = tb.id
		            ",
            'where' => "tb.active='1'",
            'type' => 'rows',
            'group' => 'tb.id',
            'order' => 'tb.sort ASC',
        ));

        $vars['parents_params'] = $this->db->rows("SELECT pr.id, lp.name FROM params AS pr
        LEFT JOIN ".$_SESSION['key_lang']."_params lp ON pr.id=lp.params_id
        WHERE pr.sub IS NULL");

        $vars['AddPhoto']=[];
        for ($i = 0; $i < count($vars['product']); $i++)
        {
            $idProduct = $vars['product'][$i]['id'];
            array_push($vars['AddPhoto'],$this->db->rows("
            SELECT photo FROM product_photo
            WHERE product_photo.product_id='$idProduct'
             ")) ;

            $vars['product'][$i]['params'] = $this->db->rows("SELECT pr.id, pr.sub, lp.name FROM ".$_SESSION['key_lang']."_params AS lp LEFT JOIN params_product prp ON prp.params_id=lp.params_id LEFT JOIN params pr ON pr.id=lp.params_id WHERE prp.product_id='$idProduct'");
        }

        $vars['catalog'] = Catalog::getObject($this->sets)->find(array('where' => "active='1'",
            'type' => 'rows',
            'order' => 'sort ASC'));
        return $vars;
    }

	function generate_static_sitemap()
	{
		file_put_contents('sitemap.xml', $this->sitemap_generate());
		return messageAdmin('Данные успешно сохранены');
	}

	function sitemap_generate()
	{
		//TODO еще можно доработать список модулей - дать в админке в управление список модулей для сайтмапа
		$this->languages = new Language($this->sets);
		$this->modules = new Modules($this->sets);
		$modules = [
			'menu',
			'pages',
			'catalog',
			'product',
			'news',
		];
		$languages = $this->languages->getAll();

		$sitemap = '<?xml version="1.0" encoding="UTF-8"?>
			<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		//Put menu links
		foreach ($modules as $module) {
			$sitemap .= $this->sitemapModule($languages, $module);
		}
		$sitemap .= '
			</urlset>';
		return $sitemap;
	}

	/**
	 * @param $languages
	 * @param $moduleName
     * @return StringLibrary
	 * Метод генерирует сслыки для краты сайта, конкретного модуля
	 */
	public function sitemapModule($languages, $moduleName)
	{
		// Если вы используете более старую версию движка(меньше 1.1) и не хотите добавлять зависимости в модели
		// вместое checkModule просто поставьте проверку на наличия модуля, $module = $this->modu....
		$return = '';
		$linkArr = [];
		$protocol = getProtocol();
		if ($this->checkModule($moduleName)) {
			$module = $this->modules->find($moduleName);
			$model = ucfirst(mb_strtolower($moduleName, 'UTF-8'));

			$moduleUrl = ($module['url'] === 'menu') ? 'pages' : $module['url'];
			if (!isset($this->{$moduleName})) $this->{$moduleName} = new $model($this->sets);
			//Получим список всех активных записей модуля
			$list = $this->{$moduleName}->getAll();
			// Нет списка или в модуле не прописан url, вернем пустую строку
			// пустой url может встретиться в модулях pages menu и infoblocks в старых версиях движка (до 1.1)
			if ( !count($list) OR !$module['url']) return $return;
			foreach ($languages as $lang) {
				foreach ($list as $row) {
					if ($row['url'] === '/') continue;
					$keyLang = ($lang['default'] === '1') ? '' : '/'.$lang['language'];// для основного языка оставим пустым ключ языка
					$linkArr[] = $protocol.$_SERVER['HTTP_HOST'].$keyLang.'/'.$moduleUrl.'/'.$row['url'];
				}
			}
			// Запись
			if (count($linkArr)){
				foreach ($linkArr as $link){
					$return .= '
					<url><loc>'.$link. '</loc></url>';
				}
			}
		}
		return $return;
	}

	public function set_meta_data($data, $topic)
	{
		//Мета-данные страницы
		if ($_SERVER['REQUEST_URI'] == '')
			$_SERVER['REQUEST_URI'] = '/';
		$protocol = getProtocol();
		$_SERVER['REQUEST_URI'] = current(explode('?', $_SERVER['REQUEST_URI']));
		//echo $topic;
		$url = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		if (substr($url, strlen($url) - 1, 1) == '/' && $_SERVER['REQUEST_URI'] != '/') {
			$lang = '';
			if ($this->registry['key_lang'] != 'ru')
				$lang = '/'.$this->registry['key_lang'];
			$url2 = $protocol.$_SERVER['HTTP_HOST'].$lang.$_SERVER['REQUEST_URI'];
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: '.substr($url2, 0, strlen($url2) - 1));
			exit();
		}
		//echo $url;
		$prefix = $this->find(array('where' => "`type`='2' AND active='1' AND title!=''"));
		$suffix = $this->find(array('where' => "`type`='3' AND active='1' AND title!=''"));
		if (!$prefix) {
			$prefix['title'] = '';
			$prefix['keywords'] = '';
			$prefix['description'] = '';
		}
		if (!$suffix) {
			$suffix['title'] = '';
			$suffix['keywords'] = '';
			$suffix['description'] = '';
		}
		$shorturl = $_SERVER['REQUEST_URI'];
		// Сперва поищем мета данные добавленные конкретно для этого URL
		$row = $this->find(array('where' => "__url:={$url}__ OR __url:={$shorturl}__ AND active='1' AND title!=''"));
		if ($row) {
			$data['title']       = $row['title'];
			$data['keywords']    = $row['keywords'];
			$data['description'] = $row['description'];
			$data['text']        = $row['body'];
			$data['excerpt']     = $row['excerpt'];
            $data['expandable']  = $row['expandable'];
		} elseif (count($data) != 0) {
			// Если массив $data (возвращается контроллером обработчиком) не пустой
			// Для генерации мета данных своего модуля, добавьте новое условие case и создайте свой метод для генерации
			// Давайте осмысленные названия методам
			switch ($topic):
				case 'catalog': $data = $this->setMetaDataCatalog($data);
					break;
				case 'product':	$data = $this->setMetaDataProduct($data);
					break;
				default:        $data = $this->setMetaDataByName($data, $prefix, $suffix);
			endswitch;
		} else {
			// Если в метод не были переданы никакие параметры
			// Мета данные по умолчанию
			$row = $this->find(array('where' => "`type`='1' AND active='1' AND title!=''"));
			$data['title']       = $row['title'];
			$data['keywords']    = $row['keywords'];
			$data['description'] = $row['description'];
		}
		// Добавим к названию номер стрницы если таковая есть
		if (isset($this->params['page']) && $this->params['page'] != '') {
			$data['title']       .= " - {$this->translation['page']} {$this->params['page']}";
			$data['keywords']    .= " - {$this->translation['page']} {$this->params['page']}";
			$data['description'] .= " - {$this->translation['page']} {$this->params['page']}";
		}
		if ($prefix) {
			$data['title']       = $prefix['title'] .' '. $data['title'];
			$data['keywords']    = $prefix['keywords'] .' '. $data['keywords'];
			$data['description'] = $prefix['description'] .' '. $data['description'];
		}
		if ($suffix) {
			$data['title']       = $data['title'].' '.$suffix['title'];
			$data['keywords']    = $data['keywords'] .' '. $suffix['keywords'];
			$data['description'] = $data['description'] .' '. $suffix['description'];
		}
		$data['title']       = str_replace('{{sitename}}', $this->settings['sitename'], $data['title']);
		$data['keywords']    = str_replace('{{sitename}}', $this->settings['sitename'], $data['keywords']);
		$data['description'] = str_replace('{{sitename}}', $this->settings['sitename'], $data['description']);
		return $data;
	}


	/**
	 * @param $data
	 * @return mixed
	 * Генерируем мета данные для товара
	 */
	public function setMetaDataProduct($data)
	{
		$product = $this->find(['where' => "`type`='5' AND active='1' AND title!=''", 'type' => 'row']);
		if ($product) {
			$data['title']       = str_replace('{{name}}', $data['name'], $product['title']);
			$data['keywords']    = str_replace('{{name}}', $data['name'], $product['keywords']);
			$data['description'] = str_replace('{{name}}', $data['name'], $product['description']);
		}
		return $data;
	}
	/**
	 * @param $data
	 * @return mixed
	 * Генерируем мета данные для каталога
	 */
	public function setMetaDataCatalog($data)
	{
		// В случае с каталогом, из catalogController передается текущий каталог, с данными о нем
//		if (isset($data['sub']) && $data['sub'] != 0) {
			// получим родителей, и добавим в мета
			$name_sub = $this->get_sub_cat($data['sub']);
//			if ($name_sub) {
//				if (isset($data['keywords']) && $data['keywords'] == "")
//				    var_info($data['name']);
//					$data['keywords'] = str_replace('@-@', ', ', $name_sub).', '.$data['name'];
//				$data['name'] = $name_sub.'@-@'.$data['name'];
//			}
//		}
		$catalog = $this->find(['where' => "`type`='4' AND active='1' AND title!=''", 'type' => 'row']);
		if ($catalog) {
			$data['title']       = str_replace('{{name}}', str_replace('@-@', ' ',  $data['name']), $catalog['title']);
			$data['keywords']    = str_replace('{{name}}', str_replace('@-@', ', ', $data['name']), $catalog['keywords']);
			$data['description'] = str_replace('{{name}}', str_replace('@-@', ' ',  $data['name']), $catalog['description']);
		}
		return $data;
	}

	/**
	 * @param $data
	 * @param array $prefix
	 * @param array $suffix
	 * @return mixed
	 * Генерируем мета данные по переданному имени, - статья, новость, страница...
	 */
	public function setMetaDataByName($data)
	{
		if ( empty($data['title']) )      $data['title']       = $data['name'];
		if ( empty($data['keywords']) )   $data['keywords']    = $data['name'];
		if ( empty($data['description'])) $data['description'] = $data['name'];
		return $data;
	}

	function get_sub_cat($sub)
	{
		if (!isset($this->catalog)) $this->catalog = Catalog::getObject($this->sets);

		$row = $this->catalog->find([
			'select' => ' tb_lang.name, tb.sub',
			'where'  => "tb.`id` = '".$sub."' AND tb.active ='1'"
		]);

		if ($row) {
			$namecat = $this->get_sub_cat($row['sub']);
			if ($namecat != '')
				$row['name'] = $namecat.'@-@'.$row['name'];
			return $row['name'];
		} else
			return false;
	}
}