<?php

    /*
	* вывод каталога компаний и их данных
	*/

    class FeedbackController extends BaseController
    {

	   protected $params;
	   protected $db;
	   private $left_menu = array(array('title' => 'Запросы на перезвон',
		  'url' => '/admin/feedback/act/callback',
		  'name' => 'callback'),
		  array('title' => 'Уведомления о наличии товара',
			 'url' => '/admin/feedback/act/exist_notification',
			 'name' => 'exist_notification'),
		  array('title' => 'Вопрос менеджеру',
			 'url' => '/admin/feedback/act/question',
			 'name' => 'question')
	   );

	   function __construct($registry, $params)
	   {
		  parent::__construct($registry, $params);
		  $this->tb = "feedback";
		  $this->name = "Обратная связь";
		  $this->registry = $registry;
		  $this->feedback = new Feedback($this->sets);
	   }

	   public function indexAction()
	   {
		  $vars['message'] = '';
		  $vars['name'] = $this->name;
		  if (isset($this->params['act'])) {
			 $act = $this->params['act'] . 'Action';
			 return $this->Index($this->$act());
		  }
		  if (isset($this->params['subsystem'])) return $this->Index($this->feedback->subsystemAction());
		  if (isset($this->registry['access'])) $vars['message'] = $this->registry['access'];
		  if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->feedback->delete();
		  elseif (isset($_POST['update'])) $vars['message'] = $this->feedback->save();
		  elseif (isset($_POST['update_close'])) $vars['message'] = $this->feedback->save();
		  elseif (isset($_POST['add_close'])) $vars['message'] = $this->feedback->add();

		  $vars['list'] = $this->view->Render('view.phtml', $this->feedback->find(array('paging' => true, 'order' => 'tb.date DESC')));
		  $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'menu2' => $this->left_menu));
		  $data['content'] = $this->view->Render('list.phtml', $vars);
		  return $this->Index($data);
	   }

	   public function editAction()
	   {
		  $vars['message'] = '';
		  $vars['edit'] = $this->feedback->find((int)$this->params['edit']);
		  $data['content'] = $this->view->Render('edit.phtml', $vars);
		  return $this->Index($data);
	   }

	   public function callbackAction()
	   {
		  $vars['message'] = '';
		  if (isset($_POST['save'])) {
			 $vars['message'] = $this->feedback->save_callback();
		  }

		  if (isset($this->params['del']) || isset($_POST['del'])) {
			 $vars['message'] .= $this->feedback->delete('callback');
		  }
		  $vars['name'] = 'Запросы на перезвон';
		  $vars['action'] = $this->tb;
		  $vars['path'] = '/act/callback';
		  $vars['list'] = $this->db->rows("SELECT * FROM `callback` ORDER BY `date` DESC");
		  $vars['status'] = $this->db->rows("SELECT * FROM `callback_status` ORDER BY `name` DESC");
		  $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'sub' => 'callback', 'menu2' => $this->left_menu));
		  $data['content'] = $this->view->Render('callback.phtml', $vars);
		  return $data;
	   }

	   public function exist_notificationAction()
	   {
		  $vars['message'] = '';
		  if (isset($_POST['save'])) {
			 $vars['message'] = $this->feedback->save_notification();
		  }

		  if (isset($this->params['del']) || isset($_POST['del'])) {
			 $vars['message'] .= $this->feedback->delete('exist_notification');
		  }
		  $vars['name'] = 'Уведомления о наличии';
		  $vars['action'] = $this->tb;
		  $vars['path'] = '/act/exist_notification';
		  $vars['list'] = $this->db->rows("SELECT * FROM `exist_notification` ORDER BY `date` DESC");
		  $vars['status'] = $this->db->rows("SELECT * FROM `callback_status` ORDER BY `name` DESC");
		  $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'sub' => 'exist_notification', 'menu2' => $this->left_menu));
		  $data['content'] = $this->view->Render('callback1.phtml', $vars);
		  return $data;
	   }

	   public function questionAction()
	   {
		  $vars['message'] = '';
		  if (isset($_POST['save'])) {
			 $vars['message'] = $this->feedback->save_question();
		  }

		  if (isset($this->params['del']) || isset($_POST['del'])) {
			 $vars['message'] .= $this->feedback->delete('question');
		  }
		  $vars['name'] = 'Вопрос менеджеру';
		  $vars['action'] = $this->tb;
		  $vars['path'] = '/act/question';
		  $vars['list'] = $this->db->rows("SELECT * FROM `question` ORDER BY `date` DESC");
		  $vars['status'] = $this->db->rows("SELECT * FROM `callback_status` ORDER BY `name` DESC");
		  $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'sub' => 'question', 'menu2' => $this->left_menu));
		  $data['content'] = $this->view->Render('callback2.phtml', $vars);
		  return $data;
	   }
    }