<?php

    class Feedback extends Model
    {
	   static $table = 'feedback'; //Главная талица
	   static $name = 'Обратная связь'; // primary key

	   public function __construct($registry)
	   {
		  parent::getInstance($registry);
	   }

	   //для доступа к классу через статичекий метод
	   public static function getObject($registry)
	   {
		  return new self::$table($registry);
	   }

	   public function save_callback()
	   {
		  $message = '';
		  if (isset($this->registry['access'])) $message = $this->registry['access'];
		  else {
			 if (isset($_POST['status'], $_POST['save_id'], $_POST['manager'])) {
				$count = count($_POST['save_id']) - 1;
				for ($i = 0; $i <= $count; $i++) {
				    $param = array($_POST['status'][$i], $_POST['manager'][$i], $_POST['save_id'][$i]);
				    $this->db->query("UPDATE `callback` SET `status`=?, `manager`=? WHERE `id`=?", $param);
				}
				$message .= messageAdmin('Данные успешно сохранены');
			 } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
		  }
		  return $message;
	   }

	   public function save_notification()
	   {
		  $message = '';
		  if (isset($this->registry['access'])) $message = $this->registry['access'];
		  else {
			 if (isset($_POST['status'], $_POST['save_id'], $_POST['manager'])) {
				$count = count($_POST['save_id']) - 1;
				for ($i = 0; $i <= $count; $i++) {
				    $param = array($_POST['status'][$i], $_POST['manager'][$i], $_POST['save_id'][$i]);
				    $this->db->query("UPDATE `exist_notification` SET `status`=?, `manager`=? WHERE `id`=?", $param);
				}
				$message .= messageAdmin('Данные успешно сохранены');
			 } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
		  }
		  return $message;
	   }

	   public function save_question()
	   {
		  $message = '';
		  if (isset($this->registry['access'])) $message = $this->registry['access'];
		  else {
			 if (isset($_POST['status'], $_POST['save_id'], $_POST['manager'])) {
				$count = count($_POST['save_id']) - 1;
				for ($i = 0; $i <= $count; $i++) {
				    $param = array($_POST['status'][$i], $_POST['manager'][$i], $_POST['save_id'][$i]);
				    $this->db->query("UPDATE `question` SET `status`=?, `manager`=? WHERE `id`=?", $param);
				}
				$message .= messageAdmin('Данные успешно сохранены');
			 } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
		  }
		  return $message;
	   }

	   public function delete($table = 'feedback')
	   {
		  $message = '';
		  if (isset($this->params['delete']) && $this->params['delete'] != '') {
			 if ($this->db->query("DELETE FROM `" . $table . "` WHERE `id`=?", array($this->params['delete']))) $message = messageAdmin('Запись успешно удалена');
		  }
		  return $message;
	   }
    }