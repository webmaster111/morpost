<?php

    class FeedbackController extends BaseController
    {
	   protected $feedback;

	   public function __construct($registry, $params)
	   {
		  parent::__construct($registry, $params);
		  $this->tb = Feedback::$table;
		  $this->feedback = new Feedback($this->sets);
	   }


	   ///Send message
	   function feedbackAction()
	   {
		  $response = array('status' => 'error', 'type' => 'toast');
		  if (isset($_POST['form_type'])) {
			 $type = trim($_POST['form_type']);
			 $letter = $this->$type($response);
			 if ($response['status'] === 'success') {
				Mailer::SendToAdmin($letter['subject'], $letter['text']);
				$this->feedback->insert(array(
				    'name' => $_POST['name'] ?? '',
				    'email' => $_POST['email'] ?? '',
				    'phone' => $_POST['phone'] ?? '',
				    'subject' => $letter['subject'],
				    'text' => $letter['text']
				));
			 }
		  }
		  return json_encode($response);
	   }

	   private function contact(array &$data): array
	   {
		  $data['type'] = $_POST['error-type'] ?? 'modal';
		  $letter = array();
		  if (isset($_POST['name'], $_POST['phone'])) {
			 //$data['message'] = !GoogleCaptcha::check() ? "<div class='alert alert-danger'>" . $this->translation['wrong-recaptcha'] . "</div>" : "";
			 $_POST['phone'] = html_entity_decode($_POST['phone']);
//			 $data['message'] .= Validate::check($_POST['phone'], $this->translation, 'phone');
			 $data['message'] .= Validate::check($_POST['text'], $this->translation, 'text');
			 if ($data['message'] === '') {
				$data['status'] = 'success';
				$data['message'] = $this->translation['message_sent'];
				$letter['subject'] = 'Обратная связь';
				$letter['text'] = "<b>Обратная связь</b>
                           <br />ФИО : {$_POST['name']}
                           <br />Телефон : <a href='tel:{$_POST['phone']}'>{$_POST['phone']}</a>
                           <br />Сообщение : {$_POST['text']}";
			 }
		  }
		  return $letter;
	   }

        private function fast(array &$data): array
        {
            $data['type'] = $_POST['error-type'] ?? 'modal';
            $letter = array();
            if (isset($_POST['name'], $_POST['phone'])) {
                //$data['message'] = !GoogleCaptcha::check() ? "<div class='alert alert-danger'>" . $this->translation['wrong-recaptcha'] . "</div>" : "";
                $_POST['phone'] = html_entity_decode($_POST['phone']);
//			 $data['message'] .= Validate::check($_POST['phone'], $this->translation, 'phone');
                    $data['status'] = 'success';
                    $data['message'] = $this->translation['message_sent'];
                    $letter['subject'] = 'Быстрая форма связи (15 секунд)';
                    $letter['text'] = "<b>Быстрая форма связи (15 секунд)</b>
                           <br />ФИО : {$_POST['name']}
                           <br />Телефон : <a href='tel:{$_POST['phone']}'>{$_POST['phone']}</a>";
            }
            return $letter;
        }

        private function cooperation(array &$data): array
        {
            $data['type'] = $_POST['error-type'] ?? 'modal';
            $letter = array();
            if (isset($_POST['name'], $_POST['phone'])) {
                //$data['message'] = !GoogleCaptcha::check() ? "<div class='alert alert-danger'>" . $this->translation['wrong-recaptcha'] . "</div>" : "";
                $_POST['phone'] = html_entity_decode($_POST['phone']);
//			 $data['message'] .= Validate::check($_POST['phone'], $this->translation, 'phone');
                    $data['status'] = 'success';
                    $data['message'] = $this->translation['message_sent'];
                    $letter['subject'] = 'Форма сотрудничества';
                    $letter['text'] = "<b>Форма сотрудничества</b>
                           <br />Контактное лицо : {$_POST['name']}
                           <br />Название компании : {$_POST['company_name']}
                           <br />Телефон : <a href='tel:{$_POST['phone']}'>{$_POST['phone']}</a>";
            }
            return $letter;
        }

        private function apply(array &$data): array
        {
            $params = array();
            $data['type'] = $_POST['error-type'] ?? 'modal';
            $letter = array();
            $list_service = '';
            foreach ($_POST['list_service_check'] as $service){
                $list_service .= '<br>Услуга - '.$service;
            }
            parse_str($_POST['data'], $params);
            if (isset($_POST['name'], $_POST['phone'])) {
                //$data['message'] = !GoogleCaptcha::check() ? "<div class='alert alert-danger'>" . $this->translation['wrong-recaptcha'] . "</div>" : "";
                $_POST['phone'] = html_entity_decode($_POST['phone']);
//			 $data['message'] .= Validate::check($_POST['phone'], $this->translation, 'phone');
                $data['status'] = 'success';
                $data['message'] = $this->translation['message_sent'];
                $letter['subject'] = 'Быстрая форма связи (15 секунд)';
                $letter['text'] = "<b>Подать заявку</b>
                           <br />Список услуг: {$list_service}
                           <br />Должность: {$_POST['list_staff_check'][0]}
                           <br />Сроки: {$_POST['list_duration_check'][0]}
                           <br />ФИО : {$_POST['name']}
                           <br />Почта : {$_POST['email']}
                           <br />Телефон : <a href='tel:{$_POST['phone']}'>{$_POST['phone']}</a>";
            }
            return $letter;
        }

	   function callback(&$data)
	   {
		  $data['type'] = isset($_POST['error-type']) ? $_POST['error-type'] : 'modal';
		  $letter = array();
		  if (isset($_POST['name'], $_POST['phone'])) {
			 $_POST['phone'] = html_entity_decode($_POST['phone']);
			 $data['message'] = !GoogleCaptcha::check() ? $this->translation['wrong-recaptcha'] : "";
			 $data['message'] .= Validate::check($_POST['phone'], $this->translation, 'phone');
			 if ($data['message'] == "") {
				$data['status'] = 'success';
				$data['message'] = $this->translation['message_sent_callback'];
				$letter['subject'] = 'Перезвоните мне';
				$letter['text'] = "<b>Перезвоните мне </b>
                           <br />Имя : {$_POST['name']}
                           <br />Телефон : <a href='tel:{$_POST['phone']}'>{$_POST['phone']}</a>";
			 }
		  }
		  return $letter;
	   }
    }