<?

class Sitemapxml extends Model
{

    static $table='sitemapxml'; // Таблица с настройками
    static $name='SitemapXML';
    static $arr_chunk = 50000; // Максимальное число url-ов в файле (участвует в разбивке урлов на несколько файлов)
    static $basic_file = 'sitemap.xml'; // Имя главного xml-файла
    public $modules = array(
        'pages' => 'Страницы',
        'news' => 'Новости',
        'article' => 'Статьи',
        'catalog' => 'Категории',
        'product_status' => 'Статусы товаров',
        'product' => 'Товары',
        'params' => 'Фильтры'
    );

    public function __construct($registry)
    {
        parent::getInstance($registry);
    }

    // Для доступа к классу через статичекий метод
    public static function getObject($registry)
    {
        return new self::$table($registry);
    }

    /*
     * Если таблица с настройками пуста задаем значения по умолчанию
     * */
    public function setDefaultSettings(){
        // Генерируем поля таблицы для настроек модуля
        foreach ($this->modules as $module => $name){
            $settings[] = array(
                'param' => $module,
                'name' => $name,
                'active' => '1',
                'excluded' => '',
                'priority' => '1.0',
                'start_url' => '',
                'file_name' => 'sitemap-' . $module . '.xml'
            );
        }
        // Устанвливаем значения
        $this->setSettingsInBD($settings);
        return true;
    }

    /*
     * Запись значений в базу данных
     * */
    public function setSettingsInBD($settings){
        foreach ($settings as $param){
            $this->db->query("INSERT INTO " . self::$table . " SET  param='" . $param['param'] . "', name='" . $param['name'] . "', active='" . $param['active'] . "', excluded='" . $param['excluded'] . "', priority='" . $param['priority'] . "', start_url='" . $param['start_url'] . "', file_name='" . $param['file_name'] . "'" );
        }
    }

    /*
     * Обновляем значения в базе данных
     * */
    public function updateSettingsInBD($settings){
        foreach ($settings as $param){
            $this->db->query("UPDATE " . self::$table . " SET  param='" . $param['param'] . "', name='" . $param['name'] . "', active='" . $param['active'] . "', excluded='" . $param['excluded'] . "', priority='" . $param['priority'] . "', start_url='" . $param['start_url'] . "', file_name='" . $param['file_name'] . "' WHERE param='" . $param['param'] . "'" );
        }
    }

    /*
     * Получение настроек с таблицы
     * */
    public function getSettings()
    {
        // Проверяем существует ли таблица
        $find_table = $this->db->row("SHOW TABLES like '". self::$table . "'");

        if( !empty($find_table) ) // Если таблица существует, получаем настройки
        {
            return $this->db->rows("SELECT * FROM " . self::$table);
        } else { // Иначе создаем таблицу
            $this->db->query("CREATE TABLE `" . self::$table . "` (
                              `id` int(11) NOT NULL,
                              `param` varchar(64) DEFAULT NULL,
                              `name` varchar(64) DEFAULT NULL,
                              `active` enum('0','1') DEFAULT '0',
                              `excluded` text,
                              `priority` float DEFAULT '1',
                              `start_url` varchar(64) DEFAULT NULL,
                              `file_name` varchar(60) DEFAULT NULL
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
            $this->db->query("ALTER TABLE `" . self::$table . "` ADD PRIMARY KEY (`id`)");
            $this->db->query("ALTER TABLE `" . self::$table . "` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");
            return array();
        }

    }

    /*
     * Обновляем настройки в базе данных и генерируем xml-файлы
     * */
    public function updateSettings($params)
    {
        $message = '';
        $settings = array();
        // Перебираем настройки модуля
        foreach ($params as $key => $param){
            $active = ( isset($param['active']) && $param['active'] == "on" ) ? '1' : '0';
            $settings[] = array(
                'param' => $key,
                'name' => $this->modules[$key],
                'active' => $active,
                'excluded' => $param['excluded'],
                'priority' => $param['priority'],
                'start_url' => $param['start_url'],
                'file_name' => $param['file_name']
            );
        }
        $this->updateSettingsInBD($settings); // Вызываем обновление в базе данных
        if( $this->generateSitemaps() ) // Вызов генерации xml-файлов
        {
            $message .=  messageAdmin('sitemap.xml обновлен');
        }
        return $message;
    }

    /*
     * Метод генерации общего sitemap.xml файла и вызова генерации дочерних файлов
     * */
    public function generateSitemaps()
    {
        $base_sitemap_xml = ""; // Контент sitemap.xml
        $settings = $this->getSettings(); // Получаем настройки
        $base_sitemap_xml .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        $base_sitemap_xml .= "<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
        // Создаем дочерние файлы и записываем их в общий sitemap.xml
        foreach ( $settings as $param ){
            if( $param['active'] == '1'){
                // Получаем урлы текущего модуля
                $urls = $this->getModuleUrls($param['param'], explode(';', $param['excluded']) );
                if( 0 < count($urls) ) { // Если в urls не пустой массив
                    if( count($urls) <= self::$arr_chunk ) // Если в urls меньше знаначений чем указано в $arr_chunk создается только один файл для модуля
                    {
                        $content = $this->getSitemapContent($urls, $param['start_url'], $param['priority'], $param['param']); // Получаем контент в xml формате для текущего модуля
                        $this->generateSitemap($param['file_name'], $content); // Создаем файл xml для текущего модуля
                        // Записываем созданый xml-файл текущего модуля в общий sitemap.xml
                        $base_sitemap_xml .= $this->addFileNameInSitemap($param['file_name']);
                    } else { // Иначе разбиваем на несколько файлов
                        $i = 0;
                        foreach(array_chunk($urls, self::$arr_chunk) as $sub_urls) // Перебираем разбитые на части массивы url-ов
                        {
                            $i += 1;
                            //Генерируем имя файла с числовым указаниме на разбитую часть
                            $file_name = str_replace('.xml', '-' . $i . '.xml', $param['file_name']);
                            $content = $this->getSitemapContent($sub_urls, $param['module']); // Получаем контент в xml формате
                            $this->generateSitemap($file_name, $content); // Создаем файл xml
                            // Записываем созданый xml-файл в общий sitemap.xml
                            $base_sitemap_xml .= $this->addFileNameInSitemap($file_name);
                        }
                    }
                }
            }
        }

        $base_sitemap_xml .= "</sitemapindex>\n";
        $this->generateSitemap(self::$basic_file, $base_sitemap_xml); // Создаем sitemap.xml

        return true;
    }

    public function addFileNameInSitemap($file_name)
    {
        $output = $base_url = '';
        if( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ){
            $base_url .= 'https://' . $_SERVER['SERVER_NAME'];
        }else{
            $base_url .= 'http://' . $_SERVER['SERVER_NAME'];
        }
        $output .= "\t<sitemap>\n";
        $output .= "\t\t<loc>" . $base_url . '/' . $file_name . "</loc>\n";
        $output .= "\t\t<lastmod>" . date("Y-m-d") . "</lastmod>\n";
        $output .= "\t</sitemap>\n";
        return $output;
    }

    /*
     * Создаем xml-файл с переданым именем и контентом
     * */
    public function generateSitemap($name, $content)
    {
        $file = fopen($name, 'w') or die("не удалось создать файл");
        fwrite($file, $content);
        fclose($file);
        return true;
    }

    //Генерируем контент для модуля
    public function getSitemapContent($urls, $start_url, $priority, $module)
    {
        $content = '';
        $content .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        $content .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
        // главную страницу
        if( $module == 'pages' ){
            $content .= $this->makeModuteItem($start_url, '', 1);
        }
        // Генерируем контент
        foreach ( $urls as $url ){
            $content .= $this->makeModuteItem($start_url, $url, $priority);
        }
        $content .= "</urlset>";
        return $content;
    }

    public function makeModuteItem($start_url, $url, $priority){
        $output = '';
        $output .= "\t<url>\n";
        $output .= "\t\t<loc>" . $start_url . $url['url'] . "</loc>\n";
        $output .= "\t\t<lastmod>" . date("Y-m-d") . "</lastmod>\n";
        $output .= "\t\t<priority>" . $priority . "</priority>\n";
        $output .= "\t</url>\n";
        return $output;
    }

    /*
     * Получаем урлы исходя от модуля
     * */
    public function getModuleUrls($module, $excluded)
    {
        switch ($module){
            case 'pages':
                // Получаем урлы страниц из модуля pages
                $page_url = $this->db->rows( "SELECT id, url FROM " . $module . " WHERE active='1'" );
                // Получаем урлы страниц из модуля menu
                $menu_url = $this->db->rows( "SELECT id, url FROM menu WHERE active='1' AND url<>'' AND url<>'/'" );
                // Объединяем урлы урлы
                $urls = array_merge($page_url, $menu_url);
                break;
            case 'product_status':
                // Получаем урлы страниц из модуля Статусы товаров
                $urls = $this->db->rows( "SELECT id, url FROM " . $module );
                break;
            case 'params':
                // Получаем урлы страниц из модуля Meta исходя от наличия в урле казания на принадлежность к фильтрам
                $urls = $this->db->rows( "SELECT id, url FROM meta WHERE active='1' AND url like '%?filters=%'" );
                break;
            default:
                // Получаем урлы страниц из модуля
                $urls = $this->db->rows( "SELECT id, url FROM " . $module . " WHERE active='1'" );
        }

        foreach ($excluded as $item) // Убираем из масива исключенные страницы
        {
            $key = array_search($item, array_column($urls, 'url')); // Поиск элемента массива url которого нужно исключить
            if( $key !== false ){
                unset($urls[$key]); // Удаляем найденый элемент
                $urls = $this->sortedUrls($urls); // Сортируем масив во избежания ошибок поиска элемента
            }
        }

        $urls = $this->sortedUrls($urls); // Подготовка порядка url-ов для записи в файл
        return $urls;
    }

    /*
     * Сортировка урлов по ID
     * */
    public function sortedUrls($urls){
        usort($urls, function($a, $b){
            return ($a["id"] <  $b["id"]) ? 1 : -1;
        });
        return $urls;
    }

}