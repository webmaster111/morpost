<?
class SitemapxmlController extends BaseController
{
    protected $params;
    protected $db;
    public $sitemap;

    function __construct($registry,$params){
        parent::__construct($registry,$params);
        $this->tb="sitemapxml";
        $this->name="SitemapXML";
        $this->registry = $registry;
        $this->sitemap = new Sitemapxml($this->sets);
    }

    public function indexAction()
    {
        $vars['message'] = '';
        $vars['name'] = $this->name;
        if (isset($_POST['update'])){
            $vars['message'] = $this->sitemap->updateSettings($_POST['settings']);
        }
        $settings = $this->sitemap->getSettings();
        if ( empty($settings) ){
            $this->sitemap->setDefaultSettings();
            $settings = $this->sitemap->getSettings();
        }
        $settings_params['settings'] = $settings;
        $vars['settings'] = $this->view->Render('settings.phtml', $settings_params);
        $data['content'] = $this->view->Render('view.phtml', $vars);
        return $this->Index($data);
    }

}