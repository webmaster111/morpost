<?php

class Branches extends Model
{
  static $table = 'branches';
  static $name = 'Филиалы';

  public function __construct($registry)
  {
    parent::getInstance($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  public function add()
  {
    $message = '';
    if (isset($_POST['active'], $_POST['name'])) {
      $sort = $this->db->cell("SELECT MAX(sort)+1 FROM " . self::$table . "`");
      $id = $this->db->insert_id("INSERT INTO `" . self::$table . "` SET active=?,`address`=?,`phones`=?,`emails`=? ,`map`=?, `map_zoom`=? ,`sort`=?",
          array($_POST['active'], $_POST['address'], implode('|&|', array_filter($_POST['phones'], 'rempty')), implode('|&|', array_filter($_POST['emails'], 'rempty')), $_POST['map'], (int)$_POST['map_zoom'], $sort ? $sort : 1));
      foreach ($this->language as $lang)
        $this->db->query("INSERT INTO `" . $lang['language'] . "_" . self::$table . "` SET `name`=?, `" . self::$table . "_id`=?", [$_POST['name'], $id]);
      $message .= messageAdmin('Данные успешно добавлены');
    } else $message .= messageAdmin('Заполнены не все обязательные поля', 'error');
    return $message;
  }

  public function save()
  {
    $message = '';
    if (isset($this->registry['access'])) return $this->registry['access'];
    if (!is_array($_POST['save_id']) && isset($_POST['active'], $_POST['id'])) {
      $this->db->query("UPDATE `" . self::$table . "` SET `active`=?,`address`=?,`phones`=?,`emails`=? ,`map`=?, `map_zoom`=?, `show_photo`=? WHERE id=?",
          [$_POST['active'], $_POST['address'], implode('|&|', array_filter($_POST['phones'], 'rempty')), implode('|&|', array_filter($_POST['emails'], 'rempty')), $_POST['map'], (int)$_POST['map_zoom'], $_POST['show_photo'], $_POST['id']]);
      $this->db->query("UPDATE `" . $this->registry['key_lang_admin'] . "_" . self::$table . "` SET `name`=? WHERE `" . self::$table . "_id`=?", [$_POST['name'], $_POST['id']]);
      $message .= messageAdmin('Данные успешно сохранены');
    } else if (isset($_POST['save_id']) && is_array($_POST['save_id'])) {
      if (isset($_POST['save_id'], $_POST['sort'])) {
        for ($i = 0; $i <= count($_POST['save_id']) - 1; $i++)
          $this->db->query("UPDATE `" . self::$table . "` SET `sort`=? WHERE id=?", [$_POST['sort'][$i], $_POST['save_id'][$i]]);
        $message .= messageAdmin('Данные успешно сохранены');
      } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
    } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
    return $message;
  }

}
