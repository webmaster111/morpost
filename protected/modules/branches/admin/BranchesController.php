<?

class BranchesController extends BaseController
{
  protected $branches;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Branches::$table;
    $this->name = "Филиалы компании";
    $this->branches = new Branches($this->sets);
  }

  public function indexAction()
  {
    $vars['message'] = '';
    $vars['name'] = $this->name;
    if (isset($this->params['subsystem'])) return $this->Index($this->branches->subsystemAction());
    if (isset($this->registry['access'])) $vars['message'] = $this->registry['access'];
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->branches->delete($this->tb);
    elseif (isset($_POST['update'])) $vars['message'] = $this->branches->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->branches->save();
    elseif (isset($_POST['add_close'])) $vars['message'] = $this->branches->add();
    $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->branches->find(['type' => 'rows', 'order' => 'tb.sort'])]);
    $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name]);
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = isset($_POST['add']) ? $this->branches->add() : '';
    $vars['width'] = $this->settings['branch_width'];
    $vars['height'] = $this->settings['branch_height'];
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = isset($_POST['update']) ? $this->branches->save() : '';
    $vars['edit'] = $this->branches->find((int)$this->params['edit']);
    $dir = Dir::createDir($this->params['edit'], '', $this->tb);
    $vars['path'] = $dir[0];
    $vars['width'] = $this->settings['branch_width'];
    $vars['height'] = $this->settings['branch_height'];
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }
}