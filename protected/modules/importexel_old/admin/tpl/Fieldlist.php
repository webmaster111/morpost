	<?php

			$export_fields = array(
		"export_id"=>"ID товара",
		"export_name"=>"Название",
		"export_body_m"=>"Краткое описание",
		"export_body"=>"Описание",
		"export_code"=>"Артикул",
		"export_image"=>"Изображение большое",
		"export_basePrice"=>"Закупочная цена",
		"export_priceMargin"=>"Наценка",
		"export_price"=>"Цена",
		"export_discount"=>"Скидка",
		"export_stock"=>"В наличии",
		"export_catalog_id"=>"ID категории",
		"export_cat_name" => "Название категории",
		//"export_delivery_times"=>"Сроки доставки",
		//"export_delivery_price"=>"Стоимость доставки",
		"export_params"=>"Фильтры",
		//"export_photos_id"=>"Фотогалерея",
		"export_provider_name"=>"Поставщик"
	);
	?>