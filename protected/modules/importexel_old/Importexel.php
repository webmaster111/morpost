<?php
	include MODULES . 'importexel/SimpleXLSX.php';

	class Importexel extends Model
	{
		static $table = 'catalog'; //Главная талица
		static $name = 'Каталог'; // primary key

		public function __construct($registry)
		{
			parent::getInstance($registry);
		}

		//для доступа к классу через статичекий метод
		///поля: артикул / название товара / текущий каталог / родительский каталог / Цена / краткое описание / дополнительное описание
		public static function getObject($registry)
		{
			return new self::$table($registry);
		}


		//Выводим шапки полей экспорта в Excell
		function abc()
		{

			$arr = array(
				'export_id',
				'export_name',
				'export_body_m',
				'export_body',
				'export_code',
				'export_image',
				'export_basePrice',
				'export_priceMargin',
				'export_price',
				'export_discount',
				'export_stock',
				'export_catalog_id',
				'export_cat_name',
				'export_delivery_times',
				'export_delivery_price',
				'export_params',
				'export_photos_id',
				'export_provider_name'
			);
			$arr2 = array();
			foreach ($arr as $row) {
				if (isset($_POST[$row])) {
					$arr['' . $_POST[$row]] = $row;
					if ($row == 'export_id') $arr[$_POST[$row] . '2'] = 'ID товара';
					elseif ($row == 'export_name') $arr[$_POST[$row] . '2'] = 'Название';
					elseif ($row == 'export_body_m') $arr[$_POST[$row] . '2'] = 'Краткое описание';
					elseif ($row == 'export_body') $arr[$_POST[$row] . '2'] = 'Описание';
					elseif ($row == 'export_code') $arr[$_POST[$row] . '2'] = 'Артикул';
					elseif ($row == 'export_image') $arr[$_POST[$row] . '2'] = 'Изображение';
					elseif ($row == 'export_basePrice') $arr[$_POST[$row] . '2'] = 'Цена закупочная';
					elseif ($row == 'export_priceMargin') $arr[$_POST[$row] . '2'] = 'Наценка';
					elseif ($row == 'export_price') $arr[$_POST[$row] . '2'] = 'Цена';
					elseif ($row == 'export_discount') $arr[$_POST[$row] . '2'] = 'Скидка';
					elseif ($row == 'export_stock') $arr[$_POST[$row] . '2'] = 'В наличии';
					elseif ($row == 'export_catalog_id') $arr[$_POST[$row] . '2'] = 'ID категории';
					elseif ($row == 'export_cat_name') $arr[$_POST[$row] . '2'] = 'Название категории';
					elseif ($row == 'export_delivery_times') $arr[$_POST[$row] . '2'] = 'Сроки доставки';
					elseif ($row == 'export_delivery_price') $arr[$_POST[$row] . '2'] = 'Стоимость доставки';
					elseif ($row == 'export_params') $arr[$_POST[$row] . '2'] = 'Фильтры';
					elseif ($row == 'export_photos_id') $arr[$_POST[$row] . '2'] = 'Фотогалерея';
					elseif ($row == 'export_provider_name') $arr[$_POST[$row] . '2'] = 'Поставщик';
				}
			}
			require_once('admin/tpl/Letterlist.php');
			foreach($letterlist as $letter) {
				if (!isset($arr[$letter])) $arr[$letter] = '';
			}

			return $arr;
		}

		function export_products()
		{
			$path = "files/tmp/";
			//, c_parent.catalog_id as parent_id, c_parent.name as parent_name
			/*$query="SELECT tb.id, tb.code, tb.stock, tb2.name, price.price, tb2.body_m, tb2.body, c2.name as cat_name, c.id as catalog_id
					FROM product tb

					LEFT JOIN ru_product tb2 ON tb.id=tb2.product_id

					LEFT JOIN product_catalog pc ON tb.id=pc.product_id

					LEFT JOIN catalog c ON c.id=pc.catalog_id

					LEFT JOIN ru_catalog c2 ON c.id=c2.catalog_id

					LEFT JOIN ru_catalog c_parent ON c.sub=c_parent.catalog_id

					LEFT JOIN price ON tb.id=price.product_id

					GROUP BY tb.id
					ORDER BY tb.sort ASC, tb.id DESC";*/
			if ($_POST['cat_id'] != 0) {
				$where = "WHERE c.id='{$_POST['cat_id']}'";
				/*$res = $this->db->rows("SELECT id FROM catalog WHERE sub='{$_POST['cat_id']}' AND active='1'");
				foreach($res as $row)
				{
					$where.=" OR pc.catalog_id='{$row['id']}' OR pc_sub.catalog_id='{$row['id']}'";
				}*/
			}
			$query = "SELECT tb.id AS export_id,
						tb.code AS export_code,
						tb.photo AS export_photos_id,
						tb.stock AS export_stock,
						tb2.name AS export_name,
						tb_price.purch_price AS export_basePrice,
						tb_price.margin AS export_priceMargin,
						tb_price.price AS export_price,
						tb2.body_m AS export_body_m,
						tb2.body AS export_body,
						prov.name AS export_provider_name,
						c2.name as export_cat_name,
						GROUP_CONCAT(DISTINCT pc2.catalog_id ORDER BY pc2.catalog_id ASC SEPARATOR ', ') AS export_catalog_id,
						GROUP_CONCAT(DISTINCT pp.params_id ORDER BY pp.params_id ASC SEPARATOR ', ') AS export_params
						FROM product tb
						LEFT JOIN ru_product tb2 ON tb.id=tb2.product_id
						LEFT JOIN providers prov ON prov.id=tb.provider_id
						LEFT JOIN product_catalog pc ON tb.id=pc.product_id
						LEFT JOIN product_catalog pc2 ON tb.id=pc2.product_id
						LEFT JOIN catalog c ON c.id=pc.catalog_id
						LEFT JOIN ru_catalog c2 ON c.id=c2.catalog_id
						LEFT JOIN (SELECT * FROM `price` ORDER BY `sort` ASC, id DESC) as `tb_price` ON `tb_price`.product_id=tb.id
						LEFT JOIN params_product pp ON pp.product_id=tb.id

						$where

						GROUP BY tb.id
						ORDER BY tb.sort DESC, tb.id ASC";
			$res = $this->db->rows($query);

			//Подгружаем названия полей в Excell для выгрузки
			require_once('admin/tpl/Letterlist.php');

			if (count($res) != 0) {
				$file = $path . "products.csv";
				if (!file_exists($file)) {
					$fp = fopen($file, "w");
					fwrite($fp, "");
					fclose($fp);
				}
				$arr = $this->abc();
				$fd = fopen($file, "w");
				$order = '';
				foreach($letterlist as $letter) {
					if($letter == end($letterlist)) {
						$col = $letter.'2';
						$order .= $arr[$col] . ";\r\n";
					} else {
						$col = $letter.'2';
						$order .= $arr[$col] . ";";
					}
				}
				fwrite($fd, iconv('UTF-8', 'WINDOWS-1251', $order));
				if ($_POST['export_id'])
					foreach ($res as $row) {
						$row['export_name'] = $this->parse2($row['export_name']);
						$row['export_cat_name'] = $this->parse2($row['export_cat_name']);
						$row['export_body_m'] = $this->parse2($row['export_body_m']);
						$row['export_body'] = $this->parse2($row['export_body']);
						$row['export_provider_name'] = $this->parse2($row['export_provider_name']);
						$row['export_code'] = $this->parse2($row['export_code']);
						$row['export_delivery_times'] = $this->parse2($row['export_delivery_times']);
						$row['export_delivery_price'] = $this->parse2($row['export_delivery_price']);
						$row['export_basePrice'] = str_replace('.', ',', $row['export_basePrice']);
						$row['export_priceMargin'] = str_replace('.', ',', $row['export_priceMargin']);
						$row['export_price'] = str_replace('.', ',', $row['export_price']);
						foreach($letterlist as $letter) {
							if (!isset($row[$arr[$letter]])) $row[$arr[$letter]] = '';
						}

						$dir = Dir::createDir($row['export_id']);
						if (file_exists($dir[0] . "{$row['export_id']}.jpg")) $row['export_image'] = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $dir[0] . "{$row['export_id']}.jpg";
						else $row['export_image'] = '';
						$order = '';
						foreach($letterlist as $letter) {
							if($letter == end($letterlist)) {
								$col = $letter;
								$order .= $row[$arr[$letter]] . ";\r\n";
							} else {
								$col = $letter;
								$order .= $row[$arr[$letter]] . ";";
							}
						}

						fwrite($fd, $order);
					}
				fclose($fd);
				header("Content-Type: application/octet-stream");
				header("Accept-Ranges: bytes");
				header("Content-Length: " . filesize($file));
				header("Content-Disposition: attachment; filename=products.csv");
				readfile($file);

			}
		}

		function parse2($str)
		{
			/*array('&','&#038;'),
				array('"','&#034;'),
				array("'",'&#039;'),
				array('%','&#037;'),
				array('(','&#040;'),
				array(')','&#041;'),
				array('+','&#043;'),
				array('<','&lt;'),
				array('>','&gt;');*/
			$replace = array(
				"&#038;" => "&",
				"&#034;" => '"',
				"&#039;" => "'",
				"&#037;" => "%",
				"&#040;" => "(",
				"&#041;" => ")",
				"&#043;" => "+",
				"&lt;"   => "<",
				"&gt;"   => ">",
				"&nbsp;" => " ",
				";"      => "");
			$str = iconv('UTF-8', 'WINDOWS-1251', htmlspecialchars_decode($str));
			$str = strtr($str, $replace);
			//$str = strip_tags($str);
			$str = str_replace(';', '', $str);
			$str = str_replace("\r\n", '', $str);
			$str = str_replace("\n", '', $str);
			$str = str_replace("\r", '', $str);
			$str = str_replace("
", '', $str);
			return $str;
		}

		function import_products($data = array())
		{
			$date = date("Y-m-d H:i:s");
			$cols = $this->select_cols();

			//Вытаскиваем все поля из временной таблицы и в том числе вычисляем айдишники Поставщиков
			$res = $this->db->rows("SELECT " . $cols['select'] . " FROM `tmp_products` ");

			$y = 0;
			if ($_POST['id'] == '') $_POST['id'] = 'id';
			if ($_POST['catalog_id'] == '') $_POST['catalog_id'] = 'catalog_id';
			$parser = new Parser();
			$parser->parse_recursive_tree($res);

			//////////////////////
			//Переменные where и where2 - это список значений которые записываются, а не условий куда
			/////////////////////////

			foreach ($res as $row) {
				$where = "";
				$where2 = "";
				//Brends
				if (isset($_POST['brend_name']) && $_POST['brend_name'] != '') {
					if (isset($_POST['brend_id']) && $_POST['brend_id'] != '') $brend = $this->db->row("SELECT id FROM brend WHERE id=?", array($row[$_POST['brend_id']]));
					else $brend = $this->db->row("SELECT brend_id as id FROM ru_brend WHERE name=?", array($row[$_POST['brend_name']]));
					if (!isset($brend['id'])) {
                        $url = StringLibrary::translit($row[$_POST['brend_name']]);
						$www = "";
						if (isset($row[$_POST['brend_id']])) $www = "id='" . $row[$_POST['brend_id']] . "', ";
						$insert_id = $this->db->insert_id("INSERT INTO brend SET $www url='$url'");
						foreach ($this->language as $row2) {
							$this->db->query("INSERT INTO " . $row2['language'] . "_brend SET brend_id='$insert_id', name='" . $row[$_POST['brend_name']] . "'");
						}
						$row[$_POST['brend_id']] = $insert_id;
					} else {
						$row[$_POST['brend_id']] = $brend['id'];
						foreach ($this->language as $row2) {
							$this->db->query("UPDATE " . $row2['language'] . "_brend SET name='" . $row[$_POST['brend_name']] . "' WHERE brend_id='{$brend['id']}'");
						}
					}
					$where .= "brend_id='" . $row[$_POST['brend_id']] . "',";
				}
				//Products
				if (isset($update)) unset($update);
				if (isset($_POST['id']) && $_POST['id'] != '' && $_POST['id'] != 'id') {
					$product = $this->db->row("SELECT id FROM product WHERE id=?", array($row[$_POST['id']]));
					if (!isset($product['id'])) {
						//$where.="id='".$row[$_POST['id']]."',";
					} elseif ($product['id'] != 0) $update = $product['id'];
				}
				if (isset($_POST['name']) && $_POST['name'] != '') $where2 .= "name='" . $row[$_POST['name']] . "',";
				if (isset($_POST['body']) && $_POST['body'] != '') {
					$row[$_POST['body']] = str_replace('<br />', '&gt;', $row[$_POST['body']]);
					$where2 .= "body='" . $row[$_POST['body']] . "',";
				}
				if (isset($_POST['body_m']) && $_POST['body_m'] != '') $where2 .= "body_m='" . $row[$_POST['body_m']] . "',";
				if (isset($_POST['code']) && $_POST['code'] != '') $where .= "code='" . $row[$_POST['code']] . "',";
				if (isset($_POST['photos_id']) && $_POST['photos_id'] != '') $where .= "photo='" . $row[$_POST['photos_id']] . "',";
				if (isset($_POST['provider_name']) && $_POST['provider_name'] != '') {
					$provider_id = $this->db->cell("SELECT id FROM providers WHERE name=?",array($row[$_POST['provider_name']]));
					if($provider_id!='') $where .= " `provider_id`='" . $provider_id . "',";
				}
//				if (isset($_POST['price']) && $_POST['price'] != '') {
//					$row[$_POST['price']] = str_replace(',', '.', $row[$_POST['price']]);
//					$where .= "price='" . $row[$_POST['price']] . "',";
//				}
				//if (isset($_POST['discount']) && $_POST['discount'] != '') $where .= "discount='" . $row[$_POST['discount']] . "',";
				//if (isset($_POST['stock']) && $_POST['stock'] != '') $where .= "stock='" . $row[$_POST['stock']] . "',";
				//if (isset($_POST['delivery_times']) && $_POST['delivery_times'] != '') $where .= "delivery_times='" . $row[$_POST['delivery_times']] . "',";
				//if (isset($_POST['delivery_price']) && $_POST['delivery_price'] != '') $where .= "delivery_price='" . $row[$_POST['delivery_price']] . "',";
				if ($where != '') $where = ', ' . substr($where, 0, strlen($where) - 1);
				#Insert new data product#
				if (!isset($update) && $where2 != '') {
					$where2 = ', ' . substr($where2, 0, strlen($where2) - 1);
					if (isset($_POST['code'], $row[$_POST['code']])) {
						$product = $this->db->row("SELECT id FROM product WHERE code=?", array($row[$_POST['code']]));
						if (isset($product['id'])) $update = $product['id'];
					}
					$q = "INSERT INTO product SET date_add='$date' $where";
					$insert_id = $this->db->insert_id($q);
					if (isset($_POST['name']) && $_POST['name'] != '') {
                        $where .= ",url='" . $insert_id . '-' . StringLibrary::translit($row[$_POST['name']]) . "'";
						$this->db->query("UPDATE product SET date_edit='$date' $where WHERE id='$insert_id'");
					}
					$_POST['id'] = '';
					$row[$_POST['id']] = $insert_id;
					if ($insert_id != '') {
						if (isset($_POST['price']) && $_POST['price'] != '') {
							$www = "";
							if (isset($row[$_POST['code']])) $www = "code='{$row[$_POST['code']]}', ";
							if (isset($row[$_POST['stock']])) $www .= "stock='{$row[$_POST['stock']]}', ";
							$this->db->query("INSERT INTO `price` SET $www `product_id`='$insert_id', `price`='" . $row[$_POST['price']] . "', `discount`='" . $row[$_POST['discount']] . "', price_type_id='1'");
						}
						foreach ($this->language as $row2) {
							$q = "INSERT INTO " . $row2['language'] . "_product SET product_id='$insert_id' $where2";//echo $q.'<br /><br />';
							$this->db->query($q);
						}
					}
				} #Update data product#
				elseif (isset($update)) {
					if ($where2 != '') $where2 = substr($where2, 0, strlen($where2) - 1);
					if (!isset($row[$_POST['id']])) $row[$_POST['id']] = $update;
					$www = "";
					if (isset($row[$_POST['code']])) $www = ",code='{$row[$_POST['code']]}'";
					if (isset($row[$_POST['basePrice']])) $www .= ",purch_price='{$row[$_POST['basePrice']]}'";
					if (isset($row[$_POST['priceMargin']])) $www .= ",margin='{$row[$_POST['priceMargin']]}'";
					if (isset($row[$_POST['price']])) $www .= ",price='{$row[$_POST['price']]}'";
					if (isset($row[$_POST['stock']])) $www .= ",stock='{$row[$_POST['stock']]}'";
					if (isset($row[$_POST['discount']])) $www .= ",discount='{$row[$_POST['discount']]}'";

					if ($www != '') {
						$row_price = $this->db->row("SELECT id FROM `price` WHERE `product_id`='$update' ORDER BY `sort` ASC, id DESC");
						$this->db->query("UPDATE `price` SET " . substr($www, 1) . " WHERE id='{$row_price['id']}'");
					}
					$q = "UPDATE product SET date_edit='$date' $where WHERE id='$update'";
					$this->db->query($q);
					if ($where2 != '') {
						foreach ($this->language as $row2) {
							$q = "UPDATE " . $row2['language'] . "_product SET $where2 WHERE product_id='$update'";
							$this->db->query($q);
						}
					}
				}
				//Upload image
//				if (isset($_POST['image'], $row[$_POST['id']], $data['width_image'], $data['height_image']) && $_POST['image'] != '' && $row[$_POST['image']] != '') {
//					$dir = Dir::createDir($row[$_POST['id']]);
//					$ext = end(explode('.', $row[$_POST['image']]));
//					$file_tmp = "files/tmp/product/tmp_image." . $ext;
//					Images::grab_image($row[$_POST['image']], $file_tmp);
//					if (file_exists($file_tmp)) {
//						//resizeImage($file_tmp, $dir['0'].$row[$_POST['id']].".jpg", $dir['0'].$row[$_POST['id']]."_s.jpg", 139, 139);
//						Images::resizeImage($file_tmp, $dir['0'] . $row[$_POST['id']] . ".jpg", $dir['0'] . $row[$_POST['id']] . "_s.jpg", $data['width_image'], $data['height_image']);
//						$q = "UPDATE product SET photo='" . $dir['0'] . $row[$_POST['id']] . "_s.jpg' WHERE id='{$row[$_POST['id']]}'";//echo $q;
//						$this->db->query($q);
//					}
//				}
				//Catalog
				if (isset($_POST['cat_name']) && $_POST['cat_name'] != '') {
					/*if(isset($_POST['catalog_id'])&&$_POST['catalog_id']!=''&&$_POST['catalog_id']!='catalog_id')
						$cat=$this->db->row("SELECT id FROM catalog WHERE id=?", array($row[$_POST['catalog_id']]));
					else
						$cat=$this->db->row("SELECT catalog_id as id FROM ru_catalog WHERE name=?", array($row[$_POST['cat_name']]));

					if(!isset($cat['id'])){
						$insert_id=$this->db->insert_id("INSERT INTO catalog SET id='".$row[$_POST['catalog_id']]."'");
						foreach($this->language as $row2){
							$this->db->query("INSERT INTO ".$row2['language']."_catalog SET catalog_id='$insert_id', name='".$row[$_POST['cat_name']]."'");
						}
						$url=$insert_id.'-'.STRING::translit($row[$_POST['cat_name']]);
						$this->db->query("UPDATE catalog SET url='$url' WHERE id='$insert_id'");
						$row[$_POST['catalog_id']] = $insert_id;
					}
					else{
						$row[$_POST['catalog_id']] = $cat['id'];
						foreach($this->language as $row2){
							$this->db->query("UPDATE ".$row2['language']."_catalog SET name='".$row[$_POST['cat_name']]."' WHERE catalog_id='{$cat['id']}'");
						}
					}
					if(isset($row[$_POST['id']])&&$row[$_POST['id']]!=''&&$row[$_POST['catalog_id']]!=''){
						//$this->db->query("DELETE FROM product_catalog WHERE product_id=?", array($row[$_POST['id']]));
						$param=array($row[$_POST['catalog_id']], $row[$_POST['id']]);
						$cat=$this->db->row("SELECT catalog_id FROM product_catalog WHERE catalog_id=? AND product_id=?", $param);
						if(!isset($cat['catalog_id'])){
							$this->db->query("INSERT INTO product_catalog SET catalog_id=?, product_id=?", $param);
						}
					}*/
					//$where.="catalog_id='".$row[$_POST['catalog_id']]."',";
				}
				//Catalog product
				if (isset($row[$_POST['catalog_id']]) && $row[$_POST['catalog_id']] != '' && $_POST['catalog_id'] != '') {
					$arr = str_replace(' ', '', $row[$_POST['catalog_id']]);
					$params = explode(',', $arr);
					$this->db->query("DELETE FROM product_catalog WHERE product_id=?", array($row[$_POST['id']]));
					foreach ($params as $row5) {
						if ($row5 != '' && $row[$_POST['id']] != '') {
							$param = array($row5, $row[$_POST['id']]);
							$this->db->query("INSERT INTO product_catalog SET catalog_id=?, product_id=?", $param);
						}
					}
				}
				//Params
				if (isset($row[$_POST['params']]) && $row[$_POST['params']] != '' && $_POST['params'] != '') {
					$arr = str_replace(' ', '', $row[$_POST['params']]);
					$params = explode(',', $arr);
					$this->db->query("DELETE FROM params_product WHERE product_id=?", array($row[$_POST['id']]));
					foreach ($params as $row5) {
						if ($row5 != '' && $row[$_POST['id']] != '') {
							$param = array($row5, $row[$_POST['id']]);
							$this->db->query("INSERT INTO params_product SET params_id=?, product_id=?", $param);
						}
					}
				}
				$y++;
				//if($y==2)break;
			}
		}

		function select_cols()
		{
			$return = array();
			$return['select'] = '';
			$fields = array('id', 'name', 'body', 'body_m', 'code', 'image', 'basePrice', 'purch_price', 'priceMargin', 'price', 'discount', 'stock', 'catalog_id', 'cat_name', 'delivery_times', 'delivery_price', 'params', 'photos_id','provider_name');
			//var_info($_POST);
			//exit();
			foreach ($fields as $row) {
				if (isset($_POST[$row]) && $_POST[$row] != '') $return['select'] .= $_POST[$row] . ',';
			}
			if ($return['select'] != '') $return['select'] = substr($return['select'], 0, strlen($return['select']) - 1);
			//exit();
			return $return;
		}

		function set_import($file, $path_to_img = '/home/yuma/www/incoming/1c_change/Pictures/')
		{
			$xlsx = new SimpleXLSX($file);
			$result = $xlsx->rows();

			$count_col = count($result[0]) - 1;
			if ($count_col != 0) {
				$date = date("Y-m-d H:i:s");
				$this->create_tmp_table($count_col);
				$y = 0;
				foreach ($result as $row) {
					if ($y > 0) {
						$cols = "";
						for ($i = 0; $i <= $count_col; $i++) {

							$cols .= "`col" . $i . "`='" . addslashes($row[$i]) . "'";
							if ($count_col != $i) $cols .= ",";
						}
						$this->db->query("INSERT INTO `tmp_products` SET " . $cols);
					} else $cols_name = $row;
					$y++;
				}
				return $cols_name;
			}
		}

		function create_tmp_table($count_col)
		{
			$this->db->query("DROP TABLE IF EXISTS `tmp_products`");
			$cols = "";
			for ($i = 0; $i <= $count_col; $i++) {
				$cols .= "`col" . $i . "` text DEFAULT NULL,";
			}
			$this->db->query("CREATE TABLE `tmp_products` (
							  `id` int(11) NOT NULL AUTO_INCREMENT,
							  " . $cols . "
							  PRIMARY KEY (`id`)
							) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
		}

		function load_img($id, $path)
		{
			$path777 = "/home/cycom/public_html/";
			$id = substr($id, 1, strlen($id) - 1);
			for ($i = 1; ; $i++) {
				$path2 = $path . $id . '_' . $i . '.JPEG';//echo $path2.'<br />';
				if (file_exists($path2)) {
					if ($i == 1) {
						$dir = createDir($id, $path777);
						//resizeImage($path2, $dir['0'].$id.".jpg", $dir['0'].$id."_s.jpg", 160, 160);
						Images::resizeImage($path2, $dir[0] . $id . ".jpg", $dir[0] . $id . "_s.jpg", 160, 160);
					} else {
						$name = $id . '_' . $i;
						$insert_id = $this->db->insert_id("INSERT INTO product_photo SET product_id=?, active=?", array($id, 1));
						foreach ($this->language as $lang) {
							$tb = $lang['language'] . "_product_photo";
							$param = array($name, $insert_id);
							$this->db->query("INSERT INTO `$tb` SET `name`=?, `photo_id`=?", $param);
						}
						$dir = createDir($id, $path777);
						//resizeImage($path2, $dir[1].$insert_id.".jpg", $dir[1].$insert_id."_s.jpg", 160, 160);
						Images::resizeImage($path2, $dir[1] . $insert_id . ".jpg", $dir[1] . $insert_id . "_s.jpg", 160, 160);
					}
					unlink($path2);
				} else break;
			}
		}
	}

?>