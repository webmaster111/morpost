<?

class Product extends Model
{
  static $table = 'product';
  static $name = 'Товары';

  public function __construct($registry)
  {
    parent::getInstance($registry);
    $this->catalog = new Catalog($registry);
    $this->paramsModel = new Params($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  function add($open = false)
  {
    if (isset($this->registry['access'])) return $this->registry['access'];
    $message = messageAdmin('Заполнены не все поля', 'error');
    if (isset($_POST['active'], $_POST['url'], $_POST['name'], $_POST['body']) && $_POST['name'] != "") {
        $url = $_POST['url'] == '' ? $url = StringLibrary::translit($_POST['name']) : StringLibrary::translit($_POST['url']);

      $insert_id = $this->insert(array(
          'date_add' => date("Y-m-d H:i:s"),
          'active' => $_POST['active'],
          'type_discount' => $_POST['type_discount'],
          'provider_id' => $_POST['provider_id'],
          'one_price' => 1
      ));

      $this->checkUrl(self::$table, $url, $insert_id);
      Meta::getObject($this->sets)->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);
      $this->savePhoto($insert_id, $_POST['tmp_image'], self::$table);

      //Set translate
      foreach ($this->db->rows("SELECT * FROM language") as $lang)
        $this->db->query("INSERT INTO `" . $lang['language'] . "_" . self::$table . "` SET `name`=?,`body`=?,`body_m`=?,`product_id`=?", [$_POST['name'], $_POST['body'], $_POST['body_m'], $insert_id]);

      //Set product categories
      foreach ($_POST['cat_id'] as $item)
        $this->db->query("insert into product_catalog set product_id=?,catalog_id=?", array($insert_id, $item));

      //Set product status
      foreach ($_POST['status_id'] as $item)
        $this->db->query("INSERT INTO product_status_set SET product_id=?,status_id=?", array($insert_id, $item));

      // Price save
      if (isset($_POST['save_price_id']) && count($_POST['save_price_id']) > 0)
        for ($i = 0; $i <= count($_POST['save_price_id']) - 1; $i++){
            if( isset($_POST['price_basePrice'][$i]) && $_POST['price_basePrice'][$i] != '' ){
                $price_basePrice = $_POST['price_basePrice'][$i];
            }else{
                $price_basePrice = 0.00;
            }
            if( isset($_POST['price_price'][$i]) && $_POST['price_price'][$i] != '' ){
                $price_price = $_POST['price_price'][$i];
            }else{
                $price_price = 0.00;
            }
            if( isset($_POST['price_stock'][$i]) && $_POST['price_stock'][$i] != '' ){
                $price_stock = $_POST['price_stock'][$i];
            }else{
                $price_stock = 1;
            }
            if( isset($_POST['price_discount'][$i]) && $_POST['price_discount'][$i] != '' ){
                $price_discount = $_POST['price_discount'][$i];
            }else{
                $price_discount = 0;
            }
            $this->insert(array(
                'code' => $_POST['price_code'][$i],
                'basePrice' => $price_basePrice,
                'price' => $price_price,
                'stock' => $price_stock,
                'discount' => $price_discount,
                'product_id' => $insert_id,
            ), 'price');
        }


      //Set params
//      if ($this->paramsModel->checkModule()) {
//        foreach ($_POST['params'] as $item)
//          $this->db->query("INSERT INTO params_product SET product_id=?, params_id=?", [$insert_id, $item]);
//      }

      if ($open) {
        header('Location: /admin/' . self::$table . '/edit/' . $insert_id);
        exit();
      }
      $message = messageAdmin('Данные успешно добавлены');
    }
    return $message;
  }

  function save()
  {
    if (isset($this->registry['access'])) return $this->registry['access'];
    $message = messageAdmin('Заполнены не все обязательные поля', 'error');
      if (isset($_POST['save_id'], $_POST['save_id'], $_POST['sort']) && is_array($_POST['save_id'])) {



      for ($i = 0; $i <= count($_POST['save_id']) - 1; $i++) {
          $this->db->query("UPDATE `" . self::$table . "` tb SET tb.`sort`=? WHERE tb.id=?", [$_POST['sort'][$i], $_POST['save_id'][$i]]);
          $this->db->query("UPDATE `price` tb SET tb.`price`=? WHERE tb.product_id=?", [$_POST['prices'][$i], $_POST['save_id'][$i]]);
      }

      $message = messageAdmin('Данные успешно сохранены');
      } else if (isset($_POST['active'], $_POST['url'], $_POST['id'], $_POST['name'], $_POST['body'])) {
          $url = $_POST['url'] == '' ? $url = StringLibrary::translit($_POST['name']) : StringLibrary::translit($_POST['url']);
      $this->checkUrl(self::$table, $url, $_POST['id']);
      Meta::getObject($this->sets)->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);

      $this->update(array(
          'date_add' => date("Y-m-d H:i:s"),
          'active' => $_POST['active'],
          'type_discount' => $_POST['type_discount'] ?? null,
          'sizes' => $_POST['sizes'],
          'packaged' => $_POST['packaged'],
          'provider_id' => $_POST['provider_id'],
          'one_price' => 1
      ), "id='" . $_POST['id'] . "'");

      //translate
      $this->update(array(
          'name' => $_POST['name'],
          'material' => $_POST['material'],
          'body' => $_POST['body'],
          'body_m' => $_POST['body_m'],
          'feats' => $_POST['feats']
      ), self::$table . "_id='" . $_POST['id'] . "'", $this->registry['key_lang_admin'] . "_" . self::$table);


      // $_POST[ ? ] => array( table => column)
      $update_data = array(
          'cat_id' => array('product_catalog' => 'catalog_id'),
          'status' => array('product_status_set' => 'status_id')
      );

      // clear + add new data
      foreach ($update_data as $post_key => $data) {
        foreach ($data as $table => $column) {
          if (isset($_POST[$post_key])) {
            $this->db->query("DELETE FROM " . $table . "  WHERE product_id=?", [$_POST['id']]);
            foreach ($_POST[$post_key] as $item)
              $this->db->query("INSERT INTO " . $table . " SET product_id=?, " . $column . "=?", [$_POST['id'], $item]);
          }
        }
      }

      // Price save
      if (isset($_POST['save_price_id']) && count($_POST['save_price_id']) > 0)
        for ($i = 0; $i <= count($_POST['save_price_id']) - 1; $i++)
          $this->update(array(
              'code' => $_POST['price_code'][$i],
              'basePrice' => $_POST['price_basePrice'][$i],
              'price' => $_POST['price_price'][$i],
              'stock' => $_POST['price_stock'][$i],
              'discount' => $_POST['price_discount'][$i]),
              "product_id='" . $_POST['id'] . "'", 'price', true);


      if (!empty($_POST['params'])) {
        $this->db->query("DELETE params_product FROM params_product
											  LEFT JOIN product_property_sets pps ON pps.params_product_id=params_product.id
											  WHERE product_id=? AND pps.params_product_id IS NULL", array($_POST['id']));
        foreach ($_POST['params'] as $param) {
          $row = $this->db->row("SELECT id FROM params_product WHERE params_id='{$param}' AND product_id='{$_POST['id']}'");
          if (!$row) $this->db->query("INSERT INTO params_product SET product_id=?, params_id=?", array($_POST['id'], $param));
        }
      }

      if (!empty($_POST['price_param'])) {
        foreach ($_POST['config_price_id'] as $price_id)
          $this->db->query("DELETE FROM product_property_sets WHERE price_id=" . $price_id);
        foreach ($_POST['price_param'] as $price_param) {
          $arr = explode('-', $price_param);
          if (isset($arr[1])) {
            $price_id = $arr[0];
            $params_id = $arr[1];
            $row = $this->db->row("SELECT id FROM params_product WHERE params_id='{$params_id}' AND product_id='{$_POST['id']}'");
            if (!$row) $insert_id = $this->db->insert_id("INSERT INTO params_product (params_id,product_id) VALUES('{$params_id}','{$_POST['id']}')");
            else $insert_id = $row['id'];
            $this->db->query("INSERT INTO product_property_sets SET price_id='{$price_id}',params_product_id='{$insert_id}'");
          }
        }
      }

      $message = messageAdmin('Данные успешно сохранены');
    }
    return $message;
  }

  function copy()
  {
    if (isset($_POST['name'], $_POST['body'])) {
      $insert_id = $this->insert(array(
          'date_add' => date("Y-m-d H:i:s"),
          'active' => 1,
          'provider_id' => $_POST['provider_id'],
          'type_discount' => $_POST['type_discount'],
          'sizes' => $_POST['sizes'],
          'packaged' => $_POST['packaged'],
          'one_price' => 1
      ));

      //Set translate
      foreach ($this->db->rows("SELECT * FROM language") as $lang)
        $this->insert(array(
            'name' => $_POST['name'],
            'body' => $_POST['body'],
            'body_m' => $_POST['body_m'],
            'material' => $_POST['material'],
            'product_id' => $insert_id,
        ), $lang['language'] . "_" . self::$table);

      // $_POST[ ? ] => array( table => column)
      $update_data = array(
        //'params' => array('params_product' => 'params_id'),
          'cat_id' => array('product_catalog' => 'catalog_id'),
          'status' => array('product_status_set' => 'status_id')
      );

      // clear + add new data
      foreach ($update_data as $post_key => $data) {
        foreach ($data as $table => $column) {
          if (isset($_POST[$post_key])) {
            foreach ($_POST[$post_key] as $item)
              $this->db->query("INSERT INTO " . $table . " SET product_id=?, " . $column . "=?", [$insert_id, $item]);
          }
        }
      }

      // Price save
      foreach ($_POST['save_price_id'] as $i => $id)
        $this->insert(array(
            'code' => $_POST['price_code'][$i],
            'basePrice' => $_POST['price_basePrice'][$i],
            'price' => $_POST['price_price'][$i],
            'stock' => $_POST['price_stock'][$i],
            'discount' => $_POST['price_discount'][$i],
            'product_id' => $insert_id,
        ), 'price');

      return $insert_id;
    }
    return false;
  }

  function listView()
  {
    $where = "";
    $join = "";
    $_SESSION['by'] = 'desc';
    //sort start
    if (!isset($_SESSION['by'])) $_SESSION['by'] = 'desc';
    if (!isset($_SESSION['order'])) $_SESSION['order'] = 'tb.sort';
    if (isset($this->params['page']) && $this->params['page'] > 0) $_SESSION['page_product'] = $this->params['page'];
    if (isset($this->params['order']) && $this->params['order'] == 'name') $_SESSION['order'] = 'tb_lang.name';
    elseif (isset($this->params['order']) && $this->params['order'] == 'catalog') $_SESSION['order'] = 'tb_cat_lang.name';
    elseif (isset($this->params['order']) && $this->params['order'] == 'id') $_SESSION['order'] = 'tb.id';
    elseif (isset($this->params['order']) && $this->params['order'] == 'price') $_SESSION['order'] = 'price.price';
    elseif (isset($this->params['order']) && $this->params['order'] == 'sort') $_SESSION['order'] = 'tb.sort';
    elseif (isset($this->params['order']) && $this->params['order'] == 'active') $_SESSION['order'] = 'tb.active';
    if (isset($this->params['by']) && $this->params['by'] == 'asc') $_SESSION['by'] = 'desc';
    elseif (isset($this->params['by']) && $this->params['by'] == 'desc') $_SESSION['by'] = 'asc';
    if (isset($this->params['order'])) $_SESSION['search_admin']['sort'] = $_SESSION['order'] . ' ' . $_SESSION['by'] . ',tb.id DESC';
    //sort end

    if ($_SESSION['search_admin']['word'] != "")
      $where = "AND (tb_lang.name LIKE '%{$_SESSION['search_admin']['word']}%' or tb_lang.body LIKE '%{$_SESSION['search_admin']['word']}%' or tb_lang.body_m LIKE '%{$_SESSION['search_admin']['word']}%' or tb_cat_lang.name LIKE '%{$_SESSION['search_admin']['word']}%' or price.code LIKE '%{$_SESSION['search_admin']['word']}%')";

    if ($_SESSION['search_admin']['price_from'] != "" && $_SESSION['search_admin']['price_to'] == "") $where .= "AND (price.price >='{$_SESSION['search_admin']['price_from']}')";
    elseif ($_SESSION['search_admin']['price_from'] == "" && $_SESSION['search_admin']['price_to'] != "") $where .= "AND (price.price <='{$_SESSION['search_admin']['price_to']}')";
    elseif ($_SESSION['search_admin']['price_from'] != "" && $_SESSION['search_admin']['price_to'] != "") $where .= "AND (price.price <='{$_SESSION['search_admin']['price_to']}' AND price.price >='{$_SESSION['search_admin']['price_from']}')";
    if (isset($this->params['cat']) && $this->params['cat'] != '') {
      $where .= " AND tb_cat.url='{$this->params['cat']}'";
      $vars['curr_cat'] = "/admin/product/cat/" . $this->params['cat'];
    }
    $sort2 = explode('-', $_SESSION['search_admin']['cat_id']);
    if ($_SESSION['search_admin']['cat_id'] != "0") {
      if ($sort2[0] == 'status') $where .= "AND (pss.status_id ='{$sort2[1]}')";
      elseif ($_SESSION['search_admin']['cat_id'] != '') $where .= "AND (tb3.catalog_id ='{$_SESSION['search_admin']['cat_id']}')";
    }
      $sort3 = explode('-', $_SESSION['search_admin']['provider_id']);
      if ($_SESSION['search_admin']['provider_id'] != "0") {
          if ($sort3[0] == 'provider')
              $where .= "AND (tb.provider_id ='{$sort3[1]}')";
      }
    if ($_SESSION['search_admin']['params'] != '') {
      $where .= "AND pp.params_id ='{$_SESSION['search_admin']['params']}'";
      $join = "LEFT JOIN params_product pp ON pp.product_id=tb.id";
    }
    if (!isset($_SESSION['search_admin']['sort'])) $_SESSION['search_admin']['sort'] = "tb.sort DESC,tb.id DESC";
    //$_SESSION['search_admin']['sort']="tb.sort ASC";
    // Чтобы в логах было понятно откуда был запрос,приклеим данные о файле и линии в запрос комментарием
//    $logSql = PHP_EOL . '-- ' . __FILE__ . ' ' . __LINE__ . PHP_EOL;
    $vars['currency'] = $this->db->row("SELECT icon FROM currency WHERE `base`='1'");
    $q = $this->catalog->queryProducts([
        "select" => "tb.active,tb.sort,tb_cat_lang.name AS catalog,tb_cat.id AS cat_id",
        "join" => "
          LEFT JOIN product_catalog tb3 ON tb3.product_id=tb.id 
          LEFT JOIN catalog tb_cat ON tb3.catalog_id=tb_cat.id 
          LEFT JOIN " . $this->registry['key_lang_admin'] . "_catalog tb_cat_lang ON tb_cat_lang.catalog_id=tb_cat.id 
          
          LEFT JOIN product_status_set pss ON pss.product_id=tb.id 
          $join ",
        "where" => $where,
        "order" => $_SESSION['search_admin']['sort'],
        'group' => 'tb.id',
        "price" => true,
        'admin' => true
    ]);

    $vars['list'] = $this->find(array_merge($q, ["paging" => true]));
    foreach ($vars['list']['list'] as &$product) {

        if($product['cat_id']) $where = 'tb.sub=' . $product['cat_id'];

      $sub = $this->catalog->find([
          'where' => $where,
          'join' => 'RIGHT JOIN product_catalog ON product_catalog.product_id=' . $product['id'] . ' AND product_catalog.catalog_id=tb.id'
      ]);
      if ($sub) {
        $product['catalog'] = $sub['name'];
        $product['cat_id'] = $sub['id'];
      }
    }
    return $vars;
  }

  function addpricetype()
  {
    if (isset($this->registry['access'])) return $this->registry['access'];
    $this->db->query("INSERT INTO `price_type` SET `name`='New type price'");
    return messageAdmin('Данные успешно сохранены');
  }

  function save_pricetype()
  {
    if (isset($this->registry['access'])) return $this->registry['access'];
    $message = messageAdmin('Заполнены не все обязательные поля', 'error');
    if (isset($_POST['name'], $_POST['price_id'])) {
      $count = count($_POST['price_id']) - 1;
      if (!isset($_POST['default'])) $_POST['default'] = 1;
      for ($i = 0; $i <= $count; $i++) {
        $default = 0;
        if ($_POST['default'] == $_POST['price_id'][$i]) $default = 1;
        $this->db->query("UPDATE `price_type` SET `name`=?,`default`=? WHERE id=?", [$_POST['name'][$i], $default, $_POST['price_id'][$i]]);
      }
      $message = messageAdmin('Данные успешно сохранены');
    }
    return $message;
  }

  function default_price_type($type = 'user')
  {
    if (isset($_SESSION['user_id'], $_SESSION['user_info']['status']) && $type == 'user') $row = $this->db->row("SELECT price_type_id AS id FROM `users` LEFT JOIN user_status ON user_status.id=users.status_id WHERE users.`id`=?", [$_SESSION['user_id']]);
    else $row = $this->db->row("SELECT id FROM `price_type` WHERE `default`='1'");
    return $row['id'];
  }

  function getPhotoProduct($product_id)
  {
    return $this->find(['table' => 'product_photo', 'where' => "__tb.product_id:={$product_id}__", 'order' => 'tb.`sort` ASC,tb.id DESC', 'type' => 'rows']);
  }

  // Price
  function addPrice($id)
  {
    $row = $this->db->row("SELECT * FROM product WHERE id=?", [$id]);
    $where = ($row['provider_id'] != 0) ? '`id`=' . $row['provider_id'] : "`default`='1'";
    $vars['def'] = $this->db->row("SELECT * FROM providers WHERE " . $where);
    $vars['def']['price'] = (empty($vars['def']['price'])) ? 0 : $vars['def']['price'];
    $param = array($row['code'], $row['unit'], $row['stock'], $id, $this->default_price_type(), $vars['def']['price']);
    $insert_id = $this->db->insert_id("INSERT INTO price SET code=?,unit=?,stock=?,product_id=?,price_type_id=?,margin=?", $param);
    $vars['price'] = $this->db->rows("SELECT * FROM price WHERE product_id=? AND `id` =? ORDER BY price_type_id ASC,sort ASC,id DESC", [$id, $insert_id]);
    $vars['price_type'] = $this->db->rows("SELECT * FROM price_type ORDER BY id ASC");
    return $vars;
  }

  function delPrice($id)
  {
    if (isset($id)) {
      $row = $this->db->row("SELECT product_id FROM price WHERE id=?", [$id]);
      $this->db->query("DELETE FROM price WHERE id=?", [$id]);
      $vars['price'] = $this->db->rows("SELECT * FROM price WHERE product_id=? ORDER BY price_type_id ASC,sort ASC,id DESC", [$row['product_id']]);
      $vars['price_type'] = $this->db->rows("SELECT * FROM price_type ORDER BY id ASC");
      return $vars;
    }
  }

  function configPrice($id, $product_id)
  {
    if (isset($id, $product_id)) {
      $vars['params'] = Params::getObject($this->sets)->find(['where' => " ((pc2.product_id='{$product_id}' AND sub IS NULL AND type!='')) or pc3.product_id='{$product_id}'", 'join' => ' LEFT JOIN params_catalog pc ON pc.params_id=tb.id LEFT JOIN product_catalog pc2 ON pc.catalog_id=pc2.catalog_id LEFT JOIN params_product pc3 ON tb.id=pc3.params_id ', 'type' => 'rows', 'select' => ' tb.* ,tb_lang.*,pc.*,pc2.* ', 'group' => 'tb.id', 'order' => 'tb.sub ASC,tb.sort ASC']);
      $vars['params_set'] = $this->db->rows("SELECT pp.*,pps.price_id FROM `params_product` pp RIGHT JOIN product_property_sets pps ON pps.params_product_id=pp.id WHERE pp.product_id=? AND pps.price_id=?", [$product_id, $id]);
      $vars['photo'] = $this->getPhotoProduct($product_id);
      return $vars;
    }
  }

  function getPrice($id, $price_type_id)
  {
    return $this->db->rows("SELECT * FROM price WHERE product_id=? AND price_type_id=? ORDER BY sort ASC,id DESC", array($id, $_SESSION['price_type_id']));
  }

  function get_param($param_id, $product_id)
  {
    $row = $this->db->row("SELECT pp.*,pps.price_id,GROUP_CONCAT(DISTINCT pps.price_id SEPARATOR ',') as name_remains FROM product_property_sets pps LEFT JOIN params_product pp ON pp.`id`=pps.`params_product_id` WHERE pp.params_id=? AND pp.product_id=?", [$param_id, $product_id]);
    $where = '';
    $arr = explode(',', $row['name_remains']);
    foreach ($arr as $row) $where .= " OR pps.price_id='{$row}'";
    $where = " AND(" . substr($where, 3, strlen($where)) . ")";
    $res = $this->db->rows("SELECT ru.name,ru.params_id as id FROM product_property_sets pps
                            LEFT JOIN params_product pp ON pp.`id`=pps.`params_product_id`
                            LEFT JOIN " . $this->registry['key_lang'] . "_params ru ON pp.params_id=ru.params_id
                            LEFT JOIN params p ON p.id=pp.params_id
                            LEFT JOIN price ON price.id=pps.price_id
                            WHERE p.id!='$param_id' AND price.price_type_id='{$_SESSION['price_type_id']}' $where GROUP BY ru.params_id ORDER BY price.product_photo_id DESC,ru.name ASC", [$product_id]);
    return $res;
  }

  function get_remains($param_id, $product_id)
  {
    asort($param_id);
    $param_id = implode(',', $param_id);
    $row = $this->db->row("SELECT price.id,price.`stock`,price.`discount`,price.`price`,price.`basePrice`,price.product_photo_id,GROUP_CONCAT(DISTINCT p.id ORDER BY p.id ASC SEPARATOR ',') as name_remains FROM price
                          LEFT JOIN product_property_sets pps ON pps.price_id=price.id
                          LEFT JOIN params_product pp ON pp.`id`=pps.`params_product_id`
                          LEFT JOIN params p ON pp.params_id=p.id
                          WHERE pp.`product_id`=? GROUP BY price.id HAVING name_remains='$param_id'", [$product_id]);
    if ($row['stock'] > 0) $stock = '<div style="font-size:16px; color:#0a8527; margin-top:5px;" id="available">Есть в наличии</div>';
    else $stock = '<div style="font-size:16px; color:#aaa; margin-top:5px;">Нет в наличии</div>';
    $price = Numeric::viewPrice($row['price'], $row['basePrice']);
    return array('cur_price' => $price['cur_price'], 'price' => $price['price'] . $price['old_price'], 'stock' => $stock, 'max' => $row['stock'], 'price_id' => $row['id'], 'photo' => $row['product_photo_id']);
  }

  public function add_provider()
  {
    if (isset($this->registry['access'])) return $this->registry['access'];
    $this->db->query("INSERT INTO `providers` SET `name`='Новый поставщик'");
    return messageAdmin('Данные успешно сохранены');
  }

  public function save_provider()
  {
    if (isset($this->registry['access'])) return $this->registry['access'];
    if (isset($_POST['name'], $_POST['id'])) {
      if (!isset($_POST['default'])) $_POST['default'] = 1;
      for ($i = 0; $i <= count($_POST['id']) - 1; $i++) {
        $default = 0;
        if ($_POST['default'] == $_POST['id'][$i]) $default = 1;
        $this->db->query("UPDATE `providers` SET `name`=?,price=?,`default`=?,`currency_id`=?,`currency_rate`=?,`sort`=? WHERE id=?", [
            $_POST['name'][$i],
            $_POST['price'][$i],
            $default,
            $_POST['currency_id'][$i],
            $_POST['currency_rate'][$i],
            $_POST['sort'][$i],
            $_POST['id'][$i]
        ]);
        if (!is_null($_POST['isgroup'][$i]) && false) {
          $this->db->query("SET @basePrice:=0.00; UPDATE price RIGHT JOIN product ON product.id=price.product_id SET price.margin='{$_POST['price'][$i]}',price.basePrice=(@basePrice:=purch_price+((purch_price*{$_POST['price'][$i]})/100)),price.price=CASE WHEN type_discount=1 THEN @basePrice-((@basePrice*price.discount)/100) ELSE @basePrice-discount END WHERE product.provider_id='{$_POST['id'][$i]}'");
        }
      }
      return messageAdmin('Данные успешно сохранены');
    }
    return messageAdmin('Заполнены не все обязательные поля', 'error');
  }

  // ----- for queries

  function getByStatus($status_id, $limit = 6, $RAND = true, $sort = 'DESC')
  {
    return $this->find([
        "select" => "
          tb.*,
          tb_lang.*,
          product_catalog.catalog_id,
          c2.name AS catalog,
          tb4.status_id,
          tb5.name AS status,
          tb_price.price,
          tb_price.basePrice,
          tb_price.discount,
          tb_price.id as price_id,
          tb_price.currency_id",
        'type' => 'rows',

        "join" => "  LEFT JOIN product_catalog ON product_catalog.product_id=tb.id
          LEFT JOIN " . $this->registry['key_lang'] . "_catalog c2 ON product_catalog.catalog_id=c2.catalog_id
          LEFT JOIN `price` as `tb_price` ON `tb_price`.product_id = tb.id
          LEFT JOIN product_status_set tb4 ON tb4.product_id=tb.id
          LEFT JOIN " . $this->registry['key_lang'] . "_product_status tb5 ON tb4.status_id=tb5.product_status_id",
        "where" => "tb4.status_id='{$status_id}'",
        'limit' => $limit,
        'group' => 'tb.id ',
        'order' => $RAND ? 'rand()' : ' tb.id '. $sort,
    ]);
  }

}