<?

class ProductController extends BaseController
{

  private $filters;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);

      $this->tb = "product";
    $this->name = "Товары";
    $this->filters = new Params($this->sets);
  }

  public function indexAction()
  {
    $vars['product'] = $this->product->find([
        "select" => " tb.*, tb_lang.*,
          product_catalog.catalog_id,
          c2.name AS catalog,
          tb4.status_id,
          tb5.name AS status,
          tb_price.price,
          tb_price.code,
          tb_price.basePrice,
          tb_price.stock,
          tb_price.discount,
          tb_price.id as price_id,
          tb_price.currency_id",

        "join" => "LEFT JOIN product_catalog ON product_catalog.product_id=tb.id
                   LEFT JOIN " . $this->key_lang . "_catalog c2 ON product_catalog.catalog_id=c2.catalog_id
                   LEFT JOIN `price` as `tb_price` ON `tb_price`.product_id = tb.id
                   LEFT JOIN product_status_set tb4 ON tb4.product_id=tb.id
                   LEFT JOIN " . $this->registry['key_lang'] . "_product_status tb5 ON tb4.status_id=tb5.product_status_id",

        "where" => "tb.url='{$this->params['product']}'"
    ]);
    $vars['product']['feats'] = $this->getFeats($vars['product']['feats']);

    if (!isset($vars['product']['id'], $this->params['product'])) return Router::act('error', $this->registry);
    // что бы не продублировать КОД
    $vars['product']['name'] = str_replace($vars['product']['code'], '', $vars['product']['name']);
    // добавляем код в название товара
//    $vars['product']['name'] .= ' (' . $vars['product']['code'] . ')';
    // убераем слово "оптом" с названия товара
    $data['meta']['name'] = str_replace('оптом', '', $vars['product']['name']);
    $data['og_image'] = file_exists($vars['product']['photo']) ? $vars['product']['photo'] : '';
    //Extra photo
    $vars['photos'] = $this->model->find(array('table' => 'product_photo',
        'where' => "tb.product_id='{$vars['product']['id']}' AND tb.active='1' AND texture='0'",
        'order' => 'tb.sort',
        'type' => 'rows'));

    //Other products
    $vars['other_in_category'] = $this->view->Render('product/thumb.phtml', array(
        'show_details' => false,
        'products' => $this->product->find(
            Catalog::getObject($this->sets)->queryProducts(array(
                'where' => " AND tb3.catalog_id = '{$vars['product']['catalog_id']}' AND tb3.product_id != '{$vars['product']['id']}' AND tb.active='1'",
                'join' => 'LEFT JOIN product_catalog tb3 ON tb.id = tb3.product_id',
                'order' => 'rand()',
                'limit' => 4))
        )
    ));

    $vars['request-form'] = $this->view->Render('forms/request.phtml', $vars);
    $vars['some-info'] = $this->model->getBlock(10);
    $data['breadcrumbs'] = $this->model->getBreadCat($vars['product']['catalog_id'], $vars['product']);

    $menu_catalogs = getTree(containArrayInHisId($this->catalog->getAll(" AND tb.active='1'")));
    $vars['menu_catalogs'] = $this->view->Render('layout/menu.phtml', array('catalogs' => $menu_catalogs));
    if (isset($data['breadcrumbs'])) $vars['breadcrumbs'] = $this->model->breadcrumbs($data['breadcrumbs'], $this->view);
    $data['content'] = $this->view->Render('product/in.phtml', $vars);
    return $this->Index($data);
  }

    public function getFeats($feats)
    {
        $vars['product']['feats'] = explode('||', htmlspecialchars_decode($feats));
        foreach ($vars['product']['feats'] as $key => $value) {
            $value = explode('=>', $value);
            $vars['product']['feats'][$key] = ['name' => $value[0], 'value' => $value[1]];
        }
        return $vars['product']['feats'];
    }
}