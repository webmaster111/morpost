<?

class ProductController extends BaseController
{
    protected $paramsModel;
    private $left_menu = [
        [
            'title' => 'Типы цен',
            'url' => '/admin/product/act/pricetype',
            'name' => 'pricetype'
        ],
        [
            'title' => 'Поставщики',
            'url' => '/admin/product/act/provider',
            'name' => 'provider'
        ]
    ];

    function __construct($registry, $params)
    {
        parent::__construct($registry, $params);
        $this->tb = "product";
        $this->name = "Товары";
        $this->paramsModel = new Params($this->sets);
    }

    public function indexAction()
    {
        $data['tb'] = $this->tb;
        if ($this->product->checkModule() === false) return $this->Index(['content' => 'Данный модуль не подключен,пожалуйста свяжитесь с разработчком сайта для подключения данного модуля']);
        if (isset($this->params['act'])) {
            $act = $this->params['act'] . 'Action';
            return $this->Index($this->$act());
        }
        if (isset($this->params['subsystem'])) return $this->Index($this->product->subsystemAction($this->left_menu));
        if (!isset($_SESSION['search_admin']) || isset($_POST['clear'])) {
            $_SESSION['search_admin'] = [];
            $_SESSION['search_admin']['cat_id'] = 0;
            $_SESSION['search_admin']['provider_id'] = 0;
            $_SESSION['search_admin']['price_from'] = "";
            $_SESSION['search_admin']['price_to'] = "";
            $_SESSION['search_admin']['word'] = "";
            $_SESSION['search_admin']['paging'] = '';
            $_SESSION['search_admin']['params'] = '';
        } elseif (isset($_POST['word'])) {
            $_SESSION['search_admin']['cat_id'] = $_POST['cat_id'];
            $_SESSION['search_admin']['provider_id'] = $_POST['provider_id'];
            $_SESSION['search_admin']['price_from'] = $_POST['price_from'];
            $_SESSION['search_admin']['price_to'] = $_POST['price_to'];
            $_SESSION['search_admin']['word'] = trim($_POST['word']);
            $_SESSION['search_admin']['params'] = $_POST['params'] ?? '';
        }
        $vars['message'] = '';
        $vars['name'] = $this->name;
        $_SESSION['return_link'] = $_SERVER['REQUEST_URI'];
        if (isset($this->registry['access'])) $vars['message'] = $this->registry['access'];
        if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->product->delete($this->tb);
        elseif (isset($_POST['update'])) $vars['message'] = $this->product->save();
        elseif (isset($_POST['changestate'])) $vars['message'] = $this->product->changestate();
        elseif (isset($_POST['update_close'])) $vars['message'] = $this->product->save();
        elseif (isset($_POST['add_open'])) $vars['message'] = $this->product->add(true);
        if (isset($this->params['cat']) && $this->params['cat'] != '') $vars['curr_cat'] = $this->catalog->find($this->params['cat']);


        $vars['list'] = $this->view->Render('view.phtml', $this->product->listView());

        $vars['status'] = Product_status::getObject($this->sets)->find(['type' => 'rows', 'order' => 'id ASC']);
        $vars['providers'] = $this->db->rows("SELECT * FROM `providers`  ");

        $vars['params'] = Params::getObject($this->sets)->find(['type' => 'rows', 'order' => 'sort ASC,id DESC']);
        $vars['catalog'] = $this->catalog->getAll();
        $vars['URL'] = '';
        $data['styles'] = ['jquery.treeview.css'];
        $data['scripts'] = ['jquery.treeview.js'];
        $vars['link'] = '/admin/product/cat/';
        if ($this->paramsModel->checkModule()) {
            $params = $this->paramsModel->find(['type' => 'rows']);
            $vars['paramsForSearch'] = $this->view->Render('listParams.phtml', ['params' => $params]);
        }
        $settings = ['arr' => $vars['catalog'], 'link' => '/admin/product/cat/', 'id' => 'tree'];
        $data['left_menu'] = $this->view->Render('cat_menu.phtml', ['cat_menu' => Arr::treeview($settings)]);
        $data['left_menu'] .= $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name, 'menu2' => $this->left_menu]);
        $data['content'] = $this->view->Render('list.phtml', $vars);
        return $this->Index($data);
    }

    public function addAction()
    {
        if ($this->product->checkModule() === false)
            return $this->Index(['content' => 'Данный модуль не подключен,пожалуйста свяжитесь с разработчком сайта для подключения данного модуля']);
        if (!isset($_SESSION['return_link'])) $vars['message'] = $_SESSION['return_link'] = '/admin/' . $this->tb;
        if (isset($this->params['cat']) && $this->params['cat'] != '') $vars['curr_cat'] = $this->params['cat'];
        $vars['message'] = '';
        if (isset($_POST['add'])) $vars['message'] = $this->product->add();
        $vars['catalog'] = $this->catalog->getAll();

        //Params set
        if ($this->paramsModel->checkModule()) {
            $vars['params'] = $this->paramsModel->find(['type' => 'rows', 'order' => 'tb.sort ASC']);
            $vars['params'] = $this->view->Render('params.phtml', $vars);
        }
        $vars['providers'] = $this->db->rows("SELECT * FROM providers ORDER BY name ASC, id DESC");
        //Price
        $price_type = $this->db->rows("SELECT * FROM price_type ORDER BY id ASC");
        $price_rows = $this->view->Render('price_row.phtml', ['price' => [1], 'price_type' => $price_type, 'type' => 'add']);
        $vars['price'] = $this->view->Render('price.phtml', ['price' => $price_rows, 'type' => 'add']);
        $vars['status'] = Product_status::getObject($this->sets)->find(['type' => 'rows', 'order' => 'id ASC']);
        $vars['currency'] = $this->db->row("SELECT icon FROM currency WHERE `base`='1'");
        $vars['height'] = $this->settings['height_product'];
        $vars['width'] = $this->settings['width_product'];
        $vars['height_extra'] = $this->settings['height_product_extra'];
        $vars['width_extra'] = $this->settings['width_product_extra'];
        $data['styles'] = ['default.css', 'uploadify.css'];
        $data['scripts'] = ['jquery.uploadify.v2.1.4.min.js', 'jquery.searchabledropdown.js', 'bootstrap-modal.js', 'features.js', 'features.js'];
        $data['content'] = $this->view->Render('add.phtml', $vars);
        return $this->Index($data);
    }

    public function editAction()
    {
        if ($this->product->checkModule() === false)
            return $this->Index(['content' => 'Данный модуль не подключен,пожалуйста свяжитесь с разработчком сайта для подключения данного модуля']);

        if (!isset($_SESSION['return_link'])) $vars['message'] = $_SESSION['return_link'] = '/admin/' . $this->tb;
        $data['breadcrumb'] = '<a href="' . $_SESSION['return_link'] . '" class="back-link">« Назад в:&nbsp;' . $this->name . '</a>';
        $vars['message'] = '';
        $dir = Dir::createDir($this->params['edit']);

        if (isset($_POST['update'])) $vars['message'] = $this->product->save();

        else if (isset($_POST['copy'])) {
            $id = $this->product->copy();
            if ($id) {
                header('Location: /admin/' . $this->tb . '/edit/' . $id);
                exit();
            } else $vars['message'] = messageAdmin('Что-то пошло не так', 'error');
        }

        if (isset($this->params['dellone']) && $this->params['dellone'] != '') {
            $id = $this->params['dellone'];
            if ($this->db->query("DELETE FROM `product_photo` WHERE `id`=?", [$id])) $vars['message'] = messageAdmin('Запись успешно удалена');
            $this->model->photo_del($dir['1'], $id);
        }

        if (!empty($_POST['dell'])) {
            foreach ($_POST['photo_id'] as $id) {
                $this->db->query("DELETE FROM `product_photo` WHERE `id`=?", [$id]);
                $this->model->photo_del($dir['1'], $id);
            }
        }

        $vars['edit'] = $this->product->find((int)$this->params['edit']);
        //Load meta
        $row = $this->meta->load_meta($this->tb, $vars['edit']['url']);
        if ($row) {
            $vars['edit']['title'] = $row['title'];
            $vars['edit']['keywords'] = $row['keywords'];
            $vars['edit']['description'] = $row['description'];
        }
        //Params set
        $params = '';
        $params_set = '';
        if ($this->paramsModel->checkModule()) {
            $params = $this->paramsModel->find(['where' => "(pc2.product_id='{$vars['edit']['id']}' AND sub IS NULL) OR sub IS NOT NULL", 'join' => 'LEFT JOIN params_catalog pc ON pc.params_id=tb.id LEFT JOIN product_catalog pc2 ON pc.catalog_id=pc2.catalog_id', 'type' => 'rows', 'group' => 'tb.id', 'order' => 'tb.sub ASC,tb.sort ASC']);
            $params_set = $this->db->rows("SELECT * FROM `params_product` WHERE product_id=?", [$vars['edit']['id']]);
            $vars['params'] = $this->view->Render('params.phtml', ['params' => $params, 'params_set' => $params_set]);
        }

        //Catalog
        $vars['catalog'] = Catalog::getObject($this->sets)->find(['select' => 'tb.*,tb_lang.*,tb2.product_id', 'join' => " LEFT JOIN `product_catalog` tb2 ON tb.id=tb2.catalog_id AND tb2.product_id='{$this->params['edit']}'", 'group' => 'tb.id', 'order' => 'tb.sort', 'type' => 'rows']);
        //Catalog
        $vars['catalog2'] = $this->catalog->getAll();
        //Catalog set
        $vars['status'] = $this->db->rows("SELECT * FROM `product_status` tb LEFT JOIN `product_status_set` tb2 ON tb.id=tb2.status_id AND product_id=?", [$vars['edit']['id']]);
        //Price
        $res = $this->db->rows("SELECT * FROM price WHERE `product_id`=? ORDER BY sort ASC,price_type_id ASC,id DESC", array($vars['edit']['id']));
        $price_type = $this->db->rows("SELECT * FROM price_type ORDER BY id ASC");
        $price_rows = $this->view->Render('price_row.phtml', array('price' => $res, 'price_type' => $price_type));
        $vars['price'] = $this->view->Render('price.phtml', array('edit' => $vars['edit'], 'price' => $price_rows, 'params' => $params, 'params_set' => $params_set));
        $vars['providers'] = $this->db->rows("SELECT * FROM providers ORDER BY name ASC, id DESC");

        $vars['currency'] = $this->db->row("SELECT name FROM currency WHERE `id`='" . $vars['providers'][$vars['edit']['provider_id']]['currency_id'] . "'");

        //Загрузка фоток для текушего альбома
        $vars['path'] = $dir[1];
        $vars['height'] = $this->settings['height_product'];
        $vars['width'] = $this->settings['width_product'];
        $vars['action'] = $this->tb;
        $vars['action2'] = 'product_photo';
        $photo = $this->product->getPhotoProduct($vars['edit']['id']);
        $vars['photo'] = $this->view->Render('extra_photo_one2.phtml', ['photo' => $photo, 'action' => $this->tb, 'path' => $dir[1], 'sub_id' => $vars['edit']['id']]);
        $vars['photo'] = $this->view->Render('extra_photo.phtml', $vars);
        $vars['path'] = $dir[0];
        $data['styles'] = ['default.css', 'uploadify.css'];
        $data['scripts'] = ['jquery.uploadify.v2.1.4.min.js', 'jquery.searchabledropdown.js', 'bootstrap-modal.js', 'features.js', 'features.js'];
        $data['content'] = $this->view->Render('edit.phtml', $vars);
        return $this->Index($data);
    }

    public function pricetypeAction()
    {
        if ($this->product->checkModule() === false) return $this->Index(['content' => 'Данный модуль не подключен,пожалуйста свяжитесь с разработчком сайта для подключения данного модуля']);
        $vars['message'] = '';
        if (isset($_POST['update'])) $vars['message'] = $this->product->save_pricetype();
        elseif (isset($this->params['addpricetype'])) $vars['message'] = $this->product->addpricetype();
        elseif (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->product->delete('price_type');
        $vars['name'] = 'Типы цен';
        $vars['action'] = $this->tb;
        $vars['path'] = '/act/pricetype';
        $vars['list'] = $this->db->rows("SELECT * FROM `price_type` ORDER BY id ASC");
        $data['left_menu'] = $this->model->left_menu_admin(array('action' => $this->tb, 'name' => $this->name, 'sub' => 'pricetype', 'menu2' => $this->left_menu));
        $data['content'] = $this->view->Render('price_type.phtml', $vars);
        return $data;
    }

    //Product extra photo view
    function photoproductAction()
    {
        if (isset($_REQUEST['id'])) {
            $res = $this->db->rows("SELECT * FROM `product_photo` tb LEFT JOIN `" . $this->key_lang . "_product_photo` tb2 ON tb.id=tb2.product_photo_id WHERE product_id=? ORDER BY sort ASC", [$_REQUEST['id']]);
            $this->registry::set('admin', 'product');
            echo $this->view->Render('photoproduct.phtml', ['photo' => $res, 'id' => $_REQUEST['id']]);
        }
    }

    function addpriceAction()
    {
        if (isset($_POST['id'])) {
            $this->registry::set('admin', 'product');
            $vars = $this->product->addPrice($_POST['id']);
            $row = $this->db->row("SELECT * FROM product WHERE id=?", [$_POST['id']]);
            $where = ($row['provider_id'] != 0) ? '`id`=' . $row['provider_id'] : "`default`='1'";
            $vars['def'] = $this->db->row("SELECT * FROM providers WHERE " . $where);
            $vars['def'] = $vars['def']['price'];
            $data['content'] = $this->view->Render('price_row.phtml', $vars);
            return json_encode($data);
        }
    }

    function delpriceAction()
    {
        if (isset($_POST['id'])) {
            $this->registry::set('admin', 'product');
            $vars = $this->product->delPrice($_POST['id']);
            $data['content'] = $this->view->Render('price_row.phtml', $vars);
            return json_encode($data);
        }
    }

    function configpriceAction()
    {
        if (isset($_POST['id'], $_POST['product_id'])) {
            $this->registry::set('admin', 'product');
            $vars = $this->product->configPrice($_POST['id'], $_POST['product_id']);
            $vars['id'] = $_POST['id'];
            $vars['photo_id'] = $_POST['photo_id'];
            $vars['texture_id'] = $_POST['texture_id'];
            $data['content'] = $this->view->Render('price_config.phtml', $vars);
            return json_encode($data);
        }
    }

    public function providerAction()
    {
        $vars['message'] = '';
        if (isset($_POST['update'])) $vars['message'] = $this->product->save_provider();
        elseif (isset($this->params['add_provider'])) $vars['message'] = $this->product->add_provider();
        elseif (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->product->delete('providers');
        $vars['name'] = 'Поставщики';
        $vars['action'] = $this->tb;
        $vars['path'] = '/act/provider';
        $vars['currencies'] = $this->db->rows("SELECT * FROM `currency` ORDER BY `id` ASC");
        $vars['list'] = $this->db->rows("SELECT * FROM `providers` ORDER BY  `name` ASC, id DESC");
        $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name, 'sub' => 'provider', 'menu2' => $this->left_menu]);
        $data['content'] = $this->view->Render('provider.phtml', $vars);
        return $data;
    }

    function manualControlAction()
    {
        $_SESSION['manual-control'] = (($_POST['status'] == 'Выкл') ? NULL : true);
    }

    function rotateImgsAction()
    {
        foreach ($_POST['photo'] as $key => $photo_one) {
            $sizes = ['', '_s', '_s_2x'];
            $photo = str_replace($sizes, '', $photo_one);
            $photo = explode('.', $photo);
            $format = end($photo);
            $photo = stristr(implode('', $photo), $format, true);
            foreach ($sizes as $key2 => $size) {
                $photos[] = $photo . $size . '.' . $format;
            }
            foreach ($photos as $key2 => $photo) {
                try {
                    $image_new = new Imagick(realpath($photo));
                } catch (ImagickException $e) {
                }
                $image_new->readImage(realpath($photo));
                $image_new->rotateImage(new ImagickPixel('none'), (float)$_POST['rotate']);
                $image_new->writeImage(realpath($photo));
                $image_new->clear();
                $image_new->destroy();
            }
        }
    }

    function setWaterMarksAction()
    {
        foreach ($_POST['photo'] as $key => $photo_one) {
            $sizes = ['', '_s', '_s_2x'];
            $photo = str_replace($sizes, '', $photo_one);
            $photo = explode('.', $photo);
            $format = end($photo);
            $photo = stristr(implode('', $photo), $format, true);
            foreach ($sizes as $key2 => $size) {
                $photos[] = $photo . $size . '.' . $format;
            }
            foreach ($photos as $key2 => $photo) {
                Images::setWatermark($this->settings['watermark'], realpath($photo), $_POST['table']);
            }
        }
    }

}