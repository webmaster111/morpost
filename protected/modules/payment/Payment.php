<?

class Payment extends Model
{
  static $table = 'payment';
  static $name = 'Способ оплаты';

  public function __construct($registry)
  {
    parent::getInstance($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  public function add()
  {
    $message = messageAdmin('При сохранение произошли ошибки', 'error');
    if (isset($_POST['name'])) {
      $id = $this->insert(array('active' => $_POST['active']));
      foreach ($this->db->rows("SELECT * FROM language") as $lang) {
        $this->insert(array(
            'name' => $_POST['name'],
            'payment_id' => $id
        ), $lang['language'] . "_" . self::$table);
      }
      $message = messageAdmin('Данные успешно добавлены');
    }
    return $message;
  }

  public function save()
  {
    if (isset($this->registry['access'])) return $this->registry['access'];
    $message = messageAdmin('При сохранение произошли ошибки', 'error');
    if (isset($_POST['save_id'], $_POST['name']) && is_array($_POST['save_id'])) {
      foreach ($_POST['save_id'] as $i => $id) {
        $this->update(
            array('name' => $_POST['name'][$i]),
            "payment_id={$id}",
            $this->registry['key_lang_admin'] . "_" . self::$table
        );
      }
      if (isset($_POST['base'])) {
        $this->update(array('base' =>'0'), '1');
        $this->update(array('base' => 1), "id={$_POST['base']}");
      }
      $message = messageAdmin('Данные успешно сохранены');
    }
    return $message;
  }
}