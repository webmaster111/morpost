<?

class PaymentController extends BaseController
{
  protected $payment;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Payment::$table;
    $this->name = "Способ оплаты";
    $this->payment = new Payment($this->sets);
  }

  public function indexAction()
  {
    $vars['message'] = '';
    $vars['name'] = $this->name;
    if (isset($this->params['subsystem'])) return $this->Index($this->payment->subsystemAction());
    if (isset($this->registry['access'])) $vars['message'] = $this->registry['access'];
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->payment->delete($this->tb);
    elseif (isset($_POST['update'])) $vars['message'] = $this->payment->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->payment->save();
    elseif (isset($_POST['add_close'])) $vars['message'] = $this->payment->add();
    $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->payment->find(['type' => 'rows', 'order' => 'tb.sort ASC'])]);
    $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name]);
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = isset($_POST['add']) ? $this->payment->add() : '';
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }
}