<?

class Infoblocks extends Model
{
  static $table = 'infoblocks';
  static $name = 'Информационные блоки';

  public function __construct($registry)
  {
    parent::getInstance($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  public function add()
  {
    if (isset($_POST['name'], $_POST['body']) && $_POST['name'] != "") {
      $insert_id = $this->insert(array('url' => $_POST['url']));
      foreach ($this->db->rows("SELECT * FROM language") as $lang)
        $this->insert(array(
            'name' => $_POST['name'],
            'body' => $_POST['body'],
            'infoblocks_id' => $insert_id
        ), $lang['language'] . "_" . self::$table);
      return messageAdmin('Данные успешно добавлены');
    }
    return messageAdmin('Заполнены не все обязательные поля', 'error');
  }

  public function save()
  {
    $message = messageAdmin('Заполнены не все обязательные поля', 'error');
    if (isset($this->registry['access'])) $message = $this->registry['access'];
    else if (isset($_POST['save_id'], $_POST['name']) && is_array($_POST['save_id'])) {
      foreach ($_POST['save_id'] as $i => $id)
        $this->update(
            array('name' => $_POST['name'][$i]),
            "infoblocks_id={$id}",
            $this->registry['key_lang_admin'] . "_" . self::$table
        );
      $message = messageAdmin('Данные успешно сохранены');
    } else if (isset($_POST['id'], $_POST['name'], $_POST['body'])) {
      $this->update(array(
          'url' => $_POST['url'],
          'type_id' => $_POST['type_id']
      ), "id={$_POST['id']}");
      $this->update(
          array('name' => $_POST['name'], 'body' => $_POST['body']),
          "infoblocks_id={$_POST['id']}",
          $this->registry['key_lang_admin'] . "_" . self::$table
      );
      $message = messageAdmin('Данные успешно сохранены');
    }
    return $message;
  }

  public function getByType($type_id)
  {
    return $this->getAll(" AND type_id={$type_id}");
  }
}