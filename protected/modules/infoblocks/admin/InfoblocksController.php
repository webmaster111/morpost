<?

class InfoblocksController extends BaseController
{
  protected $info;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Infoblocks::$table;
    $this->name = "Информационные блоки";
    $this->registry = $registry;
    $this->info = new Infoblocks($this->sets);
  }

  public function indexAction()
  {
    $vars['message'] = isset($this->registry['access']) ? $this->registry['access'] : '';
    $vars['name'] = $this->name;
    if (isset($this->params['subsystem'])) return $this->Index($this->info->subsystemAction());
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->info->delete($this->tb);
    elseif (isset($_POST['update'])) $vars['message'] = $this->info->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->info->save();
    elseif (isset($_POST['add_close'])) $vars['message'] = $this->info->add();
    $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->info->find(['type' => 'rows', 'order' => 'tb.sort ASC'])]);
    $data['left_menu'] = $this->model->left_menu_admin(['action' => 'infoblocks', 'name' => $this->name]);
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = isset($_POST['add']) ? $this->info->add() : '';
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = isset($_POST['update']) ? $this->info->save() : '';
    $vars['edit'] = $this->info->find((int)$this->params['edit']);
    $vars['types'] = $this->db->rows("SELECT * FROM infoblock_types");

    if (!empty($vars['edit']['type_id'])) {
      $vars['edit']['type'] = $this->db->row("SELECT width,height FROM infoblock_types WHERE id={$vars['edit']['type_id']}");
      $vars['width'] = $vars['edit']['type']['width'];
      $vars['height'] = $vars['edit']['type']['height'];
    } else {
      $vars['width'] = $this->settings['ib_width'];
      $vars['height'] = $this->settings['ib_height'];
    }

    $vars['path'] = Dir::createDir($vars['edit']['id'], '', $this->tb);
    $vars['path'] = $vars['path'][0];
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }
}