<?
class Product_status extends Model
{
	static $table='product_status';
	static $name='Статусы товаров';

	public function __construct($registry)
	{
		parent::getInstance($registry);
	}

	public static function getObject($registry)
	{
		return new self::$table($registry);
	}

	public function add()
	{
		$message='';
		if(isset($_POST['name'],$_POST['comment'])&&$_POST['name']!=""){
            if ($_POST['url'] == '') $url = StringLibrary::translit($_POST['name']);
            else $url = StringLibrary::translit($_POST['url']);
			$id=$this->db->insert_id("INSERT INTO `".self::$table."` SET url=?,comment=?",[$url,$_POST['comment']]);
			$languages=$this->db->rows("SELECT * FROM language");
			foreach($languages as $lang)$this->db->query("INSERT INTO `".$lang['language']."_".self::$table."` SET `name`=?,`".self::$table."_id`=?",[$_POST['name'],$id]);
			$message.=messageAdmin('Данные успешно добавлены');
		}else $message.=messageAdmin('Заполнены не все обязательные поля','error');
		return $message;
	}

	public function save()
	{
		$message='';
		if(isset($this->registry['access']))$message=$this->registry['access'];
		else{
			if(isset($_POST['save_id'])&&is_array($_POST['save_id'])){
				if(isset($_POST['save_id'],$_POST['name'],$_POST['url'],$_POST['comment'])){
					for($i=0;$i<=count($_POST['save_id'])-1;$i++){
                        if ($_POST['url'][$i] == '') $url = StringLibrary::translit($_POST['name'][$i]);
                        else $url = StringLibrary::translit($_POST['url'][$i]);
						$this->checkUrl(self::$table,$url,$_POST['save_id'][$i]);
						$this->db->query("UPDATE `".self::$table."` SET `url`=?,`comment`=? WHERE id=?",[$_POST['url'][$i],$_POST['comment'][$i],$_POST['save_id'][$i]]);
						$this->db->query("UPDATE `".$this->registry['key_lang_admin']."_".self::$table."` SET `name`=? WHERE ".self::$table."_id=?",[$_POST['name'][$i],$_POST['save_id'][$i]]);
					}
					$message.=messageAdmin('Данные успешно сохранены');
				}else $message.=messageAdmin('При сохранение произошли ошибки','error');
			}
		}
		return $message;
	}
}