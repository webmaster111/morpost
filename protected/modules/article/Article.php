<?php

class Article extends Model
{
  static $table = 'article';
  static $name = 'Статьи';

  public function __construct($registry)
  {
    parent::getInstance($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  public function add()
  {
    if (isset($_POST['active'], $_POST['date'], $_POST['url'], $_POST['name'], $_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body'], $_POST['body_m']) && $_POST['name'] != "") {

      if ($_POST['date'] == '0000-00-00 00:00:00') $_POST['date'] = date("Y-m-d H:i:s");
      $insert_id = $this->insert(array(
          'active' => $_POST['active'],
          'date' => $_POST['date'],
      ));

      // Translate
      foreach ($this->db->rows('SELECT * FROM language') as $lang) {
        $this->insert(array(
            'name' => $_POST['name'],
            'body_m' => $_POST['body_m'],
            'body' => $_POST['body'],
            self::$table . '_id' => $insert_id
        ), $lang['language'] . "_" . self::$table);
      }
      // Set unique URL
        $url = !empty($_POST['url']) ? StringLibrary::translit($_POST['url']) : StringLibrary::translit($_POST['name']);
      $this->checkUrl(self::$table, $url, $insert_id);

      // Save meta data
      Meta::getObject($this->sets)->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);
      $this->savePhoto($insert_id, $_POST['tmp_image'], self::$table);
      return messageAdmin('Данные успешно добавлены');
    }
    return messageAdmin('Заполнены не все обязательные поля', 'error');

  }

  public function save()
  {
    if (isset($this->registry['access'])) {
		return $this->registry['access'];
	}
    $message = messageAdmin('При сохранение произошли ошибки', 'error');

    if (isset($_POST['save_id'], $_POST['name']) && is_array($_POST['save_id'])) {
      foreach ($_POST['save_id'] as $i => $id) {
        if ($_POST['date'][$i] == '0000-00-00 00:00:00') $date = date("Y-m-d H:i:s");
        else $date = $_POST['date'][$i];
        $this->update(array('date' => $date), [['id', '=', $id]]);
        $this->update(
            array('name' => $_POST['name'][$i]),
            [[self::$table . '_id', '=', $id]],
            $this->registry['key_lang_admin'] . "_" . self::$table
        );
      }
      $message = messageAdmin('Данные успешно сохранены');
    } else if (isset($_POST['active'], $_POST['url'], $_POST['id'], $_POST['name'], $_POST['body_m'], $_POST['body'])) {
      $date = $_POST['date'];
      if ($date == '0000-00-00 00:00:00') $date = date("Y-m-d H:i:s");

      $this->update(
          array('active' => $_POST['active'], 'date' => $date),
          [['id', '=', $_POST['id']]]);

      $this->update(
          array('name' => $_POST['name'], 'body' => $_POST['body'], 'body_m' => $_POST['body_m']),
          [[self::$table . '_id', '=', $_POST['id']]],
          $this->registry['key_lang_admin'] . "_" . self::$table);

      // Set unique URL
        $url = !empty($_POST['url']) ? StringLibrary::translit($_POST['url']) : StringLibrary::translit($_POST['name']);
      $this->checkUrl(self::$table, $url, $_POST['id']);
      // Save meta data
      Meta::getObject($this->sets)->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);

      $message = messageAdmin('Данные успешно сохранены');
    }
    return $message;
  }
}