<?

class ArticleController extends BaseController
{
  protected $article;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Article::$table;
    $this->name = "Блог";
    $this->article = new Article($this->sets);
  }

  public function indexAction()
  {
    if (isset($this->params['subsystem'])) return $this->Index($this->article->subsystemAction());
    $vars['message'] = isset($this->registry['access']) ? $this->registry['access'] : '';
    $vars['name'] = $this->name;
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->article->delete($this->tb);
    elseif (isset($_POST['update'])) $vars['message'] = $this->article->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->article->save();
    elseif (isset($_POST['add_close'])) $vars['message'] = $this->article->add();
    $vars['list'] = $this->view->Render('view.phtml', $this->article->find(['paging' => $this->settings['paging_article_admin'], 'order' => 'tb.date DESC']));
    $data['styles'] = ['jquery.simple-dtpicker.css'];
    $data['scripts'] = ['jquery.simple-dtpicker.js'];
    $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name]);
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = isset($_POST['add']) ? $this->article->add() : '';
    $vars['width'] = $this->settings['width_article'];
    $vars['height'] = $this->settings['﻿height_article'];
    $data['styles'] = ['jquery.simple-dtpicker.css'];
    $data['scripts'] = ['jquery.simple-dtpicker.js'];
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = isset($_POST['update']) ? $this->article->save() : '';
    $vars['edit'] = $this->article->find((int)$this->params['edit']);

    $row = $this->meta->load_meta($this->tb, $vars['edit']['url']);
    if ($row) {
      $vars['edit']['title'] = $row['title'];
      $vars['edit']['keywords'] = $row['keywords'];
      $vars['edit']['description'] = $row['description'];
    }
    $vars['action'] = $this->tb;
    $vars['width'] = $this->settings['width_article'];
    $vars['height'] = $this->settings['﻿height_article'];

    $vars['path'] = "files/" . $this->tb . "/" . substr($vars['edit']['id'], -1) . "/" . $vars['edit']['id'] . "/";
    $data['styles'] = ['jquery.simple-dtpicker.css'];
    $data['scripts'] = ['jquery.simple-dtpicker.js'];
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }

}