<?php

class ArticleController extends BaseController
{
  protected $article;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Article::$table;
    $this->article = new Article($this->sets);
  }

  public function indexAction()
  {
    $vars['table'] = $this->tb;
    $data['breadcrumbs'] = [['url' => 'blog/all', 'name' => $this->translation['blog']]];
    if (!isset($this->params[$this->tb]) || $this->params[$this->tb] == 'all') {
      $articles = $this->article->find([
          'type' => 'rows',
          'where' => 'tb.active = "1"',
          'order' => 'tb.date DESC',
          'limit' => $this->settings['paging_article']
      ]);
      $vars['no-load'] = !($this->article->count("active='1'") == count($articles));
      $vars['list'] = $this->view->Render($this->tb . '/thumb.phtml', array('articles' => $articles));
      $tpl = 'list';
    } else {
      $vars['article'] = $this->article->find(array(
          'type' => 'row',
          'where' => "tb.url='{$this->params[$this->tb]}' AND tb.active='1'"
      ));
      if (!isset($vars['article']['id'])) return Router::act('error', $this->registry);
      array_push($data['breadcrumbs'], ['url' => 'article/' . $vars['article']['url'], 'name' => $vars['article']['name']]);
      $data['meta'] = $vars['article'];
      $vars['other'] = $this->article->find([
          'where' => "tb.id!={$vars['article']['id']} AND tb.active='1'",
          'type' => 'rows',
          'order' => 'tb.date DESC',
          'limit' => 4
      ]);
      $tpl = 'item';
    }
    $data['content'] = $this->view->Render($this->tb . '/' . $tpl . '.phtml', $vars);
    return $this->Index($data);
  }

  public function getMoreAction()
  {
    if (isset($_POST['offset'])) {
      $offset = $_POST['offset'];
      $on_page = $this->settings['paging_article'];
      $result = array('offset' => $offset + 1);
      $list = $this->article->find(array(
          'type' => 'rows',
          'where' => 'tb.active = "1"',
          'order' => 'tb.date DESC',
          'limit' => $on_page . ' OFFSET ' . ($offset * $on_page)
      ));
      $result['thumbs'] = $this->view->Render($this->tb . '/thumb.phtml', array('articles' => $list, 'slide_down' => true));
      if (($this->article->count("active='1'") - (count($list) + $offset * $on_page)) <= 0) $result['offset'] = 0;
      return json_encode($result);
    }
    return false;
  }

}