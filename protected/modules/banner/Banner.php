<?

class Banner extends Model
{
  static $table = 'banner';
  static $name = 'Баннеры';

  public function __construct($registry)
  {
    parent::getInstance($registry);
  }

  public static function getObject($registry)
  {
    return new self::$table($registry);
  }

  public function add($open = false)
  {
    $message = '';
    if (isset($_POST['active'], $_POST['name'], $_POST['title'], $_POST['body']) && $_POST['name'] != "") {
      $insert_id = $this->db->insert_id("INSERT INTO `" . self::$table . "` SET `active`=?,`button`=?", array($_POST['active'],$_POST['button']));
      $languages = $this->db->rows("SELECT * FROM language");
      foreach ($languages as $lang)
        $this->db->query("INSERT INTO `" . $lang['language'] . "_" . self::$table . "` SET `name`=?,`body`=?,`title`=?,`" . self::$table . "_id`=?",
            [$_POST['name'], $_POST['body'], $_POST['title'], $insert_id]);
      $this->savePhoto($insert_id, $_POST['tmp_image'], self::$table);
      if ($open) {
        header('Location: /admin/' . self::$table . '/edit/' . $insert_id);
        exit();
      }
      $message .= messageAdmin('Данные успешно добавлены');
    } else $message .= messageAdmin('При добавление произошли ошибки', 'error');
    return $message;
  }

  public function save()
  {
    $message = '';
    if (isset($this->registry['access'])) $message = $this->registry['access'];
    else {
      if (isset($_POST['save_id']) && is_array($_POST['save_id'])) {
        if (isset($_POST['save_id'], $_POST['name'])) {
          for ($i = 0; $i <= count($_POST['save_id']) - 1; $i++)
            $this->db->query("UPDATE `" . $this->registry['key_lang_admin'] . "_" . self::$table . "` SET `name`=? WHERE " . self::$table . "_id=?", [$_POST['name'][$i], $_POST['save_id'][$i]]);
          $message .= messageAdmin('Данные успешно сохранены');
        } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
      } else {
        if (isset($_POST['active'], $_POST['id'], $_POST['name'], $_POST['title'], $_POST['body'])) {
          $this->db->query("UPDATE `" . self::$table . "` SET `active`=?,`button`=? WHERE id=?", [$_POST['active'],$_POST['button'], $_POST['id']]);
          $this->db->query("UPDATE `" . $this->registry['key_lang_admin'] . "_" . self::$table . "` SET `name`=?,`body`=?,`title`=? WHERE `" . self::$table . "_id`=?", [$_POST['name'], $_POST['body'], $_POST['title'], $_POST['id']]);
          $message .= messageAdmin('Данные успешно сохранены');
        } else $message .= messageAdmin('При сохранение произошли ошибки', 'error');
      }
    }
    return $message;
  }
}