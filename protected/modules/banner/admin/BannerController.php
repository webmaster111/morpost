<?

class BannerController extends BaseController
{
  protected $params;
  protected $db;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = "banner";
    $this->name = "Баннеры";
    $this->registry = $registry;
    $this->banner = new Banner($this->sets);
  }

  public function indexAction()
  {
    $vars['message'] = '';
    $vars['name'] = $this->name;
    if (isset($this->params['subsystem'])) return $this->Index($this->banner->subsystemAction());
    if (isset($this->registry['access'])) $vars['message'] = $this->registry['access'];
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->banner->delete($this->tb);
    elseif (isset($_POST['update'])) $vars['message'] = $this->banner->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->banner->save();
    elseif (isset($_POST['add_open'])) $vars['message'] = $this->banner->add(true);
    $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->banner->getAll()]);
    $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name]);
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = '';
    if (isset($_POST['add'])) $vars['message'] = $this->banner->add();
    $vars['height'] = $this->settings['height_banner'];
    $vars['width'] = $this->settings['width_banner'];
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = '';
    if (isset($_POST['update'])) $vars['message'] = $this->banner->save();
    $vars['action']=$this->tb;
    $vars['edit'] = $this->banner->find((int)$this->params['edit']);
    $dir = Dir::createDir($this->params['edit'],'', $this->tb);
    $vars['height'] = $this->settings['height_banner'];
    $vars['width'] = $this->settings['width_banner'];
    $vars['path'] = $dir[0];
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }

}
