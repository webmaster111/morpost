<?
class Video extends Model
{
	static $table='video';
	static $name='Видео';

	public function __construct($registry)
	{
		parent::getInstance($registry);
	}

	public static function getObject($registry)
	{
		return new self::$table($registry);
	}

	public function add()
	{
		$message='';
		if(isset($_POST['name'],$_POST['body'])&&$_POST['name']!=""){
			$insert_id=$this->db->insert_id("INSERT INTO `".self::$table."` SET `sort`=?,`active`=?,`body`=?",[1,1,$_POST['body']]);
			$languages=$this->db->rows("SELECT * FROM language");
			foreach($languages as $lang)$this->db->query("INSERT INTO `".$lang['language']."_".self::$table."` SET `name`=?,`".self::$table."_id`=?",[$_POST['name'],$insert_id]);
			$message.=messageAdmin('Данные успешно добавлены');
		}
		//else $message.= messageAdmin('Заполнены не все обязательные поля','error');
		return $message;
	}

	public function save()
	{
		$message='';
		if(isset($this->registry['access'])) $message=$this->registry['access'];
		else{
			if(isset($_POST['save_id'])&&is_array($_POST['save_id'])){
				if(isset($_POST['save_id'],$_POST['name'])){
					for($i=0;$i<=count($_POST['save_id'])-1;$i++)$this->db->query("UPDATE `".$this->registry['key_lang_admin']."_".self::$table."` SET `name`=? WHERE ".self::$table."_id=?",[$_POST['name'][$i],$_POST['save_id'][$i]]);
					$message.=messageAdmin('Данные успешно сохранены');
				}else $message.=messageAdmin('При сохранение произошли ошибки','error');
			}else{
				if(isset($_POST['id'],$_POST['name'],$_POST['body'])){
					$this->db->query("UPDATE `".$this->registry['key_lang_admin']."_".self::$table."` SET `name`=? WHERE `".self::$table."_id`=?",[$_POST['name'],$_POST['id']]);
					$this->db->query("UPDATE `".self::$table."` SET `body`=? WHERE `id`=?",[$_POST['body'],$_POST['id']]);
					$message.=messageAdmin('Данные успешно сохранены');
				}else $message.=messageAdmin('При сохранение произошли ошибки','error');
			}
		}
		return $message;
	}
}