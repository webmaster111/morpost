<?
class Stat_regController extends BaseController {
	protected $params;
	protected $db;

	function __construct($registry,$params){
		parent::__construct($registry,$params);
		$this->tb="stat_reg";
		$this->name="Активность регистраций на сайте";
		$this->registry=$registry;
		$this->stat=new Stat_reg($this->sets);
		$this->cntDate=1;
	}

	public function indexAction(){
		if(isset($this->params['subsystem']))return $this->Index($this->stat->subsystemAction());
		if(isset($_POST['start'])){
			$_SESSION['date_start'.$this->cntDate]=$_POST['start'];
			$_SESSION['date_end'.$this->cntDate]=$_POST['end'];
		}elseif(!isset($_SESSION['date_start'.$this->cntDate])){
			$date=$this->model->setDateStat();
			$_SESSION['date_start'.$this->cntDate]=$date['start'];
			$_SESSION['date_end'.$this->cntDate]=$date['end'];
		}
		$vars['message']='';
		$vars['name']=$this->name;
		## Start Statistics
		$data['styles']=['jquery.simple-dtpicker.css'];
		$data['scripts']=['jquery.simple-dtpicker.js'];
		$data['left_menu']=$this->model->left_menu_admin(['action'=>$this->tb,'name'=>$this->name]);
		$data['content']=$this->view->Render('list.phtml',$vars);
		return $this->Index($data);
	}

	public function compareAction()
	{
		$compare='';
		if(!isset($_SESSION['Stat_reg_type_switcher']))$_SESSION['Stat_reg_type_switcher']=1;
		if(isset($_POST['type_switcher']))$_SESSION['Stat_reg_type_switcher']=$_POST['type_switcher'];
		if(isset($_POST['start1'])){
			$_SESSION['date_start'.$this->cntDate]=$_POST['start1'];
			$_SESSION['date_end'.$this->cntDate]=$_POST['end1'];
			if(isset($_POST['startcompare'])){
				$_SESSION['date_start_c'.$this->cntDate]=$_POST['start2'];
				$_SESSION['date_end_c'.$this->cntDate]=$_POST['end2'];
				$_SESSION['Stat_reg_startcompare']=1;
			}elseif($_SESSION['Stat_reg_startcompare']==1&&isset($_POST['start2']))$_SESSION['Stat_reg_startcompare']=0;
		}
		if($_SESSION['Stat_reg_startcompare']==1)$compare=1;
		if(!isset($_SESSION['date_start'.$this->cntDate])){
			$ranges=$this->db->row("SELECT MIN(start_date) as min_start_date,MAX(start_date) as max_start_date FROM `users`");
			$_SESSION['date_start'.$this->cntDate]=$ranges['min_start_date'];
			$_SESSION['date_end'.$this->cntDate]=$ranges['max_start_date'];
			$date=$this->model->setDateStat(2);
			$_SESSION['date_start_c'.$this->cntDate]=$date['start'];
			$_SESSION['date_end_c'.$this->cntDate]=$date['end'];
		}
		$vars['date_start']=$_SESSION['date_start'.$this->cntDate];
		$vars['date_end']=$_SESSION['date_end'.$this->cntDate];
		$vars['date_start_c']=$_SESSION['date_start_c'.$this->cntDate];
		$vars['date_end_c']=$_SESSION['date_end_c'.$this->cntDate];
		$vars['graph']=$this->stat->viewGraph($vars['date_start'],$vars['date_end'],$vars['date_start_c'],$vars['date_end_c'],$compare);
		echo $this->view->Render('graph_body.phtml',$vars);
	}
}