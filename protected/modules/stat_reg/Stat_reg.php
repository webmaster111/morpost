<?
class Stat_reg1 extends Model {
	static $table='stat_reg';
	static $name='Активность регистраций на сайте';

	public function __construct($registry){
		parent::getInstance($registry);
	}

	public static function getObject($registry){
		return new self::$table($registry);
	}

	public function viewGraph($compare=''){
		$count=$this->db->row("SELECT COUNT(*) AS cnt,start_date FROM `users` WHERE `start_date` <='".date("Y-m-d")."' ");  // POST1
		$count2=$this->db->row("SELECT COUNT(*) AS cnt,start_date FROM `users` WHERE `start_date` <='".date("Y-m-d",strtotime('-1 year'))."' ");   // POST3
		$vars['count1']=$count['cnt'];
		$vars['count2']=$count2['cnt'];
		$orders_array="['Дата','1 период','2 период']";
		for ($i=12; $i > 0; $i--){
			$date1=date("Y-m",strtotime('-'.$i.'month'));
			$count=$this->db->row("SELECT COUNT(*) AS cnt,start_date FROM `users` WHERE `start_date` BETWEEN '{$date1}-01 00:00:01' AND '{$date1}-31 23:59:59' ");
			$vars['count1']+=$count['cnt'];
			$date=date("Y-m",strtotime('-'.$i.'month -1 year'));
			$count=$this->db->row("SELECT COUNT(*) AS cnt,start_date FROM `users` WHERE `start_date` BETWEEN '{$date}-01 00:00:01' AND '{$date}-31 23:59:59' ");
			$vars['count2']+=$count['cnt'];
			$date=date("F",strtotime($date1));
			$orders_array.=",\n['{$date}',{$vars['count1']},{$vars['count2']}]";
		}
		$vars['text']=$orders_array;
		$vars['date_start']=$date_start;
		$vars['date_end']=$date_end;
		$vars['compare']=1;
		if(!isset($this->registry['admin']))$this->registry::set('admin',$this->table);
		$view=new View($this->registry);
		$content=$view->Render('graph.phtml',$vars);
		return $content;
	}
}

class Stat_reg extends Model {
	static $table='stat_reg';
	static $name='Активность регистраций на сайте';

	public function __construct($registry){
		parent::getInstance($registry);
	}

	public static function getObject($registry){
		return new self::$table($registry);
	}

	public function viewGraph($date_start,$date_end,$date_start_c,$date_end_c,$compare=''){
		$Stat_reg_type_switcher=$_SESSION['Stat_reg_type_switcher'];
		$before1=$this->db->row("SELECT COUNT(*) as count,start_date FROM `users` WHERE `start_date` < '$date_start' ORDER BY `start_date`");
		$counter1_start=(int)$before1['count'];
		$vars['list1']=$this->db->rows("SELECT start_date FROM `users` WHERE `start_date` BETWEEN '$date_start' AND '$date_end' ORDER BY `start_date`");
		$total_sum=$counter1_start;
		$order_sum=[];
		foreach($vars['list1'] as $Rows){
			$counter1_start++;
			$date=date("d.m.Y",strtotime($Rows['start_date']));
			if($Stat_reg_type_switcher==1)$order_sum[$date]=$counter1_start;
			else $order_sum[$date]=$order_sum[$date] + 1;
		}
		if($Stat_reg_type_switcher==2)$order_sum=$this->fillNullDates($date_start,$date_end,$order_sum);
		if($compare==1){
			$before2=$this->db->row("SELECT COUNT(*) as count,start_date FROM `users` WHERE `start_date` < '$date_start_c' ORDER BY `start_date`");
			$counter2_start=(int)$before2['count'];
			$vars['list2']=$this->db->rows("SELECT start_date FROM `users` WHERE `start_date` BETWEEN '$date_start_c' AND '$date_end_c' ORDER BY `start_date`");
			$total_sum2=$counter2_start;
			$order_sum2=[];
			foreach($vars['list2'] as $Rows){
				$date=date("d.m.Y",strtotime($Rows['start_date']));
				$counter2_start++;
				if($Stat_reg_type_switcher==1)$order_sum2[$date]=$counter2_start;
				else $order_sum2[$date]=$order_sum2[$date] + 1;
			}
			if($Stat_reg_type_switcher==2)$order_sum2=$this->fillNullDates($date_start_c,$date_end_c,$order_sum2);
			$order_sum_max=max($order_sum,$order_sum2);
			$order_sum_min=min($order_sum,$order_sum2);
			$orders_sum_array="['Дата','".date("d.m.Y",strtotime($date_start))." - ".date("d.m.Y",strtotime($date_end))."','".date("d.m.Y",strtotime($date_start_c))." - ".date("d.m.Y",strtotime($date_end_c))."']";
			if(sizeof($order_sum_max)==0)$orders_sum_array.=",\n[0,0,0]";
			else{
				$i=0;
				foreach($order_sum_max as $key => $value){
					$date=date("d.m.Y",strtotime($key));
					$sum=(int)$value;
					if($i==0)$sum2=current($order_sum_min);
					else $sum2=next($order_sum_min);
					if($sum2 === false)$sum2="null";
					if($order_sum === $order_sum_max){
						if($sum2!="null")$date.=" - ".key($order_sum_min);
						$orders_sum_array.=",\n['{$date}',{$sum},{$sum2}]";
					}else{
						if($sum2!="null")$date=key($order_sum)." - ".$date;
						$orders_sum_array.=",\n['{$date}',{$sum2},{$sum}]";
					}
					$i++;
				}
				$vars['total_sum']=($counter1_start - $total_sum) + ($counter2_start - $total_sum2);
			}
		}else{
			$orders_sum_array="['Дата','".date("d.m.Y",strtotime($date_start))." - ".date("d.m.Y",strtotime($date_end))."']";
			if(sizeof($order_sum)==0)$orders_sum_array.=",\n[0,0]";
			else foreach($order_sum as $key => $value){
					$date=date("d.m.Y",strtotime($key));
					$sum=(int)$value;
					$orders_sum_array.=",\n['{$date}',{$sum}]";
				}
			$vars['total_sum']=$counter1_start - $total_sum;
		}
		$vars['text']=$orders_sum_array;
		$vars['date_start']=$date_start;
		$vars['date_end']=$date_end;
		$vars['date_start_c']=$date_start_c;
		$vars['date_end_c']=$date_end_c;
		$vars['compare']=1;
		if(!isset($this->registry['admin']))$this->registry::set('admin',$this->table);
		$view=new View($this->registry);
		$content=$view->Render('graph.phtml',$vars);
		return $content;
	}
}