<?
class FavoriteController extends BaseController
{
	protected $params;
	protected $db;

	function __construct($registry,$params)
	{
		parent::__construct($registry,$params);
		$this->tb="favorites";
		$this->name="Избранное";
		$this->registry=$registry;
		$this->filters=new Params($this->sets);
		$this->catalog=new Catalog($this->sets);
		$this->product=new Product($this->sets);
	}

	public function indexAction()
	{
		$vars['type']=0;
		if(isset($this->params['favorite'])) $vars['type']=$this->params['favorite'];

    if ($this->params['favorite'] == 0) {
			// Избранное
			if(isset($this->params['del'])) {
			  $this->db->query("DELETE FROM favorites WHERE product_id=? AND session_id=? AND type=?",[$this->params['del'],session_id(),$vars['type']]);
      }
			$q = $this->catalog->queryProducts(['where'=>"AND __c.session_id:=".session_id()."__ AND tb.active='1' AND __c.type:=".$vars['type']."__",'join'=>'LEFT JOIN favorites c ON c.product_id=tb.id ']);
			$vars['product']=$this->product->find($q);
			$where='';
			foreach ($vars['product'] as $row){
				if($where!='') $where.=" OR ";
				$where.="tb.id='{$row['catalog_id']}'";
			}
			if($where!='')$vars['catalog']=$this->catalog->find(['type'=>'rows','select'=>'tb.id,tb.url,tb.sub,tb_lang.name','where'=>"tb.active='1' AND ($where)",'group'=>'tb.id','order'=>'tb.sort ASC,tb.id DESC']);

    } else {
			if(isset($this->params['del'])){
				$key=array_search($this->params['del'],$_SESSION['products'][$vars['type']]);
				unset($_SESSION['products'][$vars['type']][$key]);
			}
			$productIds=implode(',',$_SESSION['products'][$vars['type']]);
			$productIds=trim($productIds,',');
			$q=$this->catalog->queryProducts(['where'=>"AND tb.id IN (".$productIds.")"]);
			$vars['product']=$this->product->find($q);
			$where='';
			foreach ($vars['product'] as $row){
				if($where!='') $where.=" OR ";
				$where.="tb.id='{$row['catalog_id']}'";
			}
			if($where!='') $vars['catalog']=$this->catalog->find(['type'=>'rows','select'=>'tb.id,tb.url,tb.sub,tb_lang.name','where'=>"tb.active='1' AND ($where)",'group'=>'tb.id','order'=>'tb.sort ASC,tb.id DESC']);
		}

		$vars['translate']=$this->translation;
		$data['content']=$this->view->Render('catalog/favorites.phtml',$vars);
		return $this->Index($data);
	}

	function addAction()
	{
		if(isset($_POST['id'],$_POST['cat_id'])){
			$_POST['cat_id']=str_replace('licat_','',$_POST['cat_id']);
			$this->db->query("DELETE FROM compare WHERE session_id=? AND catalog_id!=?",[session_id(),$_POST['cat_id']]);
			$param=[session_id(),$_POST['cat_id'],$_POST['id']];
			$row=$this->db->row("SELECT * FROM compare WHERE session_id=? AND catalog_id=? AND product_id=?",$param);
			if(!$row){
				$this->db->query("INSERT INTO compare SET session_id=?,catalog_id=?,product_id=?",$param);
				$return['message']='Товар добавлен для сравнения!';
				$return['add']=1;
			} else $return['message']='Товар уже есть в таблице сравнений!';
			return json_encode($return);
		}
	}

	function delAction()
	{
		if(isset($_POST['id'])){
			$_POST['id']=str_replace('compare','',$_POST['id']);
			$this->db->query("DELETE FROM compare WHERE id=?",[$_POST['id']]);
		}
	}

	function loadAction()
	{
		$res=$this->db->rows("SELECT c.*,p2.name,p.url FROM compare c LEFT JOIN product p ON p.id=c.product_id LEFT JOIN ".$this->key_lang."_product p2 ON p.id=p2.product_id WHERE session_id=? ORDER BY c.id DESC",[session_id()]);
		if(isset($res[0]))return $this->view->Render('compare_block.phtml',['compare'=>$res,'translate'=>$this->translation]);
	}
}