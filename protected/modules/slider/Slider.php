<?

class Slider extends Model
{
    static $table = 'slider';
    static $name = 'Слайдер';

    public function __construct($registry)
    {
        parent::getInstance($registry);
    }

    public static function getObject($registry)
    {
        return new self::$table($registry);
    }

    public function add()
    {
        $message = messageAdmin('Заполнены не все обязательные поля', 'error');
        if (isset($_POST['name'], $_POST['url'])) {

            $insert_id = $this->insert(array(
                'active' => $_POST['active'],
                'url' => $_POST['url']
            ));

            //translate
            foreach ($this->db->rows("SELECT * FROM language") as $lang) {
                $this->insert(array(
                    'name' => $_POST['name'],
                    'body' => $_POST['body'],
                    'button' => $_POST['button'],
                    'slider_id' => $insert_id
                ), $lang['language'] . "_" . self::$table);
            }

            $this->savePhoto($insert_id, $_POST['tmp_image'], self::$table);
            $message = messageAdmin('Данные успешно добавлены');
        }
        return $message;
    }

    public function save()
    {
        $message = messageAdmin('При сохранение произошли ошибки', 'error');
        if (isset($this->registry['access'])) $message = $this->registry['access'];
        else if (isset($_POST['save_id'], $_POST['name']) && is_array($_POST['save_id'])) {

            foreach ($_POST['save_id'] as $i => $id) {
                $this->update(
                    array('name' => $_POST['name'][$i]),
                    self::$table . "_id={$id}",
                    $this->registry['key_lang_admin'] . "_" . self::$table);
            }
            $message = messageAdmin('Данные успешно сохранены');

        } else if (isset($_POST['active'], $_POST['url'], $_POST['id'], $_POST['name'])) {

            $this->update(array(
                'active' => $_POST['active'],
                'url' => $_POST['url']
            ), "id={$_POST['id']}");

            $this->update(array(
                'name' => $_POST['name'],
                'button' => $_POST['button'],
                'body' => $_POST['body']),
                self::$table . "_id={$_POST['id']}",
                $this->registry['key_lang_admin'] . "_" . self::$table);

            $message = messageAdmin('Данные успешно сохранены');
        }
        return $message;
    }
}