<?

class SliderController extends BaseController
{
  protected $slider;

  function __construct($registry, $params)
  {
    parent::__construct($registry, $params);
    $this->tb = Slider::$table;
    $this->name = "Слайдер";
    $this->slider = new Slider($this->sets);
  }

  public function indexAction()
  {
    if (isset($this->params['subsystem'])) return $this->Index($this->slider->subsystemAction());
    $vars['name'] = $this->name;
    $vars['message'] = isset($this->registry['access']) ? $this->registry['access'] : '';
    if (isset($this->params['delete']) || isset($_POST['delete'])) $vars['message'] = $this->slider->delete($this->tb);
    elseif (isset($_POST['update'])) $vars['message'] = $this->slider->save();
    elseif (isset($_POST['update_close'])) $vars['message'] = $this->slider->save();
    elseif (isset($_POST['add_close'])) $vars['message'] = $this->slider->add();
    $vars['list'] = $this->view->Render('view.phtml', ['list' => $this->slider->find(['type' => 'rows', 'order' => 'tb.sort ASC,tb.id DESC'])]);
    $data['left_menu'] = $this->model->left_menu_admin(['action' => $this->tb, 'name' => $this->name]);
    $data['content'] = $this->view->Render('list.phtml', $vars);
    return $this->Index($data);
  }

  public function addAction()
  {
    $vars['message'] = isset($_POST['add']) ? $this->slider->add() : '';
    $vars['width'] = $this->settings['width_slider'];
    $vars['height'] = $this->settings['height_slider'];
    $data['content'] = $this->view->Render('add.phtml', $vars);
    return $this->Index($data);
  }

  public function editAction()
  {
    $vars['message'] = isset($_POST['update']) ? $this->slider->save() : '';
    $vars['edit'] = $this->slider->find((int)$this->params['edit']);
    $vars['width'] = $this->settings['width_slider'];
    $vars['height'] = $this->settings['height_slider'];
    $data['content'] = $this->view->Render('edit.phtml', $vars);
    return $this->Index($data);
  }
}