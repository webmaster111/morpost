<?php

class Catalog extends Model {
	static $table = 'catalog';
	static $name = 'Каталог';

	public function __construct($registry) {
		parent::getInstance($registry);
	}

	public static function getObject($registry) {
		return new self::$table($registry);
	}

	public function add($open = false) {
		$message = messageAdmin('При добавление произошли ошибки', 'error');
		if (isset($_POST['active'], $_POST['url'], $_POST['name'], $_POST['body'], $_POST['sub']) && $_POST['name'] != '') {
			if ($_POST['sub'] == 0) {
				$_POST['sub'] = NULL;
			}
			if (!isset($_POST['position'])) {
				$_POST['position'] = 0;
			}
			$insert_id = $this->insert(array(
					'active' => $_POST['active'],
					'sub' => $_POST['sub'] == 0 ? NULL : $_POST['sub']
			));
			$url = ($_POST['url'] === '') ? ($url = StringLibrary::translit($_POST['name'])) : StringLibrary::translit($_POST['url']);
			$this->checkUrl(self::$table, $url, $insert_id);
			$this->savePhoto($insert_id, $_POST['tmp_image'], self::$table);

			foreach ($this->db->rows('SELECT * FROM language') as $lang) {
				$this->db->query('INSERT INTO `' . $lang['language'] . '_' . self::$table . '` SET `name`=?,`body`=?,`catalog_id`=?', [$_POST['name'], $_POST['body'], $insert_id]);
			}

			// Save meta data
			$meta = new Meta($this->sets);
			$meta->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);

			// Save catalog params
			if (!empty($_POST['cat_id'])) {
				foreach ($_POST['cat_id'] as $cat_id) {
					$this->db->query('INSERT INTO params_catalog SET params_id=?,catalog_id=?', [$cat_id, $insert_id]);
				}
			}

			if ($open) {
				header('Location: /admin/' . self::$table . '/edit/' . $insert_id);
				exit();
			}
			$message = messageAdmin('Данные успешно добавлены');
		}
		return $message;
	}

	public function save() {
		$message = '';
		if (isset($this->registry['access'])) {
			$message = $this->registry['access'];
		} else {
			if (isset($_POST['save_id']) && is_array($_POST['save_id'])) {
				if (isset($_POST['save_id'], $_POST['name'], $_POST['url'])) {
					$count = count($_POST['save_id']) - 1;
					for ($i = 0; $i <= $count; $i++) {
						if ($_POST['url'][$i] === '') {
							$url = StringLibrary::translit($_POST['name'][$i]);
						} else {
							$url = $_POST['url'][$i];
						}
						$this->checkUrl(self::$table, $url, $_POST['save_id'][$i]);
						$param = array($_POST['name'][$i], $_POST['save_id'][$i]);
						$this->db->query('UPDATE `' . $this->registry['key_lang_admin'] . '_' . self::$table . '` SET `name`=? WHERE catalog_id=?', $param);
					}
					$message .= messageAdmin('Данные успешно сохранены');
				} else {
					$message .= messageAdmin('Заполнены не все обязательные поля', 'error');
				}
			} else {
				if (isset($_POST['active'], $_POST['url'], $_POST['id'], $_POST['name'], $_POST['title'], $_POST['keywords'], $_POST['description'], $_POST['body'])) {
					if ($_POST['url'] === '') {
						$url = StringLibrary::translit($_POST['name'] . '-' . $_POST['id']);
					} else {
						$url = StringLibrary::translit($_POST['url']);
					}
					// Save meta data
					$meta = new Meta($this->sets);
					$meta->save_meta(self::$table, $url, $_POST['title'], $_POST['keywords'], $_POST['description']);
					if (!isset($_POST['position'])) {
						$_POST['position'] = 0;
					}
					if ($_POST['sub'] == 0) {
						$sub = NULL;
					} else {
						$sub = $_POST['sub'];
					}
					$this->checkUrl(self::$table, $url, $_POST['id']);

					$this->db->query('UPDATE `' . self::$table . '` SET `active`=?,sub=? WHERE id=?', [$_POST['active'], $sub, $_POST['id']]);
					$this->db->query('UPDATE `' . $this->registry['key_lang_admin'] . '_' . self::$table . '` SET `name`=?,`body`=? WHERE `catalog_id`=?', [$_POST['name'], $_POST['body'], $_POST['id']]);
					// Save catalog params
					$this->db->query('DELETE FROM `params_catalog` WHERE `catalog_id`=?', [$_POST['id']]);
					if (isset($_POST['cat_id']) && count($_POST['cat_id']) != 0) {
						if (isset($_POST['cat_id'])) {
							$count = count($_POST['cat_id']) - 1;
							for ($i = 0; $i <= $count; $i++) {
								$this->db->query('INSERT INTO params_catalog SET params_id=?,catalog_id=?', [$_POST['cat_id'][$i], $_POST['id']]);
							}
						}
					}
					if (isset($_FILES['icon']['tmp_name']) && $_FILES['icon']['tmp_name'] != '') {
						copy($_FILES['icon']['tmp_name'], "files/catalog/icon{$_POST['id']}.png");
					}
					$message .= messageAdmin('Данные успешно сохранены');
				} else {
					$message .= messageAdmin('Заполнены не все обязательные поля', 'error');
				}
			}
		}
		return $message;
	}

	public function queryProducts($param = []) {

		if (!isset($param['select'])) {
			$param['select'] = '';
		}
		if (!isset($param['join'])) {
			$param['join'] = '';
		}
		if (!isset($param['where'])) {
			$param['where'] = '';
		}
		if (!isset($param['group'])) {
			$param['group'] = 'tb.id';
		};
		if (!isset($param['order'])) {
			$param['order'] = 'tb.`sort` ASC,tb.id DESC';
		}
		if (!isset($param['limit'])) {
			$param['limit'] = '';
		}
		if (!isset($param['having'])) {
			$param['having'] = '';
		}

		if (!isset($param['admin'])) {
			$price_type = $_SESSION['price_type_id'];

			$param['select'] .= "
			  GROUP_CONCAT(DISTINCT pss.status_id SEPARATOR '|') AS statuses,
            price.price AS price_sort,
            pss.status_id,
            lang_ps.name AS status";
			$param['where'] .= " AND tb.active='1'";

			$param['join'] .= ' 
                LEFT JOIN product_catalog p_cat ON p_cat.product_id=tb.id
			    LEFT JOIN params_product pp ON pp.product_id = tb.id
			    LEFT JOIN product_status_set pss ON pss.product_id = tb.id
                LEFT JOIN ' . $this->registry['key_lang'] . "_product_status lang_ps ON pss.status_id = lang_ps.product_status_id
                LEFT JOIN price ON price.product_id = tb.id AND price.price_type_id = '" . $price_type . "'";
		} else {
			$param['join'] .= "LEFT JOIN price price ON price.product_id = tb.id AND price.price_type_id = '1'";
		}

		$q = [
				'select' => '
		        tb.id,
		        tb.url,
		        tb.photo,
		        tb.sizes,
		        tb.packaged,
		        tb_lang.name,
		        tb_lang.material,
		        tb_lang.body_m,
		        price.id as price_id,
		        price.price,
		        price.code,
		        price.basePrice,
		        price.discount,
		        price.stock,
		        ' . $param['select'],
				'join' => '' . $param['join'],
				'where' => "tb.id!='0' " . $param['where'],
				'having' => $param['having'],
				'group' => $param['group'],
				'order' => $param['order'],
				'limit' => $param['limit'],
				'type' => 'rows'
		];
		return $q;
	}

	public function subcats($catrow) {
		$subcats = $this->db->rows('SELECT tb.url,tb2.*,count(distinct prod.id) as count FROM `catalog` tb LEFT JOIN `ru_catalog` tb2 ON tb.id=tb2.catalog_id LEFT JOIN product_catalog prodcat ON prodcat.catalog_id=tb.id LEFT JOIN product prod ON prod.id=prodcat.product_id WHERE tb.sub=? GROUP BY tb.id', [$catrow['id']]);
		if (count($subcats) == 0 && isset($catrow['sub'])) {
			$subcats = $this->db->rows('SELECT tb.url,tb2.*,count(distinct prod.id) as count FROM `catalog` tb LEFT JOIN `ru_catalog` tb2 ON tb.id=tb2.catalog_id LEFT JOIN product_catalog prodcat ON prodcat.catalog_id=tb.id LEFT JOIN product prod ON prod.id=prodcat.product_id WHERE tb.sub=? GROUP BY tb.id', [$catrow['sub']]);
		}
		return $subcats;
	}

	public function getParams($products, $cat_id) {
		$product_q = '';
		foreach ($products as $row) {
			$product_q2 = '';
			$row2 = explode(',', $row['params']);
			foreach ($row2 as $row3) {
				if ($product_q2 != '') {
					$product_q2 .= ' OR ';
				}
				$product_q2 .= "pp.params_id='" . $row3 . "'";
			}
			if ($product_q2 != '') {
				if ($product_q != '') {
					$product_q .= ' or ';
				}
				$product_q .= "(($product_q2) AND pp.product_id='{$row['id']}')";
			}
		}
		if ($product_q != '') {
			$product_q = ' AND (' . $product_q . ')';
		}
		$params_q = '';
		$i = 0;
		if (isset($_SESSION['params'][0]) && ($_SESSION['params'][0] != '')) {
			foreach ($_SESSION['params'] as $row) {
				if ($params_q != '') {
					$params_q .= ' OR ';
				}
				$params_q = "pp.params_id='{$row}'";
				if ($i == 0) {
					$first = $row;
				}
				$i++;
			}
		}
		if ($params_q != '' && $product_q != '') {
			$row = $this->db->row("SELECT sub FROM params WHERE id='$first'");
			$product_q = substr($product_q, 0, strlen($product_q) - 1) . " OR tb1.sub='{$row['sub']}')";
			$params_q = ' AND (' . $params_q . ')';
		}
		$sub_cat = '';
		$res = $this->db->rows("SELECT id FROM catalog WHERE sub='$cat_id'");
		foreach ($res as $row2) {
			$sub_cat .= " OR pc.catalog_id='" . $row2['id'] . "'";
		}
		if ($sub_cat != '') {
			$sub_cat = "(pc.catalog_id='" . $cat_id . "' $sub_cat)";
		} else {
			$sub_cat = "pc.catalog_id='" . $cat_id . "'";
		}
		$query = '  SELECT tb1.id,tb1.url,tb1.sub,tb2.name,COUNT(DISTINCT pc.product_id) as count,pp.product_id,pp.params_id FROM `params` tb1 LEFT JOIN ' . $this->registry['key_lang'] . "_params tb2 ON tb1.id=tb2.params_id LEFT JOIN params_product pp ON pp.params_id=tb1.id LEFT JOIN product_catalog pc ON pp.product_id=pc.product_id WHERE tb1.active='1' AND (($sub_cat $product_q)  OR tb1.sub IS NULL) GROUP BY tb1.id ORDER BY tb1.`sort` ASC,tb2.`name` ASC,tb1.id DESC";
		$res = $this->db->rows($query);
		return $res;
	}

	public function sub_id($table, $id, $table_dop = 'id') {
		$sel = '';
		$queri = "SELECT * FROM `$table` WHERE `sub`=? and `active`='1' ORDER BY `$table`.`sub` asc,`$table`.`sort` asc";
		$resul = $this->db->rows($queri, [$id]);
		$sel = '';
		foreach ($resul as $row) {
			$sub = $row['id'];
			$sel = $sel . "$table_dop ='$sub' OR " . $this->sub_id($table, $sub, $table_dop);
		}
		return $sel;
	}

	public function getSubCat($id) {
		return $this->find(['select' => 'tb.id,tb.url,tb_lang.name', 'where' => '__sub:=' . $id . '__ AND __tb.active:=1__', 'order' => 'sort asc', 'type' => 'rows']);
	}

	public function calcSearch($search, $user_id, $result_count) {
		$search = trim(strip_tags(mb_strtolower($search)));
		$srch = $this->find(['table' => 'search_queries', 'where' => 'query="' . $search . '"']);
		if (!$srch) {
			$srch['id'] = $this->insert(['query' => $search, 'date_add' => date('Y-m-d H:i:s'), 'results' => $result_count], 'search_queries', true);
		} else {
			$this->update(['cnt' => ($srch['cnt'] + 1), 'results' => $result_count], ['id', '=', $srch['id']], 'search_queries', true);
		}
		if (isset($user_id) && $user_id > 0) {
			$this->insert(['found' => $result_count, 'user_id' => $user_id, 'query_id' => $srch['id']], 'search_queries_users', true);
		}
	}
}
