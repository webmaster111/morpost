<?

class CatalogController extends BaseController
{
    private $current_catalog;
    private $params_groups;
    private $active_filters;
    private $products;
    private $catalog_sort;

    function __construct($registry, $params)
    {
        parent::__construct($registry, $params);
        $this->tb = Catalog::$table;
        $this->name = "Каталог";
        $this->filters = new Filters($this->sets);
        $this->paramsModel = new Params($this->sets);

    }

    public function indexAction()
    {
        $_SESSION['page_type'] = 'category';
        if ($this->params['catalog'] == 'all') {
            $template = 'list';
            $this->current_catalog = $this->model->getPage('catalog/all');
            $data['meta']['name'] = !empty($this->current_catalog['name']) ? $this->current_catalog : $this->translation['catalog'];
            $vars['catalogs'] = $this->catalog->getAll(" AND active='1' AND tb.sub IS NULL");
            unset($this->current_catalog['id']);
            $this->current_catalog['url'] = 'all';
            $this->current_catalog['page'] = true;
        } else {


            if ($this->params['catalog'] == 'search' && strlen($_GET['word']) < 3) redirect301('/');
            $template = 'item';
            $vars['table'] = $this->tb;
            $this->current_catalog = $this->getCurrentCatalog();

            if (!$this->current_catalog && $this->params['catalog'] !== 'search' || ($this->params['catalog'] == 'search' && empty($_GET['word']))) return Router::act('error', $this->registry);
            $this->setCatalogSort();
            $this->prepareProductsAndFilters();
            $parent_ = $this->current_catalog['sub'] ? $this->current_catalog['sub'] : $this->current_catalog['id'];
            $product_sidebar = '';
            if ($this->params['catalog'] !== 'search') {
                $product_sidebar = $this->product->find(array(
                    'select' => 'tb.id,tb.url,tb.photo,tb_lang.name,catalog.id as catalog_id,price.price,price.basePrice,price.discount',
                    'type' => 'rows',
                    'join' => "LEFT JOIN product_catalog pc ON pc.product_id = tb.id
                       LEFT JOIN catalog ON catalog.id=pc.catalog_id
                       LEFT JOIN price ON price.product_id = tb.id
                       ",
                    'where' => "catalog.id=" . $this->current_catalog['id'],
                    'order' => 'tb.id DESC',
                    'limit' => 3));
            }
            $menu_catalogs = getTree(containArrayInHisId($this->catalog->getAll(" AND tb.active='1'")));
            $vars['sidebar'] = $this->view->Render('catalog/sidebar.phtml', array(
                'menu_catalogs' => $this->view->Render('layout/menu.phtml', array('catalogs' => $menu_catalogs)),
                'products' => $product_sidebar
            ));

            $vars['paging'] = $this->products['pagingMeta'];
            $vars['page_num'] = $this->params['page'] ?? '';
            $vars['filters'] = $this->renderFilters();
            $vars['list'] = $this->renderCatalogProducts();
            $vars['count_numb'] = $this->products['count'];
            $vars['count'] = $this->products['count'] - (int)$this->settings['paging_product'];
            if ($vars['count'] > 0) $vars['no-load'] = true;
            $data['meta'] = $vars['catalog'] = $this->current_catalog;
            if (isset($_GET['word']) && strlen($_GET['word']) >= 3)
                $data['meta']['name'] .= $this->translation['search'] . ' : ' . $_GET['word'];

            if (!isset($_SESSION[$vars['catalog']['id'] . 'raiting']))
                $_SESSION[$vars['catalog']['id'] . 'raiting'] = rand(47, 50) / 10;

            if (!isset($_SESSION[$vars['catalog']['id'] . 'reviews']))
                $_SESSION[$vars['catalog']['id'] . 'reviews'] = rand(10, 30);


            $data['meta']['name'] .= $this->generateMetaDataByProductFilters($this->getActiveGroups());
        }
        $data['breadcrumbs'] = $this->catalog->getBreadCat($this->current_catalog);

        if (isset($data['breadcrumbs'])) $vars['breadcrumbs'] = $this->model->breadcrumbs($data['breadcrumbs'], $this->view);
        $data['content'] = $this->view->Render('catalog/' . $template . '.phtml', $vars);

        return $this->Index($data);
    }

    public function getMoreAction()
    {
        if (isset($_POST['offset'], $_POST['catalog'])) {

            $offset = $_POST['offset'];
            $on_page = $this->settings['paging_product'];
            $this->params['catalog'] = $_POST['catalog'];
            $this->show_products = true;
            $result = array('offset' => $offset + 1);
            $this->product->params['page'] = $offset + 1;

            $this->current_catalog = $this->getCurrentCatalog();
            $this->setCatalogSort();
            $this->prepareProductsAndFilters();
            $vars['list'] = $this->renderCatalogProducts();


            $result['count'] = $this->products['count'] - $result['offset'] * $on_page;
            $_POST['offset_page'] = $result['offset'];
            $result['thumbs'] = $this->renderCatalogProducts();
            $result['active_filters'] = $this->active_filters;
            if (count($this->products['list']) < $on_page || $result['count'] <= 0)
                $result['offset'] = 0;


            return json_encode($result);
        }
        return false;
    }

    private function getActiveGroups()
    {
        if (!empty($this->active_filters)) {
            return getTree(containArrayInHisId(array_merge(
                $this->paramsModel->getAll(" AND tb . id IN({$_GET['filters']})"),
                $this->paramsModel->find(array(
                    'select' => 'tb.id,tb_lang.name,tb.sub',
                    'type' => 'rows',
                    'join' => "LEFT JOIN params ON tb . id = params . sub AND params . id IN({$_GET['filters']})",
                    'where' => "params . id IS NOT NULL",
                    'group' => 'params.id'))
            )));
        }
        return null;
    }

    private function setCatalogSort()
    {

        if (isset($_POST['catalog_sort_type'])) {
            $catalog_sort_type = $_POST['catalog_sort_type'];

        } else if (isset($_SESSION['catalog_sort_type'])) {
            $catalog_sort_type = $_SESSION['catalog_sort_type'];

        } else {
            $catalog_sort_type = 0;
        }

        $_SESSION['catalog_sort_type'] = $catalog_sort_type;

        switch ($catalog_sort_type) {
            case 1:
                // Алфавит (А-Я)
                $this->catalog_sort = 'tb_lang.name ASC, tb.sort DESC, tb.id DESC';
                break;
            case 2:
                // Алфавит (Я-А)
                $this->catalog_sort = 'tb_lang.name DESC, tb.sort DESC, tb.id DESC';
                break;
            case 3:
                // Цена (возрастание)
                $this->catalog_sort = 'price_sort ASC, tb.sort DESC, tb.id DESC';
                break;
            case 4:
                // Цена (убывание)
                $this->catalog_sort = 'price_sort DESC, tb.sort DESC, tb.id DESC';
                break;
            default:
                // По-умолчанию - по сортировке и id
                $this->catalog_sort = 'tb.sort DESC, tb.id DESC';
        }

    }

    private function prepareProductsAndFilters()
    {
        // Получаем активные фильтры
        if (isset($_GET['filters'])) {
            $this->active_filters = explode(",", $_GET['filters']);
        }
        // Получаем список продуктов каталога, с учетом фильтров
        $this->products = $this->getProductsAction($this->getFilters());
        // Получаем список фильтров, которые есть у полученных продуктов
        $this->setParamsGroups();
    }

    private function getCurrentCatalog()
    {
        return $this->catalog->find(['where' => 'tb.active = "1" AND tb.url = "' . $this->params['catalog'] . '"']);
    }

    private function renderFilters()
    {
        return $this->view->Render('catalog/filters.phtml', [
            'main_catalogs' => $this->catalog->getAll('AND tb.sub IS NULL'),
            'catalog' => (isset($this->current_catalog['page']) ? $this->current_catalog['page'] : '') ? '' : $this->current_catalog,
            'params_groups' => $this->params_groups,
            'active_filters' => $this->active_filters
        ]);
    }

    private function renderCatalogProducts()
    {
        return $this->view->Render('product/thumb.phtml', array(
            'products' => $this->products['list'],
            'md-4' => true
        ));
    }

    private function getFilters()
    {
        // Получаем список фильтров из GET-параметров
        $where = '';
        if (isset($_GET['filters']) && !empty($_GET['filters'])) {
            $filters = $_GET['filters'];
            $where = ' AND pp.params_id IN(' . $filters . ') ';
        }
        return $where;
    }

    private function getProductsAction($where)
    {
        // Формируем sql-запрос
        $having = "";

        $_SESSION['search_word'] = null;
        if ($this->current_catalog['id']) {
            $where .= ' AND p_cat.catalog_id = "' . $this->current_catalog['id'] . '" ';
        } else if ($this->params['catalog'] == 'search' && !empty($_GET['word'])) {
            $_SESSION['search_word'] = $word = strip_tags($_GET['word']);
            //$searchCat = $this->getSearchCategory($word);
            $where .= " AND (
                            tb_lang.body LIKE '%{$word}%' 
                            OR tb.code LIKE '%{$word}%' 
                            OR tb_lang.name LIKE '%{$word}%' 
                            OR tb_lang.body_m LIKE '%{$word}%'
                        )";
        }

        $count_filters = count($this->active_filters);
        if ($count_filters > 0) {
            $having = 'count(distinct pp.params_id) = ' . count($this->active_filters);
        }

        $query = $this->catalog->queryProducts([
            'where' => $where,
            'order' => $this->catalog_sort,
            'having' => $having
        ]);

        // Отправляем sql-запрос
        return $this->product->find(array_merge($query, [
                'paging' => $this->settings['paging_product']
            ]
        ));
    }

//    private function getSearchCategory($word)
//    {
//        $category = array();
//        $query =$this->db->rows();
//        return $category;
//    }

    private function setParamsGroups()
    {
        $where = $this->current_catalog['id'] ? 'AND tb.active="1" AND p_cat.catalog_id = "' . $this->current_catalog['id'] . '" ' : '';

        $products = $this->product->find($this->catalog->queryProducts(['where' => $where]));


        if (!empty($products) && count($products) > 0) {
            $ids = implode(',', array_map(
                function ($item) {
                    return $item['id'];
                }, $products));


            $products = array_filter($products, function ($products) {
                return $products['basePrice'] != '';
            });
            // set min-max price for sort
            usort($products, function ($a, $b) {
                return floatval($a['basePrice']) <= floatval($b['basePrice']);
            });
            $_SESSION['max-price'] = $products[0]['basePrice'];
            $_SESSION['min-price'] = end($products)['basePrice'];
            $_SESSION['count-product'] = count($products);


            $params_list = $this->paramsModel->find([
                'select' => 'tb.*,tb_lang.*',
                'type' => 'rows',
                'join' => 'LEFT JOIN params_product ON tb.id = params_product.params_id',
                'where' => 'tb.active = "1" AND tb.sub IS NOT NULL AND params_product.product_id IN (' . $ids . ')',
                'group' => 'tb.id'
            ]);

            $this->params_groups = getTree(containArrayInHisId(
                array_merge(
                    $this->paramsModel->getAll(' AND tb.sub IS NULL'),
                    $params_list)
            ));
        }
    }


    function searchAction()
    {
        $where = $this->search_split($_POST['word']);
        $res = $this->db->rows("SELECT tb . id,tb . url,tb . photo,tb_lang . name,tb . url FROM product tb LEFT JOIN " . $this->key_lang . "_product tb_lang ON tb_lang . product_id = tb . id LEFT JOIN product_catalog pc ON pc . product_id = tb . id WHERE tb . active = '1' AND ($where) GROUP BY tb . id ORDER BY tb_lang . name ASC,tb . id DESC LIMIT 10");
        if (count($res) > 0) {
            $result = '<ul>';
            foreach ($res as $row) {
                $src = '';
                if (file_exists($row['photo'])) $src = '<img alt="' . $row['name'] . '" title="' . $row['name'] . '" src=" / ' . str_replace('_s . ', '_s_2x . ', $row['photo']) . '" />';
                $result .= '<li><a href="' . LINK . ' / product / ' . $row['url'] . '"><div class="search_photo">' . $src . '</div><div class="search_name">' . $row['name'] . '</div></a></li>';
            }
            $result .= '</ul>';
        } else
            $result = '<div class="alert alert - info">' . $this->translation['not_found'] . '</div>';
        echo $result;
    }

    function search_split($word)
    {
        $where = "";
        $arr = explode(' ', trim($word));
        foreach ($arr as $row) {
            if ($where != '') $where .= ' AND ';
            $where .= "(tb_lang . name like '%{$row}%' OR tb_lang . body_m like '%{$row}%' OR tb_lang . body like '%{$row}%' OR tb . code like '%{$row}%')";
        }
        if ($where == '') $where .= "tb_lang . name like '%{$word}%' OR tb_lang . body_m like '%{$word}%' OR tb_lang . body like '%{$word}%' OR tb . code like '%{$word}%'";
        return $where;
    }

    /**
     * Метод задает значения если их нет. Вынесено из index сюда для улучшения читаемости
     */
    function setDefaultSessionData()
    {
        if (($_POST['items'] == '') && (!isset($_POST['clear_id']))) $_SESSION['params'] = [];
        if (!isset($_SESSION['onpage'])) $_SESSION['onpage'] = $this->onpage[0];
        if (isset($_POST['onpage'])) {
            foreach ($this->onpage as $row) if ($_POST['onpage'] == $row) $_SESSION['onpage'] = $row;
            header('Location: ' . StringLibrary::getUrl2('page'));
            exit();
        }
        if (!isset($_SESSION['search'])) $_SESSION['search'] = '';
        if (isset($_POST['search'])) $_SESSION['search'] = $_POST['search'];
        if (!isset($_SESSION['catalog'])) $_SESSION['catalog'] = '';
        if (!isset($_SESSION['sub']) || (!isset($_POST['sub']) && isset($_POST['sub_hid'])) || $_SESSION['catalog'] != $this->params['catalog']) $_SESSION['sub'] = [];
        if (isset($_POST['sub'])) $_SESSION['sub'] = $_POST['sub'];
        $_SESSION['catalog'] = $this->params['catalog'];
        $_SESSION['catalog_contin'] = LINK . " / catalog /{
                $this->params['catalog']}";
        #Start sort
        if (isset($_POST['sort']) && $_POST['sort'] != '') {
            header('Location: ' . StringLibrary::getUrl('sort', $_POST['sort']));
            exit();
        } elseif (isset($_POST['sort'])) {
            header('Location: ' . StringLibrary::getUrl2('sort'));
            exit();
        }
        $_SESSION['sort'] = "tb . sort DESC,tb . id desc";
        if (isset($this->params['sort']) && ($this->params['sort'] == "price - asc")) $_SESSION['sort'] = "price asc,tb . id desc";
        elseif (isset($this->params['sort']) && ($this->params['sort'] == "price - desc")) $_SESSION['sort'] = "price desc,tb . id desc";
        elseif (isset($this->params['sort']) && ($this->params['sort'] == "sort - asc")) $_SESSION['sort'] = "tb . sort asc,tb . id desc";
        elseif (isset($this->params['sort']) && ($this->params['sort'] == "sort - desc")) $_SESSION['sort'] = "tb . sort desc,tb . id desc";
        #!End sort
    }

    /*
     * Формируем мета данные в зависимости от выбранных фильтров пример: Штаны Цвет: синий,красный;Размер: L,XL
     * Принимает аргументом текущие выбранные фильтры
     */
    function generateMetaDataByProductFilters($paramsUrl)
    {
        if (!$paramsUrl) return '';
        $meta = ' - ';
        foreach ($paramsUrl as $key => $paramParent) {
            $meta .= $paramParent['name'] . ': ';
            foreach ($paramParent['childs'] as $childId => $child) {
                $meta .= $child['name'];
                if (!isLast($paramParent['childs'], $childId)) $meta .= ',';// для последнего из этой группы не ставим ','
            }
            if (!isLast($paramsUrl, $key)) $meta .= '; ';
        }
        return mb_strtolower($meta, 'UTF-8');
    }

    function getCatalogProductIds($catalogId)
    {
        // Получим список id товаров закрепленных за этим и дочерними каталогами
        $catalogsId = $this->filters->getSubCatalogs($catalogId);
        $productIds = $this->db->cell("SELECT GROUP_CONCAT(product_id) FROM product_catalog WHERE catalog_id IN(" . $catalogsId . ")");
        if (!$productIds) $productIds = 0;
        return " AND tb . id IN(" . $productIds . ")";
    }
}