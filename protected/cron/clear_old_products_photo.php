<?php
// Поиск и перемещние в "корзину" файлов фотографий отсутвующих товаров
error_reporting(E_ALL);
set_time_limit(9999);
mb_internal_encoding("UTF-8");
date_default_timezone_set('Europe/Kiev');

require_once('../config.php');
require_once('../libraries/Mail.php');

define('DS', DIRECTORY_SEPARATOR);
define('PATH', __DIR__ . DS . 'files' . DS . 'product'. DS); // Полный путь к папке с фото товаров на диске
define('TRASH_PATH', __DIR__ . DS . 'files' . DS . 'trash'. DS . 'product'. DS); // Полный путь к папке "корзины"
define('PATH_TO_LOG', __DIR__ . '/log.txt'); // Путь к файлу логов

$settings = [
    'sitename' => $_SERVER['HTTP_HOST'],
    'email' => 'cssskylogic@gmail.com,admin@skylogic.com.ua'
];
$dbh = new PDO('mysql:host=' . $DB_Host . ';dbname=' . $DB_Name, $DB_UserName, $DB_Password);

file_put_contents(PATH_TO_LOG, 'Start --- ' . date('Y-m-d H:i:s') . "\r\n", FILE_APPEND);

// Поиск всех товаров в базе данных
$products_ids = $dbh->query("SELECT `id` FROM `product`")->fetchAll(PDO::FETCH_COLUMN);
file_put_contents(PATH_TO_LOG, 'Найдено товаров в базе данных: ' . count($products_ids) . "\r\n", FILE_APPEND);

// Поиск корневых директорий с фото товаров (0...9)
$dirs = array_values(array_filter(scandir(PATH), static function ($dir) {
    return in_array($dir, ['0', '1', '2', '3', '5', '6', '7', '8', '9']);
}));

$dirs_count = 0; // Счетчик всех папок
$old_dirs = []; // Массив устаревших папок
$removed_dir_count = 0; // Счетчик удаленных папок
$skipped_count = 0; // Счетчик пропущенных папок

foreach ($dirs as $dir) {
    $in_dirs = array_diff(scandir(PATH . DS . $dir), ['.', '..']);
    $dirs_count += count($in_dirs);

    foreach ($in_dirs as $in_dir) {
        if (!in_array($in_dir, $products_ids)) { // Папка устревшая (нет товара с таким id) ?
            $old_dirs[] = $in_dir;
            $product_dir = 'files' . DS . 'product' . DS . $dir . DS . $in_dir;
            if (searchPhoto($dbh, 'files/product/'. $dir . '/' . $in_dir)) { // Поиск ссылок в базе
                file_put_contents(PATH_TO_LOG, 'В базе данных найдена ссылка на папку ' . $product_dir . "\r\n", FILE_APPEND);
                $skipped_count++;
            } else {
                try { // Попытка переместить папку
                    if (rename(PATH . $dir . DS . $in_dir, TRASH_PATH  . $dir . DS . $in_dir)){
                        $removed_dir_count++;
                        file_put_contents(PATH_TO_LOG, 'Папка ' . $product_dir . " перемещена в корзину \r\n", FILE_APPEND);
                    } else {
                        $skipped_count++;
                        throw new Exception('Не удалось переместить папку '. $product_dir);
                    }
                } catch (Exception $e) {
                    file_put_contents(PATH_TO_LOG, $e->getMessage() . "\r\n", FILE_APPEND);
                }
            }
        }
    }
}

file_put_contents(PATH_TO_LOG, 'Найдено папок с фото товаров: ' . $dirs_count . "\r\n", FILE_APPEND);
file_put_contents(PATH_TO_LOG, 'Найдено папок с фото несуществующих товаров: ' . count($old_dirs)
    . ' / перемещено в корзину: ' . $removed_dir_count . ' / пропущено: ' . $skipped_count . "\r\n", FILE_APPEND);

file_put_contents(PATH_TO_LOG, 'End --- ' . date('Y-m-d H:i:s') . "\r\n", FILE_APPEND);

Mail::send('Обработка фото устаревших товаров на сайте "' . $settings['sitename'] . '".',
    'Фото устаревших товаров успешно обработаны на сайте "' . $settings['sitename'] . '". Перемещено в корзину ' . $removed_dir_count . ' папок с фото.', $settings['email'], $settings['sitename']);

function searchPhoto($db, $filepath)
{
    $res_1 = $db->query("SELECT `product_id` FROM `product_photo` WHERE `photo` LIKE '" . $filepath . "/%'")->fetchAll(PDO::FETCH_COLUMN);
    $res_2 = $db->query("SELECT `id` FROM `product` WHERE `photo` LIKE '" . $filepath . "/%'")->fetchAll(PDO::FETCH_COLUMN);
    return $res_1 || $res_2;
}