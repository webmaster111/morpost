<?
// Import Products
require_once(dirname(__FILE__) . "/Import.php");

class ImportProducts extends Import
{
  public $table = 'product';
  private $language, $catalog, $product;

  function __construct($sets)
  {
    parent::__construct($sets, $this->table, 'products');
    $this->language = $this->db->rows("SELECT language.language FROM language");
    $this->catalog = new Catalog($sets);
    $this->product = new Product($sets);
  }


  public function import()
  {
    $count = 0;
    $files = $this->start();
    if (!isset($files) && count($files) == 0) return false;
    foreach ($files as $file) {
      if (file_exists($file) AND $this->isXmlFile($file)) {
        $this->LogSeparator();
        $this->AddLog('Запущена обработка файла ' . $file);
        $xml = file_get_contents($file);
        $xml = simplexml_load_string($xml);
        $xml = $xml->shop->children();
        $this->saveCategories($xml->categories->children());
        $products = $xml->offers->children();
        if ($products) $this->saveProducts($products);
        else return false;
        $count = count($products);
        // Логи
        $timeLoad = round(microtime(true) - $this->startTimer);
        $this->LogSeparator();
        $this->AddLog('Загрузка товаров за ' . $timeLoad . ' сек (' . round($timeLoad / 60) . ' мин)');
        $this->AddLog("Товары из файла {$file} импортированы!");
        $this->LogSeparator();
        $this->endFile($file);
      }
    }
    return array('log' => $this->log, 'count' => "Товаров : " . $count);
  }

  private function saveProducts($products)
  {
    foreach ($products as $key => $product) {
      $RU = [
          'name' => (string)$product->name,
          'body' => (string)$product->description,
          'body_m' => ''
      ];
      $packaged = (string)$product->sales_notes;
      if (!empty($packaged)) {
        preg_match('/^.*(.\d).*$/m', $packaged, $matches);
        $packaged = intval($matches[1]);
      } else $packaged = 1;
      if (empty($packaged)) $packaged = 1;
      $mainArr = [
          'active' => 1,
          'code' => (string)$product->vendorCode,
          'id1c' => (string)$product['id'][0],
          'packaged' => $packaged,
          'date_edit' => date("Y-m-d H:i:s"),
          'date_add' => date("Y-m-d H:i:s")
      ];

      $insert_new = true;
      $productId = $this->db->cell("SELECT id FROM product WHERE id1c=?", array($mainArr['id1c']));
      if ($productId) {
        $this->product->update($mainArr, " `id`='" . $productId . "'", Product::$table, false);
        $insert_new = false;
        // continue;
      } else $productId = $this->product->insert($mainArr, Product::$table, false);

      if ($productId) {
        $RU['product_id'] = $productId;
          $url = StringLibrary::translit($RU['name']);
        $this->product->checkUrl(Product::$table, $url, $productId);
        foreach ($this->language as $lang) {
          $this->db->query("DELETE FROM `" . $lang['language'] . "_product` WHERE `product_id`=?", [$productId]);
          $this->product->insert($RU, $lang['language'] . '_product', false);
        }

        // закрепление за каталогами
        if (!empty($product->categoryId)) {
          $this->db->query("DELETE FROM `product_catalog` WHERE `product_id`=?", [$productId]);
          $catalog = $this->db->row("SELECT id,sub FROM catalog WHERE old_id=?", array($product->categoryId));
          if ($catalog) {
            $catalogsArr = array();
            $this->getParentsByArray($catalog['id'], $catalogsArr);
            foreach ($catalogsArr as $item)
              $this->db->query("INSERT INTO `product_catalog` SET `product_id`=?, `catalog_id`=?", [$productId, $item]);
          }
        }


        // Сохраняем фото
        if (!empty($product->picture) && $insert_new) {
          $this->db->query("DELETE FROM product_photo WHERE product_id={$productId}");
          if (!is_dir(SITE_PATH . "tmp/")) mkdir(SITE_PATH . "tmp/", 0755, true);
          // lol Rm  =)
          $dir = Dir::createDir($productId, SITE_PATH);
          recurseRmdir(SITE_PATH . $dir[0]);
          $dir = Dir::createDir($productId, SITE_PATH);
          // -----------
          $key_photo = 0;
          foreach ($product->picture as $photo_url) {
            $file = explode('/', $photo_url);
            $file_name = $file[count($file) - 1];
            $file = SITE_PATH . "tmp/" . $file_name;
            copy($photo_url, $file);
            if ($key_photo == 0) {
              $photo = Images::loadOriginalImage($file, SITE_PATH . $dir[0], $productId);
              $photo = explode('/files/', $photo);
              $this->db->query("UPDATE `product` SET `photo`=? WHERE `id`=?", ['files/' . $photo[1], $productId]);
            } else {
              $id = $this->db->insert_id("INSERT INTO `product_photo` SET `product_id`=? ,`texture`='0', `active`='1' , `sort`='0'", array($productId));
              $this->db->query("INSERT INTO `ru_product_photo` SET `product_photo_id`=? ,`name`=?", array($id, $file_name));
              $photo = Images::loadOriginalImage($file, SITE_PATH . $dir[1], $id);
              $photo = explode('/files/', $photo);
              $this->db->query("UPDATE `product_photo` SET `photo`=? WHERE `id`=?", ['files/' . $photo[1], $id]);
            }
            $key_photo++;
          }

        }

        if (!empty($product->price)) {
          $this->db->query("DELETE FROM price WHERE product_id='{$productId}'");
          $price = (string)$product->price;
          $this->model->insert(array(
              'code' => (string)$product->vendorCode,
              'basePrice' => $price,
              'price' => $price,
              'stock' => 10,
              'discount' => 0,
              'product_id' => $productId,
          ), 'price');
        }

      }
    }
  }

  private function getParentsByArray($id, &$array)
  {
    if ($id) {
      $array[] = $id;
      $id_parent = $this->db->cell("SELECT sub FROM catalog WHERE id=?", array($id));
      $this->getParentsByArray($id_parent, $array);
    } else return null;
  }

  private function saveCategories($catalogs)
  {
    if (!empty($catalogs)) {
      foreach ($catalogs as $key => $catalog) {
        $saved = $this->saveCatalog($catalog);
      }

      // Сохраняем вложености
      foreach ($catalogs as $key => $catalog) {
        $parent_id = (string)$catalog['parentId'][0];
        if (!empty($parent_id)) {
          $sub = $this->db->cell("SELECT id FROM `catalog` WHERE `old_id`=?", array($parent_id));
          if ($sub) $this->db->query("UPDATE `catalog` SET `sub`=? WHERE `old_id`=?", array($sub, (string)$catalog['id'][0]));
        }
      }
    }
  }

  private function saveCatalog($catalog)
  {
    $RU = [
        'name' => (string)$catalog[0],
        'body' => '',
    ];
    $mainArr = [
        'active' => 1,
        'sub' => null,
        'old_id' => (string)$catalog['id'][0]
    ];

    $catalogId = $this->db->cell("SELECT id FROM `catalog` WHERE `old_id`=?", array($mainArr['old_id']));
    if ($catalogId) {
      $this->catalog->update($mainArr, " `id`='" . $catalogId . "'", Catalog::$table, false);
    } else {
      $catalogId = $this->catalog->insert($mainArr, Catalog::$table, false);
    }


    if ($catalogId) {
      $RU['catalog_id'] = $catalogId;
        $url = StringLibrary::translit($RU['name']);
      $this->catalog->checkUrl(Catalog::$table, $url, $catalogId);
      foreach ($this->language as $lang) {
        $this->db->query("DELETE FROM `" . $lang['language'] . "_catalog` WHERE `catalog_id`=?", [$catalogId]);
        $this->catalog->insert($RU, $lang['language'] . '_catalog', false);
      }

      $this->LogSeparator();
      $this->AddLog('Каталог сохранен ' . $catalogId . ' _ ' . $RU['name']);
      return array('id' => $catalogId, 'old_id' => $mainArr['old_id']);
    }
    return false;
  }

}