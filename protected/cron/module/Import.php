<?

// Impost Base
class Import
{

  public $table = "product";

  public $log = '';
  public $path_to_file;

  function __construct($sets, $table, $dir)
  {
    $this->sets = $sets;
    $this->db = $this->sets['db'];
    $this->model = new Model($this->sets);
    $this->log = '';
    $this->startTimer = microtime(true);

    $this->path_to_file = ROOT_CRON . 'import/' . $dir . '/';
    define('path_to_img', $this->path_to_file . 'images/');

  }

  public function start()
  {
    $files = scandir($this->path_to_file);
    $files = $this->getDirFiles($this->path_to_file, $files);
    return $files;
  }

  public function endFile($file)
  {
    // перемещение файла в архив
    $pathMove = $this->path_to_file . 'done/' . curdate('Ym');
    if (!is_dir($pathMove)) mkdir($pathMove, 0777, true);
    if (rename($file, $pathMove . '/' . curdate('Hi') . '-' . pathinfo($file, PATHINFO_BASENAME))) ;
    $this->Addlog('Файл ипорта перемещен в архив');
  }

  public function isXmlFile($fileName)
  {
    return ('xml' == end(explode('.', $fileName)));
  }

  public function isXlsxFile($fileName)
  {
    return ('xlsx' == end(explode('.', $fileName)));
  }

  public function getDirFiles($path, $files = [])
  {
    if (!$files) return $files;
    foreach ($files as $k => $file) {
      $files[$k] = $path . $file;
      if (!is_file($path . $file)) unset($files[$k]);
    }
    return $files;
  }

  public function LogSeparator()
  {
    $this->log .= "===============================\n";
  }

  public function AddLog($message)
  {
    $this->log .= $message . "\n";
  }

}