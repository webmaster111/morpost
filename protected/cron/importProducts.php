<?
require_once(dirname(__FILE__) . "/cronInit.php");
require_once(dirname(__FILE__) . "/module/ImportProducts.php");
class cron extends cronInit
{
  public $id = "1";

  public $path_to_file = '';

  public $que;
  public $logTxt = '';

  function __construct($registry, $db)
  {
    parent::__construct($registry, $db);
    $this->startCron();

    $this->logTxt = '';
    $this->startTimer = microtime(true);
    $this->path_to_file = ROOT_CRON . 'import/';
    $this->que = array();
    $this->que['Products'] = new ImportProducts($this->sets);
  }

  function __destruct(){ $this->endCron(); }

	public function todo()
	{
    $result = false;
    $text_count = array();
    //$this->disableLogging();//Выклюич логирование
    $this->setLogFile(ROOT_CRON . 'importProducts.txt')->clearLog()->enableLogging()->logTime();
    $import = array('Products');
    foreach ($import as $value){
      $result_import = $this->que[$value]->import();
      if($result_import){
        $result = true;
        $this->logTxt .= $result_import['log'];
        array_push($text_count,$result_import['count']);
      }
      else {
        $result = false;
        break;
      }
    }
    $this->logCron($this->logTxt);
    $text = 'Выгрузка завершена : ' . date('Y-m-d H:i:s') . '<br/>Обработано :<br/>';
    $text.= implode('<br/>',$text_count);
    $this->showResult($result, $text);
	}
}
$cron = new cron($registry, $db);
$cron->todo();