<?
require_once(dirname(__FILE__) . "/cronInit.php");
class cron extends cronInit
{
	public $id = "22";

	function __construct($registry, $db)
	{
		parent::__construct($registry, $db);
		$this->startCron();
	}

	function __destruct()
	{
		$this->endCron();
	}

	public function todo()
	{
		sleep(1);
	}
}
$cron = new cron($registry, $db);
$cron->todo();