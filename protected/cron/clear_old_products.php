<?php
// Поиск и удаление устаревших товаров из базы данных
error_reporting(E_ALL);
set_time_limit(999);
date_default_timezone_set('Europe/Kiev');
mb_internal_encoding("UTF-8");
define('PATH_TO_LOG', __DIR__ . '/log.txt'); // Путь к файлу логов

require_once('../config.php');
require_once('../libraries/Mail.php');

$settings = [
    'sitename' => $_SERVER['HTTP_HOST'],
    'email' => 'cssskylogic@gmail.com,admin@skylogic.com.ua'
];

$dbh = new PDO('mysql:host=' . $DB_Host . ';dbname=' . $DB_Name, $DB_UserName, $DB_Password);

file_put_contents(PATH_TO_LOG, 'Start --- ' . date('Y-m-d H:i:s') . "\r\n", FILE_APPEND);

// Поиск всех товаров в базе данных
$products_ids = $dbh->query("SELECT `id` FROM `product`")->fetchAll(PDO::FETCH_COLUMN);
file_put_contents(PATH_TO_LOG, 'Найдено товаров в базе данных: ' . count($products_ids) . "\r\n", FILE_APPEND);


// Поиск устаревших товаров в базе данных ($period - сколько времени назад товар редактировался последний раз, напр. "12 month")
$old_products_ids = getOldProducts($dbh, '16 month');
file_put_contents(PATH_TO_LOG, 'Найдено устаревших товаров в базе данных: ' . count($old_products_ids) . "\r\n", FILE_APPEND);

$removed_pr_count = 0;

if (! empty($old_products_ids)) { // Если найдены устаревшие товары
    // Удаление товаров из базы данных
    $removed_pr_count = removeProducts($dbh, $old_products_ids);
    if ($removed_pr_count) {
        file_put_contents(PATH_TO_LOG, "Удалены товары:\r\n" . implode("\r\n", $old_products_ids) . "\r\n", FILE_APPEND);
    }
}

file_put_contents(PATH_TO_LOG, 'Удалено товаров: ' . $removed_pr_count . "\r\n", FILE_APPEND);
file_put_contents(PATH_TO_LOG, 'End --- ' . date('Y-m-d H:i:s') . "\r\n", FILE_APPEND);

Mail::send('Обработка устаревших товаров на сайте "' . $settings['sitename'] . '".',
    'Устаревшие товары успешно обработаны на сайте "' . $settings['sitename'] . '". Удалено товаров ' . $removed_pr_count, $settings['email'], $settings['sitename']);


// Получение списка устаревших товаров
function getOldProducts($db, $period)
{
    $datetime = date('Y-m-d H:i:s', strtotime('-' . $period));
    return $db->query("SELECT `id` FROM `product` WHERE `brend_id` = 2 AND `date_edit` <= '" . $datetime . "'")->fetchAll(PDO::FETCH_COLUMN);
}

// Удаление товаров из БД
function removeProducts($db, $ids)
{
    // счетчик удаленных товаров
    $result = 0;

    foreach ($ids as $id) {
        try {
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->beginTransaction();
            $db->exec("DELETE FROM `product_catalog` WHERE `product_id` = " . $id);
            $db->exec("DELETE FROM `product_other` WHERE `product_id` = " . $id);
            $db->exec("DELETE FROM `product_photo` WHERE `product_id` = " . $id);
            $db->exec("DELETE FROM `product_status_set` WHERE `product_id` = " . $id);
            $db->exec("DELETE FROM `params_product` WHERE `product_id` = " . $id);
            $db->exec("DELETE FROM `ru_product` WHERE `product_id` = " . $id);
            $db->exec("DELETE FROM `edit_products` WHERE `product_id` = " . $id);
            $db->exec("DELETE FROM `product` WHERE `id` = " . $id);
            $db->commit();
            $result++;
        } catch (Exception $e) {
            $db->rollBack();
            file_put_contents(PATH_TO_LOG, $e->getMessage() . "\r\n", FILE_APPEND);
        }
    }
    return $result;
}