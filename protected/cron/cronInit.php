<?
if (version_compare(phpversion(), '5.3.0', '<') == true) {
  die('PHP 5.3 Only');
}
date_default_timezone_set('Europe/Kiev');
define('SITE_PATH', dirname(dirname(dirname(__FILE__))) . "/");
define('ROOT_CRON', SITE_PATH . 'protected/cron/');
define('CLASSES', SITE_PATH . "protected/classes/");
define('CONTROLLERS', SITE_PATH . "protected/controllers/");
define('MODULES', SITE_PATH . "protected/modules/");
define('SUBSYSTEM', SITE_PATH . "protected/subsystem/");
define('LIBRARY', SITE_PATH . "protected/libraries/");
require_once(SITE_PATH . "protected/config.php");
require_once(CLASSES . "initializer.php");

class cronInit
{
  /**
   * Connect to database
   *
   * @var array
   */
  protected $db;
  /**
   * ID row from tasks_log table
   *
   * @var int
   */
  protected $log_id;
  protected $key_lang = "ru";
  /**
   * settings from const table
   *
   * @var array
   */
  protected $settings;

  /**
   * Constructor
   *
   * @return void
   */
  function __construct($registry, $db)
  {
    $this->db = $db;
    $this->settings = $this->db->rows_key("SELECT name, value FROM config");
    $this->translation = $this->db->rows_key("SELECT tb.key,tb2.value FROM translate tb LEFT JOIN " . $this->key_lang . "_translate tb2 ON tb.id=tb2.translate_id");
    $this->registry = $registry;
    $this->sets = ['settings' => $this->settings, 'registry' => $this->registry, 'params' => $this->params, 'db' => $this->db, 'translation' => $this->translation];
    /*$file = dirname(__FILE__).'/log.txt';echo $file;
    $current = file_get_contents($file);
    $current .= $this->settings['max_execution_time']."protected/config88.php \n";
    file_put_contents($file, $current);*/
  }

  /**
   * Verifies the existence of employment cron
   * If does not exist then insert new log record
   *
   * @return void
   */
  function startCron()
  {
    ini_set('max_execution_time', 0);
    set_time_limit(0);
    $row = $this->db->row("SELECT id, date_start FROM tasks_log WHERE busy='1' AND task_id=?", [$this->id]);
    if (!$row) $this->log_id = $this->db->insert_id("INSERT INTO tasks_log SET task_id=?, date_start=?, busy=?", [$this->id, date('Y-m-d H:i:s'), 1]);
    else {
      if (isset($this->settings['max_execution_time'])) $max_execution_time = $this->settings['max_execution_time'];
      else $max_execution_time = 90;
      $date = new DateTime($row['date_start']);
      if (time() - $date->format("U") > $max_execution_time) $this->db->query("UPDATE tasks_log SET status=?, date_end=?, busy=? WHERE id=?", ['abort', date('Y-m-d H:i:s'), 0, $row['id']]);
      echo 'Busy';
      exit();
    }
  }

  /**
   * Update log row SET busy = 0 AND set next time update
   *
   * @return void
   */
  function endCron()
  {
    if ($this->log_id != 0) {
      require_once(CLASSES . 'class.tdcron.php');
      require_once(CLASSES . 'class.tdcron.entry.php');
      $row = $this->db->row("SELECT common FROM tasks WHERE id='{$this->id}'");
      $this->db->query("UPDATE tasks_log SET date_end=?, busy=?, status=? WHERE id=?", [date('Y-m-d H:i:s'), 0, 'ok', $this->log_id]);
      $unixtime = tdCron::getNextOccurrence($row['common'], time());
      $next_update = date('Y-m-d H:i:s', $unixtime);
      $this->db->query("UPDATE tasks SET next_update=? WHERE id=?", [$next_update, $this->id]);
      $this->cleanLog();
    }
  }

  public function logCron($text, $file = '')
  {
    $file = ($file) ? $file : ($this->logFile) ? $this->logFile : false;
    if (!$file) return false;
    if ($this->logEnabled) file_put_contents($file, print_r($text, true) . PHP_EOL, FILE_APPEND);
    return $this;
  }

  public function disableLogging()
  {
    $this->logEnabled = false;
  }

  public function showResult($result, $text)
  {
    if ($this->logEnabled) {
      if ($result !== false) {
        Mail::send('Завершение выгрузки ' . $this->settings['sitename'], $text, $this->settings['email_cron'], $this->settings['sitename']);
      }
      $logs = htmlspecialchars_decode(file_get_contents(ROOT_CRON . 'importProducts.txt'));
      echo str_replace("\n", '<br>', $logs);
    }
  }

  public function logTime($str = '')
  {
    return $this->logCron(curdate() . ' ' . $str . PHP_EOL);
  }

  public function clearLog()
  {
    file_put_contents($this->logFile, '');
    return $this;
  }

  public function enableLogging()
  {
    $this->logEnabled = true;
    return $this;
  }

  public function setLogFile($file)
  {
    if (!file_exists($file)) {
      file_put_contents($file, '');
      chmod($file, 0755);
    }
    $this->logFile = $file;
    return $this;
  }

  public function xml_attr($object, $attribute)
  {
    if (isset($object[$attribute]))
      return (string)$object[$attribute];
  }

  public function checkUrl($tb, $name, $url, $id)//Проверка уникальности URL
  {
    $url = ($url == '') ? Strings::translit($name) : Strings::translit($url);
    if ($this->db->row("SELECT id FROM `" . $tb . "` WHERE url!='' AND url=? AND id!=?", array($url, $id))) $url = $url . '-' . $id;
    $row = $this->db->row("SELECT url FROM `" . $tb . "` WHERE id=?", array($id));
    $this->db->query("UPDATE `" . $tb . "` SET url=? WHERE id=?", array($url, $id));
    return $url;
  }


  /**
   * Clean record if more than 50
   *
   * @return void
   */
  function cleanLog()
  {
    $row = $this->db->row("SELECT COUNT(*) AS cnt FROM tasks_log WHERE task_id='{$this->id}'");
    if ($row['cnt'] > 50) {
      $limit = $row['cnt'] - 50;
      $this->db->query("DELETE FROM tasks_log WHERE id IN (SELECT id FROM (SELECT id FROM tasks_log WHERE task_id = '{$this->id}' ORDER BY date_start ASC LIMIT " . $limit . ") a)");
    }
  }
}