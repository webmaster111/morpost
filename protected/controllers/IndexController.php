<?

class IndexController extends BaseController
{
    protected $blocks;

    function __construct($registry, $params)
    {
        parent::__construct($registry, $params);
        $this->blocks = new Infoblocks($this->sets);
    }

    function indexAction()
    {
        $vars['body'] = $this->model->getPage('/');
        $vars['banner'] = $this->model->getBlock(12);
        $vars['banners'] = $this->blocks->getByType(1);
        $vars['hww_banner'] = $this->model->getBlock(46);
        $vars['hww_text'] = $this->model->getBlock(53);

        $vars['wau_bloks'][] = $this->model->getBlock(47);
        $vars['wau_bloks'][] = $this->model->getBlock(48);
        $vars['wau_bloks'][] = $this->model->getBlock(49);
        $vars['wau_bloks'][] = $this->model->getBlock(50);
        $vars['wau_bloks'][] = $this->model->getBlock(51);
        $vars['wau_bloks'][] = $this->model->getBlock(52);

        $vars['pluses'] = $this->model->getBlock(43);
        $vars['minuses'] = $this->model->getBlock(44);
        $vars['pluses_minuses_after'] = $this->model->getBlock(45);
        $vars['about_text'] = $this->model->getBlock(54);

        $vars['contacts'] = $this->getContacts();

        $vars['news-block'] = $this->view->Render(
            'news/block.phtml',
            array('news' => News::getObject($this->sets)->getLast($this->settings['limit_news_block']))
        );

        $vars['slider'] = $this->view->Render('sliders/reviews_slider.phtml',
            array(
                'photos' => Slider::getObject($this->sets)->getAll()
            )
        );

        $vars['news-block'] = $this->view->Render(
            'news/block.phtml',
            array('news' => News::getObject($this->sets)->getLast($this->settings['limit_news_block']))
        );

        $data['content'] = $this->view->Render('pages/main.phtml', $vars);
        return $this->Index($data);
    }
}