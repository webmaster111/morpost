<?

class ErrorController extends BaseController
{
    function __construct($registry, $params)
    {
        parent::__construct($registry, $params);
    }

    function indexAction()
    {
        header("HTTP/1.1 404 Not Found");
        $data['breadcrumbs'] = array(['name' => '404', 'url' => '#']);
        $data['meta'] = array(
            'title' => 'Page not found',
            'keywords' => 'Page not found',
            'description' => 'Page not found'
        );
        $data['content'] = $this->view->Render('helpers/404.phtml');
        return $this->Index($data);
    }
}