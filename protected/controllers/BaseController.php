<?php

class BaseController
{
	protected $registry;
	protected $params;
	protected $key_lang = 'ru';

	function __construct($registry, $params)
	{
		$this->params = $params;
		$this->registry = $registry;
		$this->db = new PDOchild($registry);
		$this->key_lang = $this->registry['key_lang'];
		$this->key_lang_admin = $this->registry['key_lang_admin'];
		$this->language = $this->db->rows('SELECT * FROM language ORDER BY `id` ASC');
		$const = $this->db->rows_key('SELECT name,value FROM config');
		Registry::set('user_settings', $const);
		$this->settings = Registry::get('user_settings');
		$this->translation = $this->db->rows_key(
			'SELECT tb.key,tb2.value FROM translate tb LEFT JOIN ' . $this->key_lang . '_translate tb2 ON tb.id=tb2.translate_id'
		);
		$this->view = new View($this->registry, ['translate' => $this->translation, 'settings' => $this->settings]);
		$this->sets = [
			'settings' => $this->settings,
			'registry' => $registry,
			'params' => $params,
			'db' => $this->db,
			'translation' => $this->translation
		];
		$this->model = new Model($this->sets);
		$this->catalog = new Catalog($this->sets);
		$this->meta = new Meta($this->sets);
		$this->menu = new Menu($this->sets);

		if (isset($this->params['topic']) && $this->params['topic'] != 'admin') {
			$this->meta->check_redirects();
		}

		$this->currencies = $this->db->rows('SELECT * FROM `currency` ORDER BY `id` ASC');

		$this->product = new Product($this->sets);
		if ($this->product->checkModule()) {
			//Currency
			if (!$_SESSION['currency']) {
				$_SESSION['currency'] = $this->db->row("SELECT * FROM `currency` WHERE `base`='1' ");
			}

			$_SESSION['price_type_id'] = Product::getObject($this->sets)->default_price_type('user');
			$_SESSION['rounding'] = $this->settings['rounding'];
			$this->model->recalc_price_range();
			if (!isset($_SESSION['sort_view'])) {
				$_SESSION['sort_view'] = 'table';
			} elseif (isset($_POST['sort_view']) && $_POST['sort_view'] != '') {
				$_SESSION['sort_view'] = $_POST['sort_view'];
			}
		}
	}

	public function Index($param = [])
	{
		if (!isset($this->params['topic'])) {
			$param['topic'] = '';
		} else {
			$param['topic'] = $this->params['topic'];
		}
		$data = $param;
		$settings = Registry::get('user_settings');
		$data['active_till'] = $this->settings['active_till'] ?? null;
		$data['lang'] = $this->key_lang;
		$data['languages'] = $this->language;
		$data['currency'] = $this->db->rows('SELECT * FROM currency ORDER BY id ASC');
		if (!isset($param['styles'])) {
			$param['styles'] = [];
		}
		if (!isset($param['scripts'])) {
			$param['scripts'] = [];
		}

		//Back side
		if ($param['topic'] == 'admin') {
			$data['menu_inc'] = '';
			$data['admin'] = 'admin';
			$paramsModule = $this->params['module'] ?? '';
			$data['meta']['title'] = $paramsModule . ' - ';

			if (isset($_SESSION['admin'])) {
				$data['prices'] = $this->model->find(['table' => 'modules', 'where' => 'hidden="0" and id="185"']);

				if (!isset($data['breadcrumb'])) {
					$data['breadcrumb'] = $this->model->breadcrumbAdmin();
				}

				if ($_SESSION['admin']['type'] == 1) {
					$data['menu'] = $this->db->rows(
						"SELECT tb.*  FROM `modules` tb WHERE tb.hidden='0' ORDER BY tb.`sort` ASC"
					);
					$data['menu_subsystem'] = $this->db->rows(
						"SELECT tb.* FROM `subsystem` tb ORDER BY tb.sort ASC,tb.`id` DESC"
					);
				} else {
					$data['menu'] = $this->db->rows(
						"SELECT tb.*,subsystem_id FROM `modules` tb 
								RIGHT JOIN moderators_permission tb2 ON tb.id=tb2.module_id AND tb2.moderators_type_id=? AND tb2.permission!=? 
								WHERE tb.hidden='0' 
								GROUP BY tb.id 
								ORDER BY tb.sort ASC",
						[$_SESSION['admin']['type'], '000']
					);
					$data['menu_subsystem'] = $this->db->rows(
						"SELECT tb.* FROM `subsystem` tb 
								LEFT JOIN moderators_permission tb2 ON tb.id=tb2.subsystem_id 
								WHERE tb2.moderators_type_id=? AND tb2.permission!=? 
								GROUP BY tb.id",
						[$_SESSION['admin']['type'], '000']
					);
				}
				$data['key'] = $this->key_lang_admin;
				$data['menu_inc'] = $this->view->Render('menu.inc.phtml', $data);
			} else {
				$data['login'] = 1;
			}

			$styles = array_merge(
				[
				    'jquery.notification.css',
				    'bootstrap.min.css',
				    'font-awesome.min.css',
				    'jquery.fancybox.css',
				    'style.css',
				],
			    $param['styles'], ['default.css']
			);
			$scripts = array_merge(
				[
					'jquery-3.1.1.min.js',
					'bootstrap.min.js',
					'jquery-ui-1.10.3.custom.js',
					'jquery.touch-punch.min.js',
					'autoresize.jquery.js',
					'editors/ckeditor/ckeditor.js',
					'jquery.tablednd_0_5.js',
					'jquery.cookie.js',
					'jquery.fancybox.pack.js',
					'base.js',
					'ajax.js'
				],
				$param['scripts']
			);

			$data['styles'] = $this->view->Load($styles, 'styles', 'admin');
			$data['scripts'] = $this->view->Load($scripts, 'scripts', 'admin');
			return ($this->view->Render('index.phtml', $data));
		}
		else {
			if ($_SERVER['REQUEST_URI'] != strtolower($_SERVER['REQUEST_URI'])) {
				header(
					'Location: http://' . $_SERVER['HTTP_HOST'] .
					strtolower($_SERVER['REQUEST_URI']),
					true,
					301
				);
				exit();
			}
			//Front side
			$data['topic'] = $param['topic'];
			$data['google'] = $settings['google'];
			if ($settings['webmaster_g'] != "") {
				$data['webmaster'] = '<meta name="google-site-verification" content="' . $settings['webmaster_g'] . '"/>';
			} else {
				$data['webmaster'] = '';
			}
			$data['recaptcha_public_key'] = $settings['recaptcha_public_key'];
			$data['recaptcha_secretkey']  = $settings['recaptcha_secretkey'];

			if (!isset($data['meta'])) {
				$data['meta'] = array();
			}
			$data['meta'] = $this->meta->set_meta_data($data['meta'], $param['topic']);
			$data['meta']['url'] = getProtocol() . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$data['seo'] = $this->view->Render('layout/seo.phtml', ['meta' => $data['meta']]);

			$styles = array_merge(
				[
					'bootstrap.min.css',
					'custom-bootstrap.css',
					'normalize.css',
					'font-awesome.min.css',
					//'owl.carousel.min.css',
                    'slick.css',
                    'slick-theme.css',
					'custom-modal.css',
					'sweetalert2.min.css',
					'magnific-popup.css',
                    'style-additional.css'

				],
				$param['styles'],
				['style.css', 'responsive.css', 'pad-mar.css']
			);

			$scripts = array_merge(
				[
					'jquery-1.11.1.min.js',
					'bootstrap.min.js',
					'jquery.magnific-popup.min.js',
					//'owl.carousel.min.js',
                    'slick.min.js',
					'bootstrap-hover-dropdown.min.js',
					'jquery-migrate-1.2.1.min.js',
					'color-animate.js',
					'jquery.mask.min.js',
					'autosize.min.js',
					'sweetalert2.min.js',
					'custom.js',
					'ajax.js'
				],
				$param['scripts'],
				['main.js']
			);

			$data['styles'] = $this->view->Load($styles, 'styles');
			$data['scripts'] = $this->view->Load($scripts, 'scripts');
			$data['head'] = $this->view->Render('layout/head.phtml', $data);

			$contacts = $this->getContacts();

			$menu_catalogs = getTree(containArrayInHisId($this->catalog->getAll(" AND tb.active='1'")));
			$data['menu_catalogs'] = $this->view->Render('layout/menu.phtml', array('catalogs' => $menu_catalogs));

			$data['header'] = $this->view->Render(
				'layout/header.phtml',
				[
					'title' => $data['meta']['title'],
					'menu_catalogs' => $data['menu_catalogs'],
					'top-menu' => $this->view->Render(
						'layout/top-menu.phtml',
						array(
							'menu' => $this->menu->getAll(" AND tb.active='1'")
						)
					),
					'contacts' => $contacts
				]
			);

			$data['footer'] = $this->view->Render(
				'layout/footer.phtml',
				array(
					'menu' => $this->menu->getAll((' AND tb.sub IS NULL')),
					'contacts' => $contacts,
					'block' => $this->model->getBlock(3),
				)
			);

			if (isset($data['breadcrumbs'])) {
				$data['breadcrumbs'] = $this->model->breadcrumbs($data['breadcrumbs'], $this->view);
			}
			return $this->view->Render('index.phtml', $data);
		}
	}

	function getContacts()
	{
		$vars['phone'] = $this->getPhones('phone');
		$vars['phone-bottom'] = $this->getPhones('phone-bottom');

		$emails = trim(html_entity_decode($this->settings['email']));
		$emails = explode(',', $emails);
		$vars['emails'] = array_map(
			function ($email) {
				return trim($email);
			},
			$emails
		);

		$configs = [
			'address',
			'youtube',
			'facebook',
			'instagram'
		];

		foreach ($configs as $config) {
			$arr = $this->db->row("SELECT value, comment FROM config WHERE name = '" . $config . "'");
			$arr = array_map(
				function ($param) {
					return html_entity_decode($param);
				},
				$arr
			);

			$vars[$config] = $arr;
		}

		return $vars;
	}

	function getPhones($conf)
	{
		$phones = trim(html_entity_decode($this->settings[$conf] ?? null));
		$phones = explode(',', $phones);
		return array_map(
			function ($phone) {
				$phone = trim($phone);
				$phones = [
					'raw' => preg_replace('/\D/', '', $phone),
					'format' => $phone
				];
				preg_match("/.*\((\d{3})\).*/", $phone, $operator);
				$operator = $operator[1] ?? null;
				if ($operator == '050' || $operator == '066' || $operator == '095' || $operator == '099') {
					$phones['icon'] = '<img src="/images/operators/vodafone.png"  alt="vodafone"/>';
				} else {
					if ($operator == '093' || $operator == '063' || $operator == '073') {
						$phones['icon'] = '<img src="/images/operators/lifecell.png"  alt="lifecell"/>';
					} else {
						if ($operator == '067' || $operator == '096' || $operator == '097' || $operator == '098') {
							$phones['icon'] = '<img src="/images/operators/kyivstar.png" style="margin-bottom: 3px"  alt="kyivstar"/>';
						} else {
							$phones['icon'] = '<i class="fa fa-phone"></i>';
						}
					}
				}
				return $phones;
			},
			$phones
		);
	}

}