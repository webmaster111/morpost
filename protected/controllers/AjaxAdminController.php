<?

class AjaxAdminController extends BaseController
{
  function __construct($registry, $params)
  {
    $this->registry = $registry;
    parent::__construct($registry, $params);
  }

  function indexAction()
  {
  }

  function activeAction()
  {
    if (isset($_POST['id'], $_POST['tb'])) echo json_encode($this->model->active($_POST['id'], $_POST['tb'], $_POST['tb2']));
  }

  function sortAction()
  {
    if (isset($_POST['arr'], $_POST['tb'])) echo json_encode($this->model->sortTable($_POST['arr'], $_POST['tb'], $_POST['tb2']));
  }

  public function supportsendAction(): void
  {
	 $subject = $_POST['SUBJECT'];
	 $text = $_POST['TEXT'];
	 $body = "<table><tr><td>Адрес сайта:<br></td><td>" . $_SERVER['HTTP_HOST'] . "</td></tr><tr><td>Тема: <br></td><td>" . $subject . "</td></tr><tr><td>Описание:<br></td><td>" . $text . "</td></tr></table>";
	 Mail::send('Новое сообщение в тех.поддержку на сайте ' . $this->settings['sitename'] . ' (' . $subject . ')', $body, EMAIL_SUPPORT);
	 if (isset($this->settings['testing_project_id']) && (int)$this->settings['testing_project_id'] != 0) {
		$POST_URL = 'http://mysecretar.com/myss/post_ticket.php';
		$postData = [];
		$postData['subject'] = $subject;
		$postData['url'] = $_SERVER['HTTP_HOST'];
		$postData['project_id'] = $this->settings['testing_project_id'];
		$postData['text'] = $text;
		$postData['submit'] = 'submit';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $POST_URL);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
      curl_exec($ch);
      curl_close($ch);
    }
  }

  //Product extra photo view
  function loadextraphotoAction()
  {
    if (isset($_REQUEST['id'], $_REQUEST['tb'], $_REQUEST['fk'])) {
      $tb = $_REQUEST['tb'];
      $fk = $_REQUEST['fk'] . '_id';
      $path = $_REQUEST['path'];
      $res = $this->db->rows("SELECT * FROM `$tb` tb LEFT JOIN `" . $this->key_lang . "_{$tb}` tb2 ON tb.id=tb2.{$tb}_id WHERE {$fk}=? ORDER BY sort ASC,id DESC", [$_REQUEST['id']]);
      $this->registry->set('admin', 'product');
      echo $this->view->Render('extra_photo_one.phtml', ['photo' => $res, 'action' => $_REQUEST['fk'], 'path' => $path, 'sub_id' => $_REQUEST['id']]);
    }
  }
    function saveCroppedImageAction()

    {

        if (!is_dir($_POST['path'])) mkdir($_POST['path'], 0755, true);

        if (isset($_POST['type'], $_POST['image'], $_POST['path'])) {

            $this->delimageAction($_POST['table'], $this->db->cell("SELECT photo FROM {$_POST['table']} WHERE id=?", [$_POST['id']]));

            $copied_image = $this->createTmpImage($_POST['image'], $_POST['path'], $_POST['type']);

            $img = Images::saveCroppedArea($copied_image, $_POST['thumb_width'], $_POST['thumb_height'], $_POST['path'], $_POST['id']);

            $result = ['image' => $img . '?' . time(), 'status' => 'Изображение записи обновлено'];

            $this->set_path_image($_POST['table'], $_POST['id'], $img);

        } else $result = ['error' => 'Correct params was not given'];

        return json_encode($result);

    }

  function delimageAction($tb = NULL, $path = NULL)
  {
    $data = [];
    $data['message'] = '';
    $tb = (isset($tb)) ? $tb : $_POST['action'];
    $path = (isset($path)) ? $path : $_POST['path'];
    if (!$this->model->checkAccess('edit', $tb)) $data['access'] = messageAdmin('Отказано в доступе', 'error');
    else {
      if( is_file($path) ) {
        if (substr($path, 0, 1) == '/') $path = substr($path, 1, strlen($path));
        if (file_exists($path)) unlink($path);
        $path = str_replace('_s', '_b', $path);
        if (file_exists($path)) unlink($path);
        $path = str_replace('_b', '_s_2x', $path);
        if (file_exists($path)) unlink($path);
        $path = str_replace('_s_2x', '_m', $path);
        if (file_exists($path)) unlink($path);
        $path = str_replace('_m', '', $path);
        if (file_exists($path)) unlink($path);
      }
    }
    return json_encode($data);
  }

  function deletePhotoAction()
  {
    $this->db->query("UPDATE `" . $_POST['table'] . "` SET `photo`='' WHERE `id`=?", [$_POST['id']]);
    return $this->delimageAction($_POST['table'], $this->db->cell("SELECT `photo` FROM `" . $_POST['table'] . "` WHERE `id`=?", [$_POST['id']]));
  }

  function createTmpImage($image, $path, $type)
  {
    if ($type == 'url') $copied_image = Images::saveFromUrl($image, $path);
    elseif (is_array($image)) $copied_image = Images::saveFromFile($image, $path);
    else $copied_image = Images::saveFromBase64($image, $path);
    return $copied_image;
  }

  function set_path_image($tb, $id, $path)
  {
    if ($this->db->query("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='{$tb}' AND COLUMN_NAME='photo'")) {
        $this->db->query("UPDATE {$tb} SET photo=? WHERE id=?", [$path, $id]);
        if (isset($_SESSION['manual-control']) == NULL):
            Images::setWatermark($this->settings['watermark'], $path, $tb);
        endif;
    }
  }

  function includePhotoAction()
  {
    if (!is_dir($_POST['path'])) mkdir($_POST['path'], 0755, true);
    $file = (isset($_FILES['image'])) ? $_FILES['image'] : $_POST['image'];
    if (isset($_POST['type'], $file, $_POST['path'])) {
      $copied_image = $this->createTmpImage($file, $_POST['path'], $_POST['type']);
      $result = ['image' => $copied_image . '?' . time(), 'status' => 'image successfully copied'];
      if ($_POST['upload_type'] == 3) {
        $this->delimageAction($_POST['table'], $this->db->cell("SELECT photo FROM {$_POST['table']} WHERE id=?", [$_POST['id']]));
        $img = Images::loadOriginalImage($copied_image, $_POST['path'], $_POST['id']);
        $result = ['image' => $img . '?' . time(), 'status' => 'Изображение записи обновлено'];
        $this->set_path_image($_POST['table'], $_POST['id'], $img);
      } elseif ($_POST['upload_type'] == 2) {
        $this->delimageAction($_POST['table'], $this->db->cell("SELECT photo FROM {$_POST['table']} WHERE id=?", [$_POST['id']]));
        $img = Images::putInArea($copied_image, $_POST['thumb_width'], $_POST['thumb_height'], $_POST['path'], $_POST['id']);
        $result = ['image' => $img . '?' . time(), 'status' => 'Изображение записи обновлено'];
        $this->set_path_image($_POST['table'], $_POST['id'], $img);
      } elseif ($_POST['upload_type'] == 1) copy($copied_image, str_replace('tmp.', 'tmp_o.', $copied_image));
    } else $result = ['error' => 'Correct params was not given'];
    return json_encode($result);
  }

  function includeAdditionalPhotoAction()
  {
    if (!is_dir($_POST['path'])) mkdir($_POST['path'], 0755, true);
    $file = (isset($_FILES['image'])) ? $_FILES['image'] : $_POST['image'];
    $img = $this->createTmpImage($file, $_POST['path'], $_POST['type']);
    $new_id = $this->db->insert_id("INSERT INTO `{$_POST['table']}_photo` SET {$_POST['table']}_id=?,texture=?,active=?,sort=?", [$_POST['id'], 0, 1, 0]);
    $this->db->query("INSERT INTO `{$this->key_lang}_{$_POST['table']}_photo` SET {$_POST['table']}_photo_id=?,name=?", [$new_id, $_POST['name']]);
    $img = Images::loadOriginalImage($img, $_POST['path'], $new_id);
    $this->set_path_image($_POST['table'] . "_photo", $new_id, $img);
    $res = $this->db->rows("SELECT * FROM `{$_POST['table']}_photo` tb LEFT JOIN `{$this->key_lang}_{$_POST['table']}_photo` tb2 ON tb.id=tb2.{$_POST['table']}_photo_id WHERE {$_POST['table']}_id=? ORDER BY sort ASC,id DESC", [$_POST['id']]);
    $this->registry->set('admin', $_POST['table']);
    $content = $this->view->Render('extra_photo_one.phtml', ['admin' => $_POST['table'], 'photo' => $res, 'action' => $_POST['table'], 'path' => $_POST['path'], 'sub_id' => $_POST['id']]);
    return json_encode(['content' => $content]);
  }

  function addCommentPhotosAction()
  {
    if (!is_dir($_POST['path'])) mkdir($_POST['path'], 0755, true);
    $file = (isset($_FILES['image'])) ? $_FILES['image'] : $_POST['image'];
    $img = $this->createTmpImage($file, $_POST['path'], $_POST['type']);
    $new_id = $this->db->insert_id("INSERT INTO `{$_POST['table']}_photo` SET {$_POST['table']}_id=?,texture=?,active=?,sort=?", [$_POST['id'], 0, 1, 0]);
    $this->db->query("INSERT INTO `{$this->key_lang}_{$_POST['table']}_photo` SET {$_POST['table']}_photo_id=?, name=?", [$new_id, $_POST['image']['name']]);
    $img = Images::loadOriginalImage($img, $_POST['path'], $new_id);
    $this->set_path_image($_POST['table'] . "_photo", $new_id, $img);
    return json_encode(['new_id' => $new_id]);
  }

  function delextraAction()
  {
    $path = $_POST['path'] . $_POST['image_id'] . '_b.jpg';
    if (file_exists($path)) unlink($path);
  }

  function siteConfigAction()
  {
    if(isset($_POST['email'])&&$_POST['email']!==$this->settings['email']){
      if($this->settings['email']==NULL)$this->db->query('insert into config set modules_id=?,name=?,value=?,type=?,active=?',[0,'email',$_POST['email'],'text',1]);
      else $this->db->query('update config set modules_id=?,value=?,type=?,active=? where name=?',[0,$_POST['email'],'text',1,'email']);
    }
    if(isset($_POST['google'])&&$_POST['google']!==$this->settings['google']){
      if($this->settings['google']==NULL)$this->db->query('insert into config set modules_id=?,name=?,value=?,type=?,active=?',[0,'google',$_POST['google'],'text',1]);
      else $this->db->query('update config set modules_id=?,value=?,type=?,active=? where name=?',[0,$_POST['google'],'text',1,'google']);
    }
    if(isset($_POST['yandex'])&&$_POST['yandex']!==$this->settings['yandex']){
      if($this->settings['yandex']==NULL)$this->db->query('insert into config set modules_id=?,name=?,value=?,type=?,active=?',[0,'yandex',$_POST['yandex'],'text',1]);
      else $this->db->query('update config set modules_id=?,value=?,type=?,active=? where name=?',[0,$_POST['yandex'],'text',1,'yandex']);
    }
    if(isset($_POST['webmaster_g'])&&$_POST['webmaster_g']!==$this->settings['webmaster_g']){
      if($this->settings['webmaster_g']==NULL)$this->db->query('insert into config set modules_id=?,name=?,value=?,type=?,active=?',[0,'webmaster_g',$_POST['webmaster_g'],'textarea',1]);
      else $this->db->query('update config set modules_id=?,value=?,type=?,active=? where name=?',[0,$_POST['webmaster_g'],'text',1,'webmaster_g']);
    }
    if(isset($_POST['webmaster_y'])&&$_POST['webmaster_y']!==$this->settings['webmaster_y']){
      if($this->settings['webmaster_y']==NULL)$this->db->query('insert into config set modules_id=?,name=?,value=?,type=?,active=?',[0,'webmaster_y',$_POST['webmaster_y'],'textarea',1]);
      else $this->db->query('update config set modules_id=?,value=?,type=?,active=? where name=?',[0,$_POST['webmaster_y'],'text',1,'webmaster_y']);
    }
    if(isset($_POST['address'])&&$_POST['address']!==$this->settings['address']){
      if($this->settings['address']==NULL)$this->db->query('insert into config set modules_id=?,name=?,value=?,type=?,active=?',[0,'address',$_POST['address'],'text',1]);
      else $this->db->query('update config set modules_id=?,value=?,type=?,active=? where name=?',[0,$_POST['address'],'textarea',1,'address']);
    }
    if(isset($_POST['phones'])&&$_POST['phones']!==$this->settings['phones']){
      if($this->settings['phones']==NULL)$this->db->query('insert into config set modules_id=?,name=?,value=?,type=?,active=?',[0,'phones',$_POST['phones'],'text',1]);
      else $this->db->query('update config set modules_id=?,value=?,type=?,active=? where name=?',[0,$_POST['phones'],'textarea',1,'phones']);
    }
    if(isset($_POST['sitename'])&&$_POST['sitename']!==$this->settings['sitename']){
      if($this->settings['sitename']==NULL)$this->db->query('insert into config set modules_id=?,name=?,value=?,type=?,active=?',[0,'sitename',$_POST['sitename'],'text',1]);
      else $this->db->query('update config set modules_id=?,value=?,type=?,active=? where name=?',[0,$_POST['sitename'],'text',1,'sitename']);
    }
    return json_encode(['message'=>'<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Данные успешно сохранены</div>']);
  }

  public function generatePassAction()
  {
    return json_encode(str_shuffle(uniqid()));
  }
}