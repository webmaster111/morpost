<?php

class Router
{
	private $registry;
	private $uri_arr = null;
	public $classObj;
	protected $db;

	public function __construct($registry, $db, $uri = null)
	{
		$this->registry = $registry;
		$this->db = $db;
		if (!isset($uri)) {
			$uri = $_SERVER['REQUEST_URI'];
			$uri = filter_var($uri, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
			//удаляем последний пустой елемент если есть
			$rest = mb_substr($uri, -1);
			if ($rest === '/') {
				$uri = mb_substr($uri, 0, strlen($uri) - 1, 'UTF-8');
			}
		}
		$pos = stripos($uri, '?qqfile=');
		if ($pos !== false) {
			$uri = mb_substr($uri, 0, $pos);
		}
		$uri = current(explode('?', $uri));
		$_SERVER['REQUEST_URI'] = $uri;// Костыль для карты сайта, не выкидываем ошибку, а направляем в Мета контроллер
		if ($uri === '/sitemap.xml') {
			$uri = '/ajax/meta/sitemap';
		} elseif (!preg_match('/^[-a-zA-Z0-9_\/\=\?\;\,]*$/', $uri)) {
			$err = true;
		}
		// редирект в нижний регистр
		$this->uri_arr = explode("/", $uri);
		//удаляем первый пустой елемент
		array_splice($this->uri_arr, 0, 1);
		if (isset($err)) {
			$this->uri_arr[0] = 'Error';
			$this->uri_arr[1] = 'index';
		}
		$this->uri_arr = Meta::check_targets($db, $this->uri_arr);

	}

	public function getParams($create = true)
	{
		$url = $this->uri_arr;

		$uri_params = [];
		$params = [];
		$uri_params['action'] = 'index';

		//определения контролера и экшина
		if (isset($url[0]) && $url[0] != '') {
			if ($url[0] === 'ajax' && isset($url[1])) {
				if (isset($url[1], $url[2])) {
					$uri_params['controller'] = ucfirst($url[1]);
					$uri_params['action'] = $url[2];
				} else {
					$uri_params['controller'] = 'Ajax';
					$uri_params['action'] = $url[1];
				}
				$params['topic'] = 'ajax';
			} elseif ($url[0] === 'captcha') {
				$uri_params['controller'] = 'Captcha';
				$params['topic'] = 'captcha';
			} elseif ($url[0] === "agentik") {
				$params['topic'] = 'agentik';
				if (isset($url[2]) && $url[2] === 'logout') {
					session_destroy();
				}
				if (checkAuthAgentik($this->db)) {
					$this->registry::set('agentik', $url[1]);
					$uri_params['controller'] = 'Xmlgate';
					$uri_params['action'] = 'index';
					$params['module'] = 'xmlgate';
				} else {
					$this->registry::set('agentik', $url[1]);
					$uri_params['controller'] = 'Agentik';
					$uri_params['action'] = 'index';
				}
			} elseif ($url[0] === 'admin') {
				$params['topic'] = 'admin';
				if (isset($url[1]) && $url[1] === 'logout') {
					// удаляем авторизацию
					unset($_SESSION['admin'], $_SESSION['login_admin'], $_SESSION['password_admin']);
				}
				if ($url[0] === 'admin' && isset($url[2], $url[1]) && $url[1] === 'ajax' && (checkAuthAdmin() || $_SERVER['HTTP_USER_AGENT'] == 'Shockwave Flash')) {
					if (isset($url[2], $url[3])) {
						$uri_params['controller'] = ucfirst($url[2]);
						$uri_params['action'] = $url[3];
					} else {
						$uri_params['controller'] = 'AjaxAdmin';
						$uri_params['action'] = $url[2];
					}
				} elseif (checkAuthAdmin()) {
					$uri_params['controller'] = 'indexAdmin';
					$uri_params['action'] = 'index';
					if (isset($url[1])) {
						$row2 = $this->db->row('SELECT `id`,`name` FROM `modules` WHERE `controller`=?', [$url[1]]);
						if ($row2) {
							$param = [$row2['id'], $_SESSION['admin']['id']];
							$row = $this->db->row("SELECT mm.`permission`,m.type_moderator FROM `moderators` m LEFT JOIN `moderators_permission` mm ON mm.moderators_type_id=m.type_moderator AND mm.module_id=? WHERE m.id=?",
								$param);
							if ($row['permission'] != 000 || $row['type_moderator'] == 1) {
								$uri_params['controller'] = ucfirst($url[1]);
								if (isset($url[2]) && ($url[2] === 'edit' || $url[2] === 'add' || $url[2] === 'config' || $url[2] === 'subsystem')) {
									if (isset($url[2], $url[3]) && $url[2] == "subsystem" && $url[3] != "") {
										$subsystem = SUBSYSTEM . $url[3] . '/' . ucfirst($url[3]) . 'Controller.php';
										if (!file_exists($subsystem)) {
											return Router::act('error', $this->registry);
										}
										include $subsystem;
										$uri_params['topic'] = 'subsystem';
										$params['subsystem'] = $url[3];
									} else {
										$uri_params['action'] = $url[2];
									}
								}
								Registry::set('topic_value', $url[1]);
								/*'000'-off;
								'100'-read;
								'200'-read/edit;
								'300'-read/del;
								'400'-read/add;
								'500'-read/edit/del;
								'600'-read/edit/add;
								'700'-read/del/add;
								'800'-read/edit/del/add;*/
								if ($row['type_moderator'] != 1) {
									if (isset($url[2]) && ($url[2] == 'delete' || (isset($_POST['delete']) && $url[2] == 'update')) && ($row['permission'] != 500 && $row['permission'] != 300 && $row['permission'] != 700 && $row['permission'] != 800)) {
										$this->registry::set('access', messageAdmin('Отказано в доступе', 'error'));
										$uri_params['action'] = 'index';
									} elseif (isset($url[2]) && ($url[2] == 'edit' || $url[2] == 'update') && ($row['permission'] != 200 && $row['permission'] != 500 && $row['permission'] != 600 && $row['permission'] != 800)) {
										$this->registry::set('access', messageAdmin('Отказано в доступе', 'error'));
										$uri_params['action'] = 'index';
									} elseif (isset($url[2]) && ($url[2] == 'add' || $url[2] == 'duplicate') && ($row['permission'] != 400 && $row['permission'] != 600 && $row['permission'] != 700 && $row['permission'] != 800)) {
										$this->registry::set('access', messageAdmin('Отказано в доступе', 'error'));
										$uri_params['action'] = 'index';
									}
								}
								$this->registry::set('admin', $url[1]);
								$params['action'] = $uri_params['action'];
								$params['controller'] = ucfirst($url[1]);
								$params['module'] = $row2['name'];
							} else {
								return Router::act('error', $this->registry);
							}
						} else {
							return Router::act('error', $this->registry);
						}
					} else {
						$this->registry::set('admin', 'index');
						$uri_params['controller'] = 'IndexAdmin';
						$uri_params['action'] = 'index';
					}
				} else {
					$this->registry::set('admin', 'login');
					$uri_params['controller'] = 'Login';
					if (!empty($url[1])) {
						$params['error'] = $url[1];
					}
					$uri_params['action'] = 'index';
				}
				$params['topic'] = 'admin';
			} else {
				if (isset($_SESSION['user_info'])) {
					if ($_SESSION['user_info']['agent'] != $_SERVER['HTTP_USER_AGENT']) {
						$error = 1;
					}
					if ($_SESSION['user_info']['ip'] != $_SERVER['REMOTE_ADDR']) {
						$error = 1;
					}
				}
				if (isset($error)) {
					unset($_SESSION['user_info']);
				}
				$row = $this->db->row("SELECT `controller` FROM `modules` WHERE `url`=?", [$url[0]]);
				if ($row) {
					$uri_params['controller'] = ucfirst($row['controller']);
					$uri_params['action'] = "index";
					$params['topic'] = $url[0];
					if (isset($url[1])) {
						$params[$row['controller']] = $url[1];
					}
				} else {
					$uri_params['controller'] = 'Pages';
					$uri_params['action'] = "index";
					$params['topic'] = 'pages';
					$params['pages'] = $url[0];
					if ($url[0] == 'sitemap') {
						$params['sitemap'] = $url[1];
					}
				}
			}
		} else {
			$uri_params['controller'] = 'Index';
			$uri_params['action'] = 'index';
			$params['topic'] = 'index';
		}
		if (isset($url[0]) && !isset($url[1])) {
			if ($uri_params['controller'] === 'Catalog' || $uri_params['controller'] === 'News' || $uri_params['controller'] === 'Photos' || $uri_params['controller'] === 'Article') {
				header("Location: " . LINK . "/{$url[0]}/all");
				exit();
			}
			if ($uri_params['controller'] === 'Orders') {
				header("Location: " . LINK . "/{$url[0]}/preview");
				exit();
			}
		}
		$url_count = count($url);
		for ($i = 2; $i < $url_count;) {
			if (($url[$i] != 'delete') || (($url[$i] === 'delete') && $i == 2)) {
				if (isset($url[$i + 1])) {
					$val = $url[$i + 1];
				} else {
					$val = '';
				}
				$params[$url[$i]] = $val;
			}
			$i += 2;
		}
		$className = ucfirst($uri_params['controller'] . 'Controller');
		$filePath = CONTROLLERS . $className . '.php';
		$method_exists = false;
		if (file_exists($filePath)) {
			include_once $filePath;
			$this->classObj = new $className($this->registry, $params);
			if (method_exists($this->classObj, $uri_params['action'] . 'Action')) {
				$method_exists = true;
				if (!$create) {
					unset($this->classObj);
				}
			}
		} else {
			if ($params['topic'] === 'admin') {
				$filePath = MODULES . strtolower($uri_params['controller']) . '/admin/' . $className . '.php';
			} else {
				$filePath = MODULES . strtolower($uri_params['controller']) . '/' . $className . '.php';
			}
			if (file_exists($filePath)) {
				include_once $filePath;

				$this->classObj = new $className($this->registry, $params);

				if (method_exists($this->classObj, $uri_params['action'] . 'Action')) {
					$method_exists = true;
					if (!$create) {
						unset($this->classObj);
					}
				}
			} elseif ($params['topic'] == "admin") {
				$filePath = SUBSYSTEM . strtolower($uri_params['controller']) . '/' . ucfirst($className) . '.php';
				if (file_exists($filePath)) {
					include_once $filePath;
				}
				$this->classObj = new $className($this->registry, $params);
				if (method_exists($this->classObj, $uri_params['action'] . 'Action')) {
					$method_exists = true;
					if (!$create) {
						unset($this->classObj);
					}
				}
			}
		}
		if (!$method_exists) {
			$uri_params['controller'] = 'Error';
			$uri_params['action'] = 'index';
		}
		if ($create) {
			if (!$method_exists) {
				$className = $uri_params['controller'] . 'Controller';
				include_once CONTROLLERS . $className . '.php';
				$this->classObj = new $className($this->registry, $params);
			}
			return $this->dispatch($uri_params['action'], $this->classObj);
		}

		return $uri_params;
	}

	public function load($controller, $registry, $params = [])
	{
		$className = ucfirst($controller . 'Controller');
		include_once CONTROLLERS . $className . '.php';
		return new $className($registry, $params);
	}

	public static function act($controller, $registry, $action = 'index', $params = [])
	{
		$obj = self::load($controller, $registry, $params);
		$res = self::dispatch($action, $obj);
		return $res;
	}

	public function dispatch($strActionName = 'index', $obj = null)
	{
		$strActionName .= 'Action';

		$objName = ($obj ?: $this->classObj);

		return $objName->$strActionName();
	}
}