<?
namespace vkApi;
class post
{
	private $vk;
	private $owner;
	private $album;

	function __construct(vk $vk,$user=null,$group=null)
	{
		$this->vk=$vk;
		if(!$user && !$group)throw new \Exception('Not found group or user');
		$this->owner=['type'=>$user?'owner_id':'group_id','value'=>$user?$user:$group];
		$this->owner['value']=(int)preg_replace('/([^\d]+)/','',$this->owner['value']);
	}

	function post($text,$img=null,$url=null)
	{
		if($img){
			$data=$this->load($img);
			$img=$data->response[0]->id;
			if($url != null)$img.=','.$url;
		}
		$data=['message'=>$text,'from_group'=>1,'owner_id'=>$this->owner['value']];
		if($img)$data['attachments']=$img;
		if($this->owner['type'] == 'group_id')$data['owner_id']='-'.$data['owner_id'];
		$data=$this->vk->get('wall.post',$data);
		if(isset($data->error))throw new \Exception($data->error->error_msg);
		return $data;
	}

	function post_album($text,$img,$album)
	{
		$this->album=$album;
		$data=$this->getPhotoAlbum($img);
		$data=['album_id'=>$this->album,'caption'=>$text,'server'=>$data->server,'photos_list'=>$data->photos_list,'hash'=>$data->hash,'group_id'=>$this->owner['value']];
		$data=$this->vk->get('photos.save',$data);
		if(isset($data->error))throw new \Exception($data->error->error_msg);
		return $data;
	}

	function load($src)
	{
		$photo=(array)$this->getPhoto($src);
		$photo[$this->owner['type']]=$this->owner['value'];
		$data=$this->vk->get('photos.saveWallPhoto',$photo);
		return $data;
	}

	private function getPhoto($src)
	{
		$name='tmp/1.jpg';
		file_put_contents($name,file_get_contents($src));
		$ch=curl_init($this->getServer());
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,['photo'=>'@'.$name]);
		$response=curl_exec($ch);
		curl_close($ch);
		return json_decode($response);
	}

	private function getServer()
	{
		$data=$this->vk->get('photos.getWallUploadServer',[$this->owner['type']=>$this->owner['value']]);
		return $data->response->upload_url;
	}

	private function getPhotoAlbum($src)
	{
		$name='tmp/1.jpg';
		file_put_contents($name,file_get_contents($src));
		$ch=curl_init($this->getServerAlbum());
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,['photo'=>'@'.$name]);
		$response=curl_exec($ch);
		curl_close($ch);
		return json_decode($response);
	}

	private function getServerAlbum()
	{
		$data=$this->vk->get('photos.getUploadServer',[$this->owner['type']=>$this->owner['value'],'album_id'=>$this->album]);
		return $data->response->upload_url;
	}
}