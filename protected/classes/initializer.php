<?php

if (DEBUG) {
	error_reporting(1);
} else {
	error_reporting(0);
}

include 'redirect.php';
$sep = getenv('COMSPEC') ? ';' : ':';
$dir = '';
$scandir = scandir(MODULES);
foreach ($scandir as $i => $row) {
	if ($i > 1) {
		$dir .= $sep . MODULES . $row . '/';
	}
}
$path = CLASSES . $sep . CONTROLLERS . $sep . MODULES . $dir . LIBRARY . $dir;
require_once(SITE_PATH . 'protected/libraries/library.php');
ini_set('include_path', $path);
ini_set('session.use_trans_sid', false);
//Инициализация настроек с конфига
ini_set('memory_limit', MEMORY_LIMIT);
ini_set('upload_max_filesize', UPLOAD_MAX_FILESIZE);
ini_set('post_max_size', MEMORY_LIMIT);
date_default_timezone_set(TIMEZONE);

header('Content-Type: text/html; charset=utf-8');
session_start();

$db = [
	'host' => $DB_Host,
	'name' => $DB_Name,
	'user' => $DB_UserName,
	'password' => $DB_Password,
	'charset' => $DB_Charset
];
$tpl = [
	'source' => $PathToTemplate,
	'styles' => $PathToCSS,
	'images' => $PathToImages,
	'jscripts' => $PathToJavascripts,
	'flash' => $PathToFlash
];
$registry = new Registry;
$registry::set('db_settings', $db);
$registry::set('tpl_settings', $tpl);
$db = new PDOchild($registry);
$language = $db->rows('SELECT * FROM `language`');
$registry::set('key_lang', getUri($language, $db));
$registry::set('key_lang_admin', getUriAdm($language));
$row = $db->row("SELECT * FROM `config` WHERE `name`='theme'");
$theme_color = $db->row("SELECT * FROM `config` WHERE `name`='theme_color'");

if ($row) {
	define('IMAGES', '/' . $PathToTemplate . $row['value'] . '/colors/' . $theme_color['value'] . '/' . $PathToImages);
	$registry::set('theme', $row['value']);
	$registry::set('theme_color', $theme_color['value']);
} else {
	define('IMAGES', '/' . $PathToTemplate . THEME . '/colors/' . $theme_color['value'] . '/' . $PathToImages);
	$registry::set('theme', THEME);
	$registry::set('theme_color', $theme_color['value']);
}
if ($_SESSION['key_lang'] === 'ru') {
	define('LINK', '');
} else {
	define('LINK', '/' . $_SESSION['key_lang']);
}
$parser = new Parser();
if (!empty($_POST)) {
	$parser->parse_recursive_tree($_POST);
}
if (!empty($_GET)) {
	$parser->parse_recursive_tree($_GET);
}
if (!empty($_COOKIE)) {
	$parser->parse_recursive_tree($_COOKIE);
}