<?php

class Model extends DataBase
{
    public function __construct($registry, $table = false)
    {
        parent::getInstance($registry);
        if ($table) {
			static::$table = $table;
		}
    }

    function count($where = '')
    {
        return (int) $this->db->cell('SELECT count(*) FROM ' . static::$table . (!empty($where) ? (' WHERE ' . $where) : ""));
    }


    public function setTable($tableName)
    {
        static::$table = $tableName;
    }

    /*
     * Метод подготавливает массив к запросу и выкидывает из массива те ячейки, которых нет в оперируемой таблице
     */
    public function prepareColValues($array, $table, $checkCol = true)
    {
        $return = [
            'columns' => '',
            'values' => []
        ];
        if ($checkCol) {
            foreach ($array as $k => $item) {
                if (!$this->checkColumnInTable($k, $table)) unset($array[$k]);
            }
        }
        foreach ($array as $k => &$col) {
            $return['columns'] .= '`' . $k . '`=?';
            $return['values'][] = $col;
            if (!isLast($array, $k)) $return['columns'] .= ',';
        }

        return $return;
    }

    /*
     * Записывает в базу новое значение
     * Метод принимает массив в виде 'имя ячейки' => 'значение'
     */
    public function insert($array, $table = false, $checkCol = true)
    {
        // определим какую таблицу нам нужно использовать
        if (!$table) $table = static::$table;
        $colsValues = $this->prepareColValues($array, $table, $checkCol);

        return $this->db->insert_id("INSERT INTO `" . $table . "` SET " . $colsValues['columns'], $colsValues['values']);
    }

    /*
     * Мктод проверяет наличие ячейки в таблице
     */
    public function checkColumnInTable($column, $table)
    {
        $schema = $this->getSchemaTable($table);
        return isset($schema[$column]);
    }

    public function getSchemaTable($table)
    {
        $database = $this->registry['db_settings']['name'];
        if (isset($this->schema[$table])) return $this->schema[$table];
        $schema = $this->db->rows("SELECT COLUMN_NAME as name FROM  INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ? ", [$database, $table]);
        $this->schema[$table] = containArrayInHisId($schema, 'name');
        return $this->schema[$table];
    }

    /**
     * @param $arrWheres
     * @return array|bool
     * Метод подготавливает условие where из массива
     * поскольку условий может быть несколько, структура формирования следующая:
     * Оператором между несколькими аргументами всегда является AND, т.к этот метод не расчитан на сложные операции
     * [
     *      [
     *           'id', //ячейка таблицы
     *           '=', // оператор
     *           '7', // значение
     *      ]
     * ]
     */
    public function prepareWhere($arrWheres)
    {
        if (!$arrWheres) return false;
        $return = [
            'where' => '',
            'values' => []
        ];
        foreach ($arrWheres as $num => $arrWhere) {
            $return['where'] .= '`' . $arrWhere[0] . '` ' . $arrWhere[1] . ' ?';
            if (!isLast($arrWheres, $num)) $return['where'] .= ' AND ';
            $return['values'][] = $arrWhere[2];
        }
        return $return;
    }

    /**
     * @param $array
     * @param $where
     * @param bool $table
     * @param bool $checkCol
     * @return mixed
     *
     * Обвноляет значение поля в базе
     * Метод принимает массив в виде 'имя ячейки' => 'значение'
     * в where указываете уловие обновления, при чем можно передать массив аргуметов, если вы хотите сделать подготовленный запрос.
     */
    public function update($array, $where, $table = false, $checkCol = true)
    {
        // определим какую таблицу нам нужно использовать
        if (!$table) $table = static::$table;
        $colsValues = $this->prepareColValues($array, $table, $checkCol);

        if (is_array($where)) {
            $preparedWhere = $this->prepareWhere($where);
            $where = $preparedWhere['where'];
            $colsValues['values'] = array_merge($colsValues['values'], $preparedWhere['values']);
        }
        return $this->db->query("UPDATE `" . $table . "` SET " . $colsValues['columns'] . " WHERE " . $where, $colsValues['values']);
    }

    public function changestate($table = '')
    {
        if ($table == '') $table = static::$table;
        $message = '';
        if (isset($this->registry['access'])) $message = $this->registry['access'];
        else {
            if (isset($_POST['id']) && is_array($_POST['id'])) {
                $count = count($_POST['id']) - 1;
                for ($i = 0; $i <= $count; $i++) {
                    $sub = $this->db->row("SHOW COLUMNS FROM `{$table}` WHERE `Field`='sub'");
                    if ($sub) $child = $this->db->rows("SELECT * FROM {$table} WHERE `sub`=?", [$_POST['id'][$i]]);
                    else $child = null;
                    if (count($child) < 1) {
                        $row = $this->db->row("SELECT `active` FROM `$table` WHERE `id`=?", [$_POST['id'][$i]]);
                        if ($row['active'] == 1) {
                            $this->db->query("UPDATE `$table` SET `active`=? WHERE `id`=?", [0, $_POST['id'][$i]]);
                            $data['active'] = '<div class="selected-status status-d"><a> Выкл. </a></div>';
                        } else {
                            $this->db->query("UPDATE `$table` SET `active`=? WHERE `id`=?", [1, $_POST['id'][$i]]);
                            $data['active'] = '<div class="selected-status status-a"><a> Вкл. </a></div>';
                        }
                    } else $message .= 'Некоторые записи содержат непустые дочерние пункты,и не были удалены.<br/>';
                }
                $message .= messageAdmin($message . '<br/>Состояние изменено');
            }
        }

        return $message;
    }

    public function delete($table = '')
    {
        $meta = new Meta($this->sets);
        if ($table == '') $table = static::$table;
        $ph = $this->db->cell('select photo from modules where controller=?', [$table]);
        $message = '';
        if (isset($this->registry['access'])) $message = $this->registry['access'];
        else {
            if (isset($_POST['id']) && is_array($_POST['id'])) {
                $count = count($_POST['id']) - 1;
                for ($i = 0; $i <= $count; $i++) {
                    $sub = $this->db->row("SHOW COLUMNS FROM `{$table}` WHERE `Field`='sub'");
                    if ($sub) $child = $this->db->rows("SELECT * FROM {$table} WHERE `sub`=?", [$_POST['id'][$i]]);
                    else $child = NULL;
                    if (count($child) < 1) {
                        // Delete meta data
                        $meta->delete_meta($table, $_POST['id'][$i]);
                        $this->db->query("DELETE FROM `" . $table . "` WHERE `id`=?", [$_POST['id'][$i]]);
                        if ($ph == 1) {
                            $dir = Dir::createDir($_POST['id'][$i], '', $table);
                            if ($dir[0] != '') Dir::removeDir($dir[0]);
                        }
                    } else $message .= 'Некоторые записи содержат непустые дочерние пункты,и не были удалены.<br/>';
                }
                $message .= messageAdmin($message . '<br/>Записи успешно удалены');
            } elseif (isset($this->params['delete']) && $this->params['delete'] != '') {
                $sub = $this->db->row("SHOW COLUMNS FROM `{$table}` WHERE `Field`='sub'");
                if ($sub) $child = $this->db->rows("SELECT * FROM {$table} WHERE `sub`=?", [$this->params['delete']]);
                else $child = NULL;
                if (count($child) < 1) {
                    $id = $this->params['delete'];
                    if ($ph == 1) {
                        $dir = Dir::createDir($id, '', $table);
                        if ($dir[0] != '') Dir::removeDir($dir[0]);
                    }
                    // Delete meta data
                    $meta->delete_meta($table, $id);
                    if ($this->db->query("DELETE FROM `" . $table . "` WHERE `id`=?", [$id])) $message = messageAdmin('Запись успешно удалена');
                } else $message = messageAdmin('Запись содержит непустые дочерние пункты,и не может быть удалена', 'error');
            } elseif (isset($this->params['return']) && $this->params['return'] != '') {
                $sub = $this->db->row("SHOW COLUMNS FROM `{$table}` WHERE `Field`='sub'");
                if ($sub) $child = $this->db->rows("SELECT * FROM {$table} WHERE `sub`=?", [$this->params['return']]);
                else $child = NULL;
                if (count($child) < 1) {
                    $id = $this->params['return'];
                    if ($ph == 1) {
                        $dir = Dir::createDir($id, '', $table);
                        if ($dir[0] != '') Dir::removeDir($dir[0]);
                    }
                    // Delete meta data
                    $meta->delete_meta($table, $id);
                    if ($this->db->query("DELETE FROM `" . $table . "` WHERE `id`=?", [$id])) $message = messageAdmin('Заказ успешно отменен');
                } else $message = messageAdmin('Запись содержит непустые дочерние пункты,и не может быть удалена', 'error');
            }
        }
        return $message;
    }

    public function get_columns($row, $table, $fk = '')
    {
        $query = "";
        $fields = $this->db->rows("SHOW COLUMNS FROM $table");
        foreach ($fields as $row2) if ($row2['Field'] != 'id' && $row2['Field'] != $fk) {
            if ($row2['Field'] == 'url') $row[$row2['Field']] .= "-" . time();
            elseif ($row2['Field'] == 'name') $row[$row2['Field']] .= "-[copy]";
            $query .= "{$row2['Field']}='" . $row[$row2['Field']] . "',";
        }

        return $query = substr($query, 0, strlen($query) - 2);
    }

    public function duplicate($row, $table)
    {
        if (isset($this->registry['access'])) return $this->registry['access'];
        $message = '';
        $fk = $table . "_id";
        $query = $this->get_columns($row, $table);
        if ($query != '') {
            $insert_id = $this->db->insert_id("INSERT INTO `$table` SET " . $query);
            if ($this->db->row("SHOW TABLES LIKE '" . $this->registry['key_lang_admin'] . "_" . $table . "'")) {
                $res = $this->db->rows("SELECT * FROM language");
                foreach ($res as $lang) {
                    $tb = $lang['language'] . "_" . $table;
                    $query = $this->get_columns($row, $tb, $fk);
                    if ($query != '') $this->db->query("INSERT INTO `$tb` SET $fk='$insert_id'," . $query);
                }
            }
            header("Location: /admin/$table/edit/" . $insert_id);
        }

        return $message;
    }

    public function find($param)
    {
        if (isset($param['paging']) && is_array($param)) {
            if (is_numeric($param['paging'])) $size_page = $param['paging'];
            elseif (isset($this->settings['paging_admin_' . static::$table], $this->registry['admin'])) $size_page = $this->settings['paging_admin_' . static::$table];
            elseif (isset($this->settings['paging_' . static::$table])) $size_page = $this->settings['paging_' . static::$table];
            else $size_page = DEFAULT_PAGING;
            $start_page = 0;
            $cur_page = 0;
            $paging = '';
            $param2 = $param;
            $param['type'] = 'count';
            $count = $this->select($param);
            if (isset($this->params['page'])) {
                $cur_page = $this->params['page'];
                if ($cur_page < 2) {
                    header('Location: ' . Strings::getUrl2('page'));
                    exit();
                }
                $start_page = ($cur_page - 1) * $size_page; //номер начального элемента
            }
            if ($count > $size_page) {
                $class = new Paging($this->registry, $this->params);
                $paging = $class->MakePaging($cur_page, $count, $size_page); //вызов шаблона для постраничной навигации
            }
            $param2['limit'] = $start_page . ',' . $size_page;

            return ['list' => $this->select($param2), 'paging' => $paging['paging'] ?? '', 'pagingMeta' => $paging, 'count' => $count];
        } elseif (is_numeric($param)) return $this->select(['where' => '__tb.id:=' . $param . '__']);
        elseif (is_string($param)) return $this->select(['where' => '__tb.url:=' . $param . '__']);
        else return $this->select($param);
    }

    function select_tree($tab = 'menu', $cur_id = '')
    {
        $query = "SELECT * FROM `$tab` JOIN `{$this->registry['key_lang']}_$tab`  on `{$this->registry['key_lang']}_$tab`.`" . $tab . "_id`=`$tab`.`id` ORDER BY `sub` asc,`sort` asc";
        $arrayCategories = containArrayInHisId($this->db->rows($query));
        $text = Arr::createTree_cat($arrayCategories, 0, $cur_id);

        return $text;
    }

    public function getPage($id, $index = '*')
    {
        if (is_numeric($id)) $WHERE = 'tb1.id=?';
        else $WHERE = 'tb1.url=?';
        $page = $this->db->row("SELECT " . $index . " FROM `menu` tb1 LEFT JOIN " . $this->registry['key_lang'] . "_menu tb2 ON tb1.id=tb2.menu_id WHERE " . $WHERE . " AND tb1.active=?", [$id, 1]);
        if (!$page) {
            $page = $this->db->row("SELECT " . $index . " FROM `pages` tb1 LEFT JOIN " . $this->registry['key_lang'] . "_pages tb2 ON tb1.id=tb2.pages_id WHERE " . $WHERE . " AND tb1.active=?", [$id, 1]);
            $page['type'] = 'pages';
        } else $page['type'] = 'menu';

        return $page;
    }

    public function getBlock($id)
    {
        if (is_array($id)) {
            $where = '';
            if (!empty($id)) {
                foreach ($id as $row) $where .= "OR id='$row'";
                if ($where != '') $where = "AND (" . substr($where, 3, strlen($where)) . ")";
            }

            return $this->db->rows_key("SELECT id,body FROM infoblocks tb LEFT JOIN " . $this->registry['key_lang'] . "_infoblocks tb2 ON tb.id=tb2.infoblocks_id WHERE tb.active='1' $where");
        }
        if (is_numeric($id)) $where = "AND tb.id='$id'";
        else $where = "AND tb.url='$id'";

        return $this->db->row("SELECT * FROM infoblocks tb LEFT JOIN " . $this->registry['key_lang'] . "_infoblocks tb2 ON tb.id=tb2.infoblocks_id WHERE tb.active='1' $where");
    }

    public function breadcrumbAdmin()
    {
        if(isset($this->params['action']))
            if ($this->params['action'] === 'edit' || $this->params['action'] === 'add')
                return '<a href="/admin/' . strtolower($this->params['controller']) . '" class="back-link">« Назад в:&nbsp;' . $this->params['module'] . '</a>';

        return '';
    }

    public function breadcrumbs($links, $view)
    {
        if (count($links) > 0) {
            array_unshift($links, ['url' => '/', 'name' => $this->sets['translation']['main']]);

            foreach ($links as $i => $link) {
                if ($link['url'] != '/') $uri = $link['url'];
                else $uri = '/';


                $request = substr($_SERVER['REQUEST_URI'], 1);


                if ($uri == $request) {
                    $links[$i]['url'] = '#';
                    $links[$i]['class'] = 'active';
                } else {
                    $links[$i]['url'] = $uri;
                    $links[$i]['class'] = '';
                }

                if ($links[$i]['url'] != '#' && $links[$i]['url'] != '/') {
                    $links[$i]['url'] = '/' . $links[$i]['url'];
                }
            }

            $template = 'layout/breadcrumbs.phtml';
            $path = 'tpl/' . THEME . '/' . $template;
            if (file_exists($path)) {
                return $view->Render($template, ['links' => $links]);
            } else {
                return false;
            }
        }
    }

    /**
     * @param $catrow
     * @param StringLibrary $product_name
     * @return array
     * Метод для получения цепочки категорий!
     */
    public function getBreadCat($catrow, $product = null)
    {
        if (!isset($this->catalog)) $this->catalog = new Catalog($this->sets);
        // Если передали id категории, получим ее всю, нам нужна вся категория
        $chain = [];

        if (is_numeric($catrow)) {
            $catrow = $this->catalog->find((int)$catrow);
        }

        $return = [['url' => 'catalog/all', 'name' => $this->translation['catalog']]];
        //$return = [];

        if ($catrow['id']) {
            $chain = $this->getBreadSubChain($catrow['id'], 'catalog');
        }

        // Добавим в url каталога, url контроллера
        if (!empty($chain)) {
            foreach ($chain as &$item) {
                $item['url'] = LINK . 'catalog/' . $item['url'];
            }
        }

        $return = array_merge($return, $chain);

        if (!empty($product)) {
            $return[] = ['name' => $product['name'], 'url' => 'product/' . $product['url']];
        }

        return $return;
    }

    /**
     * @param $id
     * @param StringLibrary $table
     * @param array $links
     * @return array
     * получить цепочку вложенных элементов
     */
    public function getBreadSubChain($id, $table = 'menu', $links = [])
    {
        $menu = $this->db->row("SELECT * FROM {$table}
			 LEFT JOIN {$this->registry['key_lang']}_{$table} ON {$this->registry['key_lang']}_{$table}.{$table}_id = {$table}.id
			 WHERE {$table}_id=?", [$id]);
        $links [] = $menu;
        if ($menu['sub'] != null) return $this->getBreadSubChain($menu['sub'], $table, $links);

        $links = array_reverse($links);
        return $this->excludeNonActiveLinks($links);
    }


    /**
     * @param $links метод убират из списка, выключенные каталоги
     * Почему в getBreadSubChain не делает условие на активность сразу?
     * потому что цепочку крошек получаем в обратном порядке, и если где то в конце будет не активный
     * каталог, то он ничего не найдет вообще, поэтому всех выберем, а потом просто уберем из списка выключенные
     */
    public function excludeNonActiveLinks($links = [])
    {
        if (!$links) return $links;
        foreach ($links as $k => $link) {
            if ((int)$link['active'] === 0) unset($links[$k]);
        }
        return $links;
    }

    public function checkAccess($action, $module)
    {
        /*Проверка доступа модулей в админке
          '000'-off;
          '100'-read;
          '200'-read/edit;
          '300'-read/del;
          '400'-read/add;
          '500'-read/edit/del;
          '600'-read/edit/add;
          '700'-read/del/add;
          '800'-read/edit/del/add;
         */
        if ($_SESSION['admin']['id'] == 1) return true;
        $row = $this->db->row("SELECT m.`permission` FROM `moderators_permission` m LEFT JOIN moderators mm ON m.moderators_type_id=mm.type_moderator LEFT JOIN modules mmm ON mmm.id=m.module_id WHERE mmm.controller=? AND mm.id=?", [$module, $_SESSION['admin']['id']]);
        if ($row['permission'] == 000) return false;
        elseif ($action == 'delete' && ($row['permission'] != 500 && $row['permission'] != 300 && $row['permission'] != 700 && $row['permission'] != 800)) return false;
        elseif (($action == 'edit' || $action == 'update') && ($row['permission'] != 200 && $row['permission'] != 500 && $row['permission'] != 600 && $row['permission'] != 800)) return false;
        elseif (($action == 'add' || $action == 'duplicate') && ($row['permission'] != 400 && $row['permission'] != 600 && $row['permission'] != 700 && $row['permission'] != 800)) return false;

        return true;
    }

    public function checkUrl($tb, $url, $id)// Проверка уникальности URL
    {
        $url = strtolower($url);
        if ($this->db->row("SELECT id from `" . $tb . "` WHERE url!='' AND url=? AND id!=?", [$url, $id])) $this->db->query("UPDATE `" . $tb . "` set url=? WHERE id=?", [$url . '-' . $id, $id]);
        else $this->db->query("UPDATE `" . $tb . "` set url=? WHERE id=?", [$url, $id]);
    }

    public function currency()
    {
        return $this->db->row("SELECT * FROM `currency` WHERE `id`='" . $_SESSION['currency']['id'] . "'");
    }

    function left_menu_admin($vars)
    {
        $this->view = new View($this->registry);
        if ($_SESSION['admin']['type'] == 1) $vars['menu'] = $this->db->rows("SELECT * FROM subsystem tb ORDER BY tb.sort ASC,tb.`id` DESC");
        else $vars['menu'] = $this->db->rows("SELECT tb.* FROM subsystem tb LEFT JOIN moderators_permission tb2 ON tb.id=tb2.subsystem_id LEFT JOIN modules m ON m.id=tb2.module_id WHERE tb2.moderators_type_id=? AND tb2.permission!=? AND m.controller=? GROUP BY tb.id ORDER BY tb.sort ASC,tb.`id` DESC", [$_SESSION['admin']['type'], '000', $vars['action']]);
        foreach ($vars['menu'] as $i => $row) $vars['menu'][$i]['url'] = '/admin/' . $vars['action'] . '/subsystem/' . $row['name'];
        if (isset($vars['menu2'])) $vars['menu'] = array_merge($vars['menu'], $vars['menu2']);

        return $this->view->Render('left_menu.phtml', $vars);
    }

    public function subsystemAction($left_menu = [])
    {
        $class_name = ucfirst($this->params['subsystem']) . 'Controller';
        $class = new $class_name($this->registry, $this->params);
        $vars['message'] = '';
        $vars['subsystem'] = $this->params['subsystem'];
	   if ($this->params['controller'] === 'Novaposhta') {
		  static::$table = 'novaposhta';
	   }
        $vars['action'] = static::$table;
        $row = $this->db->row("SELECT id,name FROM modules WHERE controller='" . static::$table . "'");
        $modules_id = $row['id'];
        if (isset($this->params['delsubsystem']) || isset($_POST['delete'])) $vars['message'] = $class->delete();
        elseif (isset($_POST['update'])) $vars['message'] = $class->save();
        elseif (isset($_POST['update_close'])) $vars['message'] = $class->save();
        elseif (isset($this->params['addsubsystem'])) $vars['message'] = $class->add($modules_id);
        $vars['where'] = "WHERE `modules_id`='" . $modules_id . "'";
        $vars['modules_id'] = $modules_id;
        $vars['modules_name'] = $row['name'];
        $vars['path'] = "/subsystem/" . $this->params['subsystem'];

        if (count($left_menu) == 0)
            $left_menu = ['action' => static::$table, 'name' => $this->name, 'sub' => $this->params['subsystem']];
        else
            $left_menu = ['action' => static::$table, 'name' => $this->name, 'sub' => $this->params['subsystem'], 'menu2' => $left_menu];

        $data['left_menu'] = $this->left_menu_admin($left_menu);
        $data['content'] = $class->subcontent($vars);

        return $data;
    }

    public function insert_post_form($text)
    {
        $this->db->query("INSERT INTO `feedback` SET `text`=?", [$text]);
    }

    public function active($id, $tb, $tb2)
    {
        $data = [];
        $data['message'] = '';
        if (!$this->checkAccess('edit', $tb)) $data['message'] = messageAdmin('Отказано в доступе', 'error');
        $id = str_replace("active", "", $id);
        if ($tb == 'tasks') {
            $tasks = new Tasks($this->sets);
            $tasks->active($id);
        }
        if ($tb == 'info') $tb = 'infoblocks';
        if ($tb2 != 'undefined') $tb = $tb2;
        if ($data['message'] == '') {
            if ($tb == 'modules') {
                $row = $this->db->row("SELECT `hidden` FROM `$tb` WHERE `id`=?", [$id]);
                if ($row['hidden'] == 0) {
                    $this->db->query("UPDATE `$tb` SET `hidden`=? WHERE `id`=?", [1, $id]);
                    $data['active'] = '<div class="selected-status status-d"><a> Выкл. </a></div>';
                } else {
                    $this->db->query("UPDATE `$tb` SET `hidden`=? WHERE `id`=?", [0, $id]);
                    $data['active'] = '<div class="selected-status status-a"><a> Вкл. </a></div>';
                }
            } else {
                $row = $this->db->row("SELECT `active` FROM `$tb` WHERE `id`=?", [$id]);
                if ($row['active'] == 1) {
                    $this->db->query("UPDATE `$tb` SET `active`=? WHERE `id`=?", [0, $id]);
                    $dopclass = '';
                    if ($tb == 'catalog') $dopclass = "catalog-status";
                    $data['active'] = '<div class="' . $dopclass . ' selected-status status-d"><a> Выкл. </a></div>';
                    if ($tb == 'catalog') {
                        $rows = $this->db->rows("SELECT * FROM `product_catalog` WHERE `catalog_id`=?", [$id]);
                        foreach ($rows as $row) {
                            //Проверяем принадлежит ли товар более чем к одной категории
                            $multicatalog = $this->db->row("SELECT COUNT(`catalog_id`) as count  FROM `product_catalog` WHERE `product_id`=?", [$row['product_id']]);
                            if ($multicatalog['count'] == 1) $this->db->query("UPDATE `product` SET `active`=? WHERE `id`=?", [0, $row['product_id']]);
                        }
                    }
                } else {
                    $this->db->query("UPDATE `$tb` SET `active`=? WHERE `id`=?", [1, $id]);
                    $dopclass = '';
                    if ($tb == 'catalog') $dopclass = "catalog-status";
                    $data['active'] = '<div class="' . $dopclass . ' selected-status status-a"><a> Вкл. </a></div>';
                    if ($tb == 'catalog') {
                        $rows = $this->db->rows("SELECT * FROM `product_catalog` WHERE `catalog_id`=?", [$id]);
                        foreach ($rows as $row) {
                            //Проверяем принадлежит ли товар более чем к одной категории
                            $multicatalog = $this->db->row("SELECT COUNT(`catalog_id`) as count  FROM `product_catalog` WHERE `product_id`=?", [$row['product_id']]);
                            if ($multicatalog['count'] == 1) $this->db->query("UPDATE `product` SET `active`=? WHERE `id`=?", [1, $row['product_id']]);
                        }
                    }
                }
            }
            $data['message'] = messageAdmin('Данные успешно сохранены');
        }

        return $data;
    }

    public function sortTable($arr, $tb, $tb2)
    {
        $data = [];
        $data['message'] = '';
        if (!$this->checkAccess('edit', $tb)) $data['message'] = messageAdmin('Отказано в доступе', 'error');
        if ($tb2 != 'undefined') $tb = $tb2;
        if ($data['message'] == '') {
            $arr = str_replace("sort", "", $arr);
            preg_match_all("/=(\d+)/", $arr, $a);
            foreach ($a[1] as $pos => $id) {
                $pos2 = $pos + 1;
                $this->db->query("update `$tb` set `sort`=? WHERE `id`=?", [$pos2, $id]);
            }
            $data['message'] = messageAdmin('Данные успешно сохранены');
        }

        return $data;
    }

    function check_for_delete($subsystem_id, $tb, $group_id)
    {
        if ($this->db->query("DELETE tb.* FROM `" . $tb . "` tb LEFT JOIN `moderators_permission` mp ON mp.module_id=tb.modules_id WHERE mp.moderators_type_id=? AND `id`=? AND (permission='300' OR permission='500' OR permission='700' OR permission='800') AND mp.subsystem_id='0'", [$group_id, $subsystem_id])) return messageAdmin('Запись успешно удалена');
        else return messageAdmin('Ошибка в правах доступа!', 'error');
    }

    function check_for_update($subsystem_id, $tb, $group_id)
    {
        $row = $this->db->row("SELECT tb.* FROM `" . $tb . "` tb LEFT JOIN `moderators_permission` mp ON mp.module_id=tb.modules_id WHERE (mp.moderators_type_id=? AND `id`=? AND (permission='200' OR permission='500' OR permission='600' OR permission='800') AND mp.subsystem_id='0') OR (tb.modules_id='' AND `id`=?)", [$group_id, $subsystem_id, $subsystem_id]);

        return $row;
    }

    /**
     * @param $id
     * @param StringLibrary $type - favorites | viewed
     */
    function add_fav($id, $type = 0)
    {
        if ($type === 0) {
            $where = "";
            if (isset($_SESSION['user_id'])) $where = ",user_id='{$_SESSION['user_id']}'";
            if (!$this->db->row("SELECT * FROM favorites WHERE type='$type' AND product_id='$id' AND (session_id='" . session_id() . "' " . str_replace(',', ' OR ', $where) . ")")) {
                if ($where == '') $where = ",user_id=NULL";
                $this->db->query("INSERT INTO favorites SET type='$type',product_id='$id',session_id='" . session_id() . "' $where");
            }
        } elseif ($type === 1) {
            if (!isset($_SESSION['products'][$type])) $_SESSION['products'][$type] = [];
            if (!in_array($id, $_SESSION['products'][$type])) $_SESSION['products'][$type][] = $id;
        }
    }

    public function photo_del($dir, $id)
    {
        if (file_exists("{$dir}{$id}.jpg"))
            unlink("{$dir}{$id}.jpg");
        if (file_exists("{$dir}{$id}_s.jpg"))
            unlink("{$dir}{$id}_s.jpg");
        if (file_exists("{$dir}{$id}_m.jpg"))
            unlink("{$dir}{$id}_m.jpg");
        if (file_exists("{$dir}{$id}_s_2x.jpg"))
            unlink("{$dir}{$id}_s_2x.jpg");
    }

    function loadExtraPhoto($tempFile, $name, $tb, $fk, $id, $path, $width, $height)
    {
        $fk2 = $fk . '_id';
        if (!is_dir($path))
            mkdir($path, 0755, true);
        $insert_id = $this->db->insert_id("INSERT INTO $tb SET {$fk2}=?, active=?", [$id, 1]);
        foreach ($this->language as $lang) {
            $tb_l = $lang['language'] . '_' . $tb;
            $param = [$name, $insert_id];
            $this->db->query("INSERT INTO `$tb_l` SET `name`=?, `{$tb}_id`=?", $param);
        }
        if (!is_dir($path))
            mkdir($path, 0755, true);
        Images::resizeImage($tempFile, $path . $insert_id . ".jpg", $path . $insert_id . "_s.jpg", $width, $height);
        Images::set_watermark($this->settings['watermark'], $path . $insert_id . "_s.jpg", $fk);
        $this->db->query("UPDATE {$tb} SET photo=? WHERE id=?", [$path . $insert_id . "_s.jpg", $insert_id]);
    }

    function recalc_price_range()
    {
        if (!isset($_SESSION['price_from'])) {
            $_SESSION['price_from'][0] = $this->db->cell("SELECT MIN(price) AS price FROM `price`", [1]);
            $_SESSION['price_to'][0] = $this->db->cell("SELECT MAX(price) AS price FROM `price`", [1]);
        }
        $price_from = Numeric::viewPrice($_SESSION['price_from'][0]);
        $_SESSION['price_from'] = [];
        $_SESSION['price_from'][0] = $price_from['base_price'];
        $_SESSION['price_from'][1] = floor($price_from['cur_price']);
        $price_to = Numeric::viewPrice($_SESSION['price_to'][0]);
        $_SESSION['price_to'] = [];
        $_SESSION['price_to'][0] = $price_to['base_price'];
        $_SESSION['price_to'][1] = round($price_to['cur_price']);
    }

    function setDateStat($month = 1)
    {
        $cur_start_date = getdate(mktime(0, 0, 0, date("m") - $month, date("d"), date("Y")));
        $cur_end_date = getdate(mktime(0, 0, 0, date("m") - ((int)$month - 1), date("d"), date("Y")));
        if (strlen($cur_start_date['mon']) == 1) $cur_start_date['mon'] = '0' . $cur_start_date['mon'];
        if (strlen($cur_start_date['mday']) == 1) $cur_start_date['mday'] = '0' . $cur_start_date['mday'];
        $cur_start_date = $cur_start_date['year'] . '-' . $cur_start_date['mon'] . '-' . $cur_start_date['mday'];
        if (strlen($cur_end_date['mon']) == 1) $cur_end_date['mon'] = '0' . $cur_end_date['mon'];
        if (strlen($cur_end_date['mday']) == 1) $cur_end_date['mday'] = '0' . $cur_end_date['mday'];
        $cur_end_date = $cur_end_date['year'] . '-' . $cur_end_date['mon'] . '-' . $cur_end_date['mday'];

        return ['start' => $cur_start_date, 'end' => $cur_end_date];
    }

    public function checkModule($module = null)
    {
        if (is_null($module)) $module = static::$table;

        return $this->db->row("SELECT id FROM modules WHERE `controller`=? AND `hidden`=?", [$module, 0]);
    }

    function fillNullDates($date_start, $date_end, $array)
    {
        if (is_array($array)) {
            if (is_array($array)) {
                reset($array);
                $first_key = new DateTime($date_start);
                $begin = date_format($first_key, 'Y-m-d H:i:s');
                end($array);
                $second_key = new DateTime($date_end);
                $end = date_format($second_key, 'Y-m-d H:i:s');
                $i = new DateInterval('P1D');
                $period = new DatePeriod($first_key, $i, $second_key);
                $array_return = [];
                foreach ($period as $d) {
                    $day = $d->format('d.m.Y');
                    $usercount = isset($array[$day]) ? $array[$day] : 0;
                    $array_return[$day] = $usercount;
                }

                return $array_return;
            }
        }
    }

    /**
     * @param StringLibrary $where
     * @param bool $showHidden
     * @param null $table
     * @return array|bool
     * аргумент $showActive показывать выключенные
     * по умолчанию выключенные не показываются
     */
    public function getAll($where = '', $showHidden = false, $table = false)
    {
        // определим какую таблицу нам нужно использовать
        if (!$table) {
            if (!isset(static::$table)) return false;
            $table = static::$table;
        }
        $whereActive = ($showHidden === false AND $this->checkColumnInTable('active', $table)) ? "tb.active='1'" : "tb.id != 0";
        $order = ($this->checkColumnInTable('sort', $table)) ? 'tb.sort ASC, tb.id ASC,' : '';
        $where = $whereActive . ' ' . $where;

        return $this->find([
                'type' => 'rows',
                'table' => $table,
                'select' => '*',
                'where' => $where,
                'group' => 'tb.id',
                'order' => $order . ' tb.id DESC']
        );
    }

    /**
     * @return mixed
     * Новости для сайдбара, с лимитом на вывод и учетом активности и даты публикации
     */
    public function getSiderbarNews()
    {
        if (!isset($this->news)) $this->news = new News($this->sets);
        if (!$this->news->checkModule()) return false;

        return $this->news->find([
            'where' => '__tb.active:=1__ AND tb.`date_add` < NOW()',
            'type' => 'rows',
            'order' => 'tb.date_add DESC',
            'limit' => $this->settings['limit_news_block']]);
    }

    /**
     * @return array
     * Возвращает массив включенных элементов главного меню из таблицы menu
     */
    public function getAllMenu()
    {
        return $this->find([
                'table' => 'menu',
                'type' => 'rows',
                'where' => "tb.active='1'",
                'order' => 'tb.sort ASC, tb.id DESC',
            ]
        );
    }

    /**
     * @return mixed
     * Возвращает список модулей, использующих замену тегов блоками
     */
    public function getReplacingCodes()
    {
        return $this->db->rows("SELECT * FROM `replace_code`");
    }

    /**
     * @param $model
     * @param $catalogId
     * Принимает имя Модели, и ID каталога
     * Обращается к методу модели, для вывода рендеренного шаблона
     */
    public function getRenderedBlock($model, $catalogId)
    {
        $model = ucfirst(mb_strtolower($model, 'UTF-8'));
        if (!isset($this->$model)) $this->$model = new $model($this->sets);

        return $this->$model->getReplace($catalogId);
    }

    /**
     * @param $text
     * @return mixed
     * Заменяет теги типа {{logotypes_222}} на отрендеренный блок
     */
    public function checkReplaceCode($text)
    {
        $codes = $this->getReplacingCodes();
        foreach ($codes as $code) {
            //$mathces
            // 0 : полное совпадение
            // 1 - КОД - оно же имя модели
            // 2 : '_'
            // 3 : ID каталога
            preg_match_all('/\{\{(' . $code['code'] . ')(_)(\d+)\}\}/im', $text, $matches);
            if (!empty($matches[3])) {
                $count = count($matches[3]);
                for ($i = 0; $i < $count; $i++) {
                    $replace = $this->getRenderedBlock($matches[1][$i], $matches[3][$i]);
                    $text = str_replace($matches[0][$i], $replace, $text);
                }
            }
        }

        return $text;
    }

    //------- Start group catalog tree

    /**
     * Метод создает дерево категорий, готовое для отображения в цикле
     * @param $categories - список всех, несортированных категорий
     */
    public function catalogTree($categories, $url = false)
    {
        if (!$categories) return $categories;
        if ($url) $categories = self::isCategoryActive($categories, $url);
        $menuTree = [];
        //1 level
        foreach ($categories as $lvl1 => $menu) {
            if (!$menu['sub'] AND $menu['sub'] != '0') {
                $menuTree[$lvl1] = $menu;
            }
        }
        //2 LEVEL
        foreach ($menuTree as $num => $res) {
            $menuTree[$num]['childrens'] = self::all_my_child($res['id'], $categories);
        }
        //3 LEVEL
        foreach ($menuTree as $num => $res) {
            foreach ($menuTree[$num]['childrens'] as $num2 => $children) {
                $menuTree[$num]['childrens'][$num2]['childrens'] = self::all_my_child($children['id'], $categories);
            }
        }

        return $menuTree;
    }

    /**
     * Метод ищет дочерние элементы каталога
     * @param $id - ID нашего элемента каталога
     * @param $menu - Массив всех элементов каталона
     * @return array Массив найденных элеметов
     */
    public static function all_my_child($id, $menu)
    {
        $result = false;
        foreach ($menu as $row) {
            if ($row['sub'] == $id) {
                $result[] = $row;
            }
        }

        return $result;
    }

    public static function all_my_parents($sub, $menu, &$result)
    {
//			$result = false;
        static $count = 0;
        foreach ($menu as $row) {
            if ($row['id'] == $sub) {
                $result[] = $row;
                if ($row['sub'] AND $row['sub'] != '0') {
                    $count++;
                    self::all_my_parents($row['sub'], $menu, $result);
                }
                break;
            }
        }

        return $result;
    }

    public static function isCategoryActive(&$menuTree, $url = null)
    {
        if (is_null($url)) return $menuTree;
        foreach ($menuTree as $num => &$menu) {
            if ($menu['url'] == $url) {
                $menu['show'] = 'show';
                $menu['current'] = 'current';
                self::addStatusParents($menu['sub'], $menuTree);
                break;
            }
        }

        return $menuTree;
    }

    public static function addStatusParents($parentId, &$menu)
    {
        if (is_null($parentId) OR is_null($menu)) return $menu;
        static $count = 0;
        $count++;
        foreach ($menu as $num => &$item) {
            if ($item['id'] == $parentId) {
                $item['show'] = 'show';
                if ($item['sub'] OR $item['sub'] == '0') {
                    self::addStatusParents($item['sub'], $menu);
                }
                break;
            }
        }
    }

    /**
     * @param $products
     * Метод проверяет присутствуют ли цена в товаре, если нет то получает ее
     * Отсутствие цены может быть обусловлено типом покупателя, допустим покупатель имеет статус оптовик,
     * а такую цену товара не выгрузили
     */
    public function checkPriceExists($products = [])
    {
        if (!isset($this->product)) $this->product = Product::getObject($this->sets);
        foreach ($products as &$prod) {
            if ($prod['price_id'] === null)
                $prod = $this->product->findFirstPrice($prod);
        }
        return $products;
    }

    //------- END group catalog tree

    public function addFancy($body, $name)
    {
        $body_arr = explode('<img', htmlspecialchars_decode($body));
        for ($i = 1, $iMax = count($body_arr); $i < $iMax; $i++) {
            if (strpos($body_arr[$i], 'alt=""') || strpos($body_arr[$i], 'alt ')) $body_arr[$i] = str_replace('alt ', 'alt="' . $name . '" ', str_replace('alt=""', 'alt="' . $name . ' - ' . $i . '"', $body_arr[$i]));
            $tmp = explode('>', $body_arr[$i]);
            $tmp[1] = '</a>' . $tmp[1];
            $url = str_replace('_s.', '.', explode('"', explode('src="', $tmp[0])[1])[0]);
            $body_arr[$i] = implode('>', $tmp);
            $body_arr[$i - 1] .= '<a data-fancybox="gallery" href="' . $url . '">';
        }
        return htmlspecialchars(implode('<img', $body_arr));
    }

    public function savePostPhoto($post_photo, $id, $column = 'photo')
    {
        if (isset($post_photo['name']) && $post_photo['name'] != '') {
            $allowed_ext = explode(',', EXT_IMAGE);
            $up_file_ext = end(explode('.', $post_photo['name']));
            if(in_array($up_file_ext, $allowed_ext)){
                $path = Dir::createDir($id, '', static::$table);
                $file_info = pathinfo($post_photo['name']);
                $file = $path[0] . $id . '_' . $column . '.' . $file_info['extension'];
                move_uploaded_file($post_photo['tmp_name'], $file);
                $this->update(array($column => $file), "id={$id}");
                return 'success uploade file';
            }else{
                return 'error invalid file';
            }
        }
        return 'error uploade file';
    }


    public function savePhoto($id, $tmp_image, $tb)
    {
        $ext = 'jpg'; // default
        $ext_arr = explode(',', EXT_IMAGE);
        foreach ($ext_arr as $e) {
            if (file_exists('files/tmp/' . $tmp_image . '.' . $e)) {
                $ext = pathinfo('files/tmp/' . $tmp_image . '.' . $e, PATHINFO_EXTENSION);
            }
        }

        $ext = strtolower($ext);

        if (isset($tmp_image) && file_exists('files/tmp/' . $tmp_image . '.' . $ext)) {
            $dir = Dir::createDir($id, '', $tb);
            $tmp_photo = 'files/tmp/' . $tmp_image . '.' . $ext;
            copy($tmp_photo, $dir['0'] . $id . "." . $ext);
            unlink($tmp_photo);

            $tmp_photo_s = 'files/tmp/' . $tmp_image . '_s.' . $ext;
            copy($tmp_photo_s, $dir['0'] . $id . "_s." . $ext);
            unlink($tmp_photo_s);

            $tmp_photo_s_2x = 'files/tmp/' . $tmp_image . '_s_2x.' . $ext;

            if (file_exists($tmp_photo_s_2x)) {
                copy($tmp_photo_s_2x, $dir['0'] . $id . "_s_2x." . $ext);
                unlink($tmp_photo_s_2x);
            }

            $this->photo_del("files/tmp/", $tmp_image . "." . $ext);
            $this->photo_del("files/tmp/", $tmp_image . "_s." . $ext);
            $this->photo_del("files/tmp/", $tmp_image . "_s_2x." . $ext);
            $this->db->query("UPDATE `" . $tb . "` SET `photo`=? WHERE `id`=?", [$dir['0'] . $id . "_s." . $ext, $id]);
        }
    }
}