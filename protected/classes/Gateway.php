<?
class Gateway
{
	public function __construct($vars){
		$this->sms_length_lat = 160;
		$this->sms_length_cyr = 65;
		$this->API_url = 'http://agentik.com/api/public/';
		$this->API_key = $vars['settings']['sender_api_key'];
	}

	public function sms_send($text, $phone, $limit=1, $trans=false) {
		$message='';
		$method='sendmessage';
		$phone_reg='/[0-9\+\ \(\)\-]{1,}/';
		$text_reg='/[А-Яа-я]{1,}/';
		if($trans==true) $text=$this->translit($text);
		if(preg_match($text_reg, $text)==true) $length=$limit*$this->sms_length_cyr;
		else $length=$limit*$this->sms_length_lat;
		if(preg_match($phone_reg, $phone)==true){
			if(count($text)<=$limit*$length)$text=substr($text,0, $length);
			$text=urlencode($text);
			$data=['text'=>$text, 'phone'=>$phone];
			//forming finish query url
			$params='?key='.$this->API_key;
			$url=$this->API_url.$method.$params;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			$message=$result;
		}else $message.='Неверный формат телефона';
		return $message;
	}

	public function email_send($text, $subject, $mail) {
		$method='sendemail';
		$mail=explode(',', $mail);
		$data=['subject'=>$subject,'body'=>$text, 'email'=>$mail];
		$data=http_build_query($data);
		$params='?key='.$this->API_key;
		$url=$this->API_url.$method.$params;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		$message=$result;
		return $message;
	}

	public function sms_send_many($text, $phones, $limit=1, $trans=false) {
		$message='';
		$method='sendmessages';
		$phone_reg='/[0-9\+\ \(\)\-]{1,}/';
		$text_reg='/[А-Яа-я]{1,}/';
		if($trans==true) $text=$this->translit($text);
		if(preg_match($text_reg, $text)==true) $length=$limit*$this->sms_length_cyr;
		else $length=$limit*$this->sms_length_lat;
		if(count($text)<=$limit*$length)$text=substr($text,0, $length);
		$text=urlencode($text);
		$data=['text'=>$text, 'phones'=>$phones];
		$data=http_build_query($data);
		$params='?key='.$this->API_key;
		$url=$this->API_url.$method.$params;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		$message=$result;
		return $message;
	}

	public function get_balance($encode=false) {
		$message='';
		$method='getbalance';
		$params='?key='.$this->API_key;
		$url=$this->API_url.$method.$params;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		if($encode==true){
			$result = json_decode($result, true);
			$result = $result['data']['balance'];
		}
		$message.=$result;
		return $message;
	}

	function translit($str){
		$table = array('А' => 'A',		'Б' => 'B',		'В' => 'V',		'Г' => 'G',		'Д' => 'D',		'Е' => 'E',
						'Ё' => 'Yo',	'Ж' => 'Zh',	'З' => 'Z',		'И' => 'I',		'Й' => 'Y',		'К' => 'K',
						'Л' => 'L',		'М' => 'M',		'Н' => 'N',		'О' => 'O',		'П' => 'P',		'Р' => 'R',
						'С' => 'S',		'Т' => 'T',		'У' => 'U',		'Ф' => 'F',		'Х' => 'H',		'Ц' => 'Ts',
						'Ч' => 'Ch',	'Ш' => 'Sh',	'Щ' => 'Shch',	'Ъ' => '',		'Ы' => 'I',		'Ь' => '',
						'Э' => 'E',		'Ю' => 'Yu',	'Я' => 'Ya',	'а' => 'a',		'б' => 'b',		'в' => 'v',
						'г' => 'g',		'д' => 'd',		'е' => 'e',		'ё' => 'yo',	'ж' => 'zh',	'з' => 'z',
						'и' => 'i',		'й' => 'y',		'к' => 'k',		'л' => 'l',		'м' => 'm',		'н' => 'n',
						'о' => 'o',		'п' => 'p',		'р' => 'r',		'с' => 's',		'т' => 't',		'у' => 'u',
						'ф' => 'f',		'х' => 'h',		'ц' => 'ts',	'ч' => 'ch',	'ш' => 'sh',	'щ' => 'shch',
						'ъ' => '',		'ы' => 'i',		'ь' => '',		'э' => 'e',		'ю' => 'yu',	'я' => 'ya');
		$str = strtr($str, $table);
		$str=mb_strtolower($str);
		return $str;
	}
}