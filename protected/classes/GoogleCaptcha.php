<?php

// created by htm
class GoogleCaptcha
{

	public static function check($secretkey)
	{
		//$secretkey = "6LflGJcUAAAAAKh_3gMfx3kOR9NWzEME_selmDSM";
		$recaptchaResponse = $_POST['g-recaptcha-response'] ?? '';
		$response = file_get_contents(
			"https://www.google.com/recaptcha/api/siteverify?secret=" . $secretkey . "&response=" . $recaptchaResponse . "&remoteip=" . $_SERVER['REMOTE_ADDR']
		);
		if (($recaptchaResponse) && !empty($_POST['g-recaptcha-response'])) {
			$responseData = json_decode($response);
			if (!$responseData->success) {
				return 0;
			} // 0
		} else {
			return 0;
		} // 0
		return 1;
	}
}