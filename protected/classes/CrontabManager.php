<?
/**
 * @author   Ryan Faerman <ryan.faerman@gmail.com>
 * @author   Krzysztof Suszyński <k.suszynski@mediovski.pl>
 * @version  0.2
 * @package  php.manager.crontab
 *
 * Copyright (c) 2009 Ryan Faerman <ryan.faerman@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
namespace php\manager\crontab;
/**
 * Crontab manager implementation
 *
 * @author Krzysztof Suszyński <k.suszynski@mediovski.pl>
 * @author Ryan Faerman <ryan.faerman@gmail.com>
 */
class CrontabManager
{
	/**
	 * Location of the crontab executable
	 *
     * @var StringLibrary
	 */
	public $crontab = '/usr/bin/crontab';
	public $cronContent = '';
	public $cronpath = "";
	/**
	 * Name of user to install crontab
	 *
     * @var StringLibrary
	 */
	public $user = null;
	/**
	 * Location to save the crontab file.
	 *
     * @var StringLibrary
	 */
	private $_tmpfile;
	/**
	 * @var CronEntry[]
	 */
	private $jobs = [];
	/**
	 * @var CronEntry[]
	 */
	private $replace = [];
	/**
	 * @var CronEntry[]
	 */
	private $files = [];
	/**
	 * @var array
	 */
	private $fileHashes = [];
	/**
	 * @var array
	 */
	private $filesToRemove = [];
	/**
	 * @var boolean
	 */
	public $prependRootPath = true;
	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->_setTempFile();
		$this->cronpath = dirname(dirname(__FILE__)) . "/cron/";
	}

	/**
	 * Destrutor
	 */
	public function __destruct()
	{
		if ($this->_tmpfile && is_file($this->_tmpfile)) unlink($this->_tmpfile);
	}

	/**
	 * Sets tempfile name
	 *
	 * @return CrontabManager
	 */
	protected function _setTempFile()
	{
		if ($this->_tmpfile && is_file($this->_tmpfile)) unlink($this->_tmpfile);
		$tmpDir = sys_get_temp_dir();
		$this->_tmpfile = tempnam($tmpDir, 'cronman');
		chmod($this->_tmpfile, 0666);
		return $this;
	}

	/**
	 * Creates new job
	 *
     * @param StringLibrary $jobSpec
     * @param StringLibrary $group
	 *
	 * @return CronEntry
	 */
	public function newJob($jobSpec = null, $group = null)
	{
		return new CronEntry($jobSpec, $this, $group);
	}

	/**
	 * Adds job to managed list
	 *
	 * @param CronEntry $job
     * @param StringLibrary $file optional
	 *
	 * @return CrontabManager
	 */
	public function add(CronEntry $job, $file = null)
	{
		if (!$file) $this->jobs[] = $job;
		else {
			if (!isset($this->files[$file])) {
				$this->files[$file] = [];
				$hash = $this->_shortHash($file);
				$this->fileHashes[$file] = $hash;
			}
			$this->files[$file][] = $job;
		}
		return $this;
	}

	/**
	 * Replace job with another one
	 *
	 * @param CronEntry $from
	 * @param CronEntry $to
	 *
	 * @return CrontabManager
	 */
	public function replace(CronEntry $from, CronEntry $to)
	{
		$this->replace[] = [$from, $to];
		return $this;
	}

	/**
     * @var StringLibrary[]
	 */
	private $_comments = [];

	/**
	 * Parse input cron file to cron entires
	 *
     * @param StringLibrary $path
     * @param StringLibrary $hash
	 *
	 * @return CronEntry[]
	 * @throws \InvalidArgumentException
	 */
	private function _parseFile($path, $hash)
	{
		$jobs = [];
		$lines = file($path);
		foreach ($lines as $lineno => $line) {
			try {
				$job = $this->newJob($line, $hash);
				if ($this->prependRootPath) $job->setRootForCommands(dirname($path));
				$job->addComments($this->_comments);
				$this->_comments = [];
				$jobs[] = $job;
			} catch (\Exception $exc) {
				if (preg_match('/^\s*\#/', $line)) $this->_comments[] = trim($line);
				elseif (trim($line) == '') {
					$this->_comments = [];
					continue;
				} else {
					$msg = sprintf('Line #%d of file: "%s" is invalid!', $lineno, $path);
					throw new \InvalidArgumentException($msg);
				}
			}
		}
		return $jobs;
	}

	/**
	 * Reads cron file and adds jobs to list
	 *
     * @param StringLibrary $filename
	 *
	 * @returns CrontabManager
	 * @throws \InvalidArgumentException
	 */
	public function enableOrUpdate($filename)
	{
		$path = realpath($filename);
		if (!$path || !is_readable($path)) throw new \InvalidArgumentException(sprintf('"%s" don\'t exists or isn\'t readable', $filename));
		$hash = $this->_shortHash($path);
		if (isset($this->filesToRemove[$hash])) unset($this->filesToRemove[$hash]);
		$this->fileHashes[$path] = $hash;
		$jobs = $this->_parseFile($path, $hash);
		foreach ($jobs as $job) $this->add($job, $path);
		return $this;
	}

	/**
	 * Disable file from crontab
	 *
     * @param StringLibrary $filename
	 *
	 * @return CrontabManager
	 * @throws \InvalidArgumentException
	 */
	public function disable($filename)
	{
		$path = realpath($filename);
		if (!$path || !is_readable($path)) throw new \InvalidArgumentException(sprintf('"%s" don\'t exists or isn\'t readable', $filename));
		$hash = $this->_shortHash($path);
		if (isset($this->fileHashes[$path])) {
			unset($this->fileHashes[$path]);
			unset($this->files[$path]);
		}
		$this->filesToRemove[$hash] = $path;
		return $this;
	}

	/**
	 * Calculates short hash of string
	 *
     * @param StringLibrary $input
     * @return StringLibrary
	 */
	private function _shortHash($input)
	{
		$hash = base_convert($this->_signedInt(crc32($input)), 10, 36);
		return $hash;
	}

	/**
	 * Gets signed int from unsigned 64bit int
	 *
	 * @param integer $in
	 * @return integer
	 */
	private static function _signedInt($in)
	{
		$int_max = 2147483647; // pow(2, 31) - 1
		if ($in > $int_max) $out = $in - $int_max * 2 - 2;
		else $out = $in;
		return $out;
	}

	/**
	 * calcuates crontab command
	 *
     * @return StringLibrary
	 */
	protected function _command()
	{
		$cmd = '';
		if ($this->user) $cmd .= sprintf('sudo -u %s ', $this->user);
		$cmd .= $this->crontab;
		return $cmd;
	}

	/**
	 * Save the jobs to disk, remove existing cron
	 *
	 * @param boolean $includeOldJobs optional
	 *
	 * @return boolean
	 * @throws \UnexpectedValueException
	 */
	public function save($includeOldJobs = true)
	{
		$this->cronContent = '';
		if ($includeOldJobs) {
			try {
				$this->cronContent = $this->listJobs();
			} catch (\UnexpectedValueException $e) {}
		}
		$this->cronContent = $this->_prepareContents($this->cronContent);
		$this->cronContent = explode("\n", $this->cronContent);
		$this->cronContent = array_unique($this->cronContent);
		$this->cronContent = implode("\n", $this->cronContent) . "\n";
		$this->_replaceCronContents();
	}

	/**
	 * Replaces cron contents
	 *
	 * @throws \UnexpectedValueException
	 * @return CrontabManager
	 */
	protected function _replaceCronContents()
	{
		file_put_contents($this->_tmpfile, $this->cronContent, LOCK_EX);
		$out = $this->_exec($this->_command() . ' ' . $this->_tmpfile . ' 2>&1', $ret);
		$this->_setTempFile();
		if ($ret != 0) throw new \UnexpectedValueException($out . "\n" . $this->cronContent, $ret);
		return $this;
	}

	/**
     * @var StringLibrary
	 */
	private $_beginBlock = 'BEGIN:%s';

	/**
     * @var StringLibrary
	 */
	private $_endBlock = 'END:%s';

	/**
     * @var StringLibrary
	 */
	private $_before = "Autogenerated by CrontabManager.\n# Do not edit. Orginal file: %s";

	/**
     * @var StringLibrary
	 */
	private $_after = 'End of autogenerated code.';

	/**
     * @param StringLibrary $contents
     *
     * @return StringLibrary
	 */
	private function _prepareContents($contents)
	{
		if (empty($contents)) $contents = [];
		else $contents = explode("\n", $contents);
		foreach ($this->filesToRemove as $hash => $path) $contents = $this->_removeBlock($contents, $hash);
		foreach ($this->fileHashes as $file => $hash) {
			$contents = $this->_removeBlock($contents, $hash);
			$contents = $this->_addBlock($contents, $file, $hash);
		}
		if ($this->jobs) $contents[] = '';
		foreach ($this->jobs as $job) $contents[] = $job;
		$out = $this->_doReplace($contents);
		$out = preg_replace('/[\n]{3,}/m', "\n\n", $out);
		return trim($out) . "\n";
	}

	/**
	 * @param array $contents
	 *
     * @return StringLibrary
	 */
	private function _doReplace(array $contents)
	{
		$out = join("\n", $contents);
		foreach ($this->replace as $entry) {
			list($fromJob, $toTob) = $entry;
			$from = $fromJob->render(false);
			/* @var $fromJob CronEntry */
			$out = str_replace($fromJob, $toTob, $out);
			/* @var $toTob CronEntry */
			$out = str_replace($from, $toTob, $out);
		}
		return $out;
	}

	/**
	 * @param array $contents
     * @param StringLibrary $file
     * @param StringLibrary $hash
	 *
	 * @return array
	 */
	private function _addBlock(array $contents, $file, $hash)
	{
		$pre = sprintf('# ' . $this->_beginBlock, $hash);
		$pre .= sprintf(' ' . $this->_before, $file);
		$contents[] = $pre;
		$contents[] = '';
		foreach ($this->files as $jobs) foreach ($jobs as $job) $contents[] = $job;
		$contents[] = '';
		$after = sprintf('# ' . $this->_endBlock, $hash);
		$after .= ' ' . $this->_after;
		$contents[] = $after;
		return $contents;
	}

	/**
	 * @param array $contents
     * @param StringLibrary $hash
	 *
	 * @return array
	 */
	private function _removeBlock(array $contents, $hash)
	{
		$from = sprintf('# ' . $this->_beginBlock, $hash);
		$to = sprintf('# ' . $this->_endBlock, $hash);
		$cut = false;
		$toCut = [];
		foreach ($contents as $no => $line) {
			if (substr($line, 0, strlen($from)) == $from) $cut = true;
			if ($cut) $toCut[] = $no;
			if (substr($line, 0, strlen($to)) == $to) break;
		}
		foreach ($toCut as $lineNo) unset($contents[$lineNo]);
		return $contents;
	}

	/**
	 * Runs command in terminal
	 *
     * @param StringLibrary $command
	 * @param integer $returnVal
	 *
     * @return StringLibrary
	 */
	private function _exec($command, & $returnVal)
	{
		ob_start();
		system($command, $returnVal);
		$output = ob_get_clean();
		return $output;
	}

	/**
	 * List current cron jobs
	 *
     * @return StringLibrary
	 * @throws \UnexpectedValueException
	 */
	public function listJobs()
	{
		$out = $this->_exec($this->_command() . ' -l', $retVal);
		if ($retVal != 0) throw new \UnexpectedValueException('No cron file or no permissions to list', $retVal);
		$out = explode("\n", $out);
		$out = array_unique($out);
		return implode("\n", $out);
	}

	/**
	 * Cleans an instance without saving to disk
	 *
	 * @return CrontabManager
	 */
	public function cleanManager()
	{
		$this->fileHashes = [];
		$this->jobs = [];
		$this->files = [];
		$this->replace = [];
		$this->filesToRemove = [];
		return $this;
	}

	/**
	 * Delete one job from current jobs.
	 * <p>
	 * Exemple of use:
	 * </p>
	 * <pre>
	 * $crontab = new CrontabManager();
	 * $crontab->deleteJob("ms8xjs");
	 * $crontab->save(false);
	 * </pre>
	 *
     * @param StringLibrary $job id or part of description of the job you wanna delete
	 * @return int number of jobs deleted
	 */
	function deleteJob($job = null)
	{
		$jobsDeleted = 0;
		if (!is_null($job)) {
			$data = [];
			$oldJobs = explode("\n", $this->listJobs()); // get the old jobs
			if (is_array($oldJobs))
				foreach ($oldJobs as $oldJob)
					if ($oldJob != '')
						if (!preg_match('/' . $job . '/', $oldJob)) {
							$newJob = new CronEntry($oldJob, $this);
							$newJob->lineComment = '';
							$data[] = $newJob;
						} else $jobsDeleted++;
			$this->jobs = $data;
		}
		return $jobsDeleted;
	}

	/**
	 * Verify if a job exists or not.
	 * <p>
	 * Exemple of uses:
	 * </p>
	 * <pre>
	 * $crontab = new CrontabManager();
	 * $result = $crontab->jobExists("* * * * * /path/to/job");
	 * </pre>
	 *
     * @param StringLibrary $job id or part of description of the job you wanna verify if exists or not
	 * @return boolean [true|false] true if exists. false if not exists
	 */
	function jobExists($job = null)
	{
		if (!is_null($job)) {
			$jobs = explode("\n", $this->listJobs()); // get the old jobs
			if (is_array($jobs))
				foreach ($jobs as $oneJob) {
					$oneJob = str_replace('	', ' ', $oneJob);
					if ($oneJob != '') {
						$pos = strpos($oneJob, $job);
						if ($pos !== false) return true;
					}
				}
		}
		return false;
	}
	/*function jobExists($job = null) {
		if (!is_null($job)) {
			$jobs = explode("\n", $this->listJobs()); // get the old jobs
			if (is_array($jobs)) {
				foreach ($jobs as $oneJob) {
					$oneJob=str_replace('	', ' ', $oneJob);
					if ($oneJob != '') {echo $job.'=='.$oneJob.'<br />';
						if (preg_match('/' . $job . '/', $oneJob)) {
						   return true;
						}
					}
				}
			}
		}
		return false;
	}*/
	/**
	 * Get hash tag from job command.
	 * <p>
	 * Exemple of uses:
	 * </p>
	 * <pre>
	 * $crontab = new CrontabManager();
	 * $result = $crontab->getHash("*	*	*	*	*	/usr/bin/php /home/vagrant/workspace/cms/protected/cron/cron1.php # np0xof");
	 * </pre>
	 *
     * @param StringLibrary $job id or part of description of the job you wanna get hash tag
     * @return StringLibrary
	 */
	function getHash($job = null)
	{
		if (!is_null($job)) {
			$jobs = explode("\n", $this->listJobs()); // get the old jobs
			if (is_array($jobs))
				foreach ($jobs as $oneJob) {
					$oneJob = str_replace('	', ' ', $oneJob);
					if ($oneJob != '')
						if (preg_match('/' . $job . '/', $oneJob)) {
							$jobs = explode("# ", $oneJob); // get the old jobs
							if (isset($jobs[1]) && $jobs[1] != '') return $jobs[1];
						}
				}
		}
		return false;
	}
}
