<?
/**
 * Created by PhpStorm.
 * User: follower
 * Date: 3/12/14
 * Time: 8:51 PM
 */
class Export
{
	/*
	 * Exports array to CSV file
	 * @params array
	 */
	static public function exportCSV(array &$fields)
	{
		$delimiter=';';
		$output='';
		$tmp=[];
		foreach($fields[0] as $key=>$val)$tmp[]=$key;
		$output.=implode($delimiter,$tmp).PHP_EOL;
		foreach($fields as $row){
			$tmp=[];
			foreach($row as $field)$tmp[]=$field;
			$output.=mb_convert_encoding(implode($delimiter,$tmp),'WINDOWS-1251','UTF-8').PHP_EOL;
		}
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=export.csv');
		echo $output;
		exit();
	}

	/*
	 * Exports array to HTML file
	 * @params array
	 */
	static public function exportHTML(array &$fields)
	{
		$header='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><head><meta http-equiv="Content-type" content="text/html;charset=UTF-8"><title>Export File - Skylogic CMS</title></head><body>';
		$table='<table width="100%" cellspacing="0" border="1"><thead><tr>';
		foreach($fields[0] as $key=>$val)$table.='<td>'.$key.'</td>';
		$table.='</tr></thead><tbody>';
		foreach($fields as $row){
			$table.='<tr>';
			foreach($row as $cell)$table.='<td>'.$cell.'</td>';
		    $table .= '</tr>';
		}
	    $table .= '</tbody></table>';
	    $footer = '</body></html>';
	    header('Content-Type: text/html; charset=utf-8');
	    header('Content-Disposition: attachment; filename=export.html');
	    echo $header . $table . $footer;
	    exit();
	}

    /**
	* @description TODO Exports array to XLS file
	* @param array $fields
	*/
    public static function exportXLS(array &$fields): void
    {
	   $titles = '';
	   $data = '';
	   $output = file_get_contents(LIBRARY . 'template.html');
	   foreach ($fields[0] as $key => $val) {
		  $titles .= '<td>' . $key . '</td>';
	   }
	   foreach ($fields as $row) {
		  $data .= '<tr>';
		  foreach ($fields[0] as $key => $val) {
			 (is_numeric($row[$key])) ? $p = 'Number' : $p = 'String';
			 $data .= '<td>' . $row[$key] . '</td>';
		  }
		  $data .= '</tr>';
	   }
	   $output = str_replace(array('{columns}', '{data}'), array($titles, $data), $output);
	   header('Content-Type: application/vnd.ms-excel; format=attachment;');
	   header('Content-Disposition: attachment; filename=export.xls');
	   echo $output;
	   exit();
    }

    /*
	* Exports array to PDF file
	* @params array
	*/
    static public function exportPDF(array &$fields)
    {
	   echo '@TODO';
    }
}