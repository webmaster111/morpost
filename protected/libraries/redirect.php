<?php
$hts = 'https://';
$ht = 'http://';
$reqUri = $_SERVER['REQUEST_URI'];
$hos = $_SERVER['HTTP_HOST'];

if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === '') {
	$redirect = $hts . $hos . $reqUri;
	redirects($redirect);
}
if (strpos($reqUri, '//') !== false) {
	$redirect = $hts . $hos . str_replace('//', '/', $reqUri);
	redirects($redirect);
} elseif ($reqUri === '/?') {
	$redirect = $hts . $hos;
	redirects($redirect);
}

function redirects($redirect='') {
	header('HTTP/1.1 301 Moved Permanently');
	header("Location: $redirect");
}
