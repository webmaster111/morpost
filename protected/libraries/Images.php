<?

class Images
{
  static function saveFromUrl($url,$path)
  {
    if(self::validateFileByExt($url)){
      $destination_file=$path.'tmp.'.end(explode('.',$url));
      $source=file_get_contents($url);
      file_put_contents($destination_file,$source);
      return $destination_file;
    }else throw new Exception('incorrect file type');
  }

  static function validateFileByExt($file)
  {
    $ext=explode('.',$file);
    $ext=end($ext);
    $valid_ext=explode(',',EXT_IMAGE);
    return (in_array(strtolower($ext),$valid_ext))?true:false;
  }

  static function saveFromFile($file,$path){
    if(self::validateFileByExt($file['name'])){
      $extensions=explode('.',$file['name']);
      $ext=end($extensions);
      $destination_file=$path.'tmp.'.strtolower($ext);
      copy($file['tmp_name'],$destination_file);
      return $destination_file;
    }else throw new Exception('incorrect file type');
  }

  static function saveFromBase64($file,$path)
  {
    if(self::validateFileByImageType($file)){
      $data = self::getDataType($file);
      $extensions = explode('/',$data[0]);
      $ext = toLower(end($extensions));
      if($ext == 'jpeg') $ext='jpg';
      $destination_file = $path.'tmp.'.$ext;
      file_put_contents($destination_file,base64_decode(str_replace('base64,','',$data[1])));
      return $destination_file;
    }else throw new Exception('incorrect file type');
  }

  static function validateFileByImageType($file)
  {
    $data_type=self::getDataType($file);
    $types=explode(',',EXT_IMAGE);
    $valid_types=[];
    foreach($types as $e)$valid_types[]='data:image/'.$e;
    return (in_array($data_type[0],$valid_types))?true:false;
  }

  static function getDataType($file)
  {
    $data=str_replace(' ','+',$file);
    $data_type=explode(';',$data);
    return $data_type;
  }

  static function removeICCProfile($file){
    shell_exec('convert '.$file.' +profile "*" '.$file.'');
  }

  static function loadOriginalImage($image,$path_to_save,$id)
  {
    self::removeICCProfile($image);
    $ext=self::getFileExtension($image);
    $destination_image=$path_to_save.$id.'.'.$ext;
    if(copy($image,$destination_image)){
      $destination_image_small=(str_replace('.'.$ext,'_s.'.$ext,$destination_image));
      $destination_image_smaller=(str_replace('_s.','_s_2x.',$destination_image_small));
      $small_image=new Imagick(realpath($image));
      $small_image->thumbnailImage($small_image->getImageWidth()/2,0);
      if($ext=='png')$small_image->setImageFormat("png");
      else $small_image->setImageFormat("jpeg");
      file_put_contents($destination_image_small,$small_image);
      $small_image->thumbnailImage($small_image->getImageWidth()/2,0);
      file_put_contents($destination_image_smaller,$small_image);
      $small_image->destroy();
      unlink($image);
      return $destination_image_small;
    }else return false;
  }

  static function loadSliderOriginImage($image,$path_to_save1,$path_to_save2,$width,$height){
    self::removeICCProfile($image);
    $ext=self::getFileExtension($image);

    $destination_image_small=$path_to_save1;
    $destination_image_smaller=$path_to_save2;
    $small_image=new Imagick(realpath($image));
    $small_image->thumbnailImage($width,$height);
    if($ext=='png')$small_image->setImageFormat("png");
    else $small_image->setImageFormat("jpeg");
    file_put_contents($destination_image_small,$small_image);
    $small_image->thumbnailImage($width,$height);
    file_put_contents($destination_image_smaller,$small_image);
    $small_image->destroy();
    unlink($image);
    return $destination_image_small;
  }

  static function getFileExtension($file)
  {
    $parsed=explode('.',$file);
    return end($parsed);
  }

  static function putInArea($image,$width,$height,$path_to_save,$id)
  {
    self::removeICCProfile($image);
    $ext=self::getFileExtension($image);
    $bg=new Imagick();
    $o_image=$image;
    $image=new Imagick(realpath($image));
    if(in_array($ext,['png','gif'])){
      $image->setImageFormat('png');
      $destination_image=$path_to_save.$id.'.png';
      $destination_image_small=(str_replace('.'.$ext,'_s.png',$destination_image));
      $bg->newImage($width,$height,new ImagickPixel('none'),'png');
    }elseif(in_array($ext,['jpg','jpeg','bmp'])){
      $image->setImageFormat('jpg');
      $destination_image=$path_to_save.$id.'.jpg';
      $bg->newImage($width,$height,new ImagickPixel('white'),'jpg');
      $destination_image_small=(str_replace('.'.$ext,'_s.jpg',$destination_image));
    }else{
      $image->setImageFormat('jpg');
      $destination_image=$path_to_save.$id.'.jpg';
      $bg->newImage($width,$height,new ImagickPixel('white'),'jpg');
      $destination_image_small=(str_replace('.'.$ext,'_s.jpg',$destination_image));
    }
    //copy original image
    file_put_contents($destination_image,$image);
    //create and save smaller version
    if($image->getImageWidth()> $width)$image->thumbnailImage($width,0);
    if($image->getImageHeight()> $height)$image->thumbnailImage(0,$height);
    $composite_w=($bg->getImageWidth()-$image->getImageWidth())/2;
    $composite_h=($bg->getImageHeight()-$image->getImageHeight())/2;
    $bg->compositeImage($image,Imagick::COMPOSITE_COPY,$composite_w,$composite_h);
    $bg->setImageCompressionQuality(90);
    file_put_contents($destination_image_small,$bg);
    //copy 2x small thumb
    $bg->thumbnailImage($bg->getImageWidth()/2,0);
    file_put_contents(str_replace('_s.','_s_2x.',$destination_image_small),$bg);
    $image->destroy();
    unlink($o_image);
    return $destination_image_small;
  }

  static function saveCroppedArea($image,$width,$height,$path_to_save,$id)
  {
    self::removeICCProfile($image);
    $ext=self::getFileExtension($image);
    $bg=new Imagick();
    $o_image=$image;
    $image=new Imagick(realpath($image));
    if(in_array($ext,['png','gif'])){
      $image->setImageFormat('png');
      $destination_image=$path_to_save.$id.'.png';
      $destination_image_small=(str_replace('.'.$ext,'_s.png',$destination_image));
      $bg->newImage($width,$height,new ImagickPixel('none'),'png');
    }elseif(in_array($ext,['jpg','jpeg','bmp'])){
      $image->setImageFormat('jpg');
      $destination_image=$path_to_save.$id.'.jpg';
      $bg->newImage($width,$height,new ImagickPixel('white'),'jpg');
      $destination_image_small=(str_replace('.'.$ext,'_s.jpg',$destination_image));
    }else{
      $image->setImageFormat('jpg');
      $destination_image=$path_to_save.$id.'.jpg';
      $bg->newImage($width,$height,new ImagickPixel('white'),'jpg');
      $destination_image_small=(str_replace('.'.$ext,'_s.jpg',$destination_image));
    }
    //copy original image. Save original uploaded image
    if(file_exists($path_to_save.'tmp_o.'.$ext)){
      copy($path_to_save.'tmp_o.'.$ext,$destination_image);
      unlink($path_to_save.'tmp_o.'.$ext);
    }
    //file_put_contents($destination_image,$image);//Save only cropped canvas
    //create and save smaller version
    $image->thumbnailImage($width,$height);
    $bg->compositeImage($image,Imagick::COMPOSITE_COPY,0,0);
    $bg->setImageCompressionQuality(90);
    file_put_contents($destination_image_small,$bg);
    //copy 2x small thumb
    $bg->thumbnailImage($bg->getImageWidth()/2,0);
    file_put_contents(str_replace('_s.','_s_2x.',$destination_image_small),$bg);
    $image->destroy();
    unlink($o_image);
    return $destination_image_small;
  }

  static function setWatermark($watermark,$path,$action,$file=null)
  {
    $watermark=json_decode($watermark,true);
    if($watermark['modules']!=NULL&&in_array($action,$watermark['modules'])&&$watermark['active']){
      switch ($watermark['position']){
        case 'top_left':$gravity=Imagick::GRAVITY_NORTHEAST;break;
        case 'top_center':$gravity=Imagick::GRAVITY_NORTH;break;
        case 'top_right':$gravity=Imagick::GRAVITY_NORTHWEST;break;
        case 'center_left':$gravity=Imagick::GRAVITY_WEST;break;
        case 'center_center':$gravity=Imagick::GRAVITY_CENTER;break;
        case 'center_right':$gravity=Imagick::GRAVITY_EAST;break;
        case 'bot_left':$gravity=Imagick::GRAVITY_SOUTHWEST;break;
        case 'bot_center':$gravity=Imagick::GRAVITY_SOUTH;break;
        case 'bot_right':$gravity=Imagick::GRAVITY_SOUTHEAST;break;
        default:$gravity=Imagick::GRAVITY_CENTER;
      }
      $watermark_image=(isset($file))?$file:$watermark['image'];
      $watermark_image=new Imagick(realpath($watermark_image));
      $watermark_image->setImageFormat('png');
      $watermark_image->setCompressionQuality(100);
      $save_format=(self::getFileExtension($path)=='png')?'png':'jpg';
      if($watermark['type_image']==1||$watermark['type_image']==2){
        $image=$save_path=str_replace('_s','',$path);
        if(file_exists($image)){
          $image=new Imagick(realpath($image));
          if($image->getImageWidth()<$watermark_image->getImageWidth())$watermark_image->thumbnailImage($image->getImageWidth(),0);
          $coordinates=self::getGravityCoordinates($image,$watermark_image,$gravity);
          $image->compositeImage($watermark_image,Imagick::COMPOSITE_DEFAULT,$coordinates['x'],$coordinates['y']);
          $image->setCompressionQuality(80);
          $image->setImageFormat($save_format);
          file_put_contents($save_path,$image);
        }
      }
      if($watermark['type_image']==0||$watermark['type_image']==2){
        if(file_exists($path)){
          $image_s=new Imagick(realpath($path));
          if($image_s->getImageWidth()<$watermark_image->getImageWidth())$watermark_image->thumbnailImage($image_s->getImageWidth(),0);
          $coordinates=self::getGravityCoordinates($image_s,$watermark_image,$gravity);
          $image_s->compositeImage($watermark_image,Imagick::COMPOSITE_DEFAULT,$coordinates['x'],$coordinates['y']);
          $image_s->setCompressionQuality(80);
          $image_s->setImageFormat($save_format);
          file_put_contents($path,$image_s);
        }
        $smaller=str_replace('_s.','_s_2x.',$path);
        if(file_exists($smaller)){
          $image_s2=new Imagick(realpath($smaller));
          if($image_s2->getImageWidth()<$watermark_image->getImageWidth())$watermark_image->thumbnailImage($image_s2->getImageWidth(),0);
          $coordinates=self::getGravityCoordinates($image_s2,$watermark_image,$gravity);
          $image_s2->compositeImage($watermark_image,Imagick::COMPOSITE_DEFAULT,$coordinates['x'],$coordinates['y']);
          $image_s2->setCompressionQuality(80);
          $image_s2->setImageFormat($save_format);
          file_put_contents($smaller,$image_s2);
        }
      }
    }
  }

  static function getGravityCoordinates(Imagick $image,Imagick $watermark,$gravity,$border_offset=0)
  {
    $coordinates=[];
    switch ($gravity){
      case Imagick::GRAVITY_NORTHEAST:
        //top right point
        $coordinates['x']=$image->getImageWidth()-$watermark->getImageWidth()-$border_offset;
        $coordinates['y']=0+$border_offset;
        break;
      case Imagick::GRAVITY_NORTH:
        //top center point
        $coordinates['x']=$coordinates['x']=($image->getImageWidth()-$watermark->getImageWidth())/2;
        $coordinates['y']=0+$border_offset;
        break;
      case Imagick::GRAVITY_NORTHWEST:
        //top left point
        $coordinates['x']=0+$border_offset;
        $coordinates['y']=0+$border_offset;
        break;
      case Imagick::GRAVITY_WEST:
        //left center point
        $coordinates['x']=0+$border_offset;
        $coordinates['y']=($image->getImageHeight()-$watermark->getImageHeight())/2;
        break;
      case Imagick::GRAVITY_CENTER:
        //center center point
        $coordinates['x']=($image->getImageWidth()-$watermark->getImageWidth())/2;
        $coordinates['y']=($image->getImageHeight()-$watermark->getImageHeight())/2;
        break;
      case Imagick::GRAVITY_EAST:
        //right center point
        $coordinates['x']=$image->getImageWidth()-$watermark->getImageWidth()-$border_offset;
        $coordinates['y']=($image->getImageHeight()-$watermark->getImageHeight())/2;
        break;
      case Imagick::GRAVITY_SOUTHWEST:
        //left bottom point
        $coordinates['x']=0+$border_offset;
        $coordinates['y']=$image->getImageHeight()-$watermark->getImageHeight()-$border_offset;
        break;
      case Imagick::GRAVITY_SOUTH:
        //center bottom point
        $coordinates['x']=($image->getImageWidth()-$watermark->getImageWidth())/2;
        $coordinates['y']=$image->getImageHeight()-$watermark->getImageHeight()-$border_offset;
        break;
      case Imagick::GRAVITY_SOUTHEAST:
        //right bottom point
        $coordinates['x']=$image->getImageWidth()-$watermark->getImageWidth()-$border_offset;
        $coordinates['y']=$image->getImageHeight()-$watermark->getImageHeight()-$border_offset;
        break;
      default:
        //Equals to Imagick::GRAVITY_CENTER
        $coordinates['x']=($image->getImageWidth()-$watermark->getImageWidth())/2;
        $coordinates['y']=($image->getImageHeight()-$watermark->getImageHeight())/2;
    }
    return $coordinates;
  }
}
