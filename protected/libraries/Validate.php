<?

class Validate
{
  static function check($str, $translation, $type = 'text')
  {
    $message = '';
    if (is_array($str)) {
      foreach ($str as $row) {
        if ($type == 'email')
          if (!preg_match('|\b([a-z0-9_\.\-]{1,20})@\b([a-z0-9\.\-]{1,20}\b)\.([a-z]{2,5})|is', $row)) $message = "<div class='alert alert-danger'>" . $translation['wrong_email'] . "</div>";
          elseif ($type == 'text')
            if (!isset($row{1})) $message = "<div class='alert alert-danger'>" . $translation['required'] . "</div>";
            elseif ($type == 'password') if (!isset($row{5})) $message = "<div class='alert alert-danger'>" . $translation['required'] . "</div>";
      }
    } else {
      if ($type == 'email' && !preg_match('|\b([a-z0-9_\.\-]{1,20})@\b([a-z0-9\.\-]{1,20}\b)\.([a-z]{2,5})|is', $str)) $message = "<div class='alert alert-danger'>" . $translation['wrong_email'] . "</div>";
      if ($type == 'phone' && !preg_match('/^\(?[0-9]{3}\)\s?[0-9-]{9}$/', $str)) $message = "<div class='alert alert-danger'>" . $translation['wrong_phone'] . "</div>";
      elseif ($type == 'text' && !isset($str{3})) $message = "<div class='alert alert-danger'>" . $translation['required'] . "</div>";
    }
    return $message;
  }

  static function checkPass($pass, $pass2)
  {
    $text = $pass != $pass2 ? "<div class='alert alert-danger'>Пароли не совпадают</div>" : '';
    if (!isset($pass{6})) $text .= "<div class='alert alert-danger'>Пароль не менее 7 символов!</div>";
    return $text;
  }
}