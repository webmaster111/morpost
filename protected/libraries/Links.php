<?

class Links
{

    public static function getPointParams($point)
    {
        if (strpos($point['url'], "http://") !== false OR strpos($point['url'], "https://") !== false) {
            $p['link'] = $point['url'] . '" target="_blank" rel="nofollow';
        } elseif ($point['url'] == "/") {
            $p['link'] = Links::mainPage();
        } else {
            $p['link'] = LINK . "/" . $point['url'];
        }

        $p['link'] = str_replace('//', '/', $p['link']);
        $p['class'] = '';

        if (LINK . $_SERVER['REQUEST_URI'] == $p['link'] || $p['link'] == LINK . '/#') {
            $p['link'] = '#';
            $p['class'] = 'active';
        }

        return $p;
    }

    public static function getLink($url)
    {
        if ($_SERVER['REQUEST_URI'] == $url || empty($url)) return '#';
        return LINK . str_replace('//', '/', '/' . $url);
    }

    public static function mainPage()
    {
        if ($_SERVER['REQUEST_URI'] == '/' OR empty($_SERVER['REQUEST_URI'])) return LINK . '/#';
        return LINK . '/';
    }

    public static function setDefaultImage($src, $pathDefaultImg = '/tpl/default/img/default-image.jpg')
    {
        if (file_exists($src)) return '/' . $src;
        return $pathDefaultImg;
    }
}
