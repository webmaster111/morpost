<?php

    use PHPMailer\PHPMailer\PHPMailer;

    class Mail
    {

	   public static function errorMail($text): void
	   {
		  $contact_mail = EMAIL_ERROR;
		  $url = $_SERVER['REQUEST_URI'];
		  $refer = $_SERVER['HTTP_REFERER'] ?? '';
		  $ip_user = $_SERVER['REMOTE_ADDR'];
		  $br_user = $_SERVER['HTTP_USER_AGENT'];
		  $subject = 'Отладка ошибок в системе SkyCms:' . $_SERVER['SERVER_NAME'];
		  $body = 'SERVER_NAME:' . $_SERVER['SERVER_NAME'] . "страница: $url \nREFER страница: $refer \nIP пользователя: $ip_user \nбраузер пользователя: $br_user \n----------------------------------------- \n$text";
		  self::send($subject, $body, $contact_mail);
	   }

	   public static function send(string $subject, string $body, $email_to = NULL, $name_to = NULL): void
	   {
		  $settings = Registry::get('user_settings');
		  $subject .= ' - ' . $settings['sitename'];

		  try {
			 $mail = new PHPMailer(true);
			 $mail->isSMTP();
			 $mail->SMTPAuth = true;
			 $mail->SMTPSecure = false; //ssl или tls или false
			 $mail->SMTPAutoTLS = false;
			 $mail->Port = SMTP_PORT;
			 $mail->Host = SMTP_HOST;
			 $mail->CharSet = 'utf-8';

			 $mail->Username = SMTP_EMAIL;
			 $mail->Password = SMTP_PASSWORD;

			 $mail->setFrom(SMTP_EMAIL, $settings['sitename']);
			 $mail->addAddress($email_to ?? $settings['email'], $name_to ?? $settings['sitename']);

			 $mail->isHTML(true);
			 $mail->Subject = $subject;
			 $mail->Body = $body;
			 $mail->AltBody = strip_tags($body);

			 if (!$mail->send()) {
				$response['message'] = 'Ошибка при отправке. Ошибка: ' . $mail->ErrorInfo;
			 } else {
				$response['message'] = 'Сообщение успешно отправлено';
			 }
		  } catch (\PHPMailer\PHPMailer\Exception $exception) {
			 Log::echoLog('Ошибка отправки сообщения — ' . $exception->getMessage());
		  }
	   }
    }