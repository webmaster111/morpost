<?

class Numeric
{
    /**
     * @param $price
     * @param int $basePrice
     * @param null $currency
     * @return array
     */
    static function viewPrice($price, $basePrice = 0, $currency = NULL)
    {
        if (is_null($currency)) $currency = $_SESSION['currency'];
        $return = [];
        $return['old_price'] = ''; // Старая цена(str)
        $return['price'] = ''; // Цена с форматированием(str)
        $return['cur_price'] = 0; // Цена без форматирования(float)
        $return['base_price'] = 0; // Цена в базовой валюте(float)
        if ($currency['base'] == 1) {
            $return['base_price'] = $price;
            if ($price != $basePrice) {
                $return['old_price'] = Numeric::formatPrice($basePrice);
                $return['price'] = Numeric::formatPrice($price);
            } else $return['price'] = Numeric::formatPrice($price);
            $return['cur_price'] = round($price, 2);
        } else {
            $return['base_price'] = $price;
            $price = $price * (1 * $currency['rate']);
            if ($basePrice != 0) $basePrice = $basePrice * (1 * $currency['rate']);
            if ($price != $basePrice) {
                $return['old_price'] = Numeric::formatPrice($basePrice);
                $return['base_price'] = Numeric::discount($discount, $return['base_price']);
                $return['price'] = Numeric::formatPrice($price);
            } else $return['price'] = Numeric::formatPrice($price);
            $return['cur_price'] = round($price, 2);
        }
        if ($basePrice == 0) $return['old_price'] = '';
        return $return;
    }

    static function formatPrice($price, $currency = '')
    {
        if ($currency == '') $currency = $_SESSION['currency'];
        if (!isset($_SESSION['rounding'])) $_SESSION['rounding'] = 2;
        if ($currency['position'] == 1) $price = '<span>' . number_format($price, $_SESSION['rounding'], ',', ' ') . ' </span> <span>' . $currency['icon'] . '</span>';
        else $price = '<span>' . $currency['icon'] . '</span>' . number_format($price, $_SESSION['rounding'], ',', ' ');
        return $price;
    }

    static function discount($discount, $sum)
    {
        $discount = $discount / 100;
        return round($sum - $sum * $discount, 2);
    }

    public static function getPrice($product, $currency_rate = null)
    {
	   if (!isset($product['price'], $product['discount'])) {
		  return false;
	   }

	   if ($currency_rate === null) {
		  $currency_rate = $_SESSION['currency']['rate'];
	   }

	   //$product_currency_rate = $product['currency_rate'];

	   $return = [];
	   //$koef = $product_currency_rate / $currency_rate;
	   $koef = $currency_rate;
	   $return['price'] = $product['price'] * $koef; // Цена без форматирования(float)
	   $return['basePrice'] = $product['basePrice'] * $koef; // Старая цена(float)
	   $return['discount'] = $product['discount'] * $koef; // Скидка(float)
	   $return['price_pack'] = isset($product['price_pack']) ? $product['price_pack'] * $koef : 0; // Цена за упаковку без форматирования(float)
	   $return['price_pack_format'] = self::formatPrice($return['price_pack']); // Цена за упаковку с форматированием(str)
	   $return['price_format'] = self::formatPrice($return['price']); // Цена с форматированием(str)
	   $return['basePrice_format'] = self::formatPrice($return['basePrice']); // Старая цена с форматированием(str)
	   $return['discount_format'] = self::formatPrice($return['discount']); // Скидка с форматированием(str)

	   return $return;
    }
}