<?php
/**
 * Библиотека Методов-Хелперов
 */
function __autoload($class_name)
{
	$filename = $class_name . '.php';

	if (!include($filename)) {
		return false;
	}
}

/**
 *
 * Старый наш сервис Agentik - не пользуемся им
 * @param $db
 * @return bool
 */
function checkAuthAgentik($db)
{
  if (isset($_COOKIE['login'], $_COOKIE['password'])) {
    $sql = "SELECT id,type_moderator,login FROM `moderators` WHERE `login`=? AND `password`=? AND `active`=?";
    $param = ['admin', md5("via2012"), 1];
    $res = $db->row($sql, $param);
    if ($res['id'] != '') {
      $admin_info['agent'] = '';
      $admin_info['referer'] = '';
      $admin_info['ip'] = '';
      $admin_info['id'] = $res['id'];
      $admin_info['type'] = $res['type_moderator'];
      $admin_info['login'] = $res['login'];
      $_SESSION['admin'] = $admin_info;
    } else {
	    $error = 'error';
    }
  }
  if (isset($error)) {
	  unset($_SESSION['admin']);
  }
  if (!isset($_SESSION['admin'])) {
	  return false;
  }
	return true;
}

/**
 * @param $dataset
 * @return array
 */
function getTree($dataset)
{
	$tree = array();
	foreach ($dataset as $id => &$node) {
		if (!$node['sub']) {
			$tree[$id] = &$node;
		} else {
			$dataset[$node['sub']]['childs'][$id] = &$node;
		}
	}
	return $tree;
}

/**
 * Исользуем для генерации Yandex YML
 * @param $id
 * @param $catalog
 * @return mixed
 */
function getRootCat($id, $catalog)
{
	foreach ($catalog as $row) {
		if ($row['id'] == $id) {
			break;
		}
	}
	if ($row['sub'] != 0) {
		$row['id'] = getRootCat($row['sub'], $catalog);
	}
	return $row['id'];
}

/**
 * @param $languages
 * @return mixed|string
 */
function getUri($languages)
{
	$url = StringLibrary::sanitize($_SERVER['REQUEST_URI']);
	$value_lang = explode('/', $url);
	if ((preg_match('/^[-a-zA-Z0-9_\/\=\?\;\,]*$/', $_SERVER['REQUEST_URI']) &&
			(isset($value_lang[1]) && ($value_lang[1] != 'ajaxadmin'
										&& $value_lang[1] != 'ajax'
										&& $value_lang[1] != 'admin'
										&& $value_lang[1] != 'js'
										&& $value_lang[1] != 'server'
										&& $value_lang[1] != 'captcha'
										)
			)
		) || !isset($_SESSION['key_lang'])) {
		$_SESSION['key_lang'] = 'ru';
		$_SESSION['key_lang_name'] = 'ru';
	}
	if (!isset($value_lang[2]) || (isset($value_lang[2]) && $value_lang[2] != "admin")) {
		foreach ($languages as $row) {
			if (isset($value_lang[1]) && $value_lang[1] == $row['language']) {
				$_SESSION['key_lang'] = $row['language'];
				$_SESSION['key_lang_name'] = $row['comment'];
				$_SERVER['REQUEST_URI'] = mb_substr($_SERVER['REQUEST_URI'], 3);
			}
		}
	}
	return $_SESSION['key_lang'];
}

function getUriAdm($languages)
{
	$url = StringLibrary::sanitize($_SERVER['REQUEST_URI']);
	$value_lang = explode("/", $url);
	if (!isset($_SESSION['key_lang_admin'])) {
		$_SESSION['key_lang_admin'] = 'ru';
	}
	if (isset($value_lang[2]) && $value_lang[2] == "admin") {
		foreach ($languages as $row) {
			if (isset($value_lang[1]) && $value_lang[1] == $row['language']) {
				$_SESSION['key_lang_admin'] = $row['language'];
				$_SERVER['REQUEST_URI'] = mb_substr($_SERVER['REQUEST_URI'], 3);
			}
		}
	}
	return $_SESSION['key_lang_admin'];
}

function var_info($vars, $d = false)
{
	echo "<pre class='alert alert-info'>\n";
	var_dump($vars);
	echo "</pre>\n";
	if ($d) {
		exit();
	}
}

function showInfoMessage($message, $confirm = false)
{
	if (empty($message)) {
		return null;
	}
	$message = str_replace(chr(13), "", html_entity_decode($message));
	$message = str_replace(chr(10), "", $message);
	$message = str_replace("'", '"', $message);
	return "<script> document.addEventListener(\"DOMContentLoaded\",function() {showInfoSweet('" . $message . "'," . $confirm . ");}); </script>";
}

function genPassword($size = 8)
{
	$a = ['e', 'y', 'u', 'i', 'o', 'a'];
	$b = ['q', 'w', 'r', 't', 'p', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
	$c = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
	$e = ['-'];
	$password = $b[array_rand($b)];
	do {
		$lastChar = $password[strlen($password) - 1];
		@$predLastChar = $password[strlen($password) - 2];
		if (in_array($lastChar, $b)) {//последняя буква была согласной
			if (in_array($predLastChar, $a)) {//две последние буквы были согласными
				$r = rand(0, 2);
				if ($r) {
					$password .= $a[array_rand($a)];
				} else {
					$password .= $b[array_rand($b)];
				}
			} else {
				$password .= $a[array_rand($a)];
			}
		} elseif (!in_array($lastChar, $c) && !in_array($lastChar, $e)) {
			$r = rand(0, 2);
			if ($r == 2) {
				$password .= $b[array_rand($b)];
			} elseif ($r == 1) {
				$password .= $e[array_rand($e)];
			} else {
				$password .= $c[array_rand($c)];
			}
		} else {
			$password .= $b[array_rand($b)];
		}
	} while (($len = strlen($password)) < $size);
	return $password;
}

function checkAuthAdmin()
{
	if (isset($_SESSION['admin'])) {
		if ($_SESSION['admin']['agent'] != $_SERVER['HTTP_USER_AGENT']) {
			$error = 1;
		}
		if ($_SESSION['admin']['ip'] != $_SERVER['REMOTE_ADDR']) {
			$error = 1;
		}
	}
	if (isset($error)) {
		unset($_SESSION['admin']);
	}
	if (!isset($_SESSION['admin'])) {
		return false;
	}
	return true;
}

function showEditor($name, $body, $elm = 'elm1')
{
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
	return '<textarea name="' . $name . '" id="' . $name . '" rows="10" cols="80">' . $body . '</textarea>
			<script>CKEDITOR.replace("' . $name . '",{filebrowserBrowseUrl:"/js/editors/elfinder/elfinder-cke.html"})</script>';
}

function messageAdmin($text, $type = '')
{
	if ($type == 'error') {
		return '<div id="notification_befe1cc681ba0975b28fff2d630e6dcd" class="notification-content cm-auto-hide">
                <div class="panel panel-danger">
                  <div class="panel-heading">
                    <h3 class="panel-title">Ошибка</h3>
                    <img width="13" height="13" border="0" title="Закрыть" alt="Закрыть" src="/tpl/admin/images/icons/icon_close.gif" class="cm-notification-close hand"/>
                  </div>
                  <div class="panel-body">' . $text . '</div>
                </div>
              </div>';
	} else {
		return '<div id="notification_bdfa2a21deac3fadd3a6e5054ef77c3a" class="notification-content cm-auto-hide">
                <div class="panel panel-success">
                  <div class="panel-heading">
                    <h3 class="panel-title">Оповещение</h3>
                    <img width="13" height="13" border="0" title="Закрыть" alt="Закрыть" src="/tpl/admin/images/icons/icon_close.gif" class="cm-notification-close hand"/>
                  </div>
                  <div class="panel-body">' . $text . '</div>
                </div>
              </div>';
	}

}

function getProtocol()
{
	return $_SERVER['HTTPS'] ? 'https://' : 'http://';
}

/**
 * Проверка на суперадмина
 */
function isSuperAdmin()
{
	if ((int)$_SESSION['admin']['type'] === 1) {
		return true;
	}
	return false;
}

function dumpAdmin($var)
{
	if (isSuperAdmin()) {
		Dumphper::dump($var);
	}
	return false;
}

function dd($var)
{
	die(Dumphper::dump($var));
}

/**
 * @param $array
 * @param $key
 * @param $value
 * Метод генерирует массив ключ значение из переданного массива где ключем является переданный ключ
 * а значением соответственно поле value
 */
function makeRowsKey($array, $key, $value)
{
	$return = [];
	foreach ($array as $item) {
		$return[$item[$key]] = $item[$value];
	}
	return $return;
}

/**
 * @param               $array
 * @param StringLibrary $id = 'id'
 * @return array
 *                          Метод оборачивает каждый вложенный массив укзанным идентификатором
 *                          $array[0] = ['id' => 10, 'name' => 'John']
 *                          Превратит в
 *                          $array[10] = ['id' => 10, 'name' => 'John']
 */
function containArrayInHisId($array, $id = 'id')
{
	$return = [];
	foreach ($array as $item) {
		$return[$item[$id]] = $item;
	}
	return $return;
}

function showTrue(&$str, $type = 'true')
{
	if ($type === 'true') {
		if ($str) {
			echo $str;
		}
	} elseif ($type = 'isset') {
		if (isset($str)) {
			echo $str;
		}
	}
}

function clearStr($str)
{
	return htmlspecialchars(trim(preg_replace('/ {2,}/', ' ', $str)));
}

// генерация HASH кода для проверки
// вызов: generateCode($length=6);
function generateCode($length)
{
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
	$code = "";
	$clen = strlen($chars) - 1;
	while (strlen($code) < $length) {
		$code .= $chars[mt_rand(0, $clen)];
	}
	return $code;
}

function redirect($url, $permanent = false)
{
	ini_set('display_errors', 'Off');
	header('Location: ' . $url, true, $permanent ? 301 : 302);
	echo "<script> window.location.replace('" . $url . "')</script>";
	ini_set('display_errors', 'On');
}

/**
 * @param $arg
 * @param $default
 * Функция возвращает первое значение если оно истинно, иначе второе
 * Для чего может использоваться: например админские настройки - данные которые исользуются в SQL
 * и могут быть случайно удалены, поэтому вторым параметром можно задать дефолтное значение
 */
function oneOf($arg, $default)
{
	if ($arg) {
		return $arg;
	}
	return $default;
}

/**
 * @param array $array
 * @param       $key - ключ текущего массива
 * @return bool
 * Проверяем является ли переданный элемент, последним по нумерации в массиве
 */
function isLast($array = [], $key)
{
	$last_key = key(array_slice($array, -1, 1, true));
	if ($last_key === $key) {
		return true;
	}
	return false;
}

/**
 * @param $str
 * @param $type
 * @param $tag
 * @return string
 * Возвращает обернутое сообщение
 */
function alertMessage($str, $type = 3, $tag = 'div')
{
	if ($type == 0) {
		$type = 'danger';
	}
	if ($type == 1) {
		$type = 'success';
	}
	if ($type == 2) {
		$type = 'warning';
	}
	if ($type == 3) {
		$type = 'info';
	}
	return '<' . $tag . ' class="alert alert-' . $type . '">' . $str . '</' . $tag . '>';
}

/**
 * Функция чистит все не нужные символы, - оставляет только русский / английский алфавит, цифры и  _
 */
function clearSearch($str)
{
	return preg_replace("/[^a-zA-ZА-Яа-я0-9\_\ ]/", "", $str);
}

/**
 * Метод генерирует конкатенированную строку для использования возможно в запросе IN(...)
 * cтрока такого вида '123,124,125'...
 */
function concatArrValue($array, $field = 'id', $delimiter = ',')
{
	$str = '';
	foreach ($array as $k => $item) {
		$str .= $item[$field];
		if (!isLast($array, $k)) {
			$str .= $delimiter;
		}
	}
	return $str;
}

/**
 * Метод убирает с имени файла суфикс "_s."  - для имени большого фото
 */
function bigPhoto($photo)
{
	return str_replace('_s.', '.', $photo);
}

/**
 * Метод добавляет в имея файла суфикс "_s." к имени файла - для имени большого фото
 */
function sPhoto($photo)
{
	return str_replace('.', '_s.', $photo);
}

/**
* Получить имя самой маленькую фотку из средней
*/
function xsPhoto($sphoto)
{
	return str_replace('_s.', '_s_2x.', $sphoto);
}

/**
 * Приведение строки к нижнему регистру
 * @param $str
 * @return mixed|string|string[]|null
 */
function toLower($str)
{
	return mb_strtolower($str, 'UTF-8');
}

/**
 * Делаем 301 редирект на указанный url
 * @param $url
 */
function redirect301($url)
{
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: " . PROTOCOL . $_SERVER['HTTP_HOST'] . $url);
	exit();
}

/**
 * Удаляет теги HTML и PHP из строк POST записей
 * @return mixed
 */
function strip_post()
{
	foreach ($_POST as &$item) {
		$item = strip_tags($item);
	}
	return $_POST;
}

/**
 * Получаем чистыу код-идентификатор youtube ролика
 * @param $url
 * @return mixed
 */
function getYouTubeCode($url)
{
	$parts = parse_url($url);
	$path = explode('/', $parts['path']);
	parse_str($parts['query'], $query);
	if ($parts['host'] == 'youtu.be') {
		return $path[1];
	} else {
		if ($parts['host'] == 'www.youtube.com' && !empty($query['v'])) {
			return $query['v'];
		}
	}
}

function ruCountProducts($count)
{
	$str = $count . ' ';
	if (substr($count, -1) == 1 && $count != 11) {
		$str .= "товар";
	} elseif (substr($count, -1) > 1 && substr($count, -1) < 5 && ($count > 20 || $count < 10)) {
		$str .= "товара";
	} else {
		$str .= "товаров";
	}
	return $str;
}

function fillField($key)
{
	if (!empty($_POST[$key])) {
		return $_POST[$key];
	} else {
		if (!empty($_SESSION['user_info'][$key])) {
			return $_SESSION['user_info'][$key];
		} else {
			return null;
		}
	}
}

function curdate($format = "Y-m-d H:i:s")
{
	return date($format);
}


function recurseRmdir($dir)
{
	$files = array_diff(scandir($dir), array('.', '..'));
	foreach ($files as $file) {
		(is_dir("$dir/$file")) ? recurseRmdir("$dir/$file") : unlink("$dir/$file");
	}
	return rmdir($dir);
}

function parseDateFromReview($date)
{
	$str_pos = strpos($date, " ");
	$result = substr($date, 0, $str_pos);
	$converter = array(
		'01' => 'января',
		'02' => 'февраля',
		'03' => 'марта',
		'04' => 'апреля',
		'05' => 'мая',
		'06' => 'июня',
		'07' => 'июля',
		'08' => 'августа',
		'09' => 'сентбря',
		'10' => 'октября',
		'11' => 'ноября',
		'12' => 'декабря'
	);
	$tmp_arr = explode('-', $result);
	$tmp_arr['1'] = strtr($tmp_arr['1'], $converter);
	$result = $tmp_arr['2'] . '-' . $tmp_arr['1'] . '-' . $tmp_arr['0'];
	$time_rev = substr($date, $str_pos, strlen($date));
	$time_rev = explode(':', $time_rev);
	$time_rev = $time_rev[0] . ':' . $time_rev[1];

	$time_old = '';

	$time_passed = new DateTime($date);
	$time_passed = $time_passed->diff(new DateTime(date('Y-m-d H:i:s')));
	$tpa['y'] = $time_passed->format("%y");
	$tpa['m'] = $time_passed->format("%m");
	$tpa['d'] = $time_passed->format("%d");
	$tpa['h'] = $time_passed->format("%h");
	$tpa['i'] = $time_passed->format("%i");
	$tpa['s'] = $time_passed->format("%s");

	if ($tpa['y'] < 1) {
		if ($tpa['m'] < 1) {
			if ($tpa['d'] < 1) {
				if ($tpa['h'] < 1) {
					if ($tpa['i'] < 1) {
						$time_old .= $tpa['s'];
						if (substr($tpa['s'], -1) == 1 && $tpa['s'] != 11) {
							$time_old .= " секунда";
						} elseif (substr($tpa['s'], -1) > 1 && substr($tpa['s'],
								-1) < 5 && ($tpa['s'] > 20 || $tpa['s'] < 10)) {
							$time_old .= " секунды";
						} else {
							$time_old .= " секунд";
						}
					} else {
						$time_old .= $tpa['i'];
						if (substr($tpa['i'], -1) == 1 && $tpa['i'] != 11) {
							$time_old .= " минута";
						} elseif (substr($tpa['i'], -1) > 1 && substr($tpa['i'],
								-1) < 5 && ($tpa['i'] > 20 || $tpa['i'] < 10)) {
							$time_old .= " минуты";
						} else {
							$time_old .= " минут";
						}
					}
				} else {
					$time_old .= $tpa['h'];
					if (substr($tpa['h'], -1) == 1 && $tpa['h'] != 11) {
						$time_old .= " час";
					} elseif (substr($tpa['h'], -1) > 1 && substr($tpa['h'],
							-1) < 5 && ($tpa['h'] > 20 || $tpa['h'] < 10)) {
						$time_old .= " часа";
					} else {
						$time_old .= " часов";
					}
				}
			} else {
				$time_old .= $tpa['d'];
				if (substr($tpa['d'], -1) == 1 && $tpa['d'] != 11) {
					$time_old .= " день";
				} elseif (substr($tpa['d'], -1) > 1 && substr($tpa['d'],
						-1) < 5 && ($tpa['d'] > 20 || $tpa['d'] < 10)) {
					$time_old .= " дня";
				} else {
					$time_old .= " дней";
				}
			}
		} else {
			$time_old .= $tpa['m'];
			if (substr($tpa['m'], -1) == 1 && $tpa['m'] != 11) {
				$time_old .= " месяц";
			} elseif (substr($tpa['m'], -1) > 1 && substr($tpa['m'], -1) < 5 && ($tpa['m'] > 20 || $tpa['m'] < 10)) {
				$time_old .= " месяца";
			} else {
				$time_old .= " месяцев";
			}
		}
	} else {
		$time_old .= $tpa['y'];
		if (substr($tpa['y'], -1) == 1 && $tpa['y'] != 11) {
			$time_old .= " год";
		} elseif (substr($tpa['y'], -1) > 1 && substr($tpa['y'], -1) < 5 && ($tpa['y'] > 20 || $tpa['y'] < 10)) {
			$time_old .= " года";
		} else {
			$time_old .= " лет";
		}
	}

	return [
		'time' => $time_rev,
		'date' => $result . ' в ' . $time_rev,
		'time_old' => $time_old . ' назад',
	];
}

/**
 * @param        $path
 * @param string $size
 * @param string $default
 * @param int    $days
 * @return string
 * Clear cache of resource if resource modified < $days
 */
function getPathNoCache($path, $size = '_s', $default = '/files/default.jpg', $days = 30)
{
	$path = preg_replace('/^\//', '', $path);

	if ($size != '_s') {
		$test_path = str_replace('_s', $size, $path);

		if (file_exists($test_path)) {
			$path = $test_path;
		}
	}

	if (file_exists($path)) {
		$path = '/' . $path;
	} else {
		$path = $default;
	}

	$date1 = date_create(date('d-m-Y', filemtime($path)));
	$date2 = date_create(date('d-m-Y'));
	$interval = date_diff($date1, $date2)->format('%a');
	$prefix = '';

	if (intval($interval) < $days) {
		$prefix = '?' . filemtime($path);
	}

	return $path . $prefix;
}