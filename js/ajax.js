$(document).ready(function () {
	// Get content of the basket
	basket();

	// Change currency
	$(document).on('click', '.currency-link', function (e) {
		e.preventDefault();

		var $form = $('#form_currency'),
				$input = $form.children('input[name="currency"]');

		$input.val($(this).data('currency'));
		$form.submit();
	});

	// Add to shop cart
	$(document).on('click', '.buy', function () {
		var $this = $(this),
				$form = $this.parents('form'),
				photo = $('.product-photos-main__img').attr('src'),
				dataString = $form.serialize();

		dataString += '&photo=' + photo;

			$.ajax({
				type: "POST",
				url: "/ajax/orders/incart",
				data: dataString,
				dataType: "json",
				cache: false,
				success: function () {
					basket(true);
					$('.product .product-amount-changer__input').val(1);
				}
			});

	});

	// Update amount of product in shop cart
	$(document).on('change', '.product-amount-changer__input', function () {
		var cart_id = $(this).closest('.cart__item').data('cart-id'),
				amount = $(this).val();
		update_cart(cart_id, amount, 'update');
	});

	// Delete product from shop cart
	$(document).on('click', '.cart__item-delete', function (e) {
		e.preventDefault();
		var cart_id = $(this).closest('.cart__item').data('cart-id');
		update_cart(cart_id, 1, 'delete');
	});

	// Add to compare
	$(document).on('click', '.add_compare', function () {
		var id = $(this).attr('rel');
		var cat_id = $('#curr_cat').attr('title');
		var dataString = 'id=' + id + '&cat_id=' + cat_id;
		$.ajax({
			type: "POST",
			url: "/ajax/compare/add",
			data: dataString,
			dataType: "json",
			cache: false,
			success: function (data) {
				if (data.add == 1) {
					$.stickr({
						note: data.message,
						className: 'next',
						position: {right: '42%', bottom: '60%'},
						time: 1000,
						speed: 300
					});
				}
				else $.stickr({
					note: data.message,
					className: 'prev',
					position: {right: '42%', bottom: '60%'},
					time: 1000,
					speed: 300
				});
			}
		});
		return false;
	});

	/////Subscribers
	$(document).on('submit', '#subscribe_form', function () {
		var name = $('#subscribe-name').val();
		var email = $('#subscribe-email').val();
		var dataString = 'name=' + name + '&email=' + email;
		$.ajax({
			type: "POST",
			url: "/ajax/mailer/subscribers",
			dataType: 'json',
			data: dataString,
			cache: false,
			success: function (data) {
				if (data.err) {
					$.stickr({
						note: data.err,
						className: 'prev',
						position: {right: '42%', bottom: '60%'},
						time: 2000,
						speed: 300
					});
				}
				else {
					$.stickr({
						note: data.message,
						className: 'next',
						position: {right: '42%', bottom: '60%'},
						time: 2000,
						speed: 300
					});
				}
			}
		});
		return false;
	});

	//Search form
	x = '';
	$(document).on('keyup', "#search_query_top", function (e) {
		clearTimeout(x);
		if ($(this).val().length > 1) {
			var word = $(this).val();
			x = setTimeout(function () {
				autosearch(word);
			}, 300);
		}
		else $('#results_search').css('display', 'none');
	});

	//Select in searck list
	$(document).on('click', "#results_search li", function () {
		$('#results_search').hide(200);
	});

	//Hide search list
	$("body").on('click', function () {
		$('#results_search').hide(200);
	});

	//Add to favorites
	$(document).on('click', ".add_fav", function () {
		var id = $(this).attr('rel');
		var dataString = 'id=' + id;
		$.ajax({
			type: "POST",
			url: "/ajax/product/fav",
			data: dataString,
			dataType: "json",
			cache: false,
			success: function (data) {
				$.stickr({
					note: data.message,
					className: 'next',
					position: {right: '42%', bottom: '60%'},
					time: 2000,
					speed: 300
				});
			}
		});
		return false;
	});


});

function autosearch(word) {
	var dataString = 'word=' + word;
	$.ajax({
		type: "POST", url: "/ajax/catalog/search", data: dataString, cache: false, success: function (data) {
			$('#results_search').html(data);
			$('#results_search').show(200);
		}
	});
}


//Fast buy form load
$(document).on('click', '.buy_quick', function () {
	var id = $(this).attr('name');
	var amount = $('#cnt_' + id).val();
	var color = $('.color_select.selected').attr('id');
	var sizes = $('.size_select.selected').attr('id');
	if ($('.color_select').length > 0 && color == undefined) {
		$.stickr({
			note: 'Выберите цвет!',
			className: 'prev',
			position: {right: '42%', bottom: '60%'},
			time: 1000,
			speed: 300
		});
	}
	else if ($('.size_select').length > 0 && sizes == undefined) {
		$.stickr({
			note: 'Выберите размер!',
			className: 'prev',
			position: {right: '42%', bottom: '60%'},
			time: 1000,
			speed: 300
		});
	}
	else {
		var photo = $('#photo_basket').val();
		var dataString = 'id=' + id + '&amount=' + amount + '&color=' + color + '&size=' + sizes + '&photo=' + photo;
		$.ajax({
			type: "POST", url: "/ajax/orders/QuickBuyForm", data: dataString, cache: false, success: function (html) {
				$('#quick_buy_form').remove();
				$('.overlay').show();
				$('body').append(html);
			}
		});
	}
});
//
$(document).on('click', '#close', function () {
	$('#quick_buy_form').remove();
});
//
$(document).on('click', '#send_fast_order', function () {
	var id = $('#price_id2').val();
	var amount = $('input[name="amount1"]').val();
	var product_id = $('#product_id').val();
	var price = $('#price').val();
	var discount = $('#discount').val();
	var name = $('#name').val();
	var size = $('#size').val();
	var color = $('#color').val();
	var code = $('#code').val();
	var photo = $('#photo').val();
	var f_name = $('#name_fast_order').val();
	var f_email = $('#email_fast_order').val();
	var f_phone = $('#phone_fast_order').val();
	var dataString = 'id=' + id + '&product=' + product_id + '&name=' + name + '&price=' + price + '&discount=' + discount + '&amount=' + amount + '&f_name=' + f_name + '&f_email=' + f_email + '&f_phone=' + f_phone + '&color=' + color + '&size=' + size + '&code=' + code + '&photo=' + photo;
	$.ajax({
		type: "POST",
		url: "/ajax/orders/QuickBuyOrder",
		data: dataString,
		dataType: 'json',
		cache: false,
		success: function (data) {
			if (data.status == false) {
				$('#fast_order_form').before(data.message);
			}
			else {
				$('#fast_order_form').before(data.message);
				setTimeout(function () {
					$('#quick_buy_form').remove();
				}, 2000);
			}
		}
	});
	return false;
});

// Fetch single order info | all orders info
$(document).on('click', '.cabinet-orders__link_more, .back_to_orders', function (e) {
	e.preventDefault();
	
	var href = $(this).attr('href');

	console.log(href);

	$.ajax({
		url: href,
		dataType: 'json',
		cache: false,
		success: function (data) {
			console.log(data);
			$('#cabinet__tab_orders').html(data.content);
		}
	});
});



// Next selector

/////////////
// FUNCTIONS
/////////////

//
function autosearch(word) {
	var dataString = 'word=' + word;
	$.ajax({
		type: "POST", url: "/ajax/catalog/search", data: dataString, cache: false, success: function (data) {
			$('#results_search').html(data);
			$('#results_search').show(200);
		}
	});
}

//basket
function basket(open) {
	$.ajax({
		type: "POST",
		url: "/ajax/orders/getBasket",
		cache: false,
		dataType: 'json',
		success: function (data) {
			$(".hotlinks-link_basket").html(data.button);
			$('#cart_popup').html(data.cart);
			$('.cart_checkout').html(data.table);

			if( open ) {
        $('.hotlinks-link_basket').click();
      }

			if( data.count === 0 ) {
				setTimeout(function () {
          $.fancybox.close();
        }, 1000);
      }
		}
	});
}


// Add comments
$('#form_comment').on('submit', function () {
	var id = $("#id_comment").val();
	var type = $("#type_comment").val();
	var name = $("#name_comment_form").val();
	var email = $("#email_comment_form").val();
	var phone = $("#phone_comment_form").val();
	var message = $("#text_comment_form").val();
	var photo = $("#avatar").val();
	var err = false;
	if (name == '') {
		$("#name_comment_form").css('border', '1px solid red');
		err = true;
	}
	else $("#name_comment_form").attr('style', '');
	if (message == '') {
		$("#text_comment_form").css('border', '1px solid red');
		err = true;
	}
	else $("#text_comment_form").attr('style', '');
	if (!err) {
		add_comment(id, type, name, email, phone, message, photo, '');
	}
	return false;
});

//
function add_comment(id, type, name, email, phone, message, photo, image) {
	$('#load_comments').show();
	$('#send-rev').css({'opacity': '0.5'});
	var dataString = 'id=' + id + '&type=' + type + '&name=' + name + '&email=' + email + '&phone=' + phone + '&message=' + message + '&photo=' + photo + '&image=' + image;
	$.ajax({
		type: "POST",
		url: "/ajax/comments/addcomment",
		data: dataString,
		dataType: 'json',
		cache: false,
		success: function (data) {
			if (data.list != null) {
				$("#comment_block").replaceWith(data.list);
			}
			else {
				$('#load_comments').hide();
				$('#send-rev').css({'opacity': '1'});
			}
			$("#input").html(data.message);
			$("#text_comment_form").val('');
			var pos = $('#header').offset();
			$.scrollTo(pos.top, 500);
			//e.preventDefault();
		}
	});
}

// login pop-up
function check_auth() {
	var email = $('#email_auth').val();
	var pass = $('#pass_auth').val();
	var dataString = 'email=' + email + '&pass=' + pass;
	$.ajax({
		type: "POST",
		url: "/ajax/users/checkauth",
		data: dataString,
		dataType: "json",
		cache: false,
		success: function (data) {
			if (data.auth == 1) {
				window.setTimeout('location.reload()', 3000);
			}
			$('#auth_message').html(data.message);
		}
	});
	return false;
}
//
function update_cart(cart_id, amount, action) {
	var dataString = 'cart_id=' + cart_id + '&amount=' + amount + '&action=' + action;

	$.ajax({
		type: "POST",
		url: "/ajax/orders/updatecart",
		data: dataString,
		dataType: "json",
		cache: false,
		success: function (data) {
			basket();
		}
	});
}
