
/*Filters*/

////UI range
function init_range(start, end, maximum) {
	$("#slider-range").slider({
		range: true,
		min: 0,
		max: maximum,
		step: 3,
		values: [start, end],
		slide: function (event, ui) {
			$("#price_from").val(ui.values[0]);
			$("#price_to").val(ui.values[1]);
		}
	});
	$("#price_from").val($("#slider-range").slider("values", 0));
	$("#price_to").val($("#slider-range").slider("values", 1));
}
function get_product(only_filters) {
	show_loading();
	var price_from = $('#price_from').val();
	var price_to = $('#price_to').val();
	var items = $('#params_url').val();
	var cat_id = $('#curr_cat').attr('title');
	var url = $('#curr_cat').attr('url');
	var change_price = $('#submit_price').attr('class');
	var paramsUrl = get_params_url2();
	if (only_filters == 1)only_filters = '&only_filters=1';
	else only_filters = '';
	var dataString = 'items=' + items + '&cat_id=' + cat_id + '&price_from=' + price_from + '&price_to=' + price_to + '&url=' + paramsUrl['url'] + '&change_price=' + change_price + '&urlSlug=' + paramsUrl['urlSlug'] + '&urlDefault=' + url['urlDefault'];
	$.ajax({
		type: "POST",
		url: "/ajax/catalog/getfilter",
		data: dataString,
		dataType: 'json',
		cache: false,
		success: function (data) {
			if (data.filters)$('#load_filter').html(data.filters);//alert(data.product);
			if (data.product)$('#load_product').html(data.product);
			if (data.meta) setMetaFilters(data.meta);
			hide_loading();
			// $.scrollTo(170, 600);
			$('.product').hover(function () {
				$(this).addClass('hov_product');
				init_black_white();
			}, function () {
				$(this).removeClass('hov_product');
			});
		}
	});
}
/**
 * изменение мета данных через JS
 * @param $meta
 */
function setMetaFilters($meta) {
	$('title').text($meta.title);
	$('meta[name="keywords"]').attr('content', $meta.keywords);
	$('meta[name="description"]').attr('content', $meta.description);
}
function clear_ses(id) {
	var cat_id = $('#curr_cat').attr('title');
	var dataString = 'clear_id=' + id + '&cat_id=' + cat_id + '&url=' + get_params_url2();
	$.ajax({
		type: "POST",
		url: "/ajax/catalog/getfilter",
		data: dataString,
		dataType: 'json',
		cache: false,
		success: function (data) {
			$('#load_filter').html(data.filters);
			$('#load_product').html(data.product);
			get_params_url();
		}
	});
}
function show_loading() {
	$('#load_filter').css('opacity', 0.2);
	//$('#load_product').css('opacity', 0);
	$('#filters').append('<img src="/images/loading.gif" id="loading" />');
	var left = ($('.left_col').width() / 2) - 30;
	var top = $("body").scrollTop() + 250;
	$('#loading').css({'left': left + 'px', 'top': top + 'px'});
	return false;
}
function hide_loading() {
	$('#loading').remove();
	$('#load_filter').css('opacity', 1);
	//$('#load_product').css('opacity', 1);
}
/**
 * Удалим параметры фильров из истории
 */
function resetFilter() {
	$('#params_url').val('');
}
/**
 *
 * @returns {{url: (*|jQuery), urlDefault: string, urlSlug: string}}
 * url[
 * 	url - куда ссылаемся
 * 	urlDefault - url по дефолту(ID фильтров)
 * 	urlSlug - url с url фильтров
 * ]
 */
function get_params_url2() {
	var url = $('#curr_cat').attr('url');
	finalParams = '';
	additionalParams = '';
	var params = '';
	var paramsUrl = '';
	var sub_params = '';
	var sub_paramsUrl = '';
	var group = '';
	var groupUrl = '';
	var checked = $('.checked');
	if (checked.length === 0) resetFilter();
	checked.each(function () {
		//Сформируем url строку
		//----------------------------------------
		var arrUrl = $(this).data('url').split("-");
		if (groupUrl != arrUrl[0]) {
			groupUrl = arrUrl[0];
			if (sub_paramsUrl != '')sub_paramsUrl += ';';
			sub_paramsUrl += groupUrl + '=' + arrUrl[1];
		}
		else {
			if (sub_paramsUrl != '')sub_paramsUrl += ',';
			sub_paramsUrl += arrUrl[1];
		}
		//----------------------------------------
		//Сформируем Id строку
		var arr = $(this).attr('pid').split("-");
		if (group != arr[0]) {
			group = arr[0];
			if (sub_params != '')sub_params += ';';
			sub_params += group + '=' + arr[1];
		}
		else {
			if (sub_params != '')sub_params += ',';
			sub_params += arr[1];
		}
		//----------------------------------------
	});
	//Если используем SLUG как url
	if (useSlugParams === true){
		if (sub_params != '') {
			finalParams = sub_paramsUrl;
		}
	} else{//Если используем Id фильтров как url
		if (sub_params != '') {
			finalParams = sub_params;
		}
	}
	if (finalParams != ''){
		$('#params_url').val(finalParams);
		params = '/params/' + finalParams;
	}
	var price_url = $('#price_from').val() + '-' + $('#price_to').val();

	if ($('#submit_price').hasClass('change_price')) url = '/catalog/' + url + params + '/price-range/' + price_url;
	else url = '/catalog/' + url + params;

	if ($('#sort_form select').val() != '')url += '/sort/' + $('#sort_form select').val();

	return {'url' : url, 'urlDefault': sub_params, 'urlSlug': sub_paramsUrl};
}

function get_params_url() {
	var url = get_params_url2();
	url = url['url'];
	history.pushState(null, null, url);
}
$(document).on('click', '.set_params',function () {
	if ((!$(this).hasClass('uncheck') || ($(this).hasClass('uncheck') && $(this).hasClass('checked'))) && $('#loading').length == 0) {
		$(this).toggleClass("checked");
		get_params_url();
		get_product();
	}
	else return false;
});
$(document).on('change', '#groupprice input', function () {
	if ($('#loading').length == 0) {
		get_product();
	}
});
$(document).on('click', '.clear_param', function () {
	if ($('#loading').length == 0) {
		var id = $(this).attr('data-id');
		$('#filter' + id).trigger('click');
	}
});
$(document).ready(function () {
	$(document).on('click', '.clear_group', function () {
		if ($('#loading').length == 0) {
			var id = $(this).attr('rel');
			if (id == 'groupprice') {
				$('input[name=price_from]').val(0);
				$('input[name=price_to]').val(0);
			}
			else $('#' + id + ' div').removeClass('checked');
			get_params_url();
			get_product();
		}
	});
	$(document).on('click', '.clear_group_all', function () {
		if ($('#loading').length == 0) {
			resetFilter();
			$('#load_filter .checked').removeClass('checked');
			$('#price_from').val(0);
			$('#price_to').val(0);
			$('#submit_price').removeClass('change_price');
			setTimeout(function () {
				get_params_url();
				get_product();
			}, 100);
		}
	});
	$(document).on('click', '.clear_price_range', function () {
		$('#submit_price').removeClass('change_price');
		$('#price_from').val(0);
		$('#price_to').val(0);
		get_params_url();
		get_product();
	});
	$(document).on('click', '#submit_price', function () {
		if ($('#price_from').val() >= 0 && $('#price_to').val() != '') {
			$('#submit_price').addClass('change_price');
			get_params_url();
			get_product();
		}
	});
});