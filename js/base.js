jQuery.fn.topLink = function (settings) {
	settings = jQuery.extend({
		min: 1,
		fadeSpeed: 200,
		ieOffset: 50
	}, settings);

	return this.each(function () {
		//listen for scroll
		var el = $(this);
		el.css('display', 'none'); //in case the user forgot
		$(window).scroll(function () {
			if (!jQuery.support.hrefNormalized) {
				el.css({
					'position': 'absolute',
					'top': $(window).scrollTop() + $(window).height() - settings.ieOffset
				});
			}
			if ($(window).scrollTop() >= settings.min) {
				el.fadeIn(settings.fadeSpeed);
			} else {
				el.fadeOut(settings.fadeSpeed);
			}
		});
	});
};

$(document).ready(function () {
	$('#cke_body').css({
		position: 'relative'
	});
	
	console.log($('#cke_body'));

	//
	$('#top-link').topLink({
		min: 400,
		fadeSpeed: 500
	});

	//
	$(document).on('click', '#top-link', function (e) {
		e.preventDefault();
		$.scrollTo(0, 300);
	});

	//
	$(document).on('click', '.feedback_trigger', function () {
		$('.form_feedback').slideToggle();
		$('.form_separator').slideToggle();
	});

	//
	$(document).on('click', '#callback_show_hide', function () {
		$('#callbacks').slideToggle();
	});

	//
	$(document).on('click', 'input[name=close_callbacks]', function () {
		$('#callbacks').slideToggle();
	});

	//
	$(document).on('change', '.main_menu', function () {
		var v = $(this).find('option:selected').val();
		console.log(v);
		if (v != 'none') {
			document.location.href = v;
		}
	});

	//
	if ($('.help_o').length > 0) {
		$('.help_o').tooltip();
	}

	//
	$(document).on('click', '.nostock', function () {
		$('#inform_id').val($(this).attr('rel'));
	});

	//
	$(document).on('click', '.show_filters', function () {
		var arr = $(this).attr('rel').split('_');
		$(this).attr('rel', arr[0] + '_' + $(this).html());
		$(this).html(arr[1])
		if ($('#filters' + arr[0]).css('display') == 'none') $('#filters' + arr[0]).show(200);
		else $('#filters' + arr[0]).hide(200);
		return false;
	});

	//
	if ( $(".product_in").length ) {
		var photos = [];
		photos.push($('#img_load a').attr('href'));
		$('.more-photo a').each(function () {
			photos.push($(this).attr('href'));
		});
		preloadImages(photos);
		$(".easyzoom img").elevateZoom({
			zoomType: "inner",
			cursor: "crosshair"
		});
	}

	//
	$(document).on('click', '.load_more_photo', function () {
		$('#img_load a').attr('href', $(this).attr('href'));
		$('#img_load img').attr('src', $(this).attr('href'));
		$('#zoom').parent('a').attr('href', $(this).attr('href'));
		$('.zoomContainer').remove();
		$(".easyzoom img").elevateZoom({
			zoomType: "inner",
			cursor: "crosshair"
		});
		return false;
	});

	//
	$(document).on('click', '.show_auth', function () {
		if ($(this).val() == '1') {
			$('#formID').show();
			$('#formID2').hide();
		} else {
			$('#formID2').show();
			$('#formID').hide();
		}
	});

	// Изменение количества в карточке товара
	$(document).on('click', '.countchange', function () {
		var id = $(this).attr('rel');
		var min = 1;
		var max = parseInt($('#' + id).attr('max'));
		var current = parseInt($('#' + id).val());
		var cnt = current;
		var price = parseInt($('#fixprice').val());
		if ($(this).hasClass('cnt-up') && (current < max)) {
			var cnt = current + 1;
		}
		if ($(this).hasClass('cnt-down') && (current > min)) {
			var cnt = current - 1;
		}
		var newprice = cnt * price;
		$('#' + id).val(cnt);
		$('#total span').html(numToPrice(newprice));
		if ($('#update_order').length > 0) $('#update_order').submit();
		else if ($('.cart_popup_tb').length > 0) update_cart(id, cnt, 'update');
	});

	// Изменение количества в карточке товара
	$(document).on('click', '.countchange2', function () {
		var id = $(this).attr('rel');
		var min = 1;
		var max = parseInt($('#' + id).attr('max'));
		var current = parseInt($('#' + id).val());
		var cnt = current;
		if ($(this).hasClass('cnt-up') && (current < max)) {
			var cnt = current + 1;
		}
		if ($(this).hasClass('cnt-down') && (current > min)) {
			var cnt = current - 1;
		}
		$('#' + id).val(cnt);
	});

	// Add to shop cart
	$(document).on('change', '.cart_update', function () {
		var id = $(this).attr('id');
		var cnt = $(this).val();
		update_cart(id, cnt, 'update');
	});

	//
	$(document).on('click', '#accept_sign', function () {
		if ($(this).attr('checked') == 'checked') $('input[name=sign_up]').attr('disabled', false);
		else $('input[name=sign_up]').attr('disabled', true);
	});

	//
	$(document).on('click', '#tabs li', function () {
		$('#tabs li').removeClass('hov_tab');
		$(this).addClass('hov_tab');
	});

	//
	$(document).on('click', '#sort_block a', function () {
		var id = $(this).attr('class');
		$('#sort_form input').val(id);
		$('#sort_form').submit();
		return false;
	});

	//
	$(document).on('change', '.amount', function () {
		$('#update_order').submit();
	});

	// Lightbox
	$("a[rel=lightbox]").fancybox({
		'transitionIn': 'none',
		'transitionOut': 'none',
		'titlePosition': 'over',
		'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
			return '<span id="fancybox-title-over">' + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		}
	});

	//
	var curr_cat = $('#curr_cat').attr('title');
	if (curr_cat != '') {
		$("#" + curr_cat).parents("ul").show(200); // показать подчиненые одно последующее
		$("#" + curr_cat + ' ul:first').show(200);  // показать подчененый каталог
		$("#" + curr_cat).parents("li").addClass('hov_left'); // показать подчиненые одно последующее
		$("#" + curr_cat).addClass('hov_left');
	}

	// Next_selector



});

/////////////
// FUNCTIONS
/////////////

//
function preloadImages(images) {
	for (var i = 0; i < images.length; i++) {
		var image = images[i];
		$("<img />").attr("src", image);
	}
}

//
function mycarousel_initCallback(carousel) {
	// Disable autoscrolling if the user clicks the prev or next button.
	carousel.buttonNext.on('click', function () {
		carousel.startAuto(0);
	});
	carousel.buttonPrev.on('click', function () {
		carousel.startAuto(0);
	});
	// Pause autoscrolling if the user moves with the cursor over the clip.
	carousel.clip.hover(function () {
		carousel.stopAuto();
	}, function () {
		carousel.startAuto();
	});
}

//
function showTab(id) {
	$('#tabs div.div_tab').css('display', 'none');
	$('#' + id).css('display', 'block');
}

//
function onpage(cnt) {
	$('#onpage').val(cnt);
	$('#onpage_form').submit();
}

//
function show_filters(id, hide_text, show_text) {
	if ($('#filters' + id).css('display') == 'none') {
		$('#filters' + id).show(200);
	}
}

//
function post_currency(id) {
	$('#form_currency input').val(id);
	$('#form_currency').submit();
	return false;
}

//
function windowHeight() {
	var de = document.documentElement;
	return self.innerHeight || (de && de.clientHeight) || document.body.clientHeight;
}

//
function windowWidth() {
	var de = document.documentElement;
	return self.innerWidth || (de && de.clientWidth) || document.body.clientWidth;
}

//
function IsValidateEmail(email) {
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,6})$/;
	return reg.test(email);
}

//
function numToPrice(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}