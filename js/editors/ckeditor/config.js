/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.allowedContent = true;
    config.contentsCss = [
        "/tpl/default/css/bootstrap.min.css",
        "/tpl/default/css/custom-bootstrap.css",
        "/css/normalize.css",
        "/css/font-awesome.min.css",
        "/css/owl.carousel.min.css",
        "/tpl/default/css/custom-modal.css",
        "/tpl/default/css/sweetalert2.min.css",
        "/tpl/default/css/magnific-popup.css",
        "/tpl/default/css/style.css",
        "/tpl/default/css/responsive.css",
        "/tpl/default/css/pad-mar.css"];
    config.disallowedContent = '*[on*]';
    config.protectedSource.push(/<[a-zA-Z]+><\/[a-zA-Z]+>/g);
    //config.allowedContent = 'p(*); div(*);  h1(*);  h2(*);  h3(*);  h4(*);  h5(*); h6(*); strong(*); em(*); b(*); i(*); u(*); sup(*); sub(*); ul(*); ol(*); li(*); a[!href](*); br(*); hr(*); img{*}[*](*); iframe(*)';
    // Define changes to default configuration here. For example:
    //config.language = 'ru';
    // config.uiColor = '#AADC6E';

};
